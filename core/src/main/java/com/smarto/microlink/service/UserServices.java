package com.smarto.microlink.service;

import com.smarto.microlink.configuration.SpringJasperConfig;
import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.DeviceRepository;
import com.smarto.microlink.doa.UserRepository;
import com.smarto.microlink.domain.*;
import com.smarto.microlink.exception.ExceptionInfo;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.utils.UserPrincipal;
import com.smarto.microlink.utils.Utils;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.apache.commons.collections.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;

@Service
public class UserServices implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private TripServices tripService;

    public UserRepository getUserRepository() {
        return userRepository;
    }

    @Autowired

    private SpringMongoConfig springMongoConfig;

    @Autowired

    private SpringJasperConfig springJasperConfig;

    private List<User> existingUsers;

    private Query query;

    public List<User> findUsers() {
        return userRepository.findAll();
    }

    public List<User> findUsersWithLastPosition() throws ServiceException {
        List<User> allUsers = findUsers();
        //TODO:SMARTO à optimiser faire une requestte directe depuis mongoDB pour avoir la dernière positin
        for (User user : allUsers) {
            List<Trip> lastTrips = tripService.findLastTrip2(user.getId());
            if (CollectionUtils.isNotEmpty(lastTrips)) {
                user.setLastTrip(lastTrips.get(0));
            }
            try {


                List<Trip> allTrips = tripService.findAllTrips2(user.getId());
                double distance = 0;
                for (Trip trip : allTrips) {
                    if (trip.getTripResult() != null) {
                        distance = distance + trip.getTripResult().getTotalDistance();
                    }
                }
                user.setTotalDistance(distance);
            } catch (ServiceException e) {

            }

        }
        Collections.sort(allUsers);
        return allUsers;
    }


    public User findUserById(String userId) {
        return userRepository.findById(userId);
    }

    public Message authenticate(String mail, String mdp) throws ServiceException {

        boolean test = false;

        Query query = new Query();
        query.addCriteria(Criteria.where("mail").is(mail)).addCriteria(Criteria.where("mdp").is(mdp));// app
        // mobile
        // query.addCriteria(Criteria.where("mail").is(mail)).addCriteria(Criteria.where("mdp").is(Utils.encodeMD5(mdp)));

        MongoTemplate mongoTemplate;
        try {
            mongoTemplate = springMongoConfig.mongoTemplate();
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }
        existingUsers = mongoTemplate.find(query, User.class);
        User tmpUser = null;
        for (User user : existingUsers) {
            // if (user.getMail().equals(mail) &&
            // user.getMdp().equals(Utils.encodeMD5(mdp))) {
            if (user.getMail().equals(mail) && user.getMdp().equals(mdp)) {// app
                // mobile
                test = true;

                tmpUser = new User();
                tmpUser.setId(user.getId());
                tmpUser.setFirstName(user.getFirstName());
                tmpUser.setLastName(user.getLastName());
                tmpUser.setMail(user.getMail());

                // set devices
                List<UDevice> devices = new ArrayList<UDevice>();
                if (user.getUDevices() != null) {
                    for (UDevice device : user.getUDevices()) {
                        UDevice udevice = new UDevice();
                        udevice.setSerialNumber(device.getSerialNumber());

                        Device tmpDevice = new Device(device.getDevice().getId(), device.getDevice().getModel(),
                                device.getDevice().getSerialNumber(), device.getDevice().getBatchNumber(),
                                device.getDevice().getCustomer(), device.getDevice().getYear(),
                                device.getDevice().getReleaseDate(), device.getDevice().getMacAdress(), device.getDevice().getOperatorName(), device.getDevice().getSupplier(), device.getDevice().getHardRevision());
                        udevice.setDevice(tmpDevice);
                        devices.add(udevice);
                    }
                }
                tmpUser.setUDevices(devices);

                // set vehicle
                List<UVehicleDetail> vehicles = new ArrayList<UVehicleDetail>();
                if (user.getUVehicleDetail() != null) {
                    for (UVehicleDetail vehicle : user.getUVehicleDetail()) {
                        UVehicleDetail uvehicle = new UVehicleDetail();
                        uvehicle.setVin(vehicle.getVin());

                        Vehicle tmpVehicle = new Vehicle(vehicle.getVehicle().getId(), vehicle.getVehicle().getMake(),
                                vehicle.getVehicle().getModel(), vehicle.getVehicle().getEngine(),
                                vehicle.getVehicle().getAm(), vehicle.getVehicle().getFuel(),
                                vehicle.getVehicle().getPuissance(), vehicle.getVehicle().getProtocol(),
                                vehicle.getVehicle().getMode1(), vehicle.getVehicle().getMode2(),
                                vehicle.getVehicle().getMode5(), vehicle.getVehicle().getMode6(),
                                vehicle.getVehicle().getMode8(), vehicle.getVehicle().getMode9(),
                                vehicle.getVehicle().getSource(), vehicle.getVehicle().getVin(),
                                vehicle.getVehicle().getSuportedProtocol(), vehicle.getVehicle().getCo2Emission());
                        uvehicle.setVehicle(tmpVehicle);
                        vehicles.add(uvehicle);
                    }
                }
                tmpUser.setUVehicleDetail(vehicles);

                break;
            }
        }
        Message msg = null;
        if (test == true) {
            msg = new Message(100, "Congratulations!", "You have accessed a Basic Auth protected resource.", null);
            msg.setUser(tmpUser);

            return msg;
        } else {
            msg = new Message(500, "Alas!", "Accessing protected resource.", null);
            msg.setUser(tmpUser);

            return msg;
        }
    }

    public Message addUser(User user) throws ServiceException {
        User userCreated = null;
        Message msg = null;
        if (this.isMailExist(user.getMail())) {
            msg = new Message(500, "Alas!", "User name already exists !", null);
            msg.setUser(userCreated);

            return msg;
        } else {
            user.setId(null);
            user.setRole("User");
            user.setMdp(Utils.encodeMD5(user.getMdp()));

            userCreated = userRepository.save(user);

            msg = new Message(100, "Congratulations!", "Your account have been created !", null);
            msg.setUser(userCreated);

            return msg;
        }
    }

    public Message addUserDevice(User user) throws ServiceException {
        User userCreated = null;
        Message msg = null;
        if (this.isMailExist(user.getMail())) {
            msg = new Message(500, "Alas!", "User name already exists !", null);
            msg.setUser(userCreated);
            return msg;
        } else {
            // attach device to user
            ArrayList<Device> devices = new ArrayList<Device>();
            query = new Query();
            query.addCriteria(Criteria.where("serialNumber").is(user.getUDevices().get(0).getSerialNumber()));
            try {
                MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
                devices = (ArrayList<Device>) mongoTemplate.find(query, Device.class);
            } catch (Exception e) {
                throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                        ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
            }
            if (devices == null || devices.isEmpty()) {
                msg = new Message(500, "Alas!", "Device does not exists", null);
                msg.setUser(userCreated);
                return msg;
            } else if (devices.get(0).getIsUsed()) {
                msg = new Message(500, "Alas!", "Device already used by another person", null);
                msg.setUser(userCreated);
                return msg;
            } else {
                user.setId(null);
                user.setRole("User");
                user.setMdp(Utils.encodeMD5(user.getMdp()));

                // attach device to user
                user.getUDevices().get(0).setDevice(devices.get(0));

                userCreated = userRepository.save(user);

                devices.get(0).setIsUsed(true);
                deviceRepository.save(devices.get(0));

                msg = new Message(100, "Congratulations!", "Your account have been created !", null);
                msg.setUser(userCreated);

                return msg;
            }
        }
    }

    public User updateUser(User updatedUser, String id) {
        updatedUser.setId(id);
        return userRepository.save(updatedUser);
    }

    public void removeUser(String userId) throws ServiceException {
        List<Trip> userTrip ;

        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.addCriteria(Criteria.where("user.id").is(userId).andOperator((Criteria.where("duration").gt(0))));
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            userTrip = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }
        userRepository.delete(userId);
        for (Trip trip : userTrip) {
            tripService.removeTrip(trip.getId());
        }
    }

    public void removeAllUsers() {
        userRepository.deleteAll();
    }

    /**
     * remove the device done by the idDevice from list of devices is user
     *
     * @param userId
     * @param idDevice
     * @return the new user just updated
     */
    public User removeUserDevice(String userId, String idDevice) {
        User user = userRepository.findById(userId);
        List<UDevice> devices = user.getUDevices();
        List<UDevice> modifiedDevices = devices.stream()
                .filter(line -> !idDevice.equals(line.getDevice().getId()))
                .collect(Collectors.toList());
        user.setUDevices(modifiedDevices);
        return userRepository.save(user);
    }

    public Boolean isMailExist(String mail) throws ServiceException {

        boolean test = false;

        Query query = new Query();
        query.addCriteria(Criteria.where("mail").is(mail));

        MongoTemplate mongoTemplate;
        try {
            mongoTemplate = springMongoConfig.mongoTemplate();
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }
        existingUsers = mongoTemplate.find(query, User.class);

        for (User user : existingUsers) {
            if (user.getMail().equals(String.valueOf(mail))) {
                test = true;
                break;
            }
        }

        return test;
    }

    private byte[] generateFeesReport(String userId, Date startDate, Date endDate, String context) throws ServiceException {

        byte[] report = null;
        HashMap<String, Object> results = Utils.getFeesResults(userId, startDate, endDate, springMongoConfig, userRepository, context);
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("totalDistance", results.get("totalDistance"));
        params.put("totalFee", results.get("totalFee"));
        params.put("stringDate", results.get("stringDate"));
        params.put("userName", results.get("userName"));
        params.put("urlLogo", results.get("urlLogo"));
        params.put("power", results.get("power"));
        params.put("make", results.get("make"));
        params.put("model", results.get("model"));
        params.put("feeRule", results.get("registrationNumber"));
        params.put("registrationNumber", results.get("feeRule"));
        JRBeanCollectionDataSource dataSource = new JRBeanCollectionDataSource((ArrayList<TripHeaderFee>) results.get("list"), false);
        // CustomJRDataSource<TripHeaderFee> dataSource = new CustomJRDataSource<TripHeaderFee>().initBy((ArrayList<TripHeaderFee>)results.get("list"));
        try {
            JasperReport jasperReport = JasperCompileManager.compileReport(springJasperConfig.getFeesFilePath());
            JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, params, dataSource);
            report = JasperExportManager.exportReportToPdf(jasperPrint);
        } catch (Exception e) {
            System.err.println(e.toString());
        }
        return report;
    }

    public byte[] generateFeesReportDates(String userId, Date startDate, Date endDate, String context) throws ServiceException {

        return generateFeesReport(userId, startDate, endDate, context);
    }

    public byte[] generateFeesReportMonth(String userId, int year, int month, String context) throws ServiceException {
        Calendar calMonth = Calendar.getInstance();
        Calendar endMonth = Calendar.getInstance();
        calMonth.set(Calendar.MONTH, month - 1);
        calMonth.set(Calendar.YEAR, year);
        calMonth.set(Calendar.DAY_OF_MONTH, 1);
        calMonth.set(Calendar.HOUR_OF_DAY, 1);
        calMonth.set(Calendar.MINUTE, 0);
        calMonth.set(Calendar.SECOND, 0);
        calMonth.set(Calendar.MILLISECOND, 0);

        endMonth.set(Calendar.MONTH, month - 1);
        endMonth.set(Calendar.DATE, calMonth.getActualMaximum(Calendar.DATE));
        endMonth.set(Calendar.HOUR_OF_DAY, 24);
        endMonth.set(Calendar.MINUTE, 59);
        endMonth.set(Calendar.SECOND, 59);
        endMonth.set(Calendar.MILLISECOND, 59);

        return generateFeesReport(userId, calMonth.getTime(), endMonth.getTime(), context);
    }

    public byte[] generateFeesReportYear(String userId, int year, String context) throws ServiceException {
        Locale locale1 = Locale.FRENCH;
        TimeZone tz1 = TimeZone.getTimeZone("FR");
        Calendar calYear = Calendar.getInstance(tz1, locale1);
        Calendar endYear = Calendar.getInstance(tz1, locale1);
        calYear.set(Calendar.YEAR, year);

        calYear.set(Calendar.MONTH, 0);
        calYear.set(Calendar.DAY_OF_MONTH, 1);
        calYear.set(Calendar.HOUR_OF_DAY, 0);
        calYear.set(Calendar.MINUTE, 0);
        calYear.set(Calendar.SECOND, 0);
        calYear.set(Calendar.MILLISECOND, 0);

        endYear.set(Calendar.YEAR, year);
        endYear.set(Calendar.MONTH, 11);
        endYear.set(Calendar.DAY_OF_MONTH, 31);
        endYear.set(Calendar.HOUR_OF_DAY, 23);
        endYear.set(Calendar.MINUTE, 59);
        endYear.set(Calendar.SECOND, 59);
        endYear.set(Calendar.MILLISECOND, 59);

        return generateFeesReport(userId, calYear.getTime(), endYear.getTime(), context);
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByFirstName(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        }
        return new UserPrincipal(user);
    }
}