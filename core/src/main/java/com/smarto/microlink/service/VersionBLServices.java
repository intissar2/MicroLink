package com.smarto.microlink.service;

import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.VersionBLRepository;
import com.smarto.microlink.domain.VersionBL;
import com.smarto.microlink.exception.ServiceException;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;


/**
 * @author DELL
 * 
 */

@Service
public class VersionBLServices {

	public VersionBLRepository getVersionBLRepository() {
		return versionBLRepository;
	}

	@Autowired
	private VersionBLRepository versionBLRepository;
	
	@Autowired
	private SpringMongoConfig springMongoConfig;

	private Query query;
	
	/*public List<Fuel> findAllFuel() {
		return VersionBLRepository.findAll();
	}
*/

	/**
	 * get lastVersion BL.
	 * @return
	 * @throws ServiceException
	 */
	public List<VersionBL> findLastVersionBL() throws ServiceException {
		Query query = new Query();
		List<VersionBL> versions=null; ;
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.limit(1);
		MongoTemplate mongoTemplate;
		try {
			mongoTemplate = springMongoConfig.mongoTemplate();
			versions = mongoTemplate.find(query, VersionBL.class);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return versions;
	}

	public VersionBL addVersionBL() throws ServiceException {
		int increment=0;
		String version="BL"+new SimpleDateFormat("YY",Locale.FRENCH).format(new Date());;
		VersionBL versionBL = new VersionBL();
		List<VersionBL> versions= findLastVersionBL();
		if(versions !=null && versions.size()>0)
			increment=Integer.parseInt((versions.get(0).getVersion()).substring(4,(versions.get(0).getVersion()).length()));
		else
			increment=45;// exigée par Hatem comme initialisation
		version+="00";
		version+=++increment;

		versionBL.setDate(new Date());
		versionBL.setVersion(version);
		versionBL = versionBLRepository.save(versionBL);
		return versionBL;
	}

}
