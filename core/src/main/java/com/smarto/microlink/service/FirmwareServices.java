package com.smarto.microlink.service;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.smarto.microlink.aop.ControllerMonitor;
import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.FirmwareRepository;
import com.smarto.microlink.domain.Attribute;
import com.smarto.microlink.domain.Firmware;
import com.smarto.microlink.exception.ExceptionInfo;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.utils.Utils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

@Service
public class FirmwareServices {

    @Autowired
    private FirmwareRepository firmwareRepository;

    @Autowired
    private SpringMongoConfig springMongoConfig;

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerMonitor.class);

    private List<Firmware> firmwares = null;

	/*public Firmware findLastVersion() throws ServiceException {

		DBObject sort = new BasicDBObject();
		sort.put("creationDate", -1);

		Firmware lastVersion = null;
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			DBCursor cursor = mongoTemplate.getDb().getCollection("firmware").find().sort(sort).limit(1);

			if (cursor.hasNext()) {
				lastVersion = mongoTemplate.getConverter().read(Firmware.class, cursor.next());
			}
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		return lastVersion;
	}*/

    public Firmware findById(String id) {
        return firmwareRepository.findOne(id);
    }

    public List<Firmware> findAllFirmware() throws ServiceException {

        DBObject sort = new BasicDBObject();
        sort.put("creationDate", -1);

        firmwares = firmwareRepository.findAll();

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            mongoTemplate.getDb().getCollection("firmware").find().sort(sort);

        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return firmwares;
    }

    public List<Firmware> findByType(String firmwareType) throws ServiceException {

        DBObject sort = new BasicDBObject();
        sort.put("creationDate", -1);

        if (firmwareType == null || firmwareType.length() == 0) {
            throw new ServiceException(ExceptionInfo.MISSING_INPUT_ERROR_CODE, ExceptionInfo.MISSING_INPUT_ERROR_MSG,
                    500);
        }

        firmwares = firmwareRepository.findByType(firmwareType);

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            mongoTemplate.getDb().getCollection("firmware").find().sort(sort);

        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return firmwares;
    }

    public Firmware findByVersion(String firmwareVersion) throws ServiceException {

        if (firmwareVersion == null || firmwareVersion.length() == 0) {
            throw new ServiceException(ExceptionInfo.MISSING_INPUT_ERROR_CODE, ExceptionInfo.MISSING_INPUT_ERROR_MSG,
                    500);
        }

        return firmwareRepository.findByVersion(firmwareVersion);
    }

    public List<Firmware> findByDate(Date firmwareDate)
            throws ServiceException {

        DBObject sort = new BasicDBObject();
        sort.put("creationDate", -1);

        if (firmwareDate == null) {
            throw new ServiceException(ExceptionInfo.MISSING_INPUT_ERROR_CODE, ExceptionInfo.MISSING_INPUT_ERROR_MSG,
                    500);
        }

        firmwares = firmwareRepository.findByCreationDate(firmwareDate);

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            mongoTemplate.getDb().getCollection("firmware").find().sort(sort);

        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return firmwares;
    }

    public Boolean verifyLastVersionFirmware(Date date)
            throws ServiceException {

        Query query = new Query();
        query.addCriteria(Criteria.where("creationDate").gt(date));

        List<Firmware> lastVersions = new ArrayList<Firmware>();
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastVersions = mongoTemplate.find(query, Firmware.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastVersions.isEmpty();
    }

    public Integer addFirmware(Firmware firmware) throws ServiceException {
        System.out.println("ccccccc services");
        // verifying ------------------------------------------------------

        // Dynamic fields
        List<Attribute> listFields;
        if (firmware.getDynamicFields() == null) {
            listFields = new LinkedList<Attribute>();
            firmware.setDynamicFields(listFields);
            ;
        }

        // file Byte code
        if (firmware.getFileBytes() == null || firmware.getFileBytes().length == 0) {
            // firmware file is empty
            LOGGER.warn(ExceptionInfo.MISSING_FIRMWARE_FILE_ERROR_MSG);
            if (LOGGER.isDebugEnabled()) {
                LOGGER.debug("HTTP status 400 | " + ExceptionInfo.MISSING_FIRMWARE_FILE_ERROR_MSG);
            }
            throw new ServiceException(ExceptionInfo.MISSING_FIRMWARE_FILE_ERROR_CODE,
                    ExceptionInfo.MISSING_FIRMWARE_FILE_ERROR_MSG, 400);
        }

        // creation date
        if (firmware.getCreationDate() == null) {
            firmware.setCreationDate(new Date());
        }

        // checksum
        String checksum = firmware.getChecksum();
        if (StringUtils.isEmpty(checksum)) {
            try {
                checksum = Utils.getFileChecksum(firmware.getFileBytes());
                firmware.setChecksum(checksum);
            } catch (Exception e) {
                throw new ServiceException(ExceptionInfo.CANNOT_GET_FILE_CHECKSUM_ERROR_CODE,
                        ExceptionInfo.CANNOT_GET_FILE_CHECKSUM_ERROR_MSG, 400);
            }
        }

        Query query = new Query();
        query.addCriteria(Criteria.where("checksum").is(checksum));

        List<Firmware> existingFirmwares;
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            existingFirmwares = mongoTemplate.find(query, Firmware.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        // saving ------------------------------------------------------------
        Integer result;
        if (existingFirmwares.isEmpty()) {
            firmwareRepository.save(firmware);
            result = 0;
        } else {
            result = 1;
        }

        return result;
    }

    public Firmware updateFirmware(Firmware updatedFirmware, String id) {
        updatedFirmware.setId(id);
        Firmware updated = firmwareRepository.save(updatedFirmware);
        return updated;
    }

    public Firmware removeFirmware(String firmwareId) {
        Firmware removed = firmwareRepository.findById(firmwareId);

        firmwareRepository.delete(firmwareId);

        return removed;
    }

    public Firmware removeFirmwareByVersion(String firmwareVer) {
        Firmware removed = firmwareRepository.findByVersion(firmwareVer);

        firmwareRepository.delete(removed.getId());

        return removed;
    }

    public void removeAllFirmwares() {

        firmwareRepository.deleteAll();
    }

    public List<Firmware> getFirwaresAfterVersion(String version) {
        return firmwareRepository.findByVersionGreaterThan(version);
    }
}
