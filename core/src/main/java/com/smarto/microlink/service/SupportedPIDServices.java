package com.smarto.microlink.service;

import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.SupportedPIDRepository;
import com.smarto.microlink.domain.SupportedPID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SupportedPIDServices {

    @Autowired
    private SpringMongoConfig springMongoConfig;

    @Autowired
    private SupportedPIDRepository supportedPIDRepository;

    private List<SupportedPID> listSupportedPID;
    private List<SupportedPID> existingSupportedPIDWithMode;

    private Query query = null;
    private MongoTemplate mongoTemplate = null;

    public List<SupportedPID> findAllSupportedPIDs() {
        return supportedPIDRepository.findAll();
    }

    public SupportedPID findSupportedPIDByCode(String code) {
        return supportedPIDRepository.findByCode(code);
    }

    public List<SupportedPID> findListSupportedPID(List<String> codeList) {

        listSupportedPID = new ArrayList<SupportedPID>();
        existingSupportedPIDWithMode = new ArrayList<SupportedPID>();

        query = new Query();
        query.addCriteria(Criteria.where("mode").is(codeList.get(0)));
        codeList.remove(0);

        try {
            mongoTemplate = springMongoConfig.mongoTemplate();
            existingSupportedPIDWithMode = mongoTemplate.find(query, SupportedPID.class);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        int i = 0;

        for (SupportedPID supportedPID : existingSupportedPIDWithMode) {
            if (supportedPID.getCode().equals(codeList.get(i))) {
                listSupportedPID.add(supportedPID);
                i++;
                if (i == codeList.size()) {
                    break;
                }
            }
        }

        return listSupportedPID;
    }

    public SupportedPID addSupportedPID(SupportedPID supportedPID) {

        return supportedPIDRepository.save(supportedPID);
    }

    public SupportedPID updateSupportedPID(SupportedPID updatedSupportedPID, String id) {

        updatedSupportedPID.setId(id);

        return supportedPIDRepository.save(updatedSupportedPID);
    }

    public SupportedPID removeSupportedPID(String id) {

        SupportedPID deleted = supportedPIDRepository.findOne(id);
        supportedPIDRepository.delete(id);

        return deleted;
    }

    public void removeAllSupportedPIDs() {

        supportedPIDRepository.deleteAll();
    }

}
