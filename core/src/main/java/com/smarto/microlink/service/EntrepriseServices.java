package com.smarto.microlink.service;

import com.smarto.microlink.configuration.SpringJasperConfig;
import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.EntrepriseRepository;
import com.smarto.microlink.domain.*;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.utils.GeoLocation;
import com.smarto.microlink.utils.Utils;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.List;

@Service
public class EntrepriseServices {

    @Autowired
    private EntrepriseRepository entrepriseRepository;


    public EntrepriseRepository getEntrepriseRepository() {
        return entrepriseRepository;
    }

    @Autowired

    private SpringMongoConfig springMongoConfig;

    @Autowired

    private SpringJasperConfig springJasperConfig;


    public List<Entreprise> findEntreprise() {
        return entrepriseRepository.findAll();
    }

    public Entreprise updateEntreprise(Entreprise updatedEntreprise, String id) {
        updatedEntreprise.setId(id);
        return entrepriseRepository.save(updatedEntreprise);
    }

    public List<Entreprise> getAll() {
        List<Entreprise> allEntreprise = entrepriseRepository.findAll();
        //TODO:SMARTO à optimiser faire une requestte directe depuis mongoDB pour avoir la dernière positin

        return allEntreprise;
    }

    public Message addEntreprise(Entreprise entreprise) throws ServiceException {

        Entreprise entrepriseCreated = null;
        Message msg = null;

        entrepriseCreated = entrepriseRepository.save(entreprise);

        msg = new Message(100, "Congratulations!", "Your account have been created !", entrepriseCreated, null);
        return msg;
    }


    /*public Entreprise addEntreprise(Entreprise entreprise) {

        return entrepriseRepository.save(entreprise);
    }*/

   /* public Entreprise findEntrepriseById(String entrepriseId) {
        return entrepriseRepository.findById(entrepriseId);
    }*/


}