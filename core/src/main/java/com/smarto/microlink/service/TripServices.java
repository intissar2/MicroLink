package com.smarto.microlink.service;

import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.TripRepository;
import com.smarto.microlink.doa.UserRepository;
import com.smarto.microlink.domain.*;
import com.smarto.microlink.enumeration.ETripType;
import com.smarto.microlink.exception.ExceptionInfo;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.utils.EcoDriveCalculation;
import com.smarto.microlink.utils.GeoLocation;
import com.smarto.microlink.utils.Utils;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.BasicQuery;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.util.*;
@Service
public class TripServices {

    public TripRepository getTripRepository() {
        return tripRepository;
    }

    @Autowired

    private TripRepository tripRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private SpringMongoConfig springMongoConfig;

    private List<TripHeader> lastTripsHeaders;
    private List<Trip> lastTrips = null;
    private TripHeader tmpTripHeader = null;

    private Date currentDate = null;
    private Date laterDate = null;
    private Query query;

    private User tmpUser = null;

    private Vehicle tmpVehicle = null;

    public Trip findTripById(String userId, String tripId) {

        Trip trip = null;

        trip = tripRepository.findById(tripId);
        trip.setPoints(new ArrayList<>());


        List<Parameter> listPoint = new ArrayList<>();

        if(trip.getStartTripAdress().equals("")){
            trip.setStartTripAdress(GeoLocation.getAdressLocation(trip.getPoints().get(0).getLat(), trip.getPoints().get(0).getLon()));

            Trip tripUpdated = tripRepository.save(trip);

        }

        if(trip.getEndTripAdress().equals("")){
            trip.setEndTripAdress(GeoLocation.getAdressLocation(trip.getEndTrip().getLat(), trip.getEndTrip().getLon()));

            Trip tripUpdated = tripRepository.save(trip);

        }

        double latitude = 0;
        double longitude = 0;
        try{
            for (int i = 0; i < trip.getParameters().size(); i = i + 9
                    ) {

                if (i == 0) {
                    for (int j = i; j < i + 9; j++) {
                        listPoint.add(trip.getParameters().get(j));
                    }

                } else if (i == trip.getParameters().size() - 9)
                    for (int j = i; j < i + 9; j++) {
                        listPoint.add(trip.getParameters().get(j));
                    }
                else {

                    if (latitude != trip.getParameters().get(i).getLat() || longitude != trip.getParameters().get(i).getLon()) {

                        latitude = trip.getParameters().get(i).getLat();
                        longitude = trip.getParameters().get(i).getLon();
                        for (int j = i; j < i + 9; j++) {
                            listPoint.add(trip.getParameters().get(j));
                        }
                    }
                }


            }
        }catch(Exception e) {
            for (int i = 0; i < trip.getParameters().size(); i = i + 7
                    ) {

                if (i == 0) {
                    for (int j = i; j < i + 7; j++) {
                        listPoint.add(trip.getParameters().get(j));
                    }

                } else if (i == trip.getParameters().size() - 7)
                    for (int j = i; j < i + 7; j++) {
                        listPoint.add(trip.getParameters().get(j));
                    }
                else {

                    if (latitude != trip.getParameters().get(i).getLat() || longitude != trip.getParameters().get(i).getLon()) {

                        latitude = trip.getParameters().get(i).getLat();
                        longitude = trip.getParameters().get(i).getLon();
                        for (int j = i; j < i + 7; j++) {
                            listPoint.add(trip.getParameters().get(j));
                        }
                    }
                }


            }
        }


        trip.setParameters(listPoint);


        if (trip.getUser().getId().equals(userId)) {

            trip.setUser(null);
            trip.setVehicle(null);
            return trip;
        } else
            return null;

    }


    public Trip findTripById2(String userId, String tripId) {

        Trip trip = null;
        List<Point> listPoint = new ArrayList<>();
        trip = tripRepository.findById(tripId);

        if(trip.getStartTripAdress().equals("")){
            trip.setStartTripAdress(GeoLocation.getAdressLocation(trip.getPoints().get(0).getLat(), trip.getPoints().get(0).getLon()));

            Trip tripUpdated = tripRepository.save(trip);

        }

        if(trip.getEndTripAdress().equals("")){
            trip.setEndTripAdress(GeoLocation.getAdressLocation(trip.getEndTrip().getLat(), trip.getEndTrip().getLon()));

            Trip tripUpdated = tripRepository.save(trip);

        }


        int i = 0;
        double latitude = 0;
        double longitude = 0;
        for (Parameter point : trip.getParameters()
                ) {

            if (i == 0)
                listPoint.add(new Point(point.getLat(), point.getLon(), point.getDate(), true, false));
            else if (i == trip.getParameters().size() - 1)
                listPoint.add(new Point(point.getLat(), point.getLon(), point.getDate(), false, true));
            else {

                if (latitude != point.getLat() || longitude != point.getLon()) {

                    latitude = point.getLat();
                    longitude = point.getLon();
                    listPoint.add(new Point(point.getLat(), point.getLon(), point.getDate(), false, false));
                }
            }

            i++;

        }
        trip.setPoints(listPoint);
        trip.setParameters(new ArrayList<>());
        return trip;

    }

    public List<TripHeader> findLastTrip(String userId) throws ServiceException {
        lastTripsHeaders = new ArrayList<>();
        Query query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId));
        query.limit(1);
        MongoTemplate mongoTemplate;
        try {
            mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
            if (lastTrips != null && !lastTrips.isEmpty()) {
                Trip trip = lastTrips.get(0);
                tmpTripHeader = new TripHeader();
                tmpTripHeader.setId(trip.getId());
                tmpTripHeader.setStartTripAdress(trip.getStartTripAdress());
                tmpTripHeader.setEndTripAdress(trip.getEndTripAdress());
                tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
                tmpTripHeader.setDuration(trip.getDuration());
                tmpTripHeader.setDateTrip(trip.getStartTrip().getDate());
                tmpTripHeader.setType(trip.getType());

                tmpUser = new User();
                tmpUser.setId(trip.getUser().getId());
                tmpUser.setFirstName(trip.getUser().getFirstName());
                tmpUser.setLastName(trip.getUser().getLastName());
                tmpUser.setMail(trip.getUser().getMail());

                tmpTripHeader.setUser(tmpUser);
                tmpTripHeader.setTripResult(trip.getTripResult());
                // For Android mobile Application
                lastTripsHeaders.add(tmpTripHeader);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return lastTripsHeaders;
    }

    public List<TripHeader> findTripsLastWeek(String userId) throws ServiceException {
        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();

        Calendar calWeek = Calendar.getInstance();
        Calendar endWeek = Calendar.getInstance();
        calWeek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        endWeek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        calWeek.add(Calendar.DATE, -7);
        calWeek.set(Calendar.HOUR_OF_DAY, 1);
        calWeek.set(Calendar.MINUTE, 0);
        calWeek.set(Calendar.SECOND, 0);
        calWeek.set(Calendar.MILLISECOND, 0);

        endWeek.add(Calendar.DATE, -1);
        endWeek.set(Calendar.HOUR_OF_DAY, 24);
        endWeek.set(Calendar.MINUTE, 59);
        endWeek.set(Calendar.SECOND, 59);
        endWeek.set(Calendar.MILLISECOND, 59);
        currentDate = calWeek.getTime();
        laterDate = endWeek.getTime();

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .andOperator(Criteria.where("startTrip.date").gte(currentDate).lte(laterDate)));
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        for (Trip trip : lastTrips) {
            tmpTripHeader = new TripHeader();

            tmpTripHeader.setId(trip.getId());
            tmpTripHeader.setStartTripAdress(trip.getStartTripAdress());
            tmpTripHeader.setEndTripAdress(trip.getEndTripAdress());
            tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
            tmpTripHeader.setDuration(trip.getDuration());
            tmpTripHeader.setDateTrip(trip.getStartTrip().getDate());
            tmpTripHeader.setType(trip.getType());

            tmpUser = new User();
            tmpUser.setId(trip.getUser().getId());
            tmpUser.setFirstName(trip.getUser().getFirstName());
            tmpUser.setLastName(trip.getUser().getLastName());
            tmpUser.setMail(trip.getUser().getMail());

            tmpTripHeader.setUser(tmpUser);
            tmpTripHeader.setTripResult(trip.getTripResult());

            // For Android mobile Application
            lastTripsHeaders.add(tmpTripHeader);
        }

        return lastTripsHeaders;
    }

    public List<TripHeader> findTripsCurrentWeek(String userId) throws ServiceException {
        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();

        Calendar calCurrent = Calendar.getInstance();
        Calendar endCurrent = Calendar.getInstance();
        calCurrent.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        endCurrent.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        calCurrent.set(Calendar.HOUR_OF_DAY, 1);
        calCurrent.set(Calendar.MINUTE, 0);
        calCurrent.set(Calendar.SECOND, 0);
        calCurrent.set(Calendar.MILLISECOND, 0);

        endCurrent.add(Calendar.DATE, 6);
        endCurrent.set(Calendar.HOUR_OF_DAY, 24);
        endCurrent.set(Calendar.MINUTE, 59);
        endCurrent.set(Calendar.SECOND, 59);
        endCurrent.set(Calendar.MILLISECOND, 59);
        currentDate = calCurrent.getTime();
        laterDate = endCurrent.getTime();
        currentDate = calCurrent.getTime();
        laterDate = endCurrent.getTime();

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .andOperator(Criteria.where("startTrip.date").gte(calCurrent.getTime()).lte(endCurrent.getTime())));
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        for (Trip trip : lastTrips) {
            tmpTripHeader = new TripHeader();
            tmpTripHeader.setId(trip.getId());
            tmpTripHeader.setStartTripAdress(trip.getStartTripAdress());
            tmpTripHeader.setEndTripAdress(trip.getEndTripAdress());
            tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
            tmpTripHeader.setDuration(trip.getDuration());
            tmpTripHeader.setDateTrip(trip.getStartTrip().getDate());
            tmpTripHeader.setType(trip.getType());

            tmpUser = new User();
            tmpUser.setId(trip.getUser().getId());
            tmpUser.setFirstName(trip.getUser().getFirstName());
            tmpUser.setLastName(trip.getUser().getLastName());
            tmpUser.setMail(trip.getUser().getMail());

            tmpTripHeader.setUser(tmpUser);
            tmpTripHeader.setTripResult(trip.getTripResult());
            // For Android mobile Application
            lastTripsHeaders.add(tmpTripHeader);
        }

        return lastTripsHeaders;
    }

    public List<TripHeader> findTripsLastMonth(String userId) throws ServiceException {

        Calendar calMonth = Calendar.getInstance();
        Calendar endMonth = Calendar.getInstance();
        calMonth.add(Calendar.MONTH, -1);
        calMonth.set(Calendar.DAY_OF_MONTH, 1);
        calMonth.set(Calendar.HOUR_OF_DAY, 1);
        calMonth.set(Calendar.MINUTE, 0);
        calMonth.set(Calendar.SECOND, 0);
        calMonth.set(Calendar.MILLISECOND, 0);
        endMonth.add(Calendar.MONTH, -1);
        endMonth.set(Calendar.DATE, calMonth.getActualMaximum(Calendar.DATE));
        endMonth.set(Calendar.HOUR_OF_DAY, 24);
        endMonth.set(Calendar.MINUTE, 59);
        endMonth.set(Calendar.SECOND, 59);
        endMonth.set(Calendar.MILLISECOND, 59);
        Date currentDate = calMonth.getTime();
        Date laterDate = endMonth.getTime();

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .andOperator(Criteria.where("startTrip.date").gte(currentDate).lte(laterDate)));

        List<Trip> lastTrips = new ArrayList<>();

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        lastTripsHeaders = new ArrayList<>();
        TripHeader tmpTripHeader = null;
        for (Trip trip : lastTrips) {
            tmpTripHeader = new TripHeader();
            tmpTripHeader.setId(trip.getId());
            tmpTripHeader.setStartTripAdress(trip.getStartTripAdress());
            tmpTripHeader.setEndTripAdress(trip.getEndTripAdress());
            tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
            tmpTripHeader.setDuration(trip.getDuration());
            tmpTripHeader.setDateTrip(trip.getStartTrip().getDate());
            tmpTripHeader.setType(trip.getType());

            tmpUser = new User();
            tmpUser.setId(trip.getUser().getId());
            tmpUser.setFirstName(trip.getUser().getFirstName());
            tmpUser.setLastName(trip.getUser().getLastName());
            tmpUser.setMail(trip.getUser().getMail());

            tmpTripHeader.setUser(tmpUser);
            tmpTripHeader.setTripResult(trip.getTripResult());
            // For Android mobile Application
            lastTripsHeaders.add(tmpTripHeader);
        }
        return lastTripsHeaders;
    }

    public List<TripHeader> findTripsLastYear(String userId) throws ServiceException {

        Locale locale1 = Locale.FRENCH;
        TimeZone tz1 = TimeZone.getTimeZone("FR");
        Calendar calYear = Calendar.getInstance(tz1, locale1);
        Calendar endYear = Calendar.getInstance(tz1, locale1);
        calYear.add(Calendar.YEAR, -1);

        calYear.set(Calendar.MONTH, 0);
        calYear.set(Calendar.DAY_OF_MONTH, 1);
        calYear.set(Calendar.HOUR_OF_DAY, 0);
        calYear.set(Calendar.MINUTE, 0);
        calYear.set(Calendar.SECOND, 0);
        calYear.set(Calendar.MILLISECOND, 0);

        endYear.add(Calendar.YEAR, -1);
        endYear.set(Calendar.MONTH, 11);
        endYear.set(Calendar.DAY_OF_MONTH, 31);
        endYear.set(Calendar.HOUR_OF_DAY, 23);
        endYear.set(Calendar.MINUTE, 59);
        endYear.set(Calendar.SECOND, 59);
        endYear.set(Calendar.MILLISECOND, 59);

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .andOperator(Criteria.where("startTrip.date").gte(calYear.getTime()).lte(endYear.getTime())));

        List<Trip> lastTrips = new ArrayList<>();

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        lastTripsHeaders = new ArrayList<>();
        TripHeader tmpTripHeader = null;

        for (Trip trip : lastTrips) {
            tmpTripHeader = new TripHeader();
            tmpTripHeader.setId(trip.getId());
            tmpTripHeader.setStartTripAdress(trip.getStartTripAdress());
            tmpTripHeader.setEndTripAdress(trip.getEndTripAdress());
            tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
            tmpTripHeader.setDuration(trip.getDuration());
            tmpTripHeader.setDateTrip(trip.getStartTrip().getDate());
            tmpTripHeader.setType(trip.getType());

            tmpUser = new User();
            tmpUser.setId(trip.getUser().getId());
            tmpUser.setFirstName(trip.getUser().getFirstName());
            tmpUser.setLastName(trip.getUser().getLastName());
            tmpUser.setMail(trip.getUser().getMail());

            tmpTripHeader.setUser(tmpUser);
            tmpTripHeader.setTripResult(trip.getTripResult());

            // For Android mobile Application
            lastTripsHeaders.add(tmpTripHeader);
        }

        return lastTripsHeaders;
    }

    public List<TripHeader> findLastTripsFromDate(String userId, Date date) throws ServiceException {

        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("startTrip.date").gte(date));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);

        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        for (Trip trip : lastTrips) {

            if (trip.getUser().getId().equals(userId)) {
                tmpTripHeader = new TripHeader();

                tmpTripHeader.setId(trip.getId());
                tmpTripHeader.setStartTripAdress(trip.getStartTripAdress());
                tmpTripHeader.setEndTripAdress(trip.getEndTripAdress());
                tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
                tmpTripHeader.setDuration(trip.getDuration());
                tmpTripHeader.setDateTrip(trip.getStartTrip().getDate());
                tmpTripHeader.setType(trip.getType());

                tmpUser = new User();
                tmpUser.setId(trip.getUser().getId());
                tmpUser.setFirstName(trip.getUser().getFirstName());
                tmpUser.setLastName(trip.getUser().getLastName());
                tmpUser.setMail(trip.getUser().getMail());

                tmpTripHeader.setUser(tmpUser);
                tmpTripHeader.setTripResult(trip.getTripResult());

                // For Android mobile Application
                lastTripsHeaders.add(tmpTripHeader);
            }

        }

        return lastTripsHeaders;
    }

    public List<TripHeader> findLastTripsByPage(String userId, int pageNumber, int pageSize) throws ServiceException {

        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();

        final Pageable pageableRequest = new PageRequest(pageNumber - 1, pageSize);

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.with(pageableRequest);

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        for (Trip trip : lastTrips) {

            if (trip.getUser().getId().equals(userId)) {
                tmpTripHeader = new TripHeader();

                tmpTripHeader.setId(trip.getId());
                tmpTripHeader.setStartTripAdress(trip.getStartTripAdress());
                tmpTripHeader.setEndTripAdress(trip.getEndTripAdress());
                tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
                tmpTripHeader.setDuration(trip.getDuration());
                tmpTripHeader.setDateTrip(trip.getStartTrip().getDate());
                tmpTripHeader.setType(trip.getType());

                tmpUser = new User();
                tmpUser.setId(trip.getUser().getId());
                tmpUser.setFirstName(trip.getUser().getFirstName());
                tmpUser.setLastName(trip.getUser().getLastName());
                tmpUser.setMail(trip.getUser().getMail());

                tmpTripHeader.setUser(tmpUser);
                tmpTripHeader.setTripResult(trip.getTripResult());

                // For Android mobile Application
                lastTripsHeaders.add(tmpTripHeader);
            }

        }

        return lastTripsHeaders;
    }

    public List<TripHeader> findTripsBetweenTwoDates(String userId, Date startDate, Date endDate)
            throws ServiceException {

        lastTripsHeaders = new ArrayList<>();
        lastTrips = new ArrayList<>();

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("startTrip.date").gte(startDate).lte(endDate));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        for (Trip trip : lastTrips) {

            if (trip.getUser().getId().equals(userId)) {
                tmpTripHeader = new TripHeader();

                tmpTripHeader.setId(trip.getId());
                tmpTripHeader.setStartTripAdress(trip.getStartTripAdress());
                tmpTripHeader.setEndTripAdress(trip.getEndTripAdress());
                tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
                tmpTripHeader.setDuration(trip.getDuration());
                tmpTripHeader.setDateTrip(trip.getStartTrip().getDate());
                tmpTripHeader.setType(trip.getType());

                tmpUser = new User();
                tmpUser.setId(trip.getUser().getId());
                tmpUser.setFirstName(trip.getUser().getFirstName());
                tmpUser.setLastName(trip.getUser().getLastName());
                tmpUser.setMail(trip.getUser().getMail());

                tmpTripHeader.setUser(tmpUser);
                tmpTripHeader.setTripResult(trip.getTripResult());

                // For Android mobile Application
                lastTripsHeaders.add(tmpTripHeader);
            }

        }

        return lastTripsHeaders;
    }

    public Trip addTrip(@RequestBody @ApiParam(value = "Trip to save") Trip trip,
                        @PathVariable @ApiParam(value = "Trip period") String period,
                        @PathVariable @ApiParam(value = "Trip minPeriodStops") String minPeriodStops) throws ServiceException {

        Trip tripCreated = null;

        if (userRepository.findByMail(trip.getUser().getMail()) != null) {

            trip.setAverageSpeed(Utils.AverageSpeed(trip.getParameters()));
            trip.setStartTrip(Utils.startPoint(trip.getPoints()));
            trip.setEndTrip(Utils.endPoint(trip.getPoints()));
            // we have to set the two attributes startTrip and endTrip points
            // for
            // the calculateDuration(startTrip, endTrip) method
            if (trip.getStartTrip() == null) {
                trip.setDuration(Utils.calculateDuration(trip.getPoints().get(0), trip.getEndTrip()));
            } else {
                trip.setDuration(Utils.calculateDuration(trip.getStartTrip(), trip.getEndTrip()));
            }


            trip.setDistance(Utils.distanceCaculator(trip.getPoints()));

            // recuperer l'adresse de debut et de fin les modifier dans le trip en cours

            if (trip.getStartTrip() == null) {
                trip.setStartTripAdress(GeoLocation.getAdressLocation(trip.getPoints().get(0).getLat(), trip.getPoints().get(0).getLon()));
            } else {
                trip.setStartTripAdress(GeoLocation.getAdressLocation(trip.getStartTrip().getLat(), trip.getStartTrip().getLon()));
            }


            trip.setEndTripAdress(GeoLocation.getAdressLocation(trip.getEndTrip().getLat(), trip.getEndTrip().getLon()));
            // le calcul se fait lors de l'insertion d'un trip

            // le calcul se fait lors de l'insertion d'un trip

            if (trip.getStartTrip() == null) {
                trip.setDateTrip(trip.getPoints().get(0).getDate());
            } else {
                trip.setDateTrip(trip.getStartTrip().getDate());
            }

            trip.setTripResult(EcoDriveCalculation.calculation_Results(trip, Integer.parseInt(period), Integer.parseInt(minPeriodStops)));

            trip.setType(ETripType.PERSO);
            // userRepository.save(trip.getUser());

            tripCreated = tripRepository.save(trip);
        }

        return tripCreated;
    }

    public Trip updateTrip(@RequestBody @ApiParam(value = "Trip Object") Trip updatedTrip,
                           @PathVariable @ApiParam(value = "Trip Code") String id) {

        Trip tripUpdated = tripRepository.save(updatedTrip);

        return tripUpdated;
    }

    public void removeTrip(@PathVariable("tripId") String tripId) {

        tripRepository.delete(tripId);
    }

    public void removeAllTrips() {

        tripRepository.deleteAll();
    }

    public Score getScoreLastTrip(String userId) throws ServiceException {
        lastTripsHeaders = new ArrayList<>();
        Score score = null;
        Query query = new Query();
        List<Trip> trips = new ArrayList<>();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId));
        query.limit(1);
        MongoTemplate mongoTemplate;
        try {
            mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
            if (lastTrips != null && !lastTrips.isEmpty()) {
                Trip trip = lastTrips.get(0);
                trips.add(trip);
                score = EcoDriveCalculation.CalculateScoreTrips(trips);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return score;
    }

    public Score getScoreTripsLastWeek(String userId) throws ServiceException {
        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();
        Score score = null;

        Calendar calWeek = Calendar.getInstance();
        Calendar endWeek = Calendar.getInstance();
        calWeek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        endWeek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        calWeek.add(Calendar.DATE, -7);
        calWeek.set(Calendar.HOUR_OF_DAY, 1);
        calWeek.set(Calendar.MINUTE, 0);
        calWeek.set(Calendar.SECOND, 0);
        calWeek.set(Calendar.MILLISECOND, 0);

        endWeek.add(Calendar.DATE, -1);
        endWeek.set(Calendar.HOUR_OF_DAY, 24);
        endWeek.set(Calendar.MINUTE, 59);
        endWeek.set(Calendar.SECOND, 59);
        endWeek.set(Calendar.MILLISECOND, 59);
        currentDate = calWeek.getTime();
        laterDate = endWeek.getTime();

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .andOperator(Criteria.where("startTrip.date").gte(currentDate).lte(laterDate)));
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }
        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);

        return score;
    }

    public Score getScoreTripsCurrentWeek(String userId) throws ServiceException {
        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();
        Score score = null;

        Calendar calCurrent = Calendar.getInstance();
        Calendar endCurrent = Calendar.getInstance();
        calCurrent.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        endCurrent.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        calCurrent.set(Calendar.HOUR_OF_DAY, 1);
        calCurrent.set(Calendar.MINUTE, 0);
        calCurrent.set(Calendar.SECOND, 0);
        calCurrent.set(Calendar.MILLISECOND, 0);

        endCurrent.add(Calendar.DATE, 6);
        endCurrent.set(Calendar.HOUR_OF_DAY, 24);
        endCurrent.set(Calendar.MINUTE, 59);
        endCurrent.set(Calendar.SECOND, 59);
        endCurrent.set(Calendar.MILLISECOND, 59);
        currentDate = calCurrent.getTime();
        laterDate = endCurrent.getTime();
        currentDate = calCurrent.getTime();
        laterDate = endCurrent.getTime();

        //  query = new Query();
        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");

        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .and("startTrip.date").gte(calCurrent.getTime()).lte(endCurrent.getTime()).andOperator((Criteria.where("duration").gt(0))));
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);

        return score;
    }

    public Score getScoreTripsLastMonth(String userId) throws ServiceException {

        Calendar calMonth = Calendar.getInstance();
        Calendar endMonth = Calendar.getInstance();
        calMonth.add(Calendar.MONTH, -1);
        calMonth.set(Calendar.DAY_OF_MONTH, 1);
        calMonth.set(Calendar.HOUR_OF_DAY, 1);
        calMonth.set(Calendar.MINUTE, 0);
        calMonth.set(Calendar.SECOND, 0);
        calMonth.set(Calendar.MILLISECOND, 0);
        endMonth.add(Calendar.MONTH, -1);
        endMonth.set(Calendar.DATE, calMonth.getActualMaximum(Calendar.DATE));
        endMonth.set(Calendar.HOUR_OF_DAY, 24);
        endMonth.set(Calendar.MINUTE, 59);
        endMonth.set(Calendar.SECOND, 59);
        endMonth.set(Calendar.MILLISECOND, 59);
        Date currentDate = calMonth.getTime();
        Date laterDate = endMonth.getTime();

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .andOperator(Criteria.where("startTrip.date").gte(currentDate).lte(laterDate)));

        List<Trip> lastTrips = new ArrayList<>();
        Score score = null;

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);
        return score;
    }

    public Score getScoreTripsLastYear(String userId) throws ServiceException {

        Locale locale1 = Locale.FRENCH;
        TimeZone tz1 = TimeZone.getTimeZone("FR");
        Calendar calYear = Calendar.getInstance(tz1, locale1);
        Calendar endYear = Calendar.getInstance(tz1, locale1);
        calYear.add(Calendar.YEAR, -1);

        calYear.set(Calendar.MONTH, 0);
        calYear.set(Calendar.DAY_OF_MONTH, 1);
        calYear.set(Calendar.HOUR_OF_DAY, 0);
        calYear.set(Calendar.MINUTE, 0);
        calYear.set(Calendar.SECOND, 0);
        calYear.set(Calendar.MILLISECOND, 0);

        endYear.add(Calendar.YEAR, -1);
        endYear.set(Calendar.MONTH, 11);
        endYear.set(Calendar.DAY_OF_MONTH, 31);
        endYear.set(Calendar.HOUR_OF_DAY, 23);
        endYear.set(Calendar.MINUTE, 59);
        endYear.set(Calendar.SECOND, 59);
        endYear.set(Calendar.MILLISECOND, 59);

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .andOperator(Criteria.where("startTrip.date").gte(calYear.getTime()).lte(endYear.getTime())));

        List<Trip> lastTrips = new ArrayList<>();
        Score score = null;

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        lastTripsHeaders = new ArrayList<>();
        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);

        return score;
    }

    public Score getScoreLastTripsFromDate(String userId, Date date) throws ServiceException {

        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();
        Score score = null;

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("startTrip.date").gte(date));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);

        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }
        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);

        return score;
    }

    public Score getScoreLastTripsByPage(String userId, int pageNumber, int pageSize) throws ServiceException {

        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();
        Score score = null;

        final Pageable pageableRequest = new PageRequest(pageNumber - 1, pageSize);

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.with(pageableRequest);

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);

        return score;
    }

    public Score getScoreTripsBetweenTwoDates(String userId, Date startDate, Date endDate)
            throws ServiceException {

        lastTripsHeaders = new ArrayList<>();
        lastTrips = new ArrayList<>();
        Score score = null;

        query = new Query();
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("startTrip.date").gte(startDate).lte(endDate));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);

        return score;
    }

    public Score getScoreTripsCurrentMonth(String userId) throws ServiceException {

        Locale locale1 = Locale.FRENCH;
        TimeZone tz1 = TimeZone.getTimeZone("FR");
        Calendar calMonth = Calendar.getInstance(tz1, locale1);
        Calendar endMonth = Calendar.getInstance(tz1, locale1);
        calMonth.set(Calendar.DATE, calMonth.getActualMinimum(Calendar.DATE));
        calMonth.set(Calendar.HOUR_OF_DAY, 0);
        calMonth.set(Calendar.MINUTE, 0);
        calMonth.set(Calendar.SECOND, 0);
        calMonth.set(Calendar.MILLISECOND, 0);
        endMonth.set(Calendar.DATE, endMonth.getActualMaximum(Calendar.DATE));
        endMonth.set(Calendar.HOUR_OF_DAY, 23);
        endMonth.set(Calendar.MINUTE, 59);
        endMonth.set(Calendar.SECOND, 59);
        endMonth.set(Calendar.MILLISECOND, 59);
        Date currentDate = calMonth.getTime();
        Date laterDate = endMonth.getTime();

        // query = new Query();
        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");

        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .and("startTrip.date").gte(currentDate).lte(laterDate).andOperator((Criteria.where("duration").gt(0))));

        List<Trip> lastTrips = new ArrayList<>();
        Score score = null;

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);
        return score;
    }

    public Score getScoreTripsCurrentYear(String userId) throws ServiceException {

        Locale locale1 = Locale.FRENCH;
        TimeZone tz1 = TimeZone.getTimeZone("FR");
        Calendar calYear = Calendar.getInstance(tz1, locale1);
        Calendar endYear = Calendar.getInstance(tz1, locale1);

        calYear.set(Calendar.MONTH, 0);
        calYear.set(Calendar.DAY_OF_MONTH, 1);
        calYear.set(Calendar.HOUR_OF_DAY, 0);
        calYear.set(Calendar.MINUTE, 0);
        calYear.set(Calendar.SECOND, 0);
        calYear.set(Calendar.MILLISECOND, 0);

        endYear.set(Calendar.MONTH, 11);
        endYear.set(Calendar.DAY_OF_MONTH, 31);
        endYear.set(Calendar.HOUR_OF_DAY, 23);
        endYear.set(Calendar.MINUTE, 59);
        endYear.set(Calendar.SECOND, 59);
        endYear.set(Calendar.MILLISECOND, 59);

        //  query = new Query();
        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");

        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .and("startTrip.date").gte(calYear.getTime()).lte(endYear.getTime()).andOperator((Criteria.where("duration").gt(0))));

        List<Trip> lastTrips = new ArrayList<>();
        Score score = null;

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        lastTripsHeaders = new ArrayList<>();
        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);

        return score;
    }

    public Score getScoreTripsCurrentDay(String userId) throws ServiceException {

        Locale locale1 = Locale.FRENCH;
        TimeZone tz1 = TimeZone.getTimeZone("FR");
        Calendar calYear = Calendar.getInstance(tz1, locale1);
        Calendar endYear = Calendar.getInstance(tz1, locale1);

        calYear.set(Calendar.HOUR_OF_DAY, 0);
        calYear.set(Calendar.MINUTE, 0);
        calYear.set(Calendar.SECOND, 0);
        calYear.set(Calendar.MILLISECOND, 0);

        endYear.set(Calendar.HOUR_OF_DAY, 23);
        endYear.set(Calendar.MINUTE, 59);
        endYear.set(Calendar.SECOND, 59);
        endYear.set(Calendar.MILLISECOND, 59);

        // query = new Query();
        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");

        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .and("startTrip.date").gte(calYear.getTime()).lte(endYear.getTime()).andOperator((Criteria.where("duration").gt(0))));

        List<Trip> lastTrips = new ArrayList<>();
        Score score = null;

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        lastTripsHeaders = new ArrayList<>();
        score = EcoDriveCalculation.CalculateScoreTrips(lastTrips);

        return score;
    }


    //optimisation par Walid
    public List<Trip> findAllTrips2(String userId) throws ServiceException {
        lastTripsHeaders = new ArrayList<>();
        lastTrips = new ArrayList<>();


        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.addCriteria(Criteria.where("user.id").is(userId).andOperator((Criteria.where("duration").gt(0))));
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }


    public List<Trip> findLastTrip2(String userId) throws ServiceException {
        lastTripsHeaders = new ArrayList<>();
        lastTrips = new ArrayList<>();

        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'vehicle':0}");
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId).andOperator((Criteria.where("duration").gt(0))));
        query.limit(1);


        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }


    public List<Trip> findTripsLastWeek2(String userId) throws ServiceException {
        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();

        Calendar calWeek = Calendar.getInstance();
        Calendar endWeek = Calendar.getInstance();
        calWeek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        endWeek.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        calWeek.add(Calendar.DATE, -7);
        calWeek.set(Calendar.HOUR_OF_DAY, 1);
        calWeek.set(Calendar.MINUTE, 0);
        calWeek.set(Calendar.SECOND, 0);
        calWeek.set(Calendar.MILLISECOND, 0);

        endWeek.add(Calendar.DATE, -1);
        endWeek.set(Calendar.HOUR_OF_DAY, 24);
        endWeek.set(Calendar.MINUTE, 59);
        endWeek.set(Calendar.SECOND, 59);
        endWeek.set(Calendar.MILLISECOND, 59);
        currentDate = calWeek.getTime();
        laterDate = endWeek.getTime();

        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .and("startTrip.date").gte(currentDate).lte(laterDate).andOperator((Criteria.where("duration").gt(0))));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }


    public List<Trip> findTripsCurrentWeek2(String userId) throws ServiceException {
        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();

        Calendar calCurrent = Calendar.getInstance();
        Calendar endCurrent = Calendar.getInstance();
        calCurrent.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
        endCurrent.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);

        calCurrent.set(Calendar.HOUR_OF_DAY, 1);
        calCurrent.set(Calendar.MINUTE, 0);
        calCurrent.set(Calendar.SECOND, 0);
        calCurrent.set(Calendar.MILLISECOND, 0);

        endCurrent.add(Calendar.DATE, 6);
        endCurrent.set(Calendar.HOUR_OF_DAY, 24);
        endCurrent.set(Calendar.MINUTE, 59);
        endCurrent.set(Calendar.SECOND, 59);
        endCurrent.set(Calendar.MILLISECOND, 59);
        currentDate = calCurrent.getTime();
        laterDate = endCurrent.getTime();


        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .and("startTrip.date").gte(calCurrent.getTime()).lte(endCurrent.getTime()).andOperator((Criteria.where("duration").gt(0))));


        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }


    public List<Trip> findTripsLastMonth2(String userId) throws ServiceException {


        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();

        Calendar calMonth = Calendar.getInstance();
        Calendar endMonth = Calendar.getInstance();
        calMonth.add(Calendar.MONTH, -1);
        calMonth.set(Calendar.DAY_OF_MONTH, 1);
        calMonth.set(Calendar.HOUR_OF_DAY, 1);
        calMonth.set(Calendar.MINUTE, 0);
        calMonth.set(Calendar.SECOND, 0);
        calMonth.set(Calendar.MILLISECOND, 0);
        endMonth.add(Calendar.MONTH, -1);
        endMonth.set(Calendar.DATE, calMonth.getActualMaximum(Calendar.DATE));
        endMonth.set(Calendar.HOUR_OF_DAY, 24);
        endMonth.set(Calendar.MINUTE, 59);
        endMonth.set(Calendar.SECOND, 59);
        endMonth.set(Calendar.MILLISECOND, 59);
        Date currentDate = calMonth.getTime();
        Date laterDate = endMonth.getTime();

        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .and("startTrip.date").gte(currentDate).lte(laterDate).andOperator((Criteria.where("duration").gt(0))));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }


    public List<Trip> findTripsLastYear2(String userId) throws ServiceException {
        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();


        Locale locale1 = Locale.FRENCH;
        TimeZone tz1 = TimeZone.getTimeZone("FR");
        Calendar calYear = Calendar.getInstance(tz1, locale1);
        Calendar endYear = Calendar.getInstance(tz1, locale1);
        calYear.add(Calendar.YEAR, -1);

        calYear.set(Calendar.MONTH, 0);
        calYear.set(Calendar.DAY_OF_MONTH, 1);
        calYear.set(Calendar.HOUR_OF_DAY, 0);
        calYear.set(Calendar.MINUTE, 0);
        calYear.set(Calendar.SECOND, 0);
        calYear.set(Calendar.MILLISECOND, 0);

        endYear.add(Calendar.YEAR, -1);
        endYear.set(Calendar.MONTH, 11);
        endYear.set(Calendar.DAY_OF_MONTH, 31);
        endYear.set(Calendar.HOUR_OF_DAY, 23);
        endYear.set(Calendar.MINUTE, 59);
        endYear.set(Calendar.SECOND, 59);
        endYear.set(Calendar.MILLISECOND, 59);

        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId)
                .and("startTrip.date").gte(calYear.getTime()).lte(endYear.getTime()).andOperator((Criteria.where("duration").gt(0))));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }


    public List<Trip> findLastTripsFromDate2(String userId, Date date) throws ServiceException {

        lastTrips = new ArrayList<>();
        lastTripsHeaders = new ArrayList<>();

        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId).and("startTrip.date").gte(date).andOperator((Criteria.where("duration").gt(0))));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }


    public List<Trip> findTripsBetweenTwoDates2(String userId, Date startDate, Date endDate)
            throws ServiceException {

        lastTripsHeaders = new ArrayList<>();
        lastTrips = new ArrayList<>();

        BasicQuery query = new BasicQuery("{ }", "{'user':0, 'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("user.id").is(userId).and("startTrip.date").gte(startDate).lte(endDate).andOperator((Criteria.where("duration").gt(0))));


        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }


    /* *************************************** */

    //optimisation par Walid
    public List<Trip> findUsersTrip() throws ServiceException {
        lastTripsHeaders = new ArrayList<>();
        lastTrips = new ArrayList<>();


        BasicQuery query = new BasicQuery("{ }", "{'points' : 0, 'parameters' : 0, 'startTrip' :0 , 'endTrip':0, 'vehicle':0}");
        query.addCriteria(Criteria.where("duration").gt(0));
        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));

        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        return lastTrips;
    }

}