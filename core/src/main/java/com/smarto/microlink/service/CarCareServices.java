package com.smarto.microlink.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.smarto.microlink.configuration.SpringJasperConfig;
import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.CarCareRepository;
import com.smarto.microlink.domain.CarCare;
import com.smarto.microlink.domain.CarCareReport;
import com.smarto.microlink.exception.ExceptionInfo;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.utils.CustomJRDataSource;

import io.swagger.annotations.ApiParam;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;


/**
 * @author DELL
 * 
 */

@Service
public class CarCareServices {

	public CarCareRepository getCarCareRepository() {
		return carCareRepository;
	}

	@Autowired
	private CarCareRepository carCareRepository;
	
	@Autowired
	private SpringMongoConfig springMongoConfig;

	@Autowired
	private SpringJasperConfig springJasperConfig;
	
	private Query query;
	
	public List<CarCare> findAllCarCare() {
		return carCareRepository.findAll();
	}
	
	
	public CarCare findCarCareById(String id) {
		CarCare carCare = null;
		carCare = carCareRepository.findById(id);
		return carCare;
	}
	
	public List<CarCare> findCarCareByUserId(String userId) throws ServiceException {
		List<CarCare> carCares = null;
		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			carCares = mongoTemplate.find(query, CarCare.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		return carCares;
	}
	
	public List<CarCare> findCarCareByVehicleId(String vehicleId) throws ServiceException {
		List<CarCare> carCares = null;
		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("vehicle.id").is(vehicleId));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			carCares = mongoTemplate.find(query, CarCare.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		return carCares;
	}
	
	public List<CarCare> findCarCareByPeriod(String userId,String vehicleId,Date startDate,Date endDate) throws ServiceException {
		List<CarCare> carCares = null;
		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId)
				.and("vehicle.id").is(vehicleId)
				.and("date").gte(startDate).lte(endDate));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			carCares = mongoTemplate.find(query, CarCare.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		return carCares;
	}
	
	public byte[] generateCarCareReport(String userId, String vehicleId, Date startDate,Date endDate) throws ServiceException {

		byte[] report = null;

		query = new Query();
		query.with(new Sort(Sort.Direction.ASC, "date"));

		List<CarCare> details = null;
		List<CarCareReport> detailsReport = new ArrayList<CarCareReport>();
		Double totPrice=0d;
		String period="";
		Locale locale1 = Locale.FRENCH;
		HashMap< String, Object> parameters=new HashMap<String, Object>();
		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId)
				.and("vehicle.id").is(vehicleId)
				.and("date").gte(startDate).lte(endDate));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			details = mongoTemplate.find(query, CarCare.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		period="du "+new SimpleDateFormat("dd/MM/yyyy").format(startDate)+ " au "+new SimpleDateFormat("dd/MM/yyyy").format(endDate);
		if(details.size()>0){
			parameters.put("user", details.get(0).getUser().getFirstName()+ " " + details.get(0).getUser().getLastName());
			parameters.put("model", details.get(0).getVehicle().getModel());
			parameters.put("make", details.get(0).getVehicle().getMake());
			parameters.put("period", period);
			for(CarCare carCare:details){
				totPrice+=carCare.getPrice();
				CarCareReport carRep=new CarCareReport();
				carRep.setCompany(carCare.getCompany());
				carRep.setDate(new SimpleDateFormat("EEEE dd MMM YYYY",locale1).format(carCare.getDate()));
				carRep.setDistance(carCare.getDistance());
				carRep.setOption(carCare.getOption());
				carRep.setPrice(carCare.getPrice());
				carRep.setType(carCare.getType());
				detailsReport.add(carRep);
			}
			parameters.put("totPrice", totPrice.toString()+" €");
			parameters.put("immatricule", "");
		}
		
		CustomJRDataSource<CarCareReport> dataSource = new CustomJRDataSource<CarCareReport>().initBy(detailsReport);
		try {
			JasperReport jasperReport = JasperCompileManager.compileReport(springJasperConfig.getCarCareFilePath());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			report = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
		return report;
	}
	
	public byte[] generateCarCareReportByYear(String userId, String vehicleId, String year, String exportType) throws ServiceException {

		byte[] report = null;

		query = new Query();
		query.with(new Sort(Sort.Direction.ASC, "date"));

		List<CarCare> details = null;
		List<CarCareReport> detailsReport = new ArrayList<CarCareReport>();
		Double totPrice=0d;
		String period="";
		HashMap< String, Object> parameters=new HashMap<String, Object>();
		query = new Query();
		
		Locale locale1 = Locale.FRENCH;
		TimeZone tz1 = TimeZone.getTimeZone("FR");
		Calendar startDate = Calendar.getInstance(tz1, locale1);
		Calendar endDate = Calendar.getInstance(tz1, locale1);
		startDate.set(Calendar.YEAR, Integer.parseInt(year));

		startDate.set(Calendar.MONTH, 0);
		startDate.set(Calendar.DAY_OF_MONTH, 1);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);

		endDate.set(Calendar.YEAR, Integer.parseInt(year));
		endDate.set(Calendar.MONTH, 11);
		endDate.set(Calendar.DAY_OF_MONTH, 31);
		endDate.set(Calendar.HOUR_OF_DAY, 22);
		endDate.set(Calendar.MINUTE, 59);
		endDate.set(Calendar.SECOND, 59);
		endDate.set(Calendar.MILLISECOND, 59);
		
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId)
				.and("vehicle.id").is(vehicleId)
				.and("date").gte(startDate.getTime()).lte(endDate.getTime()));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			details = mongoTemplate.find(query, CarCare.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		
		if(details.size()>0){
			parameters.put("user", details.get(0).getUser().getFirstName()+ " " + details.get(0).getUser().getLastName());
			parameters.put("model", details.get(0).getVehicle().getModel());
			parameters.put("make", details.get(0).getVehicle().getMake());
			for(CarCare carCare:details){
				totPrice+=carCare.getPrice();
				CarCareReport carRep=new CarCareReport();
				carRep.setCompany(carCare.getCompany());
				carRep.setDate(new SimpleDateFormat("EEEE dd MMM YYYY",locale1).format(carCare.getDate()));
				carRep.setDistance(carCare.getDistance());
				carRep.setOption(carCare.getOption());
				carRep.setPrice(carCare.getPrice());
				carRep.setType(carCare.getType());
				carRep.setMonth(new SimpleDateFormat("MMM",locale1).format(carCare.getDate()));
				detailsReport.add(carRep);
			}
			parameters.put("totPrice", totPrice.toString()+" €");
			parameters.put("immatricule", "");
			// generate period Label
			Calendar cal=Calendar.getInstance();
			cal.set(Calendar.YEAR,Integer.parseInt(year));
			period= new SimpleDateFormat("YYYY",locale1).format(cal.getTime()).toUpperCase() ;
			parameters.put("period", period);
		}
		
		CustomJRDataSource<CarCareReport> dataSource = new CustomJRDataSource<CarCareReport>().initBy(detailsReport);
		try {
			JasperReport jasperReport = JasperCompileManager.compileReport(springJasperConfig.getCarCareFilePath());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			report = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
		return report;
	}
	
	public byte[] generateCarCareReportByMonth(String userId, String vehicleId, String year,String month,String reportType) throws ServiceException {

		byte[] report = null;

		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));

		List<CarCare> details = null;
		List<CarCareReport> detailsReport = new ArrayList<CarCareReport>();
		Double totPrice=0d;
		String period="";
		HashMap< String, Object> parameters=new HashMap<String, Object>();
		query = new Query();
		
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		Locale locale1 = Locale.FRENCH;
		startDate.set(Calendar.YEAR, Integer.parseInt(year));
		startDate.set(Calendar.MONTH, Integer.parseInt(month)-1);
		startDate.set(Calendar.DAY_OF_MONTH, 1);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);
		endDate.set(Calendar.YEAR, Integer.parseInt(year));
		endDate.set(Calendar.MONTH, Integer.parseInt(month)-1);
		endDate.set(Calendar.DATE, startDate.getActualMaximum(Calendar.DATE));
		endDate.set(Calendar.HOUR_OF_DAY, 23);
		endDate.set(Calendar.MINUTE, 59);
		endDate.set(Calendar.SECOND, 59);
		endDate.set(Calendar.MILLISECOND, 59);
		System.out.println("startDate=="+startDate.getTime());
		System.out.println("endDate=="+endDate.getTime());
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId)
				.and("vehicle.id").is(vehicleId)
				.and("date").gte(startDate.getTime()).lte(endDate.getTime()));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			details = mongoTemplate.find(query, CarCare.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		
		if(details.size()>0){
			parameters.put("user", details.get(0).getUser().getFirstName()+ " " + details.get(0).getUser().getLastName());
			parameters.put("model", details.get(0).getVehicle().getModel());
			parameters.put("make", details.get(0).getVehicle().getMake());
			// generate total Prices CarCare
			for(CarCare carCare:details){
				totPrice+=carCare.getPrice();
				CarCareReport carRep=new CarCareReport();
				carRep.setCompany(carCare.getCompany());
				carRep.setDate(new SimpleDateFormat("EEEE dd MMM YYYY",locale1).format(carCare.getDate()));
				carRep.setDistance(carCare.getDistance());
				carRep.setOption(carCare.getOption());
				carRep.setPrice(carCare.getPrice());
				carRep.setType(carCare.getType());
				detailsReport.add(carRep);
			}
			parameters.put("totPrice", totPrice.toString()+" €");
			// generate period Label
			Calendar cal=Calendar.getInstance();
			cal.set(Calendar.MONTH, Integer.parseInt(month)-1);
			cal.set(Calendar.YEAR,Integer.parseInt(year));
			period= new SimpleDateFormat("MMM",locale1).format(cal.getTime()).toUpperCase() + new SimpleDateFormat("YYYY",locale1).format(cal.getTime());
			parameters.put("period", period);
			parameters.put("immatricule", "");
		}
		
		CustomJRDataSource<CarCareReport> dataSource = new CustomJRDataSource<CarCareReport>().initBy(detailsReport);
		try {
			JasperReport jasperReport = JasperCompileManager.compileReport(springJasperConfig.getCarCareFilePath());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			report = JasperExportManager.exportReportToPdf(jasperPrint);

		} catch (Exception e) {
			System.err.println(e.toString());
		}
		return report;
	}

	public CarCare addCarCare(@RequestBody @ApiParam(value = "CarCare to save") CarCare carCare) throws ServiceException {
		CarCare cCare = null;
		cCare = carCareRepository.save(carCare);
		return cCare;
	}
	
	public CarCare updateCarCare(CarCare updatedCarCare, String id) {
		
		return carCareRepository.save(updatedCarCare);
	}

	public void removeCarCare(@PathVariable("carCareId") String carCareId) {
		carCareRepository.delete(carCareId);
	}

	public void removeAllCarCares() {
		carCareRepository.deleteAll();
	}
}
