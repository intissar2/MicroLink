package com.smarto.microlink.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.smarto.microlink.configuration.SpringJasperConfig;
import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.FuelRepository;
import com.smarto.microlink.domain.Fuel;
import com.smarto.microlink.domain.FuelReport;
import com.smarto.microlink.exception.ExceptionInfo;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.utils.CustomJRDataSource;
import com.smarto.microlink.utils.GeoLocation;

import io.swagger.annotations.ApiParam;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;


/**
 * @author DELL
 * 
 */

@Service
public class FuelServices {

	public FuelRepository getFuelRepository() {
		return fuelRepository;
	}

	@Autowired
	private FuelRepository fuelRepository;
	
	@Autowired
	private SpringMongoConfig springMongoConfig;

	@Autowired
	private SpringJasperConfig springJasperConfig;
	
	private Query query;
	
	public List<Fuel> findAllFuel() {
		return fuelRepository.findAll();
	}
	
	
	public Fuel findFuelById(String id) {
		Fuel fuel = null;
		fuel = fuelRepository.findById(id);
		return fuel;
	}
	
	public List<Fuel> findFuelByUserId(String userId) throws ServiceException {
		List<Fuel> fuels = null;
		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			fuels = mongoTemplate.find(query, Fuel.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		return fuels;
	}
	
	public List<Fuel> findFuelByPeriod(String userId,Date startDate,Date endDate) throws ServiceException {
		List<Fuel> fuels = null;
		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId)
				
				.and("date").gte(startDate).lte(endDate));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			fuels = mongoTemplate.find(query, Fuel.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		return fuels;
	}
	
	public byte[] generateFuelReport(String userId,String vehicleId, Date startDate,Date endDate) throws ServiceException {

		byte[] report = null;

		query = new Query();
		query.with(new Sort(Sort.Direction.ASC, "date"));
		Locale locale1 = Locale.FRENCH;
		List<Fuel> details = null;
		List<FuelReport> detailsReport = new ArrayList<FuelReport>();
		Double totPrice=0d;
		String period="";
		HashMap< String, Object> parameters=new HashMap<String, Object>();
		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId)
				.and("vehicle.id").is(vehicleId)
				.and("date").gte(startDate).lte(endDate));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			details = mongoTemplate.find(query, Fuel.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		period="du "+new SimpleDateFormat("dd/MM/yyyy",locale1).format(startDate)+ " au "+new SimpleDateFormat("dd/MM/yyyy",locale1).format(endDate);
		if(details.size()>0){
			parameters.put("user", details.get(0).getUser().getFirstName()+ " " + details.get(0).getUser().getLastName());
			parameters.put("period", period);
			parameters.put("model", details.get(0).getVehicle().getModel());
			parameters.put("make", details.get(0).getVehicle().getMake());
			for(Fuel fuel:details){
				totPrice+=fuel.getFuelPrice();
				FuelReport fuelRep=new FuelReport();
				fuelRep.setFuel(fuel.getFuel());
				fuelRep.setDate(new SimpleDateFormat("EEEE dd MMM YYYY",locale1).format(fuel.getDate()));
				fuelRep.setAdress(fuel.getAdress());
				fuelRep.setStation(fuel.getStation());
				fuelRep.setFuelPrice(fuel.getFuelPrice());
				fuelRep.setVolume(fuel.getVolume());
				fuelRep.setLatitude(fuel.getLatitude());
				fuelRep.setLongitude(fuel.getLongitude());
				detailsReport.add(fuelRep);
			}
			parameters.put("totPrice", totPrice.toString()+" €");
			parameters.put("immatricule", "");
		}
		
		CustomJRDataSource<FuelReport> dataSource = new CustomJRDataSource<FuelReport>().initBy(detailsReport);
		try {
			JasperReport jasperReport = JasperCompileManager.compileReport(springJasperConfig.getFuelFilePath());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			report = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
		return report;
	}
	
	public byte[] generateFuelReportByYear(String userId, String vehicleId, String year,String exportType) throws ServiceException {

		byte[] report = null;

		query = new Query();
		query.with(new Sort(Sort.Direction.ASC, "date"));

		List<Fuel> details = null;
		List<FuelReport> detailsReport = new ArrayList<FuelReport>();
		Double totPrice=0d;
		String period="";
		HashMap< String, Object> parameters=new HashMap<String, Object>();
		query = new Query();
		
		Locale locale1 = Locale.FRENCH;
		TimeZone tz1 = TimeZone.getTimeZone("FR");
		Calendar startDate = Calendar.getInstance(tz1, locale1);
		Calendar endDate = Calendar.getInstance(tz1, locale1);
		startDate.set(Calendar.YEAR, Integer.parseInt(year));

		startDate.set(Calendar.MONTH, 0);
		startDate.set(Calendar.DAY_OF_MONTH, 1);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);

		endDate.set(Calendar.YEAR, Integer.parseInt(year));
		endDate.set(Calendar.MONTH, 11);
		endDate.set(Calendar.DAY_OF_MONTH, 31);
		endDate.set(Calendar.HOUR_OF_DAY, 22);
		endDate.set(Calendar.MINUTE, 59);
		endDate.set(Calendar.SECOND, 59);
		endDate.set(Calendar.MILLISECOND, 59);
		
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId)
				.and("vehicle.id").is(vehicleId)
				.and("date").gte(startDate.getTime()).lte(endDate.getTime()));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			details = mongoTemplate.find(query, Fuel.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		
		if(details.size()>0){
			parameters.put("user", details.get(0).getUser().getFirstName()+ " " + details.get(0).getUser().getLastName());
			parameters.put("model", details.get(0).getVehicle().getModel());
			parameters.put("make", details.get(0).getVehicle().getMake());
			for(Fuel fuel:details){
				totPrice+=fuel.getFuelPrice();
				FuelReport fuelRep=new FuelReport();
				fuelRep.setFuel(fuel.getFuel());
				fuelRep.setDate(new SimpleDateFormat("EEEE dd MMM YYYY",locale1).format(fuel.getDate()));
				fuelRep.setAdress(fuel.getAdress());
				fuelRep.setStation(fuel.getStation());
				fuelRep.setFuelPrice(fuel.getFuelPrice());
				fuelRep.setVolume(fuel.getVolume());
				fuelRep.setLatitude(fuel.getLatitude());
				fuelRep.setLongitude(fuel.getLongitude());
				fuelRep.setMonth(new SimpleDateFormat("MMM",locale1).format(fuel.getDate()));
				detailsReport.add(fuelRep);
			}
			parameters.put("totPrice", totPrice.toString()+" €");
			parameters.put("immatricule", "");
			// generate period Label
			Calendar cal=Calendar.getInstance();
			cal.set(Calendar.YEAR,Integer.parseInt(year));
			period= new SimpleDateFormat("YYYY",locale1).format(cal.getTime()).toUpperCase() ;
			parameters.put("period", period);
		}
		
		CustomJRDataSource<FuelReport> dataSource = new CustomJRDataSource<FuelReport>().initBy(detailsReport);
		try {
			JasperReport jasperReport = JasperCompileManager.compileReport(springJasperConfig.getFuelFilePath());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			report = JasperExportManager.exportReportToPdf(jasperPrint);
		} catch (Exception e) {
			System.err.println(e.toString());
		}
		return report;
	}
	
	public byte[] generateFuelReportByMonth(String userId,String vehicleId, String year,String month,String reportType) throws ServiceException {

		byte[] report = null;

		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "date"));

		List<Fuel> details = null;
		List<FuelReport> detailsReport = new ArrayList<FuelReport>();
		Double totPrice=0d;
		String period="";
		HashMap< String, Object> parameters=new HashMap<String, Object>();
		query = new Query();
		
		Calendar startDate = Calendar.getInstance();
		Calendar endDate = Calendar.getInstance();
		Locale locale1 = Locale.FRENCH;
		startDate.set(Calendar.YEAR, Integer.parseInt(year));
		startDate.set(Calendar.MONTH, Integer.parseInt(month)-1);
		startDate.set(Calendar.DAY_OF_MONTH, 1);
		startDate.set(Calendar.HOUR_OF_DAY, 0);
		startDate.set(Calendar.MINUTE, 0);
		startDate.set(Calendar.SECOND, 0);
		startDate.set(Calendar.MILLISECOND, 0);
		endDate.set(Calendar.YEAR, Integer.parseInt(year));
		endDate.set(Calendar.MONTH, Integer.parseInt(month)-1);
		endDate.set(Calendar.DATE, startDate.getActualMaximum(Calendar.DATE));
		endDate.set(Calendar.HOUR_OF_DAY, 23);
		endDate.set(Calendar.MINUTE, 59);
		endDate.set(Calendar.SECOND, 59);
		endDate.set(Calendar.MILLISECOND, 59);
		System.out.println("startDate=="+startDate.getTime());
		System.out.println("endDate=="+endDate.getTime());
		query.with(new Sort(Sort.Direction.DESC, "date"));
		query.addCriteria(Criteria.where("user.id").is(userId)
				.and("vehicle.id").is(vehicleId)
				.and("date").gte(startDate.getTime()).lte(endDate.getTime()));
		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			details = mongoTemplate.find(query, Fuel.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}
		
		if(details.size()>0){
			parameters.put("user", details.get(0).getUser().getFirstName()+ " " + details.get(0).getUser().getLastName());
			parameters.put("model", details.get(0).getVehicle().getModel());
			parameters.put("make", details.get(0).getVehicle().getMake());
			// generate total Prices Fuel
			for(Fuel fuel:details){
				totPrice+=fuel.getFuelPrice();
				FuelReport fuelRep=new FuelReport();
				fuelRep.setFuel(fuel.getFuel());
				fuelRep.setDate(new SimpleDateFormat("EEEE dd MMM YYYY",locale1).format(fuel.getDate()));
				fuelRep.setAdress(fuel.getAdress());
				fuelRep.setStation(fuel.getStation());
				fuelRep.setFuelPrice(fuel.getFuelPrice());
				fuelRep.setVolume(fuel.getVolume());
				fuelRep.setLatitude(fuel.getLatitude());
				fuelRep.setLongitude(fuel.getLongitude());
				detailsReport.add(fuelRep);
			}
			parameters.put("totPrice", totPrice.toString()+" €");
			// generate period Label
			Calendar cal=Calendar.getInstance();
			cal.set(Calendar.MONTH, Integer.parseInt(month)-1);
			cal.set(Calendar.YEAR,Integer.parseInt(year));
			period= new SimpleDateFormat("MMM",locale1).format(cal.getTime()).toUpperCase()+ new SimpleDateFormat("YYYY",locale1).format(cal.getTime());
			parameters.put("period", period);
			parameters.put("immatricule", "");
		}
		
		CustomJRDataSource<FuelReport> dataSource = new CustomJRDataSource<FuelReport>().initBy(detailsReport);
		try {
			JasperReport jasperReport = JasperCompileManager.compileReport(springJasperConfig.getFuelFilePath());
			JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, dataSource);
			report = JasperExportManager.exportReportToPdf(jasperPrint);

		} catch (Exception e) {
			System.err.println(e.toString());
		}
		return report;
	}

	public Fuel addFuel(@RequestBody @ApiParam(value = "Fuel to save") Fuel fuel) throws ServiceException {
		Fuel fFuel = null;
		fuel.setAdress(GeoLocation.getAdressLocation(fuel.getLatitude(),fuel.getLongitude()));
		fFuel = fuelRepository.save(fuel);
		return fFuel;
	}
	
	public Fuel updateFuel(Fuel updatedFuel, String id) {
		
		return fuelRepository.save(updatedFuel);
	}

	public void removeFuel(@PathVariable("fuelId") String fuelId) {
		fuelRepository.delete(fuelId);
	}

	public void removeAllFuels() {
		fuelRepository.deleteAll();
	}
}
