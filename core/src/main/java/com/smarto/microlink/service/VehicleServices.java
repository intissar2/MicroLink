package com.smarto.microlink.service;

import com.smarto.microlink.doa.NhtsaDataVORepository;
import com.smarto.microlink.doa.VehicleRepository;
import com.smarto.microlink.doa.VinDecoderEURepositoy;
import com.smarto.microlink.domain.*;
import com.smarto.microlink.exception.ExceptionInfo;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.utils.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
public class VehicleServices {

    private static final Logger LOGGER = LoggerFactory.getLogger(VehicleServices.class);

    public VehicleRepository getVehicleRepository() {
        return vehicleRepository;
    }

    @Autowired
    private VehicleRepository vehicleRepository;

    @Value("${vin_decoder_api_key}")
    private String api_key;
    @Value("${vin_decoder_secret_key}")
    private String secret_Key;
    @Value("${vin_decoder_uri}")
    private String vin_decoder_uri;
    @Value("${vin_decoder_uri}")
    private String vin_decode_uri;
    @Value("${nhtsa_vd_uri}")
    private String nhtsa_vd_uri;
    @Autowired
    private VinDecoderEURepositoy vinDecoderEURepositoy;
    @Autowired
    private NhtsaDataVORepository nhtsaDataVORepository;


    @Autowired
    private RestTemplate restTemplate;

    private List<String> listMarks = null;
    private List<Vehicle> listVehicles = null;
    private List<VehicleHeader> listVehiclesHeader = null;
    private VehicleHeader tmpVehicleHeader = null;

    public Vehicle findVehicleById(String id) {
        return vehicleRepository.findOne(id);
    }

    public List<Vehicle> findAllVehicles() {
        return vehicleRepository.findAll();
    }

    public List<String> findAllVehiclesMakes() {

        listMarks = new ArrayList<String>();
        listVehicles = new ArrayList<Vehicle>();

        listVehicles = vehicleRepository.findAll();

        for (Vehicle vehicle : listVehicles) {

            if (vehicle.getMake().contains(" ")) {
                if (!listMarks.contains(vehicle.getMake().substring(0, vehicle.getMake().indexOf(" ")))) {
                    listMarks.add(vehicle.getMake().substring(0, vehicle.getMake().indexOf(" ")));
                }
            } else {
                if (!listMarks.contains(vehicle.getMake())) {
                    listMarks.add(vehicle.getMake());
                }
            }
        }

        Collections.sort(listMarks);

        return listMarks;
    }

    public List<VehicleHeader> findAllVehiclesHeader() {

        listVehiclesHeader = new ArrayList<VehicleHeader>();

        listVehicles = vehicleRepository.findAll();

        for (Vehicle vehicle : listVehicles) {

            tmpVehicleHeader = new VehicleHeader();

            tmpVehicleHeader.setId(vehicle.getId());
            tmpVehicleHeader.setMake(vehicle.getMake());
            tmpVehicleHeader.setModel(vehicle.getMake());
            tmpVehicleHeader.setPuissance(vehicle.getPuissance());
            tmpVehicleHeader.setAm(vehicle.getAm());
            tmpVehicleHeader.setEngine(vehicle.getEngine());
            tmpVehicleHeader.setFuel(vehicle.getFuel());

            listVehiclesHeader.add(tmpVehicleHeader);
        }

        return listVehiclesHeader;
    }

    public List<VehicleHeader> findListVehiclesHeaderByMake(String make) {

        listVehiclesHeader = new ArrayList<VehicleHeader>();

        listVehicles = vehicleRepository.findAll();

        for (Vehicle vehicle : listVehicles) {

            if (vehicle.getMake().contains(make)) {
                tmpVehicleHeader = new VehicleHeader();

                tmpVehicleHeader.setId(vehicle.getId());
                tmpVehicleHeader.setMake(vehicle.getMake());
                tmpVehicleHeader.setModel(vehicle.getModel());
                tmpVehicleHeader.setPuissance(vehicle.getPuissance());
                tmpVehicleHeader.setAm(vehicle.getAm());
                tmpVehicleHeader.setEngine(vehicle.getEngine());
                tmpVehicleHeader.setFuel(vehicle.getFuel());

                listVehiclesHeader.add(tmpVehicleHeader);
            }

        }

        return listVehiclesHeader;
    }

    public Vehicle addVehicle(Vehicle vehicle) {

        return vehicleRepository.save(vehicle);
    }

    public Vehicle updateVehicle(Vehicle updatedVehicle, String id) {

        updatedVehicle.setId(id);

        return vehicleRepository.save(updatedVehicle);
    }

    public Vehicle removeVehicle(String id) {

        Vehicle deleted = vehicleRepository.findOne(id);
        vehicleRepository.delete(id);

        return deleted;
    }

    public void removeAllVehicles() {

        vehicleRepository.deleteAll();
    }

    public Vehicle getVehicleByVin(FieldDataVehicle fieldDataVehicle) throws NoSuchAlgorithmException, UnsupportedEncodingException, ServiceException {
        String vin = fieldDataVehicle.getVin();
        NhtsaDataVO result = getVehicleNHTSAByVin(vin);

        NhtsaResultVO noMachFound = result.getResults().stream().filter(x -> x.getVariableId().equals("143")
                && x.getValueId().equals("0")).findFirst().orElse(null);

        if (noMachFound == null) {
            LOGGER.warn("no data match found in nhsta Data Base for " + vin);
            //TODO find vehicle object
            VinDecoderEU resu = getVehicleVinDecoderByVin(vin);

            Pair<String, String, String, String, String, String> values = new Pair<>();

            if (resu == null)
                throw new ServiceException(ExceptionInfo.UNKNOW_INPUT_DATA_EXCEPTION_CODE, ExceptionInfo.UNKNOW_INPUT_DATA_EXCEPTION_MSG, 404);

            resu.getDecode().stream().filter(x -> x.getLabel().equals(EVinDecodeEUCode.MAKE.getCode())
                    || x.getLabel().equals(EVinDecodeEUCode.MODEL.getCode())
                    || x.getLabel().equals(EVinDecodeEUCode.YEAR.getCode())
                    || x.getLabel().equals(EVinDecodeEUCode.ENGINE.getCode())
                    || x.getLabel().equals(EVinDecodeEUCode.FUEL.getCode())
                    || x.getLabel().equals(EVinDecodeEUCode.PUISSANCE.getCode())).forEach(item -> {
                if (item.getLabel().equals(EVinDecodeEUCode.MAKE.getCode())) {
                    values.setVal1(item.getValue());
                } else if (item.getLabel().equals(EVinDecodeEUCode.MODEL.getCode())) {
                    values.setVal2(item.getValue());
                } else if (item.getLabel().equals(EVinDecodeEUCode.YEAR.getCode())) {
                    values.setVal3(item.getValue());
                } else if (item.getLabel().equals(EVinDecodeEUCode.ENGINE.getCode())) {
                    values.setVal4(item.getValue());
                } else if (item.getLabel().equals(EVinDecodeEUCode.FUEL.getCode())) {
                    values.setVal5(item.getValue());
                } else if (item.getLabel().equals(EVinDecodeEUCode.PUISSANCE.getCode())) {
                    values.setVal6(item.getValue());
                }
            });
            resu.setVin(vin);
            vinDecoderEURepositoy.save(resu);
            return mappingVehicle(values, fieldDataVehicle, EVehicleSource.VIN_DECODE_EU);
        }

        //TODO ADD MAPPER TO VIHUCLE OBJECT
        Pair<String, String, String, String, String, String> values = new Pair<>();

        result.getResults().stream().filter(x -> x.getVariableId().equals(ENhstaCode.MAKE.getCode())
                || x.getVariableId().equals(ENhstaCode.MODEL.getCode())
                || x.getVariableId().equals(ENhstaCode.YEAR.getCode())
                || x.getVariableId().equals(ENhstaCode.ENGINE.getCode())
                || x.getVariableId().equals(ENhstaCode.FUEL.getCode())
                || x.getVariableId().equals(ENhstaCode.PUISSANCE.getCode())).forEach(item -> {
            if (item.getVariableId().equals(ENhstaCode.MAKE.getCode())) {
                values.setVal1(item.getValue());
            } else if (item.getVariableId().equals(ENhstaCode.MODEL.getCode())) {
                values.setVal2(item.getValue());
            } else if (item.getVariableId().equals(ENhstaCode.YEAR.getCode())) {
                values.setVal3(item.getValue());
            } else if (item.getVariableId().equals(ENhstaCode.ENGINE.getCode())) {
                values.setVal4(item.getValue());
            } else if (item.getVariableId().equals(ENhstaCode.FUEL.getCode())) {
                values.setVal5(item.getValue());
            } else if (item.getVariableId().equals(ENhstaCode.PUISSANCE.getCode())) {
                values.setVal6(item.getValue());
            }
        });
        result.setVin(vin);
        nhtsaDataVORepository.save(result);
        return mappingVehicle(values, fieldDataVehicle, EVehicleSource.NHTSA);
    }

    private Vehicle mappingVehicle(Pair<String, String, String, String, String, String> pair, FieldDataVehicle extraData, EVehicleSource source) throws ServiceException {
        List<Vehicle> vehicles = vehicleRepository.findByMakeIgnoreCaseAndModelIgnoreCaseAndAmIgnoreCase(Utils.formatStringManual(pair.getVal1()),
                Utils.formatStringManual(pair.getVal2()), Utils.formatStringManual(pair.getVal3()));

        if (vehicles.size() == 0) {
            //create new vehicle
            Vehicle newVehicle = new Vehicle();
            newVehicle.setMake(Utils.formatStringManual(pair.getVal1()));
            newVehicle.setModel(Utils.formatStringManual(pair.getVal2()));
            newVehicle.setAm(Utils.formatStringManual(pair.getVal3()));
            newVehicle.setEngine(pair.getVal4());
            newVehicle.setFuel(pair.getVal5());
            newVehicle.setPuissance(pair.getVal6());
            newVehicle.setMode1(extraData.getMode1());
            newVehicle.setMode2(extraData.getMode2());
            newVehicle.setMode5(extraData.getMode5());
            newVehicle.setMode6(extraData.getMode6());
            newVehicle.setMode8(extraData.getMode8());
            newVehicle.setMode9(extraData.getMode9());
            newVehicle.setProtocol(extraData.getSuportedProtocol());
            newVehicle.setSource(source);

            newVehicle = vehicleRepository.save(newVehicle);
            return newVehicle;

            //TODO add vehicle to database if not exist
        } else if (vehicles.size() == 1) {
            return vehicles.get(0);
        } else {
            LOGGER.warn("multiple vehicle values for model and make and year correct database!!!!!!");
            return vehicles.get(0);
        }
    }

    public VinDecoderEU getVehicleVinDecoderByVin(String vin) throws NoSuchAlgorithmException, UnsupportedEncodingException {
        //toDO search in databse before
        VinDecoderEU vinDecoderEU = vinDecoderEURepositoy.findOne(vin);
        if (vinDecoderEU != null)
            return vinDecoderEU;

        String sum_check = new StringBuilder(vin.toUpperCase()).append("|").append(api_key).append("|").append(secret_Key).toString();
        String hex_key = Utils.sha1(sum_check).substring(0, 10);

        List<String> params = new ArrayList<>();
        params.add(api_key);
        params.add(hex_key);
        params.add(vin);
        LOGGER.info(vin_decode_uri);
        LOGGER.info(hex_key);
        ResponseEntity<VinDecoderEU> result = restTemplate.exchange(vin_decode_uri, HttpMethod.GET, null, VinDecoderEU.class, params.toArray(new String[params.size()]));

        return result.getBody();
    }

    public NhtsaDataVO getVehicleNHTSAByVin(String vin) {
        //TODO search in database before
        NhtsaDataVO nhtsaDataVO = nhtsaDataVORepository.findOne(vin);

        if (nhtsaDataVO != null)
            return nhtsaDataVO;
        List<String> params = new ArrayList<>();
        params.add(vin);
        ResponseEntity<NhtsaDataVO> response = restTemplate.exchange(nhtsa_vd_uri, HttpMethod.GET, null,
                NhtsaDataVO.class, params.toArray(new String[params.size()]));

        return response.getBody();
    }
}
