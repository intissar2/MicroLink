package com.smarto.microlink.service;

import com.smarto.microlink.doa.TroubleCodeRepository;
import com.smarto.microlink.domain.TroubleCode;
import com.smarto.microlink.domain.TroubleCodeHeader;
import com.smarto.microlink.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TroubleCodeServices {

    @Autowired
    private TroubleCodeRepository troubleCodeRepository;

    private ArrayList<TroubleCodeHeader> listTroubleCodeHeader = null;
    private ArrayList<TroubleCode> listTroubleCode = null;
    private List<String> generalCodeList = null;
    private List<String> specificCodeList = null;
    private TroubleCode tmpTroubleCode = null;
    private TroubleCodeHeader tmpTroubleCodeHeader = null;

    public List<TroubleCode> findAllTroubleCodes() {
        return troubleCodeRepository.findAll();
    }

    public TroubleCode findTroubleCodeById(String id) {
        return troubleCodeRepository.findById(id);
    }

    public List<TroubleCodeHeader> findListTroubleCodeHeader(List<String> codeList, String manufactor) {

        generalCodeList = new ArrayList<String>();
        specificCodeList = new ArrayList<String>();
        listTroubleCodeHeader = new ArrayList<TroubleCodeHeader>();

        // ------ Prepared List -------------------
        // -----------------------------
        for (String code : codeList) {
            if (code != null) {
                if (Utils.isGeneralTroubleCode(code)) {
                    generalCodeList.add(code);
                } else if (Utils.isSpecificTroubleCode(code)) {
                    specificCodeList.add(code);
                }
            }
        }

        // ------ Start Request with generalCodeList
        // ----------------------------
        for (String code : generalCodeList) {
            if (troubleCodeRepository.findByCode(code) != null) {

                tmpTroubleCode = troubleCodeRepository.findByCode(code);

                tmpTroubleCodeHeader = new TroubleCodeHeader();
                tmpTroubleCodeHeader.setCode(tmpTroubleCode.getCode());
                tmpTroubleCodeHeader.setDescription(tmpTroubleCode.getDescription());
                tmpTroubleCodeHeader.setId(tmpTroubleCode.getId());
                tmpTroubleCodeHeader.setManufactor(tmpTroubleCode.getManufactor());

                listTroubleCodeHeader.add(tmpTroubleCodeHeader);
            }
        }

        // ------ Start Request with specificCodeList
        // ----------------------------
        for (String code : specificCodeList) {
            if (troubleCodeRepository.findByCodeAndManufactor(code, manufactor) != null) {

                tmpTroubleCode = troubleCodeRepository.findByCodeAndManufactor(code, manufactor);

                tmpTroubleCodeHeader = new TroubleCodeHeader();
                tmpTroubleCodeHeader.setId(tmpTroubleCode.getId());
                tmpTroubleCodeHeader.setCode(tmpTroubleCode.getCode());
                tmpTroubleCodeHeader.setDescription(tmpTroubleCode.getDescription());
                tmpTroubleCodeHeader.setManufactor(tmpTroubleCode.getManufactor());

                listTroubleCodeHeader.add(tmpTroubleCodeHeader);
            }
        }

        return listTroubleCodeHeader;
    }

    public List<TroubleCode> findListTroubleCode(List<String> codeList, String manufactor) {

        generalCodeList = new ArrayList<String>();
        specificCodeList = new ArrayList<String>();
        listTroubleCode = new ArrayList<TroubleCode>();

        // ------ Prepared List -------------------
        // -----------------------------
        for (String code : codeList) {
            if (code != null) {
                if (Utils.isGeneralTroubleCode(code)) {
                    generalCodeList.add(code);
                } else if (Utils.isSpecificTroubleCode(code)) {
                    specificCodeList.add(code);
                }
            }
        }

        // ------ Start Request with generalCodeList
        // ----------------------------
        for (String code : generalCodeList) {
            if (troubleCodeRepository.findByCode(code) != null) {

                tmpTroubleCode = troubleCodeRepository.findByCode(code);

                listTroubleCode.add(tmpTroubleCode);
            }
        }

        // ------ Start Request with specificCodeList
        // ----------------------------
        for (String code : specificCodeList) {
            if (troubleCodeRepository.findByCodeAndManufactor(code, manufactor) != null) {

                tmpTroubleCode = troubleCodeRepository.findByCodeAndManufactor(code, manufactor);

                listTroubleCode.add(tmpTroubleCode);
            }
        }

        return listTroubleCode;
    }

    public TroubleCode addTroubleCode(TroubleCode TroubleCode) {
        return troubleCodeRepository.save(TroubleCode);
    }

    public TroubleCode updateTroubleCode(TroubleCode updatedTroubleCode, String id) {
        updatedTroubleCode.setId(id);
        return troubleCodeRepository.save(updatedTroubleCode);
    }

    public TroubleCode removeTroubleCode(String id) {
        TroubleCode deleted = troubleCodeRepository.findOne(id);
        troubleCodeRepository.delete(id);
        return deleted;
    }

    public void removeAllTroubleCodes() {
        troubleCodeRepository.deleteAll();
    }

}
