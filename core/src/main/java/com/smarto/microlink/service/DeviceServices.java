package com.smarto.microlink.service;

import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.DeviceRepository;
import com.smarto.microlink.domain.Device;
import com.smarto.microlink.domain.DeviceMessage;
import com.smarto.microlink.exception.ExceptionInfo;
import com.smarto.microlink.exception.ServiceException;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class DeviceServices {

    public DeviceRepository getDeviceRepository() {
        return deviceRepository;
    }

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private SpringMongoConfig springMongoConfig;

    private Query query;

	/*public Trip findTripById(String userId, String tripId) {

		Trip trip = null;

		trip = tripRepository.findById(tripId);

		if (trip.getUser().getId().equals(userId)) {

			tmpUser = new User();
			tmpUser.setId(trip.getUser().getId());
			tmpUser.setFirstName(trip.getUser().getFirstName());
			tmpUser.setLastName(trip.getUser().getLastName());
			tmpUser.setMail(trip.getUser().getMail());

			trip.setUser(tmpUser);

			return trip;
		} else
			return null;

	}*/

    public List<Device> findAllDevices() {
        return deviceRepository.findAll();
    }
    public List<Device> findAllDeviceByOperator(String operator,String date) throws ParseException {

        SimpleDateFormat sdf= new SimpleDateFormat("yyyy-MM-dd'T'hh:mm:ss.SSS'Z'");

        Date d= sdf.parse(date);

        return deviceRepository.findByOperatorNameAndReleaseDate(operator,d);
    }

	/*public List<TripHeader> findLastTrip(String userId) throws ServiceException {

		lastTrips = tripRepository.findAll(new Sort(Sort.Direction.DESC, "startTrip.date"));

		lastTripsHeaders = new ArrayList<>();

		for (Trip trip : lastTrips) {

			if (trip.getUser().getId().equals(userId)) {
				tmpTripHeader = new TripHeader();

				tmpTripHeader.setId(trip.getId());
				tmpTripHeader.setStartTrip(trip.getStartTrip());
				tmpTripHeader.setEndTrip(trip.getEndTrip());
				tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
				tmpTripHeader.setDuration(trip.getDuration());

				tmpUser = new User();
				tmpUser.setId(trip.getUser().getId());
				tmpUser.setFirstName(trip.getUser().getFirstName());
				tmpUser.setLastName(trip.getUser().getLastName());
				tmpUser.setMail(trip.getUser().getMail());

				tmpTripHeader.setUser(tmpUser);

				lastTripsHeaders.add(tmpTripHeader);
			}
		}

		TripHeader lastTrip = lastTripsHeaders.get(0);

		lastTripsHeaders = new ArrayList<>();

		lastTripsHeaders.add(lastTrip);

		return lastTripsHeaders;
	}

	@SuppressWarnings("deprecation")
	public List<TripHeader> findTripsLastWeek(String userId) throws ServiceException {

		lastTrips = new ArrayList<>();
		lastTripsHeaders = new ArrayList<>();

		currentDate = new Date();
		laterDate = new Date();
		laterDate.setDate(currentDate.getDate() - 7);

		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
		query.addCriteria(Criteria.where("startTrip.date").gte(laterDate).lte(currentDate));

		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			lastTrips = mongoTemplate.find(query, Trip.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		for (Trip trip : lastTrips) {

			if (trip.getUser().getId().equals(userId)) {
				tmpTripHeader = new TripHeader();

				tmpTripHeader.setId(trip.getId());
				tmpTripHeader.setStartTrip(trip.getStartTrip());
				tmpTripHeader.setEndTrip(trip.getEndTrip());
				tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
				tmpTripHeader.setDuration(trip.getDuration());

				tmpUser = new User();
				tmpUser.setId(trip.getUser().getId());
				tmpUser.setFirstName(trip.getUser().getFirstName());
				tmpUser.setLastName(trip.getUser().getLastName());
				tmpUser.setMail(trip.getUser().getMail());

				tmpTripHeader.setUser(tmpUser);

				lastTripsHeaders.add(tmpTripHeader);
			}

		}

		return lastTripsHeaders;
	}

	@SuppressWarnings("deprecation")
	public List<TripHeader> findTripsLastMonth(String userId) throws ServiceException {

		Date currentDate = new Date();
		Date laterDate = new Date();
		laterDate.setDate(currentDate.getDate() - 30);

		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
		query.addCriteria(Criteria.where("startTrip.date").gte(laterDate).lte(currentDate));

		List<Trip> lastTrips = new ArrayList<>();

		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			lastTrips = mongoTemplate.find(query, Trip.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		lastTripsHeaders = new ArrayList<>();
		TripHeader tmpTripHeader = null;
		for (Trip trip : lastTrips) {

			if (trip.getUser().getId().equals(userId)) {
				tmpTripHeader = new TripHeader();

				tmpTripHeader.setId(trip.getId());
				tmpTripHeader.setStartTrip(trip.getStartTrip());
				tmpTripHeader.setEndTrip(trip.getEndTrip());
				tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
				tmpTripHeader.setDuration(trip.getDuration());

				tmpUser = new User();
				tmpUser.setId(trip.getUser().getId());
				tmpUser.setFirstName(trip.getUser().getFirstName());
				tmpUser.setLastName(trip.getUser().getLastName());
				tmpUser.setMail(trip.getUser().getMail());

				tmpTripHeader.setUser(tmpUser);

				lastTripsHeaders.add(tmpTripHeader);
			}

		}

		return lastTripsHeaders;
	}

	@SuppressWarnings("deprecation")
	public List<TripHeader> findTripsLastYear(String userId) throws ServiceException {

		Date currentDate = new Date();
		Date laterDate = new Date();
		laterDate.setDate(currentDate.getDate() - 365);

		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
		query.addCriteria(Criteria.where("startTrip.date").gte(laterDate).lte(currentDate));

		List<Trip> lastTrips = new ArrayList<>();

		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			lastTrips = mongoTemplate.find(query, Trip.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		lastTripsHeaders = new ArrayList<>();
		TripHeader tmpTripHeader = null;

		for (Trip trip : lastTrips) {

			if (trip.getUser().getId().equals(userId)) {
				tmpTripHeader = new TripHeader();

				tmpTripHeader.setId(trip.getId());
				tmpTripHeader.setStartTrip(trip.getStartTrip());
				tmpTripHeader.setEndTrip(trip.getEndTrip());
				tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
				tmpTripHeader.setDuration(trip.getDuration());

				tmpUser = new User();
				tmpUser.setId(trip.getUser().getId());
				tmpUser.setFirstName(trip.getUser().getFirstName());
				tmpUser.setLastName(trip.getUser().getLastName());
				tmpUser.setMail(trip.getUser().getMail());

				tmpTripHeader.setUser(tmpUser);

				lastTripsHeaders.add(tmpTripHeader);
			}

		}

		return lastTripsHeaders;
	}

	public List<TripHeader> findLastTripsFromDate(String userId, Date date) throws ServiceException {

		lastTrips = new ArrayList<>();
		lastTripsHeaders = new ArrayList<>();

		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
		query.addCriteria(Criteria.where("startTrip.date").gte(date));

		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			lastTrips = mongoTemplate.find(query, Trip.class);

		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		for (Trip trip : lastTrips) {

			if (trip.getUser().getId().equals(userId)) {
				tmpTripHeader = new TripHeader();

				tmpTripHeader.setId(trip.getId());
				tmpTripHeader.setStartTrip(trip.getStartTrip());
				tmpTripHeader.setEndTrip(trip.getEndTrip());
				tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
				tmpTripHeader.setDuration(trip.getDuration());

				tmpUser = new User();
				tmpUser.setId(trip.getUser().getId());
				tmpUser.setFirstName(trip.getUser().getFirstName());
				tmpUser.setLastName(trip.getUser().getLastName());
				tmpUser.setMail(trip.getUser().getMail());

				tmpTripHeader.setUser(tmpUser);

				lastTripsHeaders.add(tmpTripHeader);
			}

		}

		return lastTripsHeaders;
	}

	public List<TripHeader> findLastTripsByPage(String userId, int pageNumber, int pageSize) throws ServiceException {

		lastTrips = new ArrayList<>();
		lastTripsHeaders = new ArrayList<>();

		final Pageable pageableRequest = new PageRequest(pageNumber - 1, pageSize);

		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
		query.with(pageableRequest);

		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			lastTrips = mongoTemplate.find(query, Trip.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		for (Trip trip : lastTrips) {

			if (trip.getUser().getId().equals(userId)) {
				tmpTripHeader = new TripHeader();

				tmpTripHeader.setId(trip.getId());
				tmpTripHeader.setStartTrip(trip.getStartTrip());
				tmpTripHeader.setEndTrip(trip.getEndTrip());
				tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
				tmpTripHeader.setDuration(trip.getDuration());

				tmpUser = new User();
				tmpUser.setId(trip.getUser().getId());
				tmpUser.setFirstName(trip.getUser().getFirstName());
				tmpUser.setLastName(trip.getUser().getLastName());
				tmpUser.setMail(trip.getUser().getMail());

				tmpTripHeader.setUser(tmpUser);

				lastTripsHeaders.add(tmpTripHeader);
			}

		}

		return lastTripsHeaders;
	}

	public List<TripHeader> findTripsBetweenTwoDates(String userId, Date startDate, Date endDate)
			throws ServiceException {

		lastTripsHeaders = new ArrayList<>();
		lastTrips = new ArrayList<>();

		query = new Query();
		query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
		query.addCriteria(Criteria.where("startTrip.date").gte(startDate).lte(endDate));

		try {
			MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
			lastTrips = mongoTemplate.find(query, Trip.class);
		} catch (Exception e) {
			throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
					ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
		}

		for (Trip trip : lastTrips) {

			if (trip.getUser().getId().equals(userId)) {
				tmpTripHeader = new TripHeader();

				tmpTripHeader.setId(trip.getId());
				tmpTripHeader.setStartTrip(trip.getStartTrip());
				tmpTripHeader.setEndTrip(trip.getEndTrip());
				tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
				tmpTripHeader.setDuration(trip.getDuration());

				tmpUser = new User();
				tmpUser.setId(trip.getUser().getId());
				tmpUser.setFirstName(trip.getUser().getFirstName());
				tmpUser.setLastName(trip.getUser().getLastName());
				tmpUser.setMail(trip.getUser().getMail());

				tmpTripHeader.setUser(tmpUser);

				lastTripsHeaders.add(tmpTripHeader);
			}

		}

		return lastTripsHeaders;
	}*/

    public DeviceMessage addDevice(@RequestBody @ApiParam(value = "Device to save") Device device) throws ServiceException {
        Device deviceCreated = null;
        DeviceMessage msg = null;
        ArrayList<Device> devices = new ArrayList<Device>();
        query = new Query();
        query.addCriteria(Criteria.where("serialNumber").is(device.getSerialNumber()));
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            devices = (ArrayList<Device>) mongoTemplate.find(query, Device.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }
        if (devices == null || devices.isEmpty()) {
            deviceCreated = deviceRepository.save(device);
            msg = new DeviceMessage(100, "Congratulations!", "Your device has been created !", null);
            msg.setDevice(deviceCreated);
            return msg;
        } else {
            msg = new DeviceMessage(500, "Alas!", "Device already exists !", null);
            msg.setDevice(deviceCreated);
            return msg;
        }
    }

    /**
     * update the attirbute "isUsed" of device document
     * @param id
     * @return
     */
    public Device updateIsUserDevice(String id,boolean isUsed) {
        Device deviceToModify=deviceRepository.findById(id);
        deviceToModify.setIsUsed(isUsed);
        deviceToModify = deviceRepository.save(deviceToModify);
        return deviceToModify;
	}

    /**
     * remove device from device document by Id
     * @param serviceId
     */
    public void removeDevice(@PathVariable("serviceId") String serviceId) {

        deviceRepository.delete(serviceId);
    }

	/*public Trip updateTrip(@RequestBody @ApiParam(value = "Trip Object") Trip updatedTrip,
			@PathVariable @ApiParam(value = "Trip Code") String id) {

		Trip tripUpdated = tripRepository.save(updatedTrip);

		return tripUpdated;
	}

	public void removeTrip(@PathVariable("tripId") String tripId) {

		tripRepository.delete(tripId);
	}

	public void removeAllTrips() {

		tripRepository.deleteAll();
	}*/

    /**
     * Find Trips list by date
     *
     * @return Trips list by date
     * @throws ServiceException
     */
    // @ApiOperation(value = "Returns Trips list by date")
    // @RequestMapping(method = RequestMethod.GET, value =
    // "/getTripsByDate/{date}", produces = "application/json")
    // public ResponseEntity<List<TripHeader>> getTripsByDate(
    // @PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date)
    // throws ServiceException {
    //
    // Query query = new Query();
    // query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
    // query.addCriteria(Criteria.where("startTrip.date").is(date));
    //
    // List<Trip> lastTrips = new ArrayList<>();
    //
    // try {
    // MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
    // lastTrips = mongoTemplate.find(query, Trip.class);
    // } catch (Exception e) {
    // throw new
    // ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
    // ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
    // }
    //
    // lastTripsHeaders = new ArrayList<>();
    // TripHeader tmpTripHeader = null;
    // for (Trip trip : lastTrips) {
    // tmpTripHeader = new TripHeader();
    // tmpTripHeader.setId(trip.getId());
    // tmpTripHeader.setStartTrip(trip.getStartTrip());
    // tmpTripHeader.setEndTrip(trip.getEndTrip());
    // tmpTripHeader.setAverageSpeed(trip.getAverageSpeed());
    // tmpTripHeader.setDuration(trip.getDuration());
    // lastTripsHeaders.add(tmpTripHeader);
    // }
    //
    // return new ResponseEntity<List<TripHeader>>(lastTripsHeaders,
    // HttpStatus.OK);
    // }
}
