package com.smarto.microlink.service;

import com.smarto.microlink.doa.FieldDataVehicleRepository;
import com.smarto.microlink.domain.FieldDataVehicle;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FieldDataVehicleServices {

    @Autowired
    private FieldDataVehicleRepository fieldDataVehicleRepository;

    FieldDataVehicle fieldDataVehicle = null;
    FieldDataVehicle fieldDataVehicleAdded = null;

    public FieldDataVehicle findFieldDataVehicleById(String id) {
        return fieldDataVehicleRepository.findOne(id);
    }

    public List<FieldDataVehicle> findAllFieldDataVehicles() {
        return fieldDataVehicleRepository.findAll();
    }

    public FieldDataVehicle addFieldDataVehicle(FieldDataVehicle fieldDataVehicle) {

        if (fieldDataVehicleRepository.findByVin(fieldDataVehicle.getVin()) != null) {
            fieldDataVehicleRepository.delete(fieldDataVehicleRepository.findByVin(fieldDataVehicle.getVin()).getId());
            fieldDataVehicleAdded = fieldDataVehicleRepository.save(fieldDataVehicle);
        } else
            fieldDataVehicleAdded = fieldDataVehicleRepository.save(fieldDataVehicle);
        return fieldDataVehicleAdded;
    }

    public FieldDataVehicle updateFieldDataVehicle(FieldDataVehicle updatedFieldDataVehicle, String id) {

        updatedFieldDataVehicle.setId(id);

        return fieldDataVehicleRepository.save(updatedFieldDataVehicle);
    }

    public FieldDataVehicle removeFieldDataVehicle(String id) {

        FieldDataVehicle deleted = fieldDataVehicleRepository.findOne(id);
        fieldDataVehicleRepository.delete(id);

        return deleted;
    }

    public void removeAllFieldDataVehicles() {

        fieldDataVehicleRepository.deleteAll();
    }

}
