/*package com.smarto.microlink.service;

import com.smarto.microlink.doa.FeeByDistanceRepository;
import com.smarto.microlink.domain.FeeByDistance;
import com.smarto.microlink.exception.ServiceException;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestBody;

@Service
public class FeeByDistanceServices {

    public FeeByDistanceRepository getFeeByDistanceRepository() {
        return feeByDistanceRepository;
    }

    @Autowired
    private FeeByDistanceRepository feeByDistanceRepository;

    public Integer addFeeByDistance(@RequestBody @ApiParam(value = "FeeByDistance to save") FeeByDistance feeByDistance)
            throws ServiceException {
        FeeByDistance feeByDistanceCreated = feeByDistanceRepository.save(feeByDistance);
        if (feeByDistanceCreated != null)
            return 1;
        else
            return -1;

    }

}*/
