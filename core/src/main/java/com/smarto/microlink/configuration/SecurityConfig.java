package com.smarto.microlink.configuration;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.service.UserServices;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.authentication.encoding.Md5PasswordEncoder;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.LogoutSuccessHandler;
import org.springframework.security.web.authentication.logout.SimpleUrlLogoutSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import java.util.Arrays;

@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                    .antMatchers("/css/**", "/index").permitAll()
                .antMatchers(ApiController.API_PATH+"/**").authenticated()
                .antMatchers(ApiController.API_PATH+"/login").permitAll()
                .and()
                .csrf()
                    .disable()
                .cors()
                    .configurationSource(corsConfigurationSource())
                    .and()
                .formLogin()
                    .loginProcessingUrl(ApiController.API_PATH + "/login")
                    .successHandler(new CustomAuthenticationSuccessHandler())
                    //.loginPage("/login")
                    .failureHandler(new CustomAuthenticationFailureHandler())
                    .and()
                .logout()
                    .logoutSuccessHandler(new CustomLogoutSuccessHandler())
                    .logoutUrl(ApiController.API_PATH + "/logout");
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        /*
        auth
                .inMemoryAuthentication()
                .withUser("user").password("password").roles("USER");
        */
        auth.authenticationProvider(authenticationProvider());
    }
    @Bean
    public DaoAuthenticationProvider authenticationProvider() {
        DaoAuthenticationProvider authProvider
                = new DaoAuthenticationProvider();
        authProvider.setUserDetailsService(userDetailsService);
        authProvider.setPasswordEncoder(encoder());
        return authProvider;
    }
    @Autowired
    UserServices userDetailsService;
    @Bean
    public Md5PasswordEncoder encoder() {
        return new Md5PasswordEncoder();
    }

    @Value("${allowed_origin}")
    private String allowedOrigin;

    public String getAllowedOrigin() {
        return allowedOrigin;
    }

    @Bean
    CorsConfigurationSource corsConfigurationSource() {

        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config.addAllowedOrigin(this.allowedOrigin);
        config.addAllowedHeader("*");
        config.addAllowedMethod("*");
        source.registerCorsConfiguration(ApiController.API_PATH  + "/**", config);

        return source;
    }
}