package com.smarto.microlink.configuration;

import com.mongodb.MongoClient;
import com.smarto.microlink.doa.UserRepository;
import com.smarto.microlink.domain.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@EnableMongoRepositories(basePackageClasses = UserRepository.class)
@Configuration
public class SpringMongoConfig {



    @Value("${spring.data.mongodb.host}")
    private String host;

    @Value("${spring.data.mongodb.database}")
    private String dbName;

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        MongoTemplate mongoTemplate = new MongoTemplate(new MongoClient(host), dbName);
        return mongoTemplate;
    }

//    @Bean
//    CommandLineRunner commandLineRunner(UserRepository userRepository){
//        return strings -> userRepository.save(new User("","lastName", "firstName", "intissar@gmail.com", "123","ADMIN"));
//            }
        };
