package com.smarto.microlink.configuration;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class SpringJasperConfig {

    @Value("${fees_report_jrxml}")
    private String feesFilePath;

    @Bean
    public String getFeesFilePath() throws Exception {
        return feesFilePath;
    }

    @Value("${carcare_report_jrxml}")
    private String carCareFilePath;

    @Bean
    public String getCarCareFilePath() throws Exception {
        return carCareFilePath;
    }
    
    @Value("${fuel_report_jrxml}")
    private String fuelFilePath;

    @Bean
    public String getFuelFilePath() throws Exception {
        return fuelFilePath;
    }
}