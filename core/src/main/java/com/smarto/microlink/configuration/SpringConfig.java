package com.smarto.microlink.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.concurrent.ThreadPoolExecutorFactoryBean;
import org.springframework.web.client.RestTemplate;

/**
 * Created by bassem smida on 16/11/2016.
 */
@Configuration
public class SpringConfig {
    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public ThreadPoolExecutorFactoryBean taskExcutor() {
        ThreadPoolExecutorFactoryBean taskExcutor = new ThreadPoolExecutorFactoryBean();
        taskExcutor.setMaxPoolSize(20);
        taskExcutor.setCorePoolSize(20);
        taskExcutor.setKeepAliveSeconds(60);
        return taskExcutor;
    }
}
