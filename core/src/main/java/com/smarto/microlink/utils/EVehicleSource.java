package com.smarto.microlink.utils;

/**
 * Created by bassem smida on 24/11/2016.
 */
public enum EVehicleSource {
    DEFAULT, VIN_DECODE_EU, NHTSA;
}
