package com.smarto.microlink.utils;

/**
 * Created by bassem smida on 23/11/2016.
 */
public class Pair<T, R, Z, Q, L, V> {
    public Q getVal4() {
        return val4;
    }

    public void setVal4(Q val4) {
        this.val4 = val4;
    }

    public L getVal5() {
        return val5;
    }

    public void setVal5(L val5) {
        this.val5 = val5;
    }

    public V getVal6() {
        return val6;
    }

    public void setVal6(V val6) {
        this.val6 = val6;
    }

    private Q val4;
    private L val5;
    private V val6;

    public Z getVal3() {
        return val3;
    }

    public void setVal3(Z val3) {
        this.val3 = val3;
    }

    private Z val3;

    private T val1;

    public T getVal1() {
        return val1;
    }

    public void setVal1(T val1) {
        this.val1 = val1;
    }

    public R getVal2() {
        return val2;
    }

    public void setVal2(R val2) {
        this.val2 = val2;
    }

    private R val2;

    public Pair(T val1, R Val2, Z val3) {
        this.val1 = val1;
        this.val2 = val2;
        this.val3 = val3;
    }

    public Pair() {
        super();
    }

    @Override
    public int hashCode() {
        return val1.hashCode() ^ val2.hashCode();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Pair)) return false;
        Pair pairo = (Pair) o;
        return this.val1.equals(pairo.getVal1()) &&
                this.val2.equals(pairo.getVal2());
    }


}
