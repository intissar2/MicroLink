package com.smarto.microlink.utils;

import com.smarto.microlink.exception.ServiceException;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;

import java.lang.reflect.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Created by DELL on 26/12/2016.
 */
@Component
public class FieldHelper {

    /**
     * get a Field including superclasses
     *
     * @param c
     * @param fieldName
     * @return
     */
    public Field getField(Class<?> c, String fieldName) {
        Field result = null;
        try {
            result = c.getDeclaredField(StandarizeFieldName(fieldName));
        } catch (NoSuchFieldException nsfe) {
            Class<?> sc = c.getSuperclass();
            result = getField(sc, fieldName);
        }
        return result;
    }

    /**
     * set a field Value by name
     *
     * @param fieldName
     * @param value
     * @throws Exception
     */
    public void setFieldValue(Object target, String fieldName, Object value) throws ServiceException {
        Class<? extends Object> c = target.getClass();
        Field field = getField(c, StandarizeFieldName(fieldName));
        field.setAccessible(true);
        // beware of ...
        // http://docs.oracle.com/javase/tutorial/reflect/member/fieldTrouble.html
        try {
            Type type = field.getType();
            if (type instanceof Class && ((Class<?>) type).isEnum()) {
                field.set(target, Enum.valueOf((Class<Enum>) field.getType(), value.toString()));
                //DO NOTHING
            } else if (type.getTypeName().contains(".") && List.class.isAssignableFrom(Class.forName(type.getTypeName()))) {
                Object list = getFieldValue(target, StandarizeFieldName(fieldName));
                if (list == null) {
                    if (!Class.forName(type.getTypeName()).isInterface())
                        list = field.getType().newInstance();
                    else list = new ArrayList<>();
                }
                Method add = list.getClass().getDeclaredMethod("add", Object.class);
                ParameterizedType integerListType = (ParameterizedType) field.getGenericType();
                Class<?> gType = (Class<?>) integerListType.getActualTypeArguments()[0];
                add.invoke(target, SerializeLinkedHashMapToObject(gType, value));
            } else {
                field.set(target, value);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            throw new ServiceException("50000", "InternalError", 500);
        }
    }

    /*public static void main(String[] arv) {
        Object c = new ArrayList<String>();
        List d = (List) c;
        System.out.println(c instanceof Collection);
    }*/

    /**
     * get a field Value by name
     *
     * @param fieldName
     * @return
     * @throws Exception
     */
    private Object getFieldValue(Object target, String fieldName) throws Exception {
        Class<? extends Object> c = target.getClass();
        Field field = getField(c, StandarizeFieldName(fieldName));
        field.setAccessible(true);
        Object result = field.get(target);
        return result;
    }

    private Object getIdFieldName(Class<?> c) {
        return Arrays.asList(c.getDeclaredFields()).parallelStream().filter(_f -> {
            return _f.getAnnotatedType().equals(Id.class);
        }).findFirst().get();
    }

    private Object SerializeLinkedHashMapToObject(Class _class, Object values) throws IllegalAccessException, InstantiationException {
        LinkedHashMap<String, String> lhmValues = (LinkedHashMap<String, String>) values;
        Object obj = _class.newInstance();
        Arrays.stream(_class.getDeclaredFields()).filter(_field -> !Modifier.isStatic(_field.getModifiers()) ||
                !Modifier.isFinal(_field.getModifiers())
                || !Modifier.isTransient(_field.getModifiers())).forEach(_field -> {
            try {
                if (_field.getType().isPrimitive() || _field.getType().isEnum() || _field.getType().isArray() || isJavaLang(_field.getType()))
                    setFieldValue(obj, _field.getName(), lhmValues.get(_field.getName()));
                else
                    SerializeLinkedHashMapToObject(Class.forName(_field.getType().getTypeName()), lhmValues.get(_field.getName()));
            } catch (ServiceException | ClassNotFoundException |
                    InstantiationException | IllegalAccessException ex) {
                throw new RuntimeException(ex);
            }
        });

        return obj;
    }

    private static boolean isJavaLang(Type check) {
        return check.getTypeName().startsWith("java.lang");
    }


    private static String StandarizeFieldName(String patchValue) {
        if (patchValue.contains("/")) {
            patchValue = patchValue.substring(patchValue.lastIndexOf("/") + 1, patchValue.length());
        }
        return patchValue;
    }
}
