package com.smarto.microlink.utils;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import com.smarto.microlink.domain.Parameter;
import com.smarto.microlink.domain.Score;
import com.smarto.microlink.domain.Trip;
import com.smarto.microlink.domain.TripResult;
import com.smarto.microlink.enumeration.EnameValue;

/**
 * Created by A0105774 on 07/01/2017.
 */
public class EcoDriveCalculation {

    private static final float LAMBDA_ACT = 1;
    private static final float CONST_AIR_FUEL_RATIO = 14.64F;
    private static final float CONST_FUEL_DENST = 0.845F;
    private static final float CO2_FACT = 2.68F;

    /*Main method for Average Fuel Consumption*/
    public static final TripResult calculation_Results(Trip trip,int period,int minPeriodStops) {
//        List<Parameter> addedParams = create_fuel_flow_and_distance_parameter(trip);
//        trip.getParameters().addAll(addedParams);
        TripResult tripResult = generateTripResult(_calculate_average_fuel_consu(trip),period,minPeriodStops);
        return tripResult;
    }

    /* return avrg_fuel_consu  for every Paramter DELTA T = start trip time to paramter time*/
    private static TripResult generateTripResult(Map<Date, List<Parameter>> params,int period, int minPeriodStops){
        boolean firstElement = true;
        double total_avg_cons = 0;
        double acc_avg_cons = 0.0f;
        TripResult tripRes= new TripResult();
        Parameter previews_speed = null; //calculate VSP
        Parameter current_speed = null;
        Double stanadrdScore = 0.0d;
        int insNbStops = 0;
        int index = 0;
        @SuppressWarnings("unused")
        Date previewsDate = new Date();
        double engine_rpm=0;

        Double current_distance;
        Double total_distance = 0d;
        Double max_speed=0d;
        Double mean_speed=0d;
        Double avgFuel=0d;
        Double avgCo2=0d;
        int stops_number=0;
        long total_stops_time=0;
        Double old_efficiency_score =0d;
        Double  efficiency_score=0d;
        Double old_pro_score = 0d;
        Double pro_score=0d;
        Double sommeFuelFlowInst=0d;
        for(Map.Entry<Date, List<Parameter>> entry : params.entrySet()) {
            index++;
            if(firstElement) {
                firstElement = false;
                previews_speed = __getParamByName(entry.getValue(), EnameValue.Speed);
                previewsDate = entry.getKey();
            } else {
                current_speed = __getParamByName(entry.getValue(), EnameValue.Speed);
                Date currentDate = entry.getKey();

                Double fuel_flow = ((Double.parseDouble(__getValByName(entry.getValue(), EnameValue.AirFlowRate)) * (Double.parseDouble(__getValByName(entry.getValue(), EnameValue.EngineLoad)) / 100))
                        / (LAMBDA_ACT * CONST_AIR_FUEL_RATIO * CONST_FUEL_DENST) )/ (1000/period);
                total_avg_cons += fuel_flow;

                if ((Double.parseDouble(current_speed.getValue())) == 0)
                    acc_avg_cons = Double.parseDouble(__getValByName(entry.getValue(), EnameValue.EngineRpm))
                            * Double.parseDouble(__getValByName(entry.getValue(), EnameValue.AirFlowRate))
                            / (Double.parseDouble(__getValByName(entry.getValue(), EnameValue.IntAirTemperature)) + 273)
                            / 85;
                else if ((Double.parseDouble(current_speed.getValue())) < 30) {
                    acc_avg_cons = fuel_flow * 12000;
                    sommeFuelFlowInst = sommeFuelFlowInst + acc_avg_cons;
                } else {
                    acc_avg_cons = fuel_flow * (100 / ((Double.parseDouble(current_speed.getValue())) / 3600));
                    sommeFuelFlowInst = sommeFuelFlowInst + acc_avg_cons;
                }

                /*CO2 Emission from Departure*/
                double co2_emission_from_departure_val = total_avg_cons * CO2_FACT; //l

                /*Calculate VSP*/
                Double _ac = (double)(((Double.parseDouble(current_speed.getValue())) - (Double.parseDouble(previews_speed.getValue())))*((double)1000/3600)) / period;
                Double _speed = Double.parseDouble(current_speed.getValue());

                Double vsp = calculateVsp(_speed,_ac);
                Double vsp_score = calculate_vspScore(vsp);
                stanadrdScore = (stanadrdScore + vsp_score) / 2;

                /*  max_speed  */
                if(_speed > max_speed)
                    max_speed=_speed;
                /*  SUM speed   */
                mean_speed+=_speed;

                /*   stops_number && total_stops_time   */
                if(_speed >0){
                    if(insNbStops >= minPeriodStops){
                        stops_number++;
                        total_stops_time+=insNbStops;
                        insNbStops=0;
                    }else{
                        insNbStops=0;
                    }
                } else if(_speed==0){
                    insNbStops+=period;
                    if(index==params.entrySet().size() && insNbStops >= minPeriodStops){
                        stops_number++;
                        total_stops_time+=insNbStops;
                        insNbStops=0;
                    }
                }

               /* fin stops_number && total_stops_time   */

                /*  total distance */
                if (Double.parseDouble(current_speed.getValue())==0) {
                    current_distance = 0d;
                } else {
                    current_distance = (Double) (Double.parseDouble(current_speed.getValue()) / 3600) * period;
                    total_distance += current_distance;
                }
				/*    Efficient Score     */
                efficiency_score = calculateEfficientScore(old_efficiency_score, vsp_score, Double.parseDouble(current_speed.getValue())*period);

                engine_rpm = Double.parseDouble(__getValByName(entry.getValue(), EnameValue.EngineRpm));
                /*      Pro Score    */
                pro_score = calculateProScore(old_pro_score, vsp_score, Double.parseDouble(current_speed.getValue())*period, engine_rpm);

                /*  Si on a aboutit à la dernière itération on rajoute le resultat final  */
                if(index==params.entrySet().size()){
                	/*Average Fuel*/
                    avgFuel= total_avg_cons * (100/total_distance);
                	/* Average CO2 Emission*/
                    avgCo2= avgFuel * CO2_FACT * 10;
                	/* mean_speed*/
                    mean_speed=((double)mean_speed/index);
                    tripRes.setMaxSpeed(max_speed);
                    tripRes.setMeanSpeed(mean_speed);
                    tripRes.setNumberStops(stops_number);
                    tripRes.setTotalStopsTime(total_stops_time);
                    tripRes.setTotalDistance(total_distance);
                    tripRes.setAvgCo2Emiss(avgCo2);
                    tripRes.setConsFuelFromDep(total_avg_cons);
                    tripRes.setEmissCo2FromDep(co2_emission_from_departure_val);
                    tripRes.setAvgFuelCons(avgFuel);
                    tripRes.setStandardScore(stanadrdScore);
                    tripRes.setEffciencyScore(((double)total_distance==0f? efficiency_score:(double)efficiency_score/total_distance));
                    tripRes.setProScore(((double)total_distance==0f? pro_score:(Double)pro_score/total_distance));
                }
                previews_speed = current_speed;
                previewsDate = currentDate;
                old_efficiency_score = efficiency_score;
                old_pro_score = pro_score;
            }
        }

        return tripRes;
    }

    private static Double calculateProScore(double old_pro_score, double vsp_score, double vss, double rpm) {
        Double score_pro = 0d;
        if (rpm > 2000) {
            score_pro = (double) (old_pro_score + (vsp_score * ((double) vss / 3600) * ((double) 1000 / rpm)));
        } else {
            score_pro = (double) (old_pro_score + (vsp_score * ((double) vss / 3600)));
        }
        return score_pro;
    }

    private static Double calculateEfficientScore(double old_efficiency_score, double vsp_score, double vss) {
        Double efficiency_score = 0d;
        efficiency_score = (double) (old_efficiency_score + ((double) vsp_score * ((double) vss / 3600)));
        return efficiency_score;
    }

    private static Double calculate_vspScore(double vsp) {
        Double vsp_score = 0d;
        if (vsp < -2) {
            vsp_score = 5d;
        } else if (-2 < vsp && vsp < 0) {
            vsp_score = 8d;
        } else if (0 <= vsp && vsp < 1) {
            vsp_score = 7d;
        } else if (1 <= vsp && vsp < 4) {
            vsp_score = 9d;
        } else if (4 <= vsp && vsp < 7) {
            vsp_score = 5d;
        } else if (7 <= vsp && vsp < 10) {
            vsp_score = 8d;
        } else if (10 <= vsp && vsp < 13) {
            vsp_score = 7.5d;
        } else if (13 <= vsp && vsp < 16) {
            vsp_score = 6d;
        } else if (16 <= vsp && vsp < 19) {
            vsp_score = 5.75d;
        } else if (19 <= vsp && vsp < 23) {
            vsp_score = 5.5d;
        } else if (23 <= vsp && vsp < 28) {
            vsp_score = 5.25d;
        } else if (28 <= vsp && vsp < 33) {
            vsp_score = 5d;
        } else if (33 <= vsp && vsp < 39) {
            vsp_score = 5d;
        } else if (39 <= vsp) {
            vsp_score = 8d;
        }
        return vsp_score;
    }

    private static double calculateVsp(double vss, double ac) {
        double vss3 = (double) Math.pow(vss * ((double) 1000 / 3600), 3);
        double res2 = ((vss * ((double) 1000 / 3600)) * (((double) 1.1d * ac) + 0.132d) + (double) 0.000302d * vss3);
        return res2;
    }

    private static final Float __distance_by_speed(Float speed){
        return speed /1/3600;
    }

    /*main method  retunr FUEL_FLOW for every parameter*/
    @SuppressWarnings("unused")
	private static List<Parameter> create_fuel_flow_and_distance_parameter(Trip trip){
        List<Parameter>  results = new ArrayList<>();

        trip.getParameters()
                .stream()
                .collect(Collectors
                        .groupingBy(Parameter::getDate/*, TreeMap::new, Collectors.mapping((Parameter s) -> s, Collectors.toList())*/))
                .entrySet()
                .forEach(item -> {
                    List<Parameter> params = item.getValue();
                    Parameter ref = params.get(0);
                    String airFlowRate = params.stream().filter(it -> it.getName().equals(EnameValue.AirFlowRate)).findFirst().get().getValue();
                    String engineLoad = params.stream().filter(it -> it.getName().equals(EnameValue.EngineLoad)).findFirst().get().getValue();
                    String speed =params.stream().filter(it -> it.getName().equals(EnameValue.Speed)).findFirst().get().getValue();

                    Parameter resul = __generate_fuel_flow_param(ref.getLat(), ref.getLon(), item.getKey(), airFlowRate, engineLoad);
                    Parameter distanceParam = new Parameter();
                    distanceParam.setLat(ref.getLat());
                    distanceParam.setLon(ref.getLon());
                    distanceParam.setValue(__distance_by_speed(Float.parseFloat(speed))+"");
                    distanceParam.setName(EnameValue.Distance);
                    distanceParam.setDate(ref.getDate());
                    results.add(resul);
                    results.add(distanceParam);
                });

        return results;
    }

    private static Parameter __generate_fuel_flow_param(double lat, double lon, Date date, String airFlowRate, String engineLoad) {
        Parameter param = new Parameter();
        param.setName(EnameValue.FUEL_FLOW);
        param.setDate(date);
        param.setLat(lat);
        param.setLon(lon);

        Float value = ((Float.parseFloat(airFlowRate) * (Float.parseFloat(engineLoad) / 100))
                / (LAMBDA_ACT * CONST_AIR_FUEL_RATIO * CONST_FUEL_DENST) )/ 1000;//%L/KM

        param.setValue(roundOff(value, 8).toString());
        return param;
    }

    private static Map<Date, List<Parameter>> _calculate_average_fuel_consu(Trip trip) {
        Map<Date, List<Parameter>> fuel_flows = trip.getParameters()
                .stream()
                .collect(Collectors
                        .groupingBy(Parameter::getDate, TreeMap::new, Collectors.mapping((Parameter s) -> s, Collectors.toList())));
        return fuel_flows;
    }

    private static Parameter __getParamByName(List<Parameter> params, EnameValue enameValue) {
        return params.stream().filter(item -> item.getName().equals(enameValue)).findFirst().orElse(null);
    }

    private static String __getValByName(List<Parameter> params, EnameValue enameValue) {
        Parameter res =  __getParamByName(params, enameValue);
        return res == null? null : res.getValue();
    }

    private final static Float roundOff(float x, int precision)
    {
        float a = x;
        double temp = Math.pow(10.0, precision);
        a *= temp;
        a = Math.round(a);
        return (a / (float)temp);
    }

    public static void main(String argv[]) {
//        String csvFile = "C:/Users/A0105774/private/input/inputdatatr1.csv";
        String csvFile = "C://Users//DELL//Desktop//Dhya_Dossier//INPUT_TEST_SCORE//Amin//inputdata2.csv";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyyHH:mm:ss", Locale.ENGLISH) ;//23-Dec-201610:25:43
        BufferedReader br = null;
        String line = "";
        String cvsSplitBy = ";";
        boolean firsLine = true;
        Trip trip = new Trip();
        try {

            br = new BufferedReader(new FileReader(csvFile));
            List<Parameter> params = new ArrayList<>();

            while ((line = br.readLine()) != null) {
                if(firsLine) {
                    firsLine = false;
                    continue;
                }


                String[] values = line.split(cvsSplitBy);
                Date date = null;
                for(int i = 0 ; i < values.length; i++) {
                    if (i == 0 ) {
                        date = sdf.parse(values[i].trim());

                    } else if(i == 1) {
                        Parameter  param = new Parameter();
                        param.setDate(date);
                        param.setName(EnameValue.IntAirTemperature);
                        param.setValue(values[i]);
                        param.setLat(Double.parseDouble(values[8]));
                        param.setLon(Double.parseDouble(values[6]));
                        params.add(param);
                    } else if( i == 2 ) {
                        Parameter  param = new Parameter();
                        param.setDate(date);
                        param.setName(EnameValue.Speed);
                        param.setValue(values[i]);
                        param.setLat(Double.parseDouble(values[8]));
                        param.setLon(Double.parseDouble(values[6]));
                        params.add(param);
                    } else if( i == 3) {
                        Parameter  param = new Parameter();
                        param.setDate(date);
                        param.setName(EnameValue.EngineRpm);
                        param.setValue(values[i]);
                        param.setLat(Double.parseDouble(values[8]));
                        param.setLon(Double.parseDouble(values[6]));
                        params.add(param);
                    } else if( i == 4) {
                        Parameter  param = new Parameter();
                        param.setDate(date);
                        param.setName(EnameValue.AirFlowRate);
                        param.setValue(values[i]);
                        param.setLat(Double.parseDouble(values[8]));
                        param.setLon(Double.parseDouble(values[6]));
                        params.add(param);
                    }else if( i == 5) {
                        Parameter  param = new Parameter();
                        param.setDate(date);
                        param.setName(EnameValue.ManAbsPressure);
                        param.setValue(values[i]);
                        param.setLat(Double.parseDouble(values[8]));
                        param.setLon(Double.parseDouble(values[6]));
                        params.add(param);
                    }else if( i == 6) {

                    }else if( i == 7) {
                        Parameter  param = new Parameter();
                        param.setDate(date);
                        param.setName(EnameValue.EngineLoad);
                        param.setValue(values[i]);
                        param.setLat(Double.parseDouble(values[8]));
                        param.setLon(Double.parseDouble(values[6]));
                        params.add(param);
                    }else if( i == 8) {

                    }
                    else if( i == 9) {
                        Parameter  param = new Parameter();
                        param.setDate(date);
                        param.setName(EnameValue.Batterie);
                        param.setValue(values[i]);
                        param.setLat(Double.parseDouble(values[8]));
                        param.setLon(Double.parseDouble(values[6]));
                        params.add(param);
                    }

                }
            }


            trip.setParameters(params);

            calculation_Results(trip,2,21);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException | ParseException e ) {
            e.printStackTrace();
            throw new RuntimeException();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    /*
     * Calculate avg score Trips
     * */
    public static Score CalculateScoreTrips(List<Trip> trips) {
        Score score = new Score();
        Double standardScore = 0d;
        Double efficiencyScore = 0d;
        Double proScore = 0d;
        int index = 1;

        for (Trip trip : trips) {
            standardScore += trip.getTripResult()!=null ? trip.getTripResult().getStandardScore():0;
            efficiencyScore += trip.getTripResult()!=null ? trip.getTripResult().getEffciencyScore():0;
            proScore += trip.getTripResult()!=null ? trip.getTripResult().getProScore():0;
            index++;
        }
        if(trips.size()>0){
            score.setStandardScore(standardScore != null ? standardScore / (index-1) : 0);
            score.setEfficiencyScore(efficiencyScore != null ? efficiencyScore / (index-1) : 0);
            score.setProScore(proScore != null ? proScore / (index-1) : 0);
            score.setGeneralScore(((standardScore!=null ? standardScore:0) + (efficiencyScore!=null ? efficiencyScore:0) + (proScore!=null?proScore:0)) /  (3*(index-1)));
        }
        return score;
    }
}