package com.smarto.microlink.utils;

import com.smarto.microlink.configuration.SpringMongoConfig;
import com.smarto.microlink.doa.UserRepository;
import com.smarto.microlink.domain.*;
import com.smarto.microlink.enumeration.ETripType;
import com.smarto.microlink.enumeration.EnameValue;
import com.smarto.microlink.exception.ExceptionInfo;
import com.smarto.microlink.exception.ServiceException;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import javax.script.ScriptException;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils {

    private static Pattern pattern1;
    private static Matcher matcher;
    private static final int _R = 6378137; // Earth's mean radius in meter

    public static boolean isEmptyString(String str) {
        return str == null || "".equals(str);
    }

    public static byte[] getFileBytes(String filePath) throws Exception {
        System.out.println("getFileBytes je suis la");
        File file = new File(filePath);
        int size = (int) file.length();
        byte[] buffer = new byte[size];
        FileInputStream in = new FileInputStream(file);
        in.read(buffer);
        in.close();
        return buffer;
    }

    public static String getFileChecksum(byte[] byteArray) throws Exception {

        MessageDigest md5Digest = MessageDigest.getInstance("MD5");
        md5Digest.update(byteArray, 0, byteArray.length);

        // Get the hash's bytes
        byte[] bytes = md5Digest.digest();

        // Convert bytes[] from decimal to hexadecimal format
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < bytes.length; i++) {
            sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
        }

        // return complete hash
        return sb.toString();
    }

    public static String encodeMD5(String password) {
        byte[] uniqueKey = password.getBytes();
        byte[] hash = null;

        try {
            hash = MessageDigest.getInstance("MD5").digest(uniqueKey);
        } catch (NoSuchAlgorithmException e) {
            throw new Error("No MD5 support in this VM.");
        }

        StringBuilder hashString = new StringBuilder();
        for (int i = 0; i < hash.length; i++) {
            String hex = Integer.toHexString(hash[i]);
            if (hex.length() == 1) {
                hashString.append('0');
                hashString.append(hex.charAt(hex.length() - 1));
            } else
                hashString.append(hex.substring(hex.length() - 2));
        }
        return hashString.toString();
    }

    public static double AverageSpeed(List<Parameter> parameters) {
        return parameters.stream().filter(p -> p.getName().equals(EnameValue.Speed))
                .mapToDouble(param -> Double.parseDouble(param.getValue()))
                .average().getAsDouble();
    }

    public static double distanceCaculator(List<Point> points) {
        double acc = 0;
        Point _tmp = null;
        for (Point p : points) {
            if (_tmp != null)
                acc += _distanceCalculator(_tmp, p);
            _tmp = p;
        }
        return acc;
    }

    private static double _distanceCalculator(Point p1, Point p2) {
        double dLat = __rad(p2.getLat() - p1.getLat());
        double dLong = __rad(p2.getLon() - p1.getLon());
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(__rad(p1.getLat())) * Math.cos(__rad(p2.getLat())) *
                        Math.sin(dLong / 2) * Math.sin(dLong / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        return (_R * c) / 1000;
    }

    private static double __rad(double x) {
        return x * Math.PI / 180;
    }

//	public static double getSpeed(List<Parameter> parameters) {
//		double speed = 0.0;
//		for (Parameter parameter : parameters) {
//			if (parameter.getName().getName().equals("Speed")) {
//				speed = Double.parseDouble(parameter.getValue());
//			}
//		}
//		return speed;
//	}

    public static long calculateDuration(Point startPoint, Point endPoint) {
        return getDateDiff(startPoint.getDate(), endPoint.getDate(), TimeUnit.MINUTES);
    }

    public static Point startPoint(List<Point> points) {

        Point startPoint = null;

        for (Point point : points) {
            if (point.isLeaving() == true)
                startPoint = point;
        }

        return startPoint;
    }

    public static Point endPoint(List<Point> points) {

        Point endPoint = null;

        for (Point point : points) {
            if (point.isArriving() == true)
                endPoint = point;
        }

        return endPoint;
    }

    /**
     * Get a diff between two dates
     *
     * @param date1    the oldest date
     * @param date2    the newest date
     * @param timeUnit the unit in which you want the diff
     * @return the diff value, in the provided unit
     */
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    public static boolean isGeneralTroubleCode(String code) {

        boolean found = false;

        pattern1 = Pattern.compile("P0\\d\\d\\d|P2\\d\\d\\d|P3\\d\\d\\d");
        matcher = pattern1.matcher(code);

        if (matcher.matches()) {
            found = true;
        }

        return found;

    }

    public static boolean isSpecificTroubleCode(String code) {

        boolean found = false;

        pattern1 = Pattern.compile("P1\\d\\d\\d");
        matcher = pattern1.matcher(code);

        if (matcher.matches()) {
            found = true;
        }

        return found;
    }

    public static String sha1(String s) {
        try {
            // Create MD5 Hash
            MessageDigest digest = java.security.MessageDigest.getInstance("SHA-1");
            digest.update(s.getBytes());
            byte messageDigest[] = digest.digest();

            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            for (int i = 0; i < messageDigest.length; i++)
                hexString.append(String.format("%02x", 0xFF & messageDigest[i]));
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    static public String formatStringManual(String string) {
        char[] charsData = new char[string.length()];
        string.getChars(0, charsData.length, charsData, 0);

        char c;
        for (int i = 0; i < charsData.length; i++) {
            if ((c = charsData[i]) >= 'A' && c <= 'Z') {
                charsData[i] = (char) (c - 'A' + 'a');
            } else {
                switch (c) {
                    case '\u00e0':
                    case '\u00e2':
                    case '\u00e4':
                        charsData[i] = 'a';
                        break;
                    case '\u00e7':
                        charsData[i] = 'c';
                        break;
                    case '\u00e8':
                    case '\u00e9':
                    case '\u00ea':
                    case '\u00eb':
                        charsData[i] = 'e';
                        break;
                    case '\u00ee':
                    case '\u00ef':
                        charsData[i] = 'i';
                        break;
                    case '\u00f4':
                    case '\u00f6':
                        charsData[i] = 'o';
                        break;
                    case '\u00f9':
                    case '\u00fb':
                    case '\u00fc':
                        charsData[i] = 'u';
                        break;
                }
            }
        }

        return new String(charsData);
    }

    public static HashMap<String, Object> getFeesResults(String userId, Date startDate, Date endDate, SpringMongoConfig springMongoConfig, UserRepository userRepository, String context) throws ServiceException {
        Query query = new Query();
        Query query1 = new Query();
        List<Trip> lastTrips = null;
        List<FeeConfiguration> configList = null;
        List<TripHeaderFee> fees = new ArrayList<TripHeaderFee>();
        double totalDistance = 0.0;
        double totalFee = 0.0;
        String feeRule = null;
        HashMap<String, Object> results = new HashMap<String, Object>();
        DateFormat dateFormatF = new SimpleDateFormat(" dd/MM/yyyy HH:mm:ss");
        DateFormat dateFormatFT = new SimpleDateFormat("HH:mm");
        SimpleDateFormat ffrd = new SimpleDateFormat("E dd MMMM yyyy", new Locale("fr"));
        SimpleDateFormat ffrw = new SimpleDateFormat("W", new Locale("fr"));
        DecimalFormat ddf = new DecimalFormat("#.##");

        if(context.equalsIgnoreCase("1")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            SimpleDateFormat ffr = new SimpleDateFormat("MMMM", new Locale("fr"));
            SimpleDateFormat ffry = new SimpleDateFormat("yy", new Locale("fr"));
            results.put("stringDate", ffr.format(startDate).toUpperCase() + "-" +  ffry.format(startDate));
        } else if(context.equalsIgnoreCase("2")) {
            Calendar cal = Calendar.getInstance();
            cal.setTime(startDate);
            results.put("stringDate", "Année " +  cal.get(Calendar.YEAR));
        } else {
            DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
            results.put("stringDate", "Du " + dateFormat.format(startDate) + " au " + dateFormat.format(endDate));
        }

        query.with(new Sort(Sort.Direction.DESC, "startTrip.date"));
        query.addCriteria(Criteria.where("startTrip.date").gte(startDate).lte(endDate).and("user.id").is(userId).and("type").is(ETripType.PRO));
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            lastTrips = mongoTemplate.find(query, Trip.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        if(lastTrips != null && !lastTrips.isEmpty()) {
            for (Trip trip : lastTrips) {
                TripHeaderFee headerFee = new TripHeaderFee(dateFormatFT.format(trip.getStartTrip().getDate()) + "\n" + trip.getStartTripAdress(), dateFormatFT.format(trip.getEndTrip().getDate()) + "\n" + trip.getEndTripAdress(), ddf.format(trip.getDistance() )+ " Km", trip.getClient(), ffrd.format(trip.getStartTrip().getDate()), ffrw.format(trip.getStartTrip().getDate()), null);
                headerFee.setDistanceD(trip.getDistance());
                fees.add(headerFee);
                totalDistance += trip.getDistance();
            }
        }

        User user = userRepository.findById(userId);

        // to do : calculate feeAmount
        // PF = (CO2 : 45) + ((CD * 0,736) : 40) puissance 1,6
        double fPower = (user.getUVehicleDetail().get(0).getVehicle().getCo2Emission()/45) + ((Double.parseDouble(user.getUVehicleDetail().get(0).getVehicle().getPuissance())*0.736)/40);
        int fiscalPower = (int) Math.pow(fPower, 1.6);

//        query1.addCriteria(Criteria.where("entreprise.id").is(user.getEntreprise().getId()));
        try {
            MongoTemplate mongoTemplate = springMongoConfig.mongoTemplate();
            configList = mongoTemplate.find(query1, FeeConfiguration.class);
        } catch (Exception e) {
            throw new ServiceException(ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_CODE,
                    ExceptionInfo.CANNOT_PRODUCE_DB_QUERY_ERROR_MSG, 500);
        }

        if(configList != null && !configList.isEmpty()) {
            if (configList.get(0).getcFees() != null && !configList.get(0).getcFees().isEmpty()) {
                for (FeeByDistance config : configList.get(0).getcFees()) {
                    if (fiscalPower > config.getSupportedFiscalPower().getMin() && fiscalPower <= config.getSupportedFiscalPower().getMax()) {
                        if (totalDistance > (double) config.getSupportedDistance().getMin() && totalDistance <= (double) config.getSupportedDistance().getMax()) {
                            // apply formula
                            feeRule = config.getFormula();
                            String newFormula = config.getFormula().replaceAll("d", "" + totalDistance);
                            ScriptEngineManager manager = new ScriptEngineManager();
                            ScriptEngine engine = manager.getEngineByName("JavaScript");
                            try {
                                totalFee = (double) engine.eval(newFormula);
                            } catch (ScriptException e) {
                                System.err.println(e.toString());
                            }
                            break;
                        }
                    }
                }
            }
        }

        if(fees != null && !fees.isEmpty()) {
            for (TripHeaderFee headerFee : fees) {
                headerFee.setFee(ddf.format((totalFee*headerFee.getDistanceD())/totalDistance) + " €");
            }
        }

        results.put("list", fees);
        results.put("totalDistance", ddf.format(totalDistance));
        results.put("totalFee", ddf.format(totalFee));
        results.put("userName", user.getFirstName() + " " + user.getLastName());
//        results.put("urlLogo", user.getEntreprise().getLogo());
        results.put("power", fiscalPower + "");
        results.put("feeRule", feeRule);
        results.put("make", user.getUVehicleDetail().get(0).getVehicle().getMake());
        results.put("model", user.getUVehicleDetail().get(0).getVehicle().getModel());
        results.put("registrationNumber", user.getUVehicleDetail().get(0).getRegistrationNumber());

        return results;
    }
}
