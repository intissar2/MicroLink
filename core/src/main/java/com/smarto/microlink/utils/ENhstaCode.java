package com.smarto.microlink.utils;

/**
 * Created by bassem smida on 23/11/2016.
 */
public enum ENhstaCode {
    MAKE("26", "make"), MODEL("28", "model"), YEAR("29", "make year"),
    ENGINE("13", "Engine"), FUEL("24", "Fuel"), PUISSANCE("12", "Puissance");

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    private String code;
    private String name;

    ENhstaCode(String code, String name) {
        this.code = code;
        this.name = name;
    }

}
