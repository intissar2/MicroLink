package com.smarto.microlink.utils;

/**
 * Created by bassem smida on 23/11/2016.
 */
public enum EVinDecodeEUCode {

    MODEL("Model", "model"), MAKE("Make", "make"), YEAR("Model Year", "model year"),
    ENGINE("Engine Displacement (ccm)", "Engine"), FUEL("Fuel Type - Primary", "Fuel"), PUISSANCE("Engine Power (kW)", "Puissance");

    EVinDecodeEUCode(String code, String name) {
        this.code = code;
        this.name = name;
    }

    private String code;

    public String getCode() {
        return code;
    }

    public String getName() {
        return name;
    }

    private String name;
}
