package com.smarto.microlink.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JSONSerializer;

/**
 * @author DELL
 * Get Adress By Altitude & longitude
 */
public class GeoLocation {
	public static String _URL = "https://maps.googleapis.com/maps/api/geocode/json?latlng=";

	/*
	 * A partir de longitude et de l'alititude on récupère l'adresse
	 * ceci via le service http de google qui retoune un flux de données Json
	 * qui contient parmis les données retounrnées l'adresse formattée
	 */
	public static String getAdressLocation(double lat, double lon) {
		String formatted_address = "";

		try {
			URL url = new URL(_URL + lat + "," + lon + "&sensor=true&key=AIzaSyAcXgGZ0d9ujapO3SMXvq5EeVG1Utb4wVI");
			// etablir la Connexion
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			// lecture du flux de données via l'URL
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

			String output;
			String out = "";

			while ((output = br.readLine()) != null) {
				out += output;
			}

			// conversion du String vers JSONObject
			JSONObject json = (JSONObject) JSONSerializer.toJSON(out);
			JSONArray results = json.getJSONArray("results");
			JSONObject rec = results.getJSONObject(0);
			formatted_address = rec.getString("formatted_address");

			conn.disconnect();
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(IndexOutOfBoundsException e){
			e.printStackTrace();
		}

		return formatted_address;
	}

	public static void main(String[] args) {
		System.out.println("#First=="+new Date());
		System.out.println("adresse== " + GeoLocation.getAdressLocation(48.8705378, 2.2315169));
		System.out.println("#End=="+new Date());

	}
}