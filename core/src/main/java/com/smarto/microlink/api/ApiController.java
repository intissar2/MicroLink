package com.smarto.microlink.api;

public abstract class ApiController {

    public static final String API_PATH = "/api/v1";

//    public static final String AUTHENTICATE_URL = API_PATH + "/authenticate";

    // MicroLink Actuator services
    public static final String FIRMWARE_ENDPOINT = API_PATH + "/firmwares";
    public static final String SUPPORTEDPID_ENDPOINT = API_PATH + "/supportedPIDs";
    public static final String TRIP_ENDPOINT = API_PATH + "/trips";
    public static final String TROUBLECODE_ENDPOINT = API_PATH + "/troublecodes";
    public static final String USER_ENDPOINT = API_PATH + "/users";
    public static final String VEHICLE_ENDPOINT = API_PATH + "/vehicles";
    public static final String FIELD_DATA_VEHICLE_ENDPOINT = API_PATH + "/fieldsDataVehicles";
    public static final String DEVICE_ENDPOINT = API_PATH + "/devices";
    public static final String FEE_ENDPOINT = API_PATH + "/fees";
    public static final String CARCARE_ENDPOINT = API_PATH + "/carCare";
    public static final String FUEL_ENDPOINT = API_PATH + "/fuel";
    public static final String VERSIONBL_ENDPOINT = API_PATH + "/bl";

    /* ***************** ENTREPRISE ************************ */
    public static final String ENTREPRISE_ENDPOINT = API_PATH + "/entreprise";

    public static final String GET_ALL_ENTREPRISE_ENDPOINT = "/entrepriseId";


    public static final String POST_ENTREPRISE_ENDPOINT = "/addEntreprise";

    public static final String PUT_ENTREPRISE_ENDPOINT = "/{id}";


    /* ************************************************************************************** */
    //NEW
    public static final String VEHICULE_VIN_ENDPOINT = "/vin/{vin}";
    //END NEW

    // **********************        FIRMWARE REST API services      ***************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_FIRMWARE_LAST_VERSION_ENDPOINT2 = FIRMWARE_ENDPOINT + "/lastVersion/{firmwareVerCan}/{firmwareVerBle}/";
    public static final String GET_FIRMWARE_LAST_VERSION_ENDPOINT = FIRMWARE_ENDPOINT + "/lastVersion/{firmwareVer}";
    public static final String GET_FIRMWARE_BY_ID_ENDPOINT = FIRMWARE_ENDPOINT + "/{firmwareId}";
    public static final String GET_ALL_FIRMWARE_ENDPOINT = FIRMWARE_ENDPOINT;
    public static final String GET_FIRMWARE_BY_TYPE_ENDPOINT = FIRMWARE_ENDPOINT + "/types/{firmwareType}";
    public static final String GET_FIRMWARE_BY_VERSION_ENDPOINT = FIRMWARE_ENDPOINT + "/versions/{firmwareVersion}";
    public static final String GET_FIRMWARE_BY_DATE_ENDPOINT = FIRMWARE_ENDPOINT + "/dates/{firmwareDate}";
    public static final String GET_VERIFY_LAST_VERSION_FIRMWARE_ENDPOINT = FIRMWARE_ENDPOINT + "/verifyLastVersion";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_FIRMWARE_ENDPOINT = FIRMWARE_ENDPOINT;

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_FIRMWARE_ENDPOINT = FIRMWARE_ENDPOINT + "/{id}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_FIRMWARE_ENDPOINT = FIRMWARE_ENDPOINT + "/{firmwareId}";
    public static final String DELETE_FIRMWARE_BY_VERSION_ENDPOINT = FIRMWARE_ENDPOINT + "/versions/{firmwareVer}";
    public static final String DELETE_ALL_FIRMWARE_ENDPOINT = FIRMWARE_ENDPOINT;


    // *******************          SUPPORTEDPID REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_ALL_SUPPORTEDPID_ENDPOINT = SUPPORTEDPID_ENDPOINT;
    public static final String GET_SUPPORTEDPID_BY_CODE_ENDPOINT = SUPPORTEDPID_ENDPOINT + "/code/{code}";
    public static final String GET_LIST_SUPPORTEDPID_ENDPOINT = SUPPORTEDPID_ENDPOINT + "/codeList/{codeList}";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_SUPPORTEDPID_ENDPOINT = SUPPORTEDPID_ENDPOINT;

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_SUPPORTEDPID_ENDPOINT = SUPPORTEDPID_ENDPOINT + "/{id}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_SUPPORTEDPID_ENDPOINT = SUPPORTEDPID_ENDPOINT + "/{id}";
    public static final String DELETE_ALL_SUPPORTEDPID_ENDPOINT = SUPPORTEDPID_ENDPOINT;


    // *******************          TROUBLECODE REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_ALL_TROUBLECODE_ENDPOINT = TROUBLECODE_ENDPOINT;
    public static final String GET_TROUBLECODE_BY_ID_ENDPOINT = TROUBLECODE_ENDPOINT + "/{id}";
    public static final String GET_LIST_TROUBLECODE_HEADER_ENDPOINT = TROUBLECODE_ENDPOINT + "/headers/{codeList}/{manufactor}";
    public static final String GET_LIST_TROUBLECODE_ENDPOINT = TROUBLECODE_ENDPOINT + "/{codeList}/{manufactor}";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_TROUBLECODE_ENDPOINT = TROUBLECODE_ENDPOINT;

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_TROUBLECODE_ENDPOINT = TROUBLECODE_ENDPOINT + "/{id}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_TROUBLECODE_ENDPOINT = TROUBLECODE_ENDPOINT + "/{id}";
    public static final String DELETE_ALL_TROUBLECODE_ENDPOINT = TROUBLECODE_ENDPOINT;


    // *******************          TRIP REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_ALL_TRIP_ENDPOINT = "/{userId}";
    public static final String GET_ALL_USERS_TRIP_ENDPOINT = "/allUsersTrip/show";
    public static final String GET_TRIP_BY_ID_ENDPOINT = "/{userId}" + "/{tripId}";
    public static final String GET_TRIP_BY_ID_ENDPOINT2 = "2/{userId}" + "/{tripId}";
    public static final String GET_LAST_TRIP_ENDPOINT = "/last" + "/{userId}";
    public static final String GET_TRIPS_LAST_WEEK_ENDPOINT = "/lastWeek" + "/{userId}";
    public static final String GET_TRIPS_CURRENT_WEEK_ENDPOINT = "/currentWeek" + "/{userId}";
    public static final String GET_TRIPS_LAST_MONTH_ENDPOINT = "/lastMonth" + "/{userId}";
    public static final String GET_TRIPS_LAST_YEAR_ENDPOINT = "/lastYear" + "/{userId}";
    public static final String GET_LAST_TRIPS_FROM_DATE_ENDPOINT = "/from/{date}" + "/{userId}";
    public static final String GET_LAST_TRIPS_BY_PAGE_ENDPOINT = "/pages/{pageNumber}/{pageSize}" + "/{userId}";
    public static final String GET_LAST_TRIPS_BETWEEN_TWO_DATES_ENDPOINT = "/between/{startDate}/{endDate}" + "/{userId}";

    public static final String GET_LAST_SCORETRIP_ENDPOINT = "/score/last" + "/{userId}";
    public static final String GET_SCORETRIPS_LAST_WEEK_ENDPOINT = "/score/lastWeek" + "/{userId}";
    public static final String GET_SCORETRIPS_CURRENT_WEEK_ENDPOINT = "/score/currentWeek" + "/{userId}";
    public static final String GET_SCORETRIPS_LAST_MONTH_ENDPOINT = "/score/lastMonth" + "/{userId}";
    public static final String GET_SCORETRIPS_LAST_YEAR_ENDPOINT = "/score/lastYear" + "/{userId}";
    public static final String GET_LAST_SCORETRIPS_FROM_DATE_ENDPOINT = "/score/from/{date}" + "/{userId}";
    public static final String GET_LAST_SCORETRIPS_BY_PAGE_ENDPOINT = "/score/pages/{pageNumber}/{pageSize}" + "/{userId}";
    public static final String GET_LAST_SCORETRIPS_BETWEEN_TWO_DATES_ENDPOINT = "/score/between/{startDate}/{endDate}" + "/{userId}";
    public static final String GET_SCORETRIPS_CURRENT_YEAR_ENDPOINT = "/score/currentYear" + "/{userId}";
    public static final String GET_SCORETRIPS_CURRENT_MONTH_ENDPOINT = "/score/currentMonth" + "/{userId}";
    public static final String GET_SCORETRIPS_CURRENT_DAY_ENDPOINT = "/score/currentDay" + "/{userId}";
    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_TRIP_ENDPOINT = "/{period}/{minPeriodStops}";

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_TRIP_ENDPOINT = "/{tripId}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_TRIP_ENDPOINT = "/{tripId}";
    public static final String DELETE_ALL_TRIP_ENDPOINT = "";


    // *******************          USER REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_ALL_USER_ENDPOINT = "";
    public static final String GET_ALL_USER_LAST_POSITION_ENDPOINT = "/allUserlastPosition";
    public static final String GET_USER_BY_ID_ENDPOINT = "/{userId}";
    public static final String GET_IS_MAIL_USER_EXIST_BY_ID_ENDPOINT = "/mails/{mail}";
    public static final String GET_AUTHENTICATE_ENDPOINT = "/authenticate/{mail}/{mdp}";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_USER_ENDPOINT = "";

    public static final String POST_USER_DEVICE_ENDPOINT = "/device";

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_USER_ENDPOINT = "/{id}";
    public static final String PUT_USER_DEVICE = "/{userId}/{idDevice}/{isUsed}";


    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_USER_ENDPOINT = "/{userId}";
    public static final String DELETE_ALL_USER_ENDPOINT = "";

    // *******************          VEHICLE REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    //public static final String GET_ALL_VEHICLES_ENDPOINT = VEHICLE_ENDPOINT;
    public static final String GET_VEHICLE_BY_ID_ENDPOINT = "/{id}";
    public static final String GET_ALL_VEHICLES_MAKES_ENDPOINT = "/makes";
    public static final String GET_ALL_VEHICLES_HEADER_ENDPOINT = "/headers";
    public static final String GET_LIST_VEHICLES_HEADER_BY_MAKE_ENDPOINT = "/headers/{make}";
    public static final String PATCH_BY_ID_ENDPOINT = "/{id}";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_VEHICLE_ENDPOINT = "";

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_VEHICLE_ENDPOINT = "/{id}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_VEHICLE_ENDPOINT = "/{id}";
    public static final String DELETE_ALL_VEHICLE_ENDPOINT = "";

    // *******************          FIELD_DATA_VEHICLE REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_ALL_FIELD_DATA_VEHICLES_ENDPOINT = FIELD_DATA_VEHICLE_ENDPOINT;
    public static final String GET_FIELD_DATA_VEHICLE_BY_ID_ENDPOINT = FIELD_DATA_VEHICLE_ENDPOINT + "/{id}";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_FIELD_DATA_VEHICLE_ENDPOINT = FIELD_DATA_VEHICLE_ENDPOINT;

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_FIELD_DATA_VEHICLE_ENDPOINT = FIELD_DATA_VEHICLE_ENDPOINT + "/{id}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_FIELD_DATA_VEHICLE_ENDPOINT = FIELD_DATA_VEHICLE_ENDPOINT + "/{id}";
    public static final String DELETE_ALL_FIELD_DATA_VEHICLE_ENDPOINT = FIELD_DATA_VEHICLE_ENDPOINT;

// *******************          Device REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_ALL_DEVICE_ENDPOINT = "";
    public static final String GET_ALL_DEVICE_BY_OPERATOR_ENDPOINT = "/operatorName/{operatorName}";
    public static final String GET_DEVICE_BY_ID_ENDPOINT = "/{deviceId}";
    public static final String GET_LAST_DEVICE_ENDPOINT = "/last";
    public static final String GET_DEVICES_LAST_WEEK_ENDPOINT = "/lastWeek";
    public static final String GET_DEVICES_CURRENT_WEEK_ENDPOINT = "/lastWeek";
    public static final String GET_DEVICES_LAST_MONTH_ENDPOINT = "/lastMonth";
    public static final String GET_DEVICES_LAST_YEAR_ENDPOINT = "/lastYear";
    public static final String GET_LAST_DEVICES_FROM_DATE_ENDPOINT = "/from/{date}";
    public static final String GET_LAST_DEVICES_BY_PAGE_ENDPOINT = "/pages/{pageNumber}/{pageSize}";
    public static final String GET_LAST_DEVICES_BETWEEN_TWO_DATES_ENDPOINT = "/between/{startDate}/{endDate}";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_DEVICE_ENDPOINT = "";

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_DEVICE_ENDPOINT = "/{deviceId}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_DEVICE_ENDPOINT = "/{deviceId}";
    public static final String DELETE_ALL_DEVICE_ENDPOINT = "";

// *******************          Fee Plug REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_FEE_PLUG_REPORT_BY_USER_BETWEEN_TWO_DATES = "/fees/between/{userId}/{startDate}/{endDate}/{context}";

    public static final String GET_FEE_PLUG_REPORT_BY_USER_MONTH = "/fees/{userId}/{year}/{month}/{context}";

    public static final String GET_FEE_PLUG_REPORT_BY_USER_YEAR = "/fees/{userId}/{year}/{context}";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_FEE_DISTANCE_POWER_ENDPOINT = "";

// *******************          CarCare REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_ALL_CARCARE_ENDPOINT = "";
    public static final String GET_CARCARE_BY_USER = "/user/{userId}";
    public static final String GET_CARCARE_BY_VEHICLE = "/vehicle/{vehicleId}";
    public static final String GET_CARCARE_BETWEEN_TWO_DATES_ENDPOINT = "/between/{userId}/{vehicleId}/{firstDate}/{endDate}";
    public static final String GET_CARCARE_REPORT_BY_YEAR = "/byYear/{userId}/{vehicleId}/{year}";
    public static final String GET_CARCARE_REPORT_BY_MONTH = "/byMonth/{userId}/{vehicleId}/{year}/{month}";
    public static final String GET_CARCARE_REPORT_BETWEEN_TWO_DATES_ENDPOINT = "/report" + "/between/{userId}/{vehicleId}/{firstDate}/{endDate}";
    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_CARCARE_ENDPOINT = "";

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_CARCARE_ENDPOINT = "/{carCareId}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_CARCARE_ENDPOINT = "/{carCareId}";
    public static final String DELETE_ALL_CARCARE_ENDPOINT = "";

    // *******************          Fuel REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_ALL_FUEL_ENDPOINT = "";
    public static final String GET_FUEL_BY_USER = "/user/{userId}";
    //    public static final String GET_FUEL_BY_VEHICLE = "/vehicle/{vehicleId}";
    public static final String GET_FUEL_BETWEEN_TWO_DATES_ENDPOINT = "/between/{userId}/{vehicleId}/{firstDate}/{endDate}";
    public static final String GET_FUEL_REPORT_BY_YEAR = "/byYear/{userId}/{vehicleId}/{year}";
    public static final String GET_FUEL_REPORT_BY_MONTH = "/byMonth/{userId}/{vehicleId}/{year}/{month}";
    public static final String GET_FUEL_REPORT_BETWEEN_TWO_DATES_ENDPOINT = "/report" + "/between/{userId}/{vehicleId}/{firstDate}/{endDate}";
    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_FUEL_ENDPOINT = "";

    //------------------ PUT SERVICES
    //--------------------------------------------------------
    public static final String PUT_FUEL_ENDPOINT = "/{fuelId}";

    //------------------ DELETE SERVICES
    //--------------------------------------------------------
    public static final String DELETE_FUEL_ENDPOINT = "/{fuelId}";
    public static final String DELETE_ALL_FUEL_ENDPOINT = "";

    // *******************          VersionBL REST API services      **************************************** //

    //------------------ GET SERVICES
    //--------------------------------------------------------
    public static final String GET_LAST_VERSIONBL = "";

    //------------------ POST SERVICES
    //--------------------------------------------------------
    public static final String POST_VERSIONBL_ENDPOINT = "";

    //------------------ PUT SERVICES
    //--------------------------------------------------------

    //------------------ DELETE SERVICES
    //--------------------------------------------------------

}
