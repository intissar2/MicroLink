package com.smarto.microlink.exception;

public class ServiceException extends Exception {

    /**
     *
     */
    private static final long serialVersionUID = 1L;
    private final String code;
    private final String message;
    private final int httpStatusCode;

    public ServiceException(String code, String message, int httpStatusCode) {
        super();
        this.code = code;
        this.message = message;
        this.httpStatusCode = httpStatusCode;
    }

    public String getCode() {
        return code;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public int getHttpStatusCode() {
        return httpStatusCode;
    }
}