package com.smarto.microlink.exception;

public interface ExceptionInfo {

    // Error codes
    public static final String MISSING_INPUT_ERROR_CODE = "1000";

    public static final String MISSING_DATA_VIN_ERROR_CODE = "2001";
    public static final String MISSING_DATA_ADDRESS_ERROR_CODE = "2002";

    public static final String MISSING_FIRMWARE_FILE_ERROR_CODE = "3001";
    public static final String CANNOT_GET_FILE_CHECKSUM_ERROR_CODE = "3002";

    public static final String UNEXPECTED_ERROR_CODE = "5000";

    public static final String CANNOT_PRODUCE_DB_QUERY_ERROR_CODE = "4000";

    public static final String UNKNOW_INPUT_DATA_EXCEPTION_CODE = "4004";

    // Error messages
    public static final String MISSING_INPUT_ERROR_MSG = "Input is missing";

    public static final String MISSING_DATA_VIN_ERROR_MSG = "Data vin is missing";
    public static final String MISSING_DATA_ADDRESS_ERROR_MSG = "Data address is missing";

    public static final String MISSING_FIRMWARE_FILE_ERROR_MSG = "Firmware file is missing";
    public static final String CANNOT_GET_FILE_CHECKSUM_ERROR_MSG = "Cannot get file checksum";

    public static final String UNEXPECTED_ERROR_MSG = "Service unexpected error";

    public static final String CANNOT_PRODUCE_DB_QUERY_ERROR_MSG = "Cannot produce mongo query";

    public static final String UNKNOW_INPUT_DATA_EXCEPTION_MSG = "unknown input data exception";
}
