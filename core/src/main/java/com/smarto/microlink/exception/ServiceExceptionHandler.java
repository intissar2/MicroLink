package com.smarto.microlink.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletResponse;

@ControllerAdvice
public class ServiceExceptionHandler {

    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceExceptionHandler.class);

    // error 400
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    ErrorResponse error400(Exception ex) {
        LOGGER.error("HTTP status " + HttpStatus.BAD_REQUEST + " | " + ExceptionInfo.MISSING_INPUT_ERROR_MSG);
        ex.printStackTrace();
        return new ErrorResponse(ExceptionInfo.MISSING_INPUT_ERROR_CODE, ExceptionInfo.MISSING_INPUT_ERROR_MSG);
    }

    // error 500
    @ExceptionHandler
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ResponseBody
    ErrorResponse error500(Exception ex, HttpServletResponse response) {
        response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
        LOGGER.error("HTTP status " + HttpStatus.INTERNAL_SERVER_ERROR + " | " + ex.getMessage());
        ex.printStackTrace();
        return new ErrorResponse(ExceptionInfo.UNEXPECTED_ERROR_CODE, ExceptionInfo.UNEXPECTED_ERROR_MSG);
    }

    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public ErrorResponse errorResponse(ServiceException ex, HttpServletResponse response) {
        response.setStatus(ex.getHttpStatusCode());
        if (ex.getHttpStatusCode() == HttpStatus.BAD_REQUEST.value()) {
            LOGGER.error(HttpStatus.BAD_REQUEST + " | " + ex.getMessage());
            ex.printStackTrace();
            return new ErrorResponse(ex.getCode(), ex.getMessage());
        } else {
            LOGGER.error(HttpStatus.INTERNAL_SERVER_ERROR + " | " + ex.getMessage());
            ex.printStackTrace();
            return new ErrorResponse(ExceptionInfo.UNEXPECTED_ERROR_CODE,
                    ExceptionInfo.UNEXPECTED_ERROR_MSG);
        }
    }

	/*
    @ExceptionHandler
	@ResponseStatus(HttpStatus.REQUEST_TIMEOUT)
	@ResponseBody
	ErrorResponse error408(ApiTimeOutException ex, HttpServletResponse response) {
		response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		ex.printStackTrace();
		LOGGER.error("OUT|OPE|" + HttpStatus.REQUEST_TIMEOUT + "|" + ex.getMessage());
		return new ErrorResponse(ServiceExceptionCodesAndMsgs.APITIMEOUTCODE,
				ServiceExceptionCodesAndMsgs.APITIMEOUTMSG);
	}
	
	/*
	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ResponseBody
	ErrorResponse error404() {
		LOGGER.error("OUT|OPE|" + HttpStatus.NOT_FOUND);
		return new ErrorResponse(ExceptionInfo.NOT_FOUND_CODE, ExceptionInfo.NOT_FOUND_MSG);
	}
	*/
}
