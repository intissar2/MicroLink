package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "feeDetail")
public class FeeDetail {

    @Id
    String id;

    private Date date;

    private Double distance;

    private String timeUsed;

    private Double feeAmount;

    private String comment;

    public FeeDetail(String id, Date date, Double distance, String timeUsed, Double feeAmount, String comment) {
        super();
        this.id = id;
        this.date = date;
        this.distance = distance;
        this.timeUsed = timeUsed;
        this.feeAmount = feeAmount;
        this.comment = comment;
    }

    public FeeDetail() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Double getDistance() {
        return distance;
    }

    public void setDistance(Double distance) {
        this.distance = distance;
    }

    public String getTimeUsed() {
        return timeUsed;
    }

    public void setTimeUsed(String timeUsed) {
        this.timeUsed = timeUsed;
    }

    public Double getFeeAmount() {
        return feeAmount;
    }

    public void setFeeAmount(Double feeAmount) {
        this.feeAmount = feeAmount;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}