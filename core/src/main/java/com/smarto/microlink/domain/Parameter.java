package com.smarto.microlink.domain;

import com.smarto.microlink.enumeration.EnameValue;

import java.util.Date;

public class Parameter {

    private EnameValue name;
    private String value;
    private double lat;
    private double lon;
    private Date date;

    public Parameter(EnameValue name, String value) {
        super();
        this.name = name;
        this.value = value;
    }

    public Parameter(EnameValue name, String value, double lat, double lon, Date date) {
        super();
        this.name = name;
        this.value = value;
        this.lat = lat;
        this.lon = lon;
        this.date = date;
    }

    public Parameter() {
        super();
        // TODO Auto-generated constructor stub
    }

    public EnameValue getName() {
        return name;
    }

    public void setName(EnameValue name) {
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
