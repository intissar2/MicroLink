package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "troublecode")
public class TroubleCode {

    @Id
    private String id;
    private String code;
    private String description;
    private String url;
    private Detail detail;
    private String manufactor;

    public TroubleCode() {
        super();
        // TODO Auto-generated constructor stub
    }

    public TroubleCode(String id, String code, String description, String url, String manufactor, Detail detail) {
        super();
        this.id = id;
        this.code = code;
        this.description = description;
        this.url = url;
        this.detail = detail;
        this.manufactor = manufactor;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public Detail getDetail() {
        return detail;
    }

    public void setDetail(Detail detail) {
        this.detail = detail;
    }

    public String getManufactor() {
        return manufactor;
    }

    public void setManufactor(String manufactor) {
        this.manufactor = manufactor;
    }

}
