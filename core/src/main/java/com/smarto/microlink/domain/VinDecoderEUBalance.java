package com.smarto.microlink.domain;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Created by bassem smida on 23/11/2016.
 */
public class VinDecoderEUBalance {
    public Integer getApi_decode() {
        return api_decode;
    }

    public void setApi_decode(Integer api_decode) {
        this.api_decode = api_decode;
    }

    @JsonProperty("API DECODE")
    private Integer api_decode;
}
