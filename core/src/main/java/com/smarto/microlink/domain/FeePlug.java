package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "feePlug")
public class FeePlug {

    @Id
    String id;

    private int year;

    private int month;

    private Double totalDisatnce;

    private Double totalFee;

    private String reportName;

    private List<FeeDetail> details;

    @DBRef(lazy = true)
    private User user;

    public FeePlug(String id, int year, int month, Double totalDisatnce, Double totalFee, String reportName,
                   User user) {
        super();
        this.id = id;
        this.year = year;
        this.month = month;
        this.totalDisatnce = totalDisatnce;
        this.totalFee = totalFee;
        this.reportName = reportName;
        this.user = user;
    }

    public FeePlug() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public Double getTotalDisatnce() {
        return totalDisatnce;
    }

    public void setTotalDisatnce(Double totalDisatnce) {
        this.totalDisatnce = totalDisatnce;
    }

    public Double getTotalFee() {
        return totalFee;
    }

    public void setTotalFee(Double totalFee) {
        this.totalFee = totalFee;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public List<FeeDetail> getDetails() {
        return details;
    }

    public void setDetails(List<FeeDetail> details) {
        this.details = details;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}