package com.smarto.microlink.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

public class FeeByDistance {

    private String formula;

    @DBRef(lazy = true)
    private SupportedDistance distance;

    @DBRef(lazy = true)
    private SupportedFiscalPower power;

    public FeeByDistance() {
        super();
    }

    public FeeByDistance(String formula, SupportedDistance distance, SupportedFiscalPower power) {
        super();
        this.formula = formula;
        this.distance = distance;
        this.power = power;
    }

    public String getFormula() {
        return formula;
    }

    public void setFormula(String formula) {
        this.formula = formula;
    }

    public SupportedDistance getSupportedDistance() {
        return distance;
    }

    public void setSupportedDistance(SupportedDistance distance) {
        this.distance = distance;
    }

    public SupportedFiscalPower getSupportedFiscalPower() {
        return power;
    }

    public void setSupportedFiscalPower(SupportedFiscalPower power) {
        this.power = power;
    }

}