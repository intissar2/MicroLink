package com.smarto.microlink.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

public class UDevice {
    private String serialNumber;
    @DBRef(lazy = true)
    private Device device;

    public UDevice() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

}
