package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "supportedPID")
public class SupportedPID {

    @Id
    private String id;
    private String mode;
    private String code;
    private String description;

    public SupportedPID() {
        super();
        // TODO Auto-generated constructor stub
    }

    public SupportedPID(String id, String mode, String code, String description) {
        super();
        this.id = id;
        this.mode = mode;
        this.code = code;
        this.description = description;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
