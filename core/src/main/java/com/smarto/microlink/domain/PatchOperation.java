package com.smarto.microlink.domain;

import com.smarto.microlink.enumeration.EPatchOp;

/**
 * Created by DELL on 26/12/2016.
 */
public class PatchOperation {

    private EPatchOp op;

    public EPatchOp getOp() {
        return op;
    }

    public void setOp(EPatchOp op) {
        this.op = op;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    private Object value;
    private String from;
    private String path;
}
