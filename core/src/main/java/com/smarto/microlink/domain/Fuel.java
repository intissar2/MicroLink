package com.smarto.microlink.domain;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "fuel")
public class Fuel {

	@Id
	private String id;
	String fuel;
    double fuelPrice;
    String station;
    String adress;
    double volume;
    double latitude;
    double longitude;
    Date date;
	
	@DBRef(lazy = true)
	private User user;
	
	@DBRef(lazy = true)
	private Vehicle vehicle;

	public Fuel() {
		super();
	}

	public Fuel(String id, String fuel, double fuelPrice, String station, String adress, double volume,
			double latitude, double longitude, Date date, User user, Vehicle vehicle) {
		super();
		this.id = id;
		this.fuel = fuel;
		this.fuelPrice = fuelPrice;
		this.station = station;
		this.adress = adress;
		this.volume = volume;
		this.latitude = latitude;
		this.longitude = longitude;
		this.date = date;
		this.user = user;
		this.vehicle= vehicle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFuel() {
		return fuel;
	}

	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	public double getFuelPrice() {
		return fuelPrice;
	}

	public void setFuelPrice(double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}
	
	
}
