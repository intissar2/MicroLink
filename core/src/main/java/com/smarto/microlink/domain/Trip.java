package com.smarto.microlink.domain;

import com.smarto.microlink.enumeration.ETripType;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "trip")
public class Trip {

    @Id
    private String id;
    private List<Point> points;
    private List<Parameter> parameters;
    private Point startTrip;
    private Point endTrip;
    private Date dateTrip;
    private String startTripAdress;
    private String endTripAdress;



    private long duration;



    private TripResult tripResult;
    private double averageSpeed;
    private double distance;
    private ETripType type;
    private String comment;
    private String client;

    @DBRef(lazy = true)
    private User user;

    @DBRef(lazy = true)
    private Vehicle vehicle;


    public Trip(String id, List<Point> points, List<Parameter> parameters, Point startTrip, Point endTrip, Date dateTrip, String startTripAdress, String endTripAdress, long duration, TripResult tripResult, double averageSpeed, double distance, ETripType type, String comment, String client, User user, Vehicle vehicle) {
        this.id = id;
        this.points = points;
        this.parameters = parameters;
        this.startTrip = startTrip;
        this.endTrip = endTrip;
        this.dateTrip = dateTrip;
        this.startTripAdress = startTripAdress;
        this.endTripAdress = endTripAdress;
        this.duration = duration;
        this.tripResult = tripResult;
        this.averageSpeed = averageSpeed;
        this.distance = distance;
        this.type = type;
        this.comment = comment;
        this.client = client;
        this.user = user;
        this.vehicle = vehicle;
    }


    public Trip(String id, List<Point> points, List<Parameter> parameters, Point startTrip, Point endTrip,
                long duration, double averageSpeed, User user, double distance, ETripType type) {
        super();
        this.id = id;
        this.points = points;
        this.parameters = parameters;
        this.startTrip = startTrip;
        this.endTrip = endTrip;
        this.duration = duration;
        this.averageSpeed = averageSpeed;
        this.user = user;
        this.distance = distance;
        this.type = type;
    }

    public Trip(String id, List<Point> points, List<Parameter> parameters, Point startTrip, Point endTrip,
                long duration, double averageSpeed, User user) {
        super();
        this.id = id;
        this.points = points;
        this.parameters = parameters;
        this.startTrip = startTrip;
        this.endTrip = endTrip;
        this.duration = duration;
        this.averageSpeed = averageSpeed;
        this.user = user;
    }

    public Trip(String id, List<Point> points, List<Parameter> parameters, Point startTrip, Point endTrip,
                long duration, double averageSpeed) {
        super();
        this.id = id;
        this.points = points;
        this.parameters = parameters;
        this.startTrip = startTrip;
        this.endTrip = endTrip;
        this.duration = duration;
        this.averageSpeed = averageSpeed;
    }

    public Trip(String id, List<Point> points, List<Parameter> parameters, Point startTrip, Point endTrip,
                long duration, double averageSpeed, User user, double distance, ETripType type, Vehicle vehicle) {
        super();
        this.id = id;
        this.points = points;
        this.parameters = parameters;
        this.startTrip = startTrip;
        this.endTrip = endTrip;
        this.duration = duration;
        this.averageSpeed = averageSpeed;
        this.user = user;
        this.vehicle = vehicle;
        this.distance = distance;
        this.type = type;
    }

    public Trip() {
        super();
        // TODO Auto-generated constructor stub
    }
    public Date getDateTrip() {
        return dateTrip;
    }

    public void setDateTrip(Date dateTrip) {
        this.dateTrip = dateTrip;
    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Point> getPoints() {
        return points;
    }

    public void setPoints(List<Point> points) {
        this.points = points;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public Point getStartTrip() {
        return startTrip;
    }

    public void setStartTrip(Point startTrip) {
        this.startTrip = startTrip;
    }

    public Point getEndTrip() {
        return endTrip;
    }

    public void setEndTrip(Point endTrip) {
        this.endTrip = endTrip;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long duration) {
        this.duration = duration;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public ETripType getType() {
        return type;
    }

    public void setType(ETripType type) {
        this.type = type;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public TripResult getTripResult() {
        return tripResult;
    }

    public void setTripResult(TripResult tripResult)
    {
        this.tripResult = tripResult;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getStartTripAdress() {
        return startTripAdress;
    }

    public void setStartTripAdress(String startTripAdress) {
        this.startTripAdress = startTripAdress;
    }

    public String getEndTripAdress() {
        return endTripAdress;
    }

    public void setEndTripAdress(String endTripAdress) {
        this.endTripAdress = endTripAdress;
    }
}