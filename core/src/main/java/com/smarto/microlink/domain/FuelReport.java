package com.smarto.microlink.domain;

public class FuelReport {

	String fuel;
    double fuelPrice;
    String station;
    String adress;
    double volume;
    double latitude;
    double longitude;
    String date;
    private String month;

	public FuelReport() {
		super();
	}

	public FuelReport(String fuel, double fuelPrice, String station, String adress, double volume, double latitude,
			double longitude, String date, String month) {
		super();
		this.fuel = fuel;
		this.fuelPrice = fuelPrice;
		this.station = station;
		this.adress = adress;
		this.volume = volume;
		this.latitude = latitude;
		this.longitude = longitude;
		this.date = date;
		this.month=month;
	}



	public String getFuel() {
		return fuel;
	}

	public void setFuel(String fuel) {
		this.fuel = fuel;
	}

	public double getFuelPrice() {
		return fuelPrice;
	}

	public void setFuelPrice(double fuelPrice) {
		this.fuelPrice = fuelPrice;
	}

	public String getStation() {
		return station;
	}

	public void setStation(String station) {
		this.station = station;
	}

	public String getAdress() {
		return adress;
	}

	public void setAdress(String adress) {
		this.adress = adress;
	}

	public double getVolume() {
		return volume;
	}

	public void setVolume(double volume) {
		this.volume = volume;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongitude() {
		return longitude;
	}

	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMonth() {
		return month;
	}

	public void setMonth(String month) {
		this.month = month;
	}

}
