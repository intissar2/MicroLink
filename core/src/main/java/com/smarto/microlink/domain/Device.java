package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "device")
public class Device {

    @Id
    String id;

    private String model;

    private String serialNumber; // @ID Semantic

    private String batchNumber;

    private String customer;

    private int year;

    private Date releaseDate;

    private String macAdress;

    private boolean isUsed;

    private String operatorName;

    private String supplier;
    
    private String hardRevision;

    public Device(String id, String model, String serialNumber, String batchNumber, String customer, int year,
                  Date releaseDate, String macAdress, String operatorName, String supplier, String hardRevision) {
        super();
        this.id = id;
        this.model = model;
        this.serialNumber = serialNumber;
        this.batchNumber = batchNumber;
        this.customer = customer;
        this.year = year;
        this.releaseDate = releaseDate;
        this.macAdress = macAdress;
        this.operatorName = operatorName;
        this.supplier = supplier;
        this.hardRevision=hardRevision;
    }

    public Device() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getBatchNumber() {
        return batchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        this.batchNumber = batchNumber;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public Date getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(Date releaseDate) {
        this.releaseDate = releaseDate;
    }

    public String getMacAdress() {
        return macAdress;
    }

    public void setMacAdress(String macAdress) {
        this.macAdress = macAdress;
    }

    public String getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(String operatorName) {
        this.operatorName = operatorName;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

    public boolean getIsUsed() {
        return isUsed;
    }

    public void setIsUsed(boolean isUsed) {
        this.isUsed = isUsed;
    }

	public String getHardRevision() {
		return hardRevision;
	}

	public void setHardRevision(String hardRevision) {
		this.hardRevision = hardRevision;
	}

}
