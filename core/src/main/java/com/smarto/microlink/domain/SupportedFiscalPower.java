package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "supportedFiscalPower")
public class SupportedFiscalPower {

    @Id
    private String id;
    private int min;
    private int max;

    public SupportedFiscalPower() {
        super();
        // TODO Auto-generated constructor stub
    }

    public SupportedFiscalPower(String id, int min, int max) {
        super();
        this.id = id;
        this.min = min;
        this.max = max;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getMin() {
        return min;
    }

    public void setMin(int min) {
        this.min = min;
    }

    public int getMax() {
        return max;
    }

    public void setMax(int max) {
        this.max = max;
    }

}