package com.smarto.microlink.domain;

public class TripResult {

    private double avgFuelCons;
    private double consFuelFromDep;
    private double avgCo2Emiss;
    private double emissCo2FromDep;

    private double emissFuelFromDep;
    private long numberStops;
    private double meanSpeed;
    private double maxSpeed;
    private double totalDistance;
    private long totalStopsTime;


    private double standardScore;
    private double effciencyScore;
    private double proScore;

    public TripResult(double avgFuelCons, double consFuelFromDep, double avgCo2Emiss, double emissCo2FromDep,
                      double emissFuelFromDep, long numberStops, double meanSpeed, double maxSpeed, double totalDistance,
                      long totalStopsTime, double standardScore, double effciencyScore, double proScore) {
        super();
        this.avgFuelCons = avgFuelCons;
        this.consFuelFromDep = consFuelFromDep;
        this.avgCo2Emiss = avgCo2Emiss;
        this.emissCo2FromDep = emissCo2FromDep;
        this.emissFuelFromDep = emissFuelFromDep;
        this.numberStops = numberStops;
        this.meanSpeed = meanSpeed;
        this.maxSpeed = maxSpeed;
        this.totalDistance = totalDistance;
        this.totalStopsTime = totalStopsTime;
        this.standardScore = standardScore;
        this.effciencyScore = effciencyScore;
        this.proScore = proScore;
    }

    public TripResult() {

    }

    public double getAvgFuelCons() {
        return avgFuelCons;
    }

    public void setAvgFuelCons(double avgFuelCons) {
        this.avgFuelCons = avgFuelCons;
    }

    public double getConsFuelFromDep() {
        return consFuelFromDep;
    }

    public void setConsFuelFromDep(double consFuelFromDep) {
        this.consFuelFromDep = consFuelFromDep;
    }

    public double getAvgCo2Emiss() {
        return avgCo2Emiss;
    }

    public void setAvgCo2Emiss(double avgCo2Emiss) {
        this.avgCo2Emiss = avgCo2Emiss;
    }

    public double getEmissCo2FromDep() {
        return emissCo2FromDep;
    }

    public void setEmissCo2FromDep(double emissCo2FromDep) {
        this.emissCo2FromDep = emissCo2FromDep;
    }

    public double getEmissFuelFromDep() {
        return emissFuelFromDep;
    }

    public void setEmissFuelFromDep(double emissFuelFromDep) {
        this.emissFuelFromDep = emissFuelFromDep;
    }

    public long getNumberStops() {
        return numberStops;
    }

    public void setNumberStops(long numberStops) {
        this.numberStops = numberStops;
    }

    public double getMeanSpeed() {
        return meanSpeed;
    }

    public void setMeanSpeed(double meanSpeed) {
        this.meanSpeed = meanSpeed;
    }

    public double getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(double maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    public long getTotalStopsTime() {
        return totalStopsTime;
    }

    public void setTotalStopsTime(long totalStopsTime) {
        this.totalStopsTime = totalStopsTime;
    }

    public double getStandardScore() {
        return standardScore;
    }

    public void setStandardScore(double standardScore) {
        this.standardScore = standardScore;
    }

    public double getEffciencyScore() {
        return effciencyScore;
    }

    public void setEffciencyScore(double effciencyScore) {
        this.effciencyScore = effciencyScore;
    }

    public double getProScore() {
        return proScore;
    }

    public void setProScore(double proScore) {
        this.proScore = proScore;
    }

}
