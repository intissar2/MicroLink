package com.smarto.microlink.domain;

public class Attribute {

    private String key;
    private String nom;
    private String type;
    private String value;

    public Attribute(String key, String nom, String type, String value) {
        super();
        this.key = key;
        this.nom = nom;
        this.type = type;
        this.value = value;
    }

    public Attribute() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

}
