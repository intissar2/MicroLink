package com.smarto.microlink.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

public class Message {

    private long id;

    private String subject;

    private String text;

    @DBRef(lazy = true)
    private User user;
    private  Entreprise entreprise;

    public Message() {
    }

    public Message(long id, String subject, String text, User user) {
        super();
        this.id = id;
        this.subject = subject;
        this.text = text;
        this.user = user;
    }

    public Message(long id, String subject, String text, Entreprise entreprise , User user) {
        super();
        this.id = id;
        this.subject = subject;
        this.text = text;
        this.entreprise = entreprise;
        this.user = user;
    }

    public Message(long id, String subject, String text) {
        this.id = id;
        this.subject = subject;
        this.text = text;
    }


    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public String toString() {
        return "Id:[" + this.getId() + "] Subject:[" + this.getSubject() + "] Text:[" + this.getText() + "]";
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }
}