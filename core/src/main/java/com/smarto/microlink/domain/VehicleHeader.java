package com.smarto.microlink.domain;

public class VehicleHeader {

    private String id;
    private String make;
    private String model;
    private String engine;
    private String am;
    private String fuel;
    private String puissance;

    public VehicleHeader() {
        super();
        // TODO Auto-generated constructor stub
    }

    public VehicleHeader(String id, String make, String model, String engine, String am, String fuel,
                         String puissance) {
        super();
        this.id = id;
        this.make = make;
        this.model = model;
        this.engine = engine;
        this.am = am;
        this.fuel = fuel;
        this.puissance = puissance;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getAm() {
        return am;
    }

    public void setAm(String am) {
        this.am = am;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getPuissance() {
        return puissance;
    }

    public void setPuissance(String puissance) {
        this.puissance = puissance;
    }

}
