package com.smarto.microlink.domain;

public class Detail {

    private String definition;
    private String symptom;
    private String cause;
    private String solution;

    public Detail(String definition, String symptom, String cause, String solution) {
        super();
        this.definition = definition;
        this.symptom = symptom;
        this.cause = cause;
        this.solution = solution;
    }

    public Detail() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getDefinition() {
        return definition;
    }

    public void setDefinition(String definition) {
        this.definition = definition;
    }

    public String getSymptom() {
        return symptom;
    }

    public void setSymptom(String symptom) {
        this.symptom = symptom;
    }

    public String getCause() {
        return cause;
    }

    public void setCause(String cause) {
        this.cause = cause;
    }

    public String getSolution() {
        return solution;
    }

    public void setSolution(String solution) {
        this.solution = solution;
    }


}
