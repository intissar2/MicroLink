package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "vehicleDetail")
public class VehicleModelDetails {

    @Id
    private String model_id;
    private String make;
    private String model;
    private String model_0_to_100_kph;
    private String model_body;
    private String model_co2;
    private String model_doors;
    private String model_drive;
    private String model_trim;
    private String model_year;
    private String model_engine_bore_mm;
    private String model_engine_cc;
    private String model_engine_compression;
    private String model_engine_cyl;
    private String model_engine_fuel;
    private String model_engine_position;
    private String model_engine_power_ps;
    private String model_engine_power_rpm;
    private String model_engine_stroke_mm;
    private String model_engine_torque_nm;
    private String model_engine_torque_rpm;
    private String model_engine_type;
    private String model_engine_valves_per_cyl;
    private String fuel_cap_l;
    private String height_mm;
    private String model_length_mm;
    private String model_lkm_city;
    private String model_lkm_hwy;
    private String model_lkm_mixed;
    private String model_make_display;
    private String model_seats;
    private String model_sold_in_us;
    private String model_top_speed_kph;
    private String model_transmission_type;
    private String model_weight_kg;
    private String model_wheelbase_mm;
    private String model_width_mm;
    private String puissance;
    private String protocol;
    private String mode1;
    private String mode2;
    private String mode5;
    private String mode6;
    private String mode7;
    private String mode9;

    public VehicleModelDetails() {
        super();
        // TODO Auto-generated constructor stub
    }

    public VehicleModelDetails(String model_id, String make, String model, String model_0_to_100_kph, String model_body,
                               String model_co2, String model_doors, String model_drive, String model_trim, String model_year,
                               String model_engine_bore_mm, String model_engine_cc, String model_engine_compression,
                               String model_engine_cyl, String model_engine_fuel, String model_engine_position,
                               String model_engine_power_ps, String model_engine_power_rpm, String model_engine_stroke_mm,
                               String model_engine_torque_nm, String model_engine_torque_rpm, String model_engine_type,
                               String model_engine_valves_per_cyl, String fuel_cap_l, String height_mm, String model_length_mm,
                               String model_lkm_city, String model_lkm_hwy, String model_lkm_mixed, String model_make_display,
                               String model_seats, String model_sold_in_us, String model_top_speed_kph, String model_transmission_type,
                               String model_weight_kg, String model_wheelbase_mm, String model_width_mm, String puissance, String protocol,
                               String mode1, String mode2, String mode5, String mode6, String mode7, String mode9) {
        super();
        this.model_id = model_id;
        this.make = make;
        this.model = model;
        this.model_0_to_100_kph = model_0_to_100_kph;
        this.model_body = model_body;
        this.model_co2 = model_co2;
        this.model_doors = model_doors;
        this.model_drive = model_drive;
        this.model_trim = model_trim;
        this.model_year = model_year;
        this.model_engine_bore_mm = model_engine_bore_mm;
        this.model_engine_cc = model_engine_cc;
        this.model_engine_compression = model_engine_compression;
        this.model_engine_cyl = model_engine_cyl;
        this.model_engine_fuel = model_engine_fuel;
        this.model_engine_position = model_engine_position;
        this.model_engine_power_ps = model_engine_power_ps;
        this.model_engine_power_rpm = model_engine_power_rpm;
        this.model_engine_stroke_mm = model_engine_stroke_mm;
        this.model_engine_torque_nm = model_engine_torque_nm;
        this.model_engine_torque_rpm = model_engine_torque_rpm;
        this.model_engine_type = model_engine_type;
        this.model_engine_valves_per_cyl = model_engine_valves_per_cyl;
        this.fuel_cap_l = fuel_cap_l;
        this.height_mm = height_mm;
        this.model_length_mm = model_length_mm;
        this.model_lkm_city = model_lkm_city;
        this.model_lkm_hwy = model_lkm_hwy;
        this.model_lkm_mixed = model_lkm_mixed;
        this.model_make_display = model_make_display;
        this.model_seats = model_seats;
        this.model_sold_in_us = model_sold_in_us;
        this.model_top_speed_kph = model_top_speed_kph;
        this.model_transmission_type = model_transmission_type;
        this.model_weight_kg = model_weight_kg;
        this.model_wheelbase_mm = model_wheelbase_mm;
        this.model_width_mm = model_width_mm;
        this.puissance = puissance;
        this.protocol = protocol;
        this.mode1 = mode1;
        this.mode2 = mode2;
        this.mode5 = mode5;
        this.mode6 = mode6;
        this.mode7 = mode7;
        this.mode9 = mode9;
    }

    public String getModel_id() {
        return model_id;
    }

    public void setModel_id(String model_id) {
        this.model_id = model_id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getModel_0_to_100_kph() {
        return model_0_to_100_kph;
    }

    public void setModel_0_to_100_kph(String model_0_to_100_kph) {
        this.model_0_to_100_kph = model_0_to_100_kph;
    }

    public String getModel_body() {
        return model_body;
    }

    public void setModel_body(String model_body) {
        this.model_body = model_body;
    }

    public String getModel_co2() {
        return model_co2;
    }

    public void setModel_co2(String model_co2) {
        this.model_co2 = model_co2;
    }

    public String getModel_doors() {
        return model_doors;
    }

    public void setModel_doors(String model_doors) {
        this.model_doors = model_doors;
    }

    public String getModel_drive() {
        return model_drive;
    }

    public void setModel_drive(String model_drive) {
        this.model_drive = model_drive;
    }

    public String getModel_trim() {
        return model_trim;
    }

    public void setModel_trim(String model_trim) {
        this.model_trim = model_trim;
    }

    public String getModel_year() {
        return model_year;
    }

    public void setModel_year(String model_year) {
        this.model_year = model_year;
    }

    public String getModel_engine_bore_mm() {
        return model_engine_bore_mm;
    }

    public void setModel_engine_bore_mm(String model_engine_bore_mm) {
        this.model_engine_bore_mm = model_engine_bore_mm;
    }

    public String getModel_engine_cc() {
        return model_engine_cc;
    }

    public void setModel_engine_cc(String model_engine_cc) {
        this.model_engine_cc = model_engine_cc;
    }

    public String getModel_engine_compression() {
        return model_engine_compression;
    }

    public void setModel_engine_compression(String model_engine_compression) {
        this.model_engine_compression = model_engine_compression;
    }

    public String getModel_engine_cyl() {
        return model_engine_cyl;
    }

    public void setModel_engine_cyl(String model_engine_cyl) {
        this.model_engine_cyl = model_engine_cyl;
    }

    public String getModel_engine_fuel() {
        return model_engine_fuel;
    }

    public void setModel_engine_fuel(String model_engine_fuel) {
        this.model_engine_fuel = model_engine_fuel;
    }

    public String getModel_engine_position() {
        return model_engine_position;
    }

    public void setModel_engine_position(String model_engine_position) {
        this.model_engine_position = model_engine_position;
    }

    public String getModel_engine_power_ps() {
        return model_engine_power_ps;
    }

    public void setModel_engine_power_ps(String model_engine_power_ps) {
        this.model_engine_power_ps = model_engine_power_ps;
    }

    public String getModel_engine_power_rpm() {
        return model_engine_power_rpm;
    }

    public void setModel_engine_power_rpm(String model_engine_power_rpm) {
        this.model_engine_power_rpm = model_engine_power_rpm;
    }

    public String getModel_engine_stroke_mm() {
        return model_engine_stroke_mm;
    }

    public void setModel_engine_stroke_mm(String model_engine_stroke_mm) {
        this.model_engine_stroke_mm = model_engine_stroke_mm;
    }

    public String getModel_engine_torque_nm() {
        return model_engine_torque_nm;
    }

    public void setModel_engine_torque_nm(String model_engine_torque_nm) {
        this.model_engine_torque_nm = model_engine_torque_nm;
    }

    public String getModel_engine_torque_rpm() {
        return model_engine_torque_rpm;
    }

    public void setModel_engine_torque_rpm(String model_engine_torque_rpm) {
        this.model_engine_torque_rpm = model_engine_torque_rpm;
    }

    public String getModel_engine_type() {
        return model_engine_type;
    }

    public void setModel_engine_type(String model_engine_type) {
        this.model_engine_type = model_engine_type;
    }

    public String getModel_engine_valves_per_cyl() {
        return model_engine_valves_per_cyl;
    }

    public void setModel_engine_valves_per_cyl(String model_engine_valves_per_cyl) {
        this.model_engine_valves_per_cyl = model_engine_valves_per_cyl;
    }

    public String getFuel_cap_l() {
        return fuel_cap_l;
    }

    public void setFuel_cap_l(String fuel_cap_l) {
        this.fuel_cap_l = fuel_cap_l;
    }

    public String getHeight_mm() {
        return height_mm;
    }

    public void setHeight_mm(String height_mm) {
        this.height_mm = height_mm;
    }

    public String getModel_length_mm() {
        return model_length_mm;
    }

    public void setModel_length_mm(String model_length_mm) {
        this.model_length_mm = model_length_mm;
    }

    public String getModel_lkm_city() {
        return model_lkm_city;
    }

    public void setModel_lkm_city(String model_lkm_city) {
        this.model_lkm_city = model_lkm_city;
    }

    public String getModel_lkm_hwy() {
        return model_lkm_hwy;
    }

    public void setModel_lkm_hwy(String model_lkm_hwy) {
        this.model_lkm_hwy = model_lkm_hwy;
    }

    public String getModel_lkm_mixed() {
        return model_lkm_mixed;
    }

    public void setModel_lkm_mixed(String model_lkm_mixed) {
        this.model_lkm_mixed = model_lkm_mixed;
    }

    public String getModel_make_display() {
        return model_make_display;
    }

    public void setModel_make_display(String model_make_display) {
        this.model_make_display = model_make_display;
    }

    public String getModel_seats() {
        return model_seats;
    }

    public void setModel_seats(String model_seats) {
        this.model_seats = model_seats;
    }

    public String getModel_sold_in_us() {
        return model_sold_in_us;
    }

    public void setModel_sold_in_us(String model_sold_in_us) {
        this.model_sold_in_us = model_sold_in_us;
    }

    public String getModel_top_speed_kph() {
        return model_top_speed_kph;
    }

    public void setModel_top_speed_kph(String model_top_speed_kph) {
        this.model_top_speed_kph = model_top_speed_kph;
    }

    public String getModel_transmission_type() {
        return model_transmission_type;
    }

    public void setModel_transmission_type(String model_transmission_type) {
        this.model_transmission_type = model_transmission_type;
    }

    public String getModel_weight_kg() {
        return model_weight_kg;
    }

    public void setModel_weight_kg(String model_weight_kg) {
        this.model_weight_kg = model_weight_kg;
    }

    public String getModel_wheelbase_mm() {
        return model_wheelbase_mm;
    }

    public void setModel_wheelbase_mm(String model_wheelbase_mm) {
        this.model_wheelbase_mm = model_wheelbase_mm;
    }

    public String getModel_width_mm() {
        return model_width_mm;
    }

    public void setModel_width_mm(String model_width_mm) {
        this.model_width_mm = model_width_mm;
    }

    public String getPuissance() {
        return puissance;
    }

    public void setPuissance(String puissance) {
        this.puissance = puissance;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getMode1() {
        return mode1;
    }

    public void setMode1(String mode1) {
        this.mode1 = mode1;
    }

    public String getMode2() {
        return mode2;
    }

    public void setMode2(String mode2) {
        this.mode2 = mode2;
    }

    public String getMode5() {
        return mode5;
    }

    public void setMode5(String mode5) {
        this.mode5 = mode5;
    }

    public String getMode6() {
        return mode6;
    }

    public void setMode6(String mode6) {
        this.mode6 = mode6;
    }

    public String getMode7() {
        return mode7;
    }

    public void setMode7(String mode7) {
        this.mode7 = mode7;
    }

    public String getMode9() {
        return mode9;
    }

    public void setMode9(String mode9) {
        this.mode9 = mode9;
    }

}
