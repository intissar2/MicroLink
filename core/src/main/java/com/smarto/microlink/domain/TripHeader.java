package com.smarto.microlink.domain;

import com.smarto.microlink.enumeration.ETripType;
import org.springframework.data.mongodb.core.mapping.DBRef;

import java.util.Date;

public class TripHeader {

    private String id;
    private String startTripAdress;
    private String endTripAdress;
    private Date dateTrip;
    private ETripType type;
    private long duration;
    private double averageSpeed;
    private TripResult tripResult;

    @DBRef(lazy = true)
    private User user;

    public TripHeader() {
        super();
        // TODO Auto-generated constructor stub
    }

//	public TripHeader(String id, Point startTrip, Point endTrip, long duration, double averageSpeed) {
//		super();
//		this.id = id;
//		this.startTrip = startTrip;
//		this.endTrip = endTrip;
//		this.duration = duration;
//		this.averageSpeed = averageSpeed;
//	}

    public TripHeader(String id, String startTripAdress, String endTripAdress, long duration, double averageSpeed, User user, TripResult tripResult,Date dateTrip,ETripType type) {
        super();
        this.id = id;
        this.startTripAdress = startTripAdress;
        this.endTripAdress = endTripAdress;
        this.duration = duration;
        this.averageSpeed = averageSpeed;
        this.user = user;
        this.tripResult=tripResult;
        this.dateTrip=dateTrip;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStartTripAdress() {
        return startTripAdress;
    }

    public void setStartTripAdress(String startTripAdress) {
        this.startTripAdress = startTripAdress;
    }

    public String getEndTripAdress() {
        return endTripAdress;
    }

    public void setEndTripAdress(String endTripAdress) {
        this.endTripAdress = endTripAdress;
    }

    public long getDuration() {
        return duration;
    }

    public void setDuration(long l) {
        this.duration = l;
    }

    public double getAverageSpeed() {
        return averageSpeed;
    }

    public void setAverageSpeed(double averageSpeed) {
        this.averageSpeed = averageSpeed;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public TripResult getTripResult() {
        return tripResult;
    }

    public void setTripResult(TripResult tripResult) {
        this.tripResult = tripResult;
    }

    public Date getDateTrip() {
        return dateTrip;
    }

    public void setDateTrip(Date dateTrip) {
        this.dateTrip = dateTrip;
    }

    public ETripType getType() {
        return type;
    }

    public void setType(ETripType type) {
        this.type = type;
    }

}