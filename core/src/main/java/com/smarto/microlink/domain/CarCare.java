package com.smarto.microlink.domain;

import java.util.Date;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "carCare")
public class CarCare {

	@Id
	private String id;
	String type;
	String option;
	double distance;
	String company;
	double price;
	Date date;
	boolean toDo;

	@DBRef(lazy = true)
	private User user;

	@DBRef(lazy = true)
	private Vehicle vehicle;

	public CarCare() {
		super();
	}

	
	public CarCare(String id, String type, String option, double distance, String company, double price, Date date,
			boolean toDo, User user, Vehicle vehicle) {
		super();
		this.id = id;
		this.type = type;
		this.option = option;
		this.distance = distance;
		this.company = company;
		this.price = price;
		this.date = date;
		this.toDo = toDo;
		this.user = user;
		this.vehicle = vehicle;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Vehicle getVehicle() {
		return vehicle;
	}

	public void setVehicle(Vehicle vehicle) {
		this.vehicle = vehicle;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public double getDistance() {
		return distance;
	}

	public void setDistance(double distance) {
		this.distance = distance;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public boolean isToDo() {
		return toDo;
	}

	public void setToDo(boolean toDo) {
		this.toDo = toDo;
	}
	
}
