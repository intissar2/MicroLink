/*
 * Copyright (c) 2018.
  * @ author intissar CHOUAIEB
 */

package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "entreprise")
public class Entreprise {

    @Id
    String id;

    private String name;

    private String logo;

    private String num;

    private String street;

    private String town;

    private String zipCode;

    public Entreprise() {
        super();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

   public Entreprise(String id, String name, String logo, String num, String street, String town, String zipCode) {
        this.id = id;
        this.name = name;
        this.logo = logo;
        this.num = num;
        this.street = street;
        this.town = town;
        this.zipCode = zipCode;
    }

    public Entreprise(String name, String logo, String num, String street, String town, String zipCode) {
        this.name = name;
        this.logo = logo;
        this.num = num;
        this.street = street;
        this.town = town;
        this.zipCode = zipCode;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getNum() {
        return num;
    }

    public void setNum(String num) {
        this.num = num;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getTown() {
        return town;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }
}