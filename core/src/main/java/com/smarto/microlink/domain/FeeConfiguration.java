/*
 * Copyright (c) 2017.
  * @ author Aymen BOUHASTINE
 */

package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "feeConfiguration")
public class FeeConfiguration {

    @Id
    private String id;

    @DBRef(lazy = true)
    private Entreprise entreprise;

    private List<FeeByDistance> cFees;

    public FeeConfiguration() {
        super();
    }

    public FeeConfiguration(String id, Entreprise entreprise, List<FeeByDistance> cFees) {
        this.id = id;
        this.entreprise = entreprise;
        this.cFees = cFees;
    }

    public FeeConfiguration(Entreprise entreprise, List<FeeByDistance> cFees) {
        this.entreprise = entreprise;
        this.cFees = cFees;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<FeeByDistance> getcFees() {
        return cFees;
    }

    public void setcFees(List<FeeByDistance> cFees) {
        this.cFees = cFees;
    }
}