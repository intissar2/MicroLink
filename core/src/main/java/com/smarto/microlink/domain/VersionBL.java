package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "versionbl")
public class VersionBL {

	@Id
	private String id;
	String version;
    Date date;

	public VersionBL(String id, String version, Date date) {
		this.id = id;
		this.version = version;
		this.date = date;
	}

	public VersionBL(String version, Date date) {
		this.version = version;
		this.date = date;
	}

	public VersionBL() {
		super();
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Date getDate() {
		return date;
	}

	public void setDate(Date date) {
		this.date = date;
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}
}
