package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;

public class TroubleCodeHeader {

    @Id
    private String id;
    private String code;
    private String description;
    private String manufactor;

    public TroubleCodeHeader(String id, String code, String description, String manufactor) {
        super();
        this.id = id;
        this.code = code;
        this.description = description;
        this.manufactor = manufactor;
    }

    public TroubleCodeHeader() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getManufactor() {
        return manufactor;
    }

    public void setManufactor(String manufactor) {
        this.manufactor = manufactor;
    }

}
