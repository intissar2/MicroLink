package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "firmware")
public class Firmware {

    @Id
    String id;

    List<Attribute> dynamicFields; // dynamic fields

    private String version;

    private String type; // BLE ou CAN

    private Date creationDate;

    private byte[] fileBytes;

    private String checksum;

    public Firmware(String id, List<Attribute> dynamicFields, String version, String type, Date creationDate,
                    byte[] fileBytes, String checksum) {
        super();
        this.id = id;
        this.dynamicFields = dynamicFields;
        this.version = version;
        this.type = type;
        this.creationDate = creationDate;
        this.fileBytes = fileBytes;
        this.checksum = checksum;
    }

    public Firmware(String id, String version, String type, Date creationDate, byte[] fileBytes, String checksum) {
        super();
        this.id = id;
        this.version = version;
        this.type = type;
        this.creationDate = creationDate;
        this.fileBytes = fileBytes;
        this.checksum = checksum;
    }

    public Firmware() {
        super();
        // TODO Auto-generated constructor stub
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public List<Attribute> getDynamicFields() {
        return dynamicFields;
    }

    public void setDynamicFields(List<Attribute> dynamicFields) {
        this.dynamicFields = dynamicFields;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public byte[] getFileBytes() {
        return fileBytes;
    }

    public void setFileBytes(byte[] fileBytes) {
        this.fileBytes = fileBytes;
    }

    public String getChecksum() {
        return checksum;
    }

    public void setChecksum(String checksum) {
        this.checksum = checksum;
    }

    @Override
    public String toString() {
        String basicDescription = "Firmware Type : " + type + "creationDate: " + creationDate + ", version: " + version;
        /*
		String dynamicDescription = null;
		for (Attribute tmp : dynamicFields){
			dynamicDescription = dynamicDescription + tmp.getNom() + " " + tmp.getValue(); 
		}*/

        return basicDescription /*+ dynamicDescription*/;
    }
}
