/*
 * Copyright (c) 2017.
  * @ author Aymen BOUHASTINE
 */

package com.smarto.microlink.domain;

import java.util.Date;

public class TripHeaderFee {

    private String startTripAdress;
    private String endTripAdress;
    private String distance;
    private Double distanceD;
    private String client;
    private String date;
    private String week;
    private String fee;

    public TripHeaderFee(String startTripAdress, String endTripAdress, String distance, String client, String date, String week, String fee) {
        this.startTripAdress = startTripAdress;
        this.endTripAdress = endTripAdress;
        this.client = client;
        this.distance = distance;
        this.date = date;
        this.week = week;
        this.fee = fee;
    }

    public String getStartTripAdress() {
        return startTripAdress;
    }

    public void setStartTripAdress(String startTripAdress) {
        this.startTripAdress = startTripAdress;
    }

    public String getEndTripAdress() {
        return endTripAdress;
    }

    public void setEndTripAdress(String endTripAdress) {
        this.endTripAdress = endTripAdress;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getWeek() {
        return week;
    }

    public void setWeek(String week) {
        this.week = week;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public Double getDistanceD() {
        return distanceD;
    }

    public void setDistanceD(Double distanceD) {
        this.distanceD = distanceD;
    }

}

