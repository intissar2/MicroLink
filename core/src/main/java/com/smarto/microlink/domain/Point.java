package com.smarto.microlink.domain;

import java.util.Date;

public class Point {

    private double lat;
    private double lon;
    private Date date;
    private boolean leaving = false;
    private boolean arriving = false;

    public Point() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Point(double lat, double lon, Date date, boolean leaving,
                 boolean arriving) {
        super();
        this.lat = lat;
        this.lon = lon;
        this.date = date;
        this.leaving = leaving;
        this.arriving = arriving;
    }

    public Point(Date date) {
        super();
        this.date = date;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLon() {
        return lon;
    }

    public void setLon(double lon) {
        this.lon = lon;
    }

    public boolean isLeaving() {
        return leaving;
    }

    public void setLeaving(boolean leaving) {
        this.leaving = leaving;
    }

    public boolean isArriving() {
        return arriving;
    }

    public void setArriving(boolean arriving) {
        this.arriving = arriving;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
