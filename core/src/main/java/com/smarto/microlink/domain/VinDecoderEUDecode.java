package com.smarto.microlink.domain;

/**
 * Created by bassem smida on 23/11/2016.
 */
public class VinDecoderEUDecode {
    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    private String label;
    private String value;
}