package com.smarto.microlink.domain;

import com.smarto.microlink.enumeration.EDungleLocation;
import com.smarto.microlink.utils.EVehicleSource;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "vehicle")
public class Vehicle {

    @Id
    private String id;
    private String make;
    private String model;
    private String engine;
    private String am;
    private String fuel;
    private String puissance;
    private String protocol;
    private String mode1;
    private String mode2;
    private String mode5;
    private String mode6;
    private String mode8;
    private String mode9;
    private EVehicleSource source;
    private EDungleLocation dLocation;
    private String vin;
    private String suportedProtocol;
    private double co2Emission;

    public EDungleLocation getdLocation() {
        return dLocation;
    }

    public void setdLocation(EDungleLocation dLocation) {
        this.dLocation = dLocation;
    }

    public EVehicleSource getSource() {
        if (source == null)
            return EVehicleSource.DEFAULT;
        return source;
    }

    public void setSource(EVehicleSource source) {
        this.source = source;
    }

    public Vehicle() {
        super();
        // TODO Auto-generated constructor stub
    }

    public Vehicle(String id, String make, String model, String engine, String am, String fuel, String puissance,
                   String protocol, String mode1, String mode2, String mode5, String mode6, String mode8, String mode9,
                   EVehicleSource source, String vin, String suportedProtocol, double co2Emission) {
        super();
        this.id = id;
        this.make = make;
        this.model = model;
        this.engine = engine;
        this.am = am;
        this.fuel = fuel;
        this.puissance = puissance;
        this.protocol = protocol;
        this.mode1 = mode1;
        this.mode2 = mode2;
        this.mode5 = mode5;
        this.mode6 = mode6;
        this.mode8 = mode8;
        this.mode9 = mode9;
        this.source = source;
        this.vin = vin;
        this.suportedProtocol = suportedProtocol;
        this.co2Emission = co2Emission;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    public String getAm() {
        return am;
    }

    public void setAm(String am) {
        this.am = am;
    }

    public String getFuel() {
        return fuel;
    }

    public void setFuel(String fuel) {
        this.fuel = fuel;
    }

    public String getPuissance() {
        return puissance;
    }

    public void setPuissance(String puissance) {
        this.puissance = puissance;
    }

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public String getMode1() {
        return mode1;
    }

    public void setMode1(String mode1) {
        this.mode1 = mode1;
    }

    public String getMode2() {
        return mode2;
    }

    public void setMode2(String mode2) {
        this.mode2 = mode2;
    }

    public String getMode5() {
        return mode5;
    }

    public void setMode5(String mode5) {
        this.mode5 = mode5;
    }

    public String getMode6() {
        return mode6;
    }

    public void setMode6(String mode6) {
        this.mode6 = mode6;
    }

    public String getMode8() {
        return mode8;
    }

    public void setMode8(String mode8) {
        this.mode8 = mode8;
    }

    public String getMode9() {
        return mode9;
    }

    public void setMode9(String mode9) {
        this.mode9 = mode9;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getSuportedProtocol() {
        return suportedProtocol;
    }

    public void setSuportedProtocol(String suportedProtocol) {
        this.suportedProtocol = suportedProtocol;
    }

    public double getCo2Emission() {
        return co2Emission;
    }

    public void setCo2Emission(double co2Emission) {
        this.co2Emission = co2Emission;
    }

}
