package com.smarto.microlink.domain;

public class Score {

	private double standardScore;
	private double efficiencyScore;
	private double proScore;
	private double generalScore;
	
	
	public Score(double standardScore, double efficiencyScore, double proScore, double generalScore) {
		super();
		this.standardScore = standardScore;
		this.efficiencyScore = efficiencyScore;
		this.proScore = proScore;
		this.generalScore = generalScore;
	}
	public double getStandardScore() {
		return standardScore;
	}
	public void setStandardScore(double standardScore) {
		this.standardScore = standardScore;
	}
	public double getEfficiencyScore() {
		return efficiencyScore;
	}
	public void setEfficiencyScore(double efficiencyScore) {
		this.efficiencyScore = efficiencyScore;
	}
	public double getProScore() {
		return proScore;
	}
	public void setProScore(double proScore) {
		this.proScore = proScore;
	}
	public double getGeneralScore() {
		return generalScore;
	}
	public void setGeneralScore(double generalScore) {
		this.generalScore = generalScore;
	}
	public Score (){
		
	}

}
