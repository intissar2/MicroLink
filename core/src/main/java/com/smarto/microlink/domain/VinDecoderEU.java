package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;

import java.util.List;

/**
 * Created by bassem smida on 23/11/2016.
 */
public class VinDecoderEU {
    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Integer getPrice() {
        return price;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public String getPrice_currency() {
        return price_currency;
    }

    public void setPrice_currency(String price_currency) {
        this.price_currency = price_currency;
    }

    public VinDecoderEUBalance getBalance() {
        return balance;
    }

    public void setBalance(VinDecoderEUBalance balance) {
        this.balance = balance;
    }

    public List<VinDecoderEUDecode> getDecode() {
        return decode;
    }

    public void setDecode(List<VinDecoderEUDecode> decode) {
        this.decode = decode;
    }

    private Integer price;
    private String price_currency;
    private VinDecoderEUBalance balance;
    private List<VinDecoderEUDecode> decode;
    @Id
    private String vin;
}
