package com.smarto.microlink.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

public class DeviceMessage {

    private long id;

    private String subject;

    private String text;

    @DBRef(lazy = true)
    private Device device;

    public DeviceMessage() {
    }

    public DeviceMessage(long id, String subject, String text, Device device) {
        super();
        this.id = id;
        this.subject = subject;
        this.text = text;
        this.device = device;
    }

    public DeviceMessage(long id, String subject, String text) {
        this.id = id;
        this.subject = subject;
        this.text = text;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public String toString() {
        return "Id:[" + this.getId() + "] Subject:[" + this.getSubject() + "] Text:[" + this.getText() + "]";
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }


}