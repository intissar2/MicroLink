package com.smarto.microlink.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.List;

@JsonIgnoreProperties("mdp")
@Document(collection = "user")
public class User implements Serializable, Comparable<User>{

    @Id
    private String id;
    private String lastName;
    private String firstName;
    private String mail;
    private String mdp;
    private String role;
    private double totalDistance;
    private List<UVehicleDetail> uVehicleDetails;
    private List<UDevice> uDevices;
    private Trip lastTrip;

    @DBRef(lazy = true)
    private Entreprise entreprise;

    // private boolean activated = false;
    // private String activationKey;

    // //@DateTimeFormat(pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSZ")
    // private Date lastLogin;

    public User() {
        super();
        // TODO Auto-generated constructor stub
    }

    // public User(String id, String lastName, String firstName, String mail,
    // String mdp, String role, boolean activated,
    // String activationKey, Date lastLogin) {
    // super();
    // this.id = id;
    // this.lastName = lastName;
    // this.firstName = firstName;
    // this.mail = mail;
    // this.mdp = mdp;
    // this.role = role;
    // this.activated = activated;
    // this.activationKey = activationKey;
    // this.lastLogin = lastLogin;
    // }

    public User(String id, String lastName, String firstName, String mail, String mdp, String role) {
        super();
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.mail = mail;
        this.mdp = mdp;
        this.role = role;
    }

    public List<UVehicleDetail> getUVehicleDetail() {
        return uVehicleDetails;
    }

    public void setUVehicleDetail(List<UVehicleDetail> uVehicleDetails) {
        this.uVehicleDetails = uVehicleDetails;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getMdp() {
        return mdp;
    }

    public void setMdp(String mdp) {
        this.mdp = mdp;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public List<UDevice> getUDevices() {
        return uDevices;
    }

    public void setUDevices(List<UDevice> uDevices) {
        this.uDevices = uDevices;
    }

    public Entreprise getEntreprise() {
        return entreprise;
    }

    public void setEntreprise(Entreprise entreprise) {
        this.entreprise = entreprise;
    }

    // public boolean isActivated() {
    // return activated;
    // }
    //
    // public void setActivated(boolean activated) {
    // this.activated = activated;
    // }
    //
    // public String getActivationKey() {
    // return activationKey;
    // }
    //
    // public void setActivationKey(String activationKey) {
    // this.activationKey = activationKey;
    // }
    //
    // public Date getLastLogin() {
    // return lastLogin;
    // }
    //
    // public void setLastLogin(Date lastLogin) {
    // this.lastLogin = lastLogin;
    // }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", lastName='" + lastName + '\'' +
                ", firstName='" + firstName + '\'' +
                ", mail='" + mail + '\'' +
                ", mdp='" + mdp + '\'' +
                ", role='" + role + '\'' +
                ", uVehicleDetails=" + uVehicleDetails +
                ", uDevices=" + uDevices +
                ", entreprise=" + entreprise +
                '}';
    }

	public Trip getLastTrip() {
		return lastTrip;
	}

	public void setLastTrip(Trip lastTrip) {
		this.lastTrip = lastTrip;
	}

    public double getTotalDistance() {
        return totalDistance;
    }

    public void setTotalDistance(double totalDistance) {
        this.totalDistance = totalDistance;
    }

    @Override
    public int compareTo(User o) {
        if(o.getLastTrip()!= null) {
            return 1;
        }else{
            return  -1;
        }
    }
}