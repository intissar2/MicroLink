package com.smarto.microlink.domain;

import org.springframework.data.mongodb.core.mapping.DBRef;

/**
 * Created by DELL on 29/12/2016.
 */
public class UVehicleDetail {
    private String vin;
    private String registrationNumber;
    @DBRef(lazy = true)
    private Vehicle vehicle;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public String getRegistrationNumber() {
        return registrationNumber;
    }

    public void setRegistrationNumber(String registrationNumber) {
        this.registrationNumber = registrationNumber;
    }
}
