package com.smarto.microlink.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "fieldDataVehicle")
public class FieldDataVehicle {

    @Id
    private String id;
    private String vin;
    private String suportedProtocol;
    private String mode1;
    private String mode2;
    private String mode5;
    private String mode6;
    private String mode8;
    private String mode9;
    private String make;
    private String model;

    public FieldDataVehicle() {
        super();
        // TODO Auto-generated constructor stub
    }

    public FieldDataVehicle(String id, String vin, String suportedProtocol, String mode1, String mode2, String mode5,
                            String mode6, String mode8, String mode9, String make, String model) {
        super();
        this.id = id;
        this.vin = vin;
        this.suportedProtocol = suportedProtocol;
        this.mode1 = mode1;
        this.mode2 = mode2;
        this.mode5 = mode5;
        this.mode6 = mode6;
        this.mode8 = mode8;
        this.mode9 = mode9;
        this.model = model;
        this.make = make;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getSuportedProtocol() {
        return suportedProtocol;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setSuportedProtocol(String suportedProtocol) {
        this.suportedProtocol = suportedProtocol;
    }

    public String getMode1() {
        return mode1;
    }

    public void setMode1(String mode1) {
        this.mode1 = mode1;
    }

    public String getMode2() {
        return mode2;
    }

    public void setMode2(String mode2) {
        this.mode2 = mode2;
    }

    public String getMode5() {
        return mode5;
    }

    public void setMode5(String mode5) {
        this.mode5 = mode5;
    }

    public String getMode6() {
        return mode6;
    }

    public void setMode6(String mode6) {
        this.mode6 = mode6;
    }

    public String getMode8() {
        return mode8;
    }

    public void setMode8(String mode8) {
        this.mode8 = mode8;
    }

    public String getMode9() {
        return mode9;
    }

    public void setMode9(String mode9) {
        this.mode9 = mode9;
    }

}
