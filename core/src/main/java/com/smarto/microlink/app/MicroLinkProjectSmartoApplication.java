package com.smarto.microlink.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@SpringBootApplication
@ComponentScan("com.smarto.microlink.*")
@EnableAutoConfiguration
public class MicroLinkProjectSmartoApplication {

    public static void main(String[] args) {
        SpringApplication.run(MicroLinkProjectSmartoApplication.class, args);
    }
}
