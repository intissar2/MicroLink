package com.smarto.microlink.aop;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;


@Aspect
@Component
public class ControllerMonitor {

    private static final Logger LOGGER = LoggerFactory.getLogger(ControllerMonitor.class);

    @AfterReturning("execution(* com.smarto..*Controller.*(..))")
    public void logAfterReturning(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().toString();
        LOGGER.info("===== Method \"" + methodName + "\" finished");
    }

    @Before("execution(* com.smarto..*Controller.*(..))")
    public void logBefore(JoinPoint joinPoint) {
        String methodName = joinPoint.getSignature().toString();
        Object[] args = joinPoint.getArgs();

        String argsStr = "args: ";

        if (args.length > 0) {
            argsStr = "args: " + args[0].toString();
        } else {
            argsStr = "no args";
        }

        for (int i = 1; i < args.length; i++) {
            if (args[i] != null) {
                argsStr += ", " + args[i].toString();
            }
        }

        LOGGER.info("===== Method \"" + methodName + "\" launched with " + argsStr);
    }
}
