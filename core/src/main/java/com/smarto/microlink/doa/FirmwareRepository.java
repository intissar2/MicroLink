package com.smarto.microlink.doa;

import com.smarto.microlink.domain.Firmware;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface FirmwareRepository extends MongoRepository<Firmware, String> {

    List<Firmware> findByCreationDate(Date creationdate);

    Firmware findByChecksum(String checksum);

    Firmware findByVersion(String version);

    List<Firmware> findByType(String type);

    Firmware findById(String id);

    List<Firmware> findByVersionGreaterThan(String version);
}
