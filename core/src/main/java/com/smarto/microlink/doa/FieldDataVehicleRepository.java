package com.smarto.microlink.doa;

import com.smarto.microlink.domain.FieldDataVehicle;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface FieldDataVehicleRepository extends MongoRepository<FieldDataVehicle, String> {

    FieldDataVehicle findById(String id);

    FieldDataVehicle findByVin(String vin);

}
