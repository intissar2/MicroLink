package com.smarto.microlink.doa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.smarto.microlink.domain.CarCare;

public interface CarCareRepository extends MongoRepository<CarCare, String> {
	CarCare findById(String id);
	Page<CarCare> findById(String id, Pageable pageable);
}
