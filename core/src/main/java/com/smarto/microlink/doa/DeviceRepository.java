package com.smarto.microlink.doa;

import com.smarto.microlink.domain.Device;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.Date;
import java.util.List;

public interface DeviceRepository extends MongoRepository<Device, String> {
    Device findById(String id);

    Page<Device> findById(String id, Pageable pageable);
    @Query("{$and: [ {'releaseDate' : { '$gte' : ?1 }}, { 'operatorName': ?0 }   ]}")
    List<Device> findByOperatorNameAndReleaseDate(String operatorName,Date releasedate);
    List<Device> findByOperatorName(String operatorName,String releasedate);

}
