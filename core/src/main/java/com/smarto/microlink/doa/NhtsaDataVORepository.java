package com.smarto.microlink.doa;

import com.smarto.microlink.domain.NhtsaDataVO;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by bassem smida on 24/11/2016.
 */
public interface NhtsaDataVORepository extends MongoRepository<NhtsaDataVO, String> {
}
