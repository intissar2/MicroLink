package com.smarto.microlink.doa;

import com.smarto.microlink.domain.User;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface UserRepository extends MongoRepository<User, String> {

    User findByMail(String mail);

    User findById(String id);

    List<User> findByRole(String role);

    //	User findByLastLogin(Date lastlogin);
    User findByFirstName(String firstname);

}
