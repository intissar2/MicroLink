package com.smarto.microlink.doa;

import com.smarto.microlink.domain.SupportedPID;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;


public interface SupportedPIDRepository extends MongoRepository<SupportedPID, String> {

    SupportedPID findByCode(String code);

    SupportedPID findByCodeAndMode(String mode, String code);

    SupportedPID findById(String id);

    List<SupportedPID> findByMode(String mode);

}
