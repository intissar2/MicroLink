package com.smarto.microlink.doa;

import com.smarto.microlink.domain.Entreprise;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface EntrepriseRepository extends MongoRepository<Entreprise, String>{
    Entreprise findById(String id);
    Entreprise findByNum(String num);
}
