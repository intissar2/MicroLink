package com.smarto.microlink.doa;

import com.smarto.microlink.domain.Vehicle;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface VehicleRepository extends MongoRepository<Vehicle, String> {

    Vehicle findById(String id);

    List<Vehicle> findByMake(String make);

    List<Vehicle> findByEngine(String engine);

    List<Vehicle> findByFuel(String fuel);

    List<Vehicle> findByModel(String model);

    List<Vehicle> findByProtocol(String protocol);

    List<Vehicle> findByPuissance(String puissance);

    List<Vehicle> findByMakeIgnoreCaseAndModelIgnoreCase(String make, String model);

    List<Vehicle> findByMakeIgnoreCaseAndModelIgnoreCaseAndAmIgnoreCase(String make, String model, String year);
}
