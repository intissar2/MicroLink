package com.smarto.microlink.doa;

import com.smarto.microlink.domain.Trip;
import com.smarto.microlink.domain.VersionBL;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface VersionBLRepository extends MongoRepository<VersionBL, String> {
    Trip findById(String id);

}
