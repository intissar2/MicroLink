package com.smarto.microlink.doa;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import com.smarto.microlink.domain.Fuel;

public interface FuelRepository extends MongoRepository<Fuel, String> {
	Fuel findById(String id);
	Page<Fuel> findById(String id, Pageable pageable);
}
