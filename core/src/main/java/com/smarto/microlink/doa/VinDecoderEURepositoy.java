package com.smarto.microlink.doa;

import com.smarto.microlink.domain.VinDecoderEU;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * Created by bassem smida on 24/11/2016.
 */
public interface VinDecoderEURepositoy extends MongoRepository<VinDecoderEU, String> {
}
