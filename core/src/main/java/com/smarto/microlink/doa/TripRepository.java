package com.smarto.microlink.doa;

import com.smarto.microlink.domain.Trip;
import com.smarto.microlink.domain.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface TripRepository extends MongoRepository<Trip, String> {
    Trip findById(String id);

    Page<Trip> findById(String id, Pageable pageable);

    List<Trip> findByUserAndDurationGreaterThan(User user, long min, Sort sort);
}
