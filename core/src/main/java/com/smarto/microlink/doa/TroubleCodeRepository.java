package com.smarto.microlink.doa;

import com.smarto.microlink.domain.TroubleCode;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface TroubleCodeRepository extends MongoRepository<TroubleCode, String> {

    TroubleCode findByCode(String code);

    TroubleCode findByCodeRegex(String code);

    TroubleCode findById(String id);

    TroubleCode findByCodeAndManufactor(String code, String manufactor);

}
