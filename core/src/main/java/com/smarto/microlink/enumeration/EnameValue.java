package com.smarto.microlink.enumeration;

public enum EnameValue {

    Speed("Speed", "Km/h"),
    Temperature("Temperature", "C"),
    Distance("Distance", "Km"),
    EngineRpm("EngineRpm", "Rpm"),
    EngineLoad("EngineLoad", ""),
    ManAbsPressure("ManAbsPressure", ""),
    IntAirTemperature("IntAirTemperature", ""),
    AirFlowRate("AirFlowRate", ""),
    FuelLevelInput("FuelLevelInput", ""),
    Batterie("Batterie", "V"),
    AcceleroX("AcceleroX", ""),
    AcceleroY("AcceleroY", ""),
    //to be calculate on the end of trip
    FUEL_FLOW("fuel flow", "%L/KM");

    private String name;
    private String unite;

    EnameValue(String name, String unite) {
        // TODO Auto-generated constructor stub
        this.setName(name);
        this.setUnite(unite);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnite() {
        return unite;
    }

    public void setUnite(String unite) {
        this.unite = unite;
    }

}
