package com.smarto.microlink.enumeration;

/**
 * Created by DELL on 26/12/2016.
 */
public enum EPatchOp {
    test, remove, add, replace, move, copy;
}
