package com.smarto.microlink.enumeration;

/**
 * Created by DELL on 26/12/2016.
 */

// I => IN/ON, B=> Bellow
public enum EDungleLocation {
    DEFAULT(0, "UNKNOWN"), I_FUSE_BOX(1, "In the fuse box"), B_STEERING_WHEEL(1, "Below Steering Wheel"),
    I_CONSOLE_CENTER(1, "On the center console"), I_BOX_CLOVES(1, "In the box Gloves"), B_GLOVE_BOW(1, "Below the Glove box");

    EDungleLocation(int code, String name) {
        this.code = code;
        this.name = name;
    }

    private int code;
    private String name;

    public int getCode() {
        return code;
    }

    protected void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    protected void setName(String name) {
        this.name = name;
    }

}
