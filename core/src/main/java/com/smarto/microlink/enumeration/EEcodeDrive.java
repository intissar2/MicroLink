package com.smarto.microlink.enumeration;

/**
 * Created by A0105774 on 07/01/2017.
 */
public enum EEcodeDrive {

    DEFUALT("unknown", "unknown"),
    AVG_FUEL_CONS("Average Fuel Consumption", "l/100km"),
    CONSUPTION_FUEL_F_DEPARTURE("Consumption Fuel from Departure", "l"),
    AVG_CO2_EMISSION("Average CO2 Emission", "g/km"),
    CO2_EMISSION_FROM_DEPARTURE("CO2 Emission from Departure in", "km"),
    STANDARD_SCORE("STANDARD Score ", null),
    EFFICENCY_SCORE("EFFICIENCY Score", null),
    PROFESSIONAL_SCORE("PROFESSIONAL Score ", null);

    EEcodeDrive(String type, String unit) {
        this.type = type;
        this.unit = unit;
    }

    private String type;
    private String unit;
}
