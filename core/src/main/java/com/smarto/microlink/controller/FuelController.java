package com.smarto.microlink.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.Fuel;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.FuelServices;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(ApiController.FUEL_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class FuelController  extends GenericController<Fuel, String>{

	@Autowired
	private FuelServices fuelServices;

	@Override
	protected MongoRepository<Fuel, String> getMongoRepository() {
		return fuelServices.getFuelRepository();
	}
	
	/**
	 * Return all Fuel version
	 * 
	 * @return List of Fuel version
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns all Fuel List")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_FUEL_ENDPOINT, produces = "application/json")
	public List<Fuel> getAllFuel() throws ServiceException {
		
		return fuelServices.findAllFuel();
	}

	/**
	 * Find last Fuels list by User
	 * 
	 * @return last Fuels list by User
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns last Fuels list by User")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FUEL_BY_USER, produces = "application/json")
	public ResponseEntity<List<Fuel>> getFuelsByUser(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {
		
		return new ResponseEntity<List<Fuel>>(fuelServices.findFuelByUserId(userId), HttpStatus.OK);
	}
	
	/**
	 * Return PDF FUEL REPORTS between two dates by user
	 * 
	 * @return PDF Report between two dates by user
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns PDF report between two dates by user")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FUEL_BETWEEN_TWO_DATES_ENDPOINT, produces = "application/pdf")
	public ResponseEntity<byte[]> getFuelsBetweenTwoDates(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
			@PathVariable("firstDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date firstDate,
			@PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) throws ServiceException {
		
		return new ResponseEntity<byte[]>(fuelServices.generateFuelReport(userId, vehicleId, firstDate, endDate), HttpStatus.OK);
	}
	
	/**
	 * return fuel report by user by period
	 * 
	 * @return fuel report by user by period
	 * @throws ServiceException
	 */
	@ApiOperation(value = "generate PDF by period -- Return fuel report by user by period")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FUEL_REPORT_BETWEEN_TWO_DATES_ENDPOINT, produces = "application/pdf")
	public ResponseEntity<byte[]> getFuelReportByPeriod(
			@PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
			@PathVariable("firstDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date firstDate,
			@PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) throws ServiceException {
		String reportName = "FuelReport_" + userId + "_" + firstDate + "_" + endDate+".pdf";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.add("content-disposition", "attachment; filename=" + reportName);
		return new ResponseEntity<byte[]>(fuelServices.generateFuelReport(userId, vehicleId, firstDate, endDate), headers, HttpStatus.OK);
	}

	/**
	 * return fuel report by user by year, PDF
	 * 
	 * @return fuel report by user by year, PDF
	 * @throws ServiceException
	 */
	@ApiOperation(value = "generate PDF by year -- Return fuel report by user by year, PDF")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FUEL_REPORT_BY_YEAR, produces = "application/pdf")
	public ResponseEntity<byte[]> getFuelPdfByYear(
			@PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
			@PathVariable("year") @ApiParam(value = "year") String year) throws ServiceException {
		String reportName = "FuelReport_" + userId + "_" + year+".pdf" ;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.add("content-disposition", "attachment; filename=" + reportName);
		return new ResponseEntity<byte[]>(fuelServices.generateFuelReportByYear(userId,vehicleId, year,"pdf"), headers, HttpStatus.OK);
	}
	
	/**
	 * return fuel report by user by month, PDF
	 * 
	 * @return fuel report by user by month, PDF
	 * @throws ServiceException
	 */
	@ApiOperation(value = "generate PDF by month -- Return fuel report by user by month, PDF")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FUEL_REPORT_BY_MONTH, produces = "application/pdf")
	public ResponseEntity<byte[]> getFuelPdfByMonth(
			@PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
			@PathVariable("year") @ApiParam(value = "year") String year,
			@PathVariable("month") @ApiParam(value = "month") String month) throws ServiceException {
		String reportName = "FuelReport_" + userId + "_" + month+ "_" + year+".pdf" ;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.add("content-disposition", "attachment; filename=" + reportName);
		return new ResponseEntity<byte[]>(fuelServices.generateFuelReportByMonth(userId,vehicleId, year, month,"pdf"), headers, HttpStatus.OK);
	}
	
	
	/**
	 * create Fuel
	 * 
	 * @return 0 - if Fuel object created - if not (already exists)
	 * @throws ServiceException
	 */
	@ApiOperation(value = "created Fuel. Returns 0 - created, 1 - already exists")
	@RequestMapping(method = RequestMethod.POST, value = ApiController.POST_FUEL_ENDPOINT, produces = "application/json")
	public ResponseEntity<Fuel> createFuel(@RequestBody @ApiParam(value = "Fuel to save") Fuel fuel)
			throws ServiceException {
		
		return new ResponseEntity<Fuel>(fuelServices.addFuel(fuel), HttpStatus.OK);
	}

	/**
	 * Update Fuel
	 * 
	 * @param fuelId
	 *            - id of Fuel
	 * @param updatedFuel
	 *            - Fuel object
	 * 
	 * @return return Fuel updated or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Update Fuel. Returns Fuel updated or null")
	@RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_FUEL_ENDPOINT, produces = "application/json")
	public ResponseEntity<Fuel> updateFuel(@RequestBody @ApiParam(value = "Fuel Object") Fuel updatedFuel,
			@PathVariable @ApiParam(value = "Fuel Code") String fuelId) {
		
		return new ResponseEntity<Fuel>(fuelServices.updateFuel(updatedFuel, fuelId), HttpStatus.OK);
	}

	/**
	 * Delete Fuel
	 * 
	 * @param fuelId
	 *            - id of Fuel
	 * 
	 * @return return Fuel deleted or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Delete Fuel. Returns Fuel deleted or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_FUEL_ENDPOINT, produces = "application/json")
	public ResponseEntity<Fuel> deleteFuel(@PathVariable("fuelId") String fuelId) {
		fuelServices.removeFuel(fuelId);
		
		return new ResponseEntity<Fuel>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all Fuels
	 * 
	 * @return return non content or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Delete all Fuels, return non content or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_FUEL_ENDPOINT)
	public ResponseEntity<Fuel> deleteAllFuels() {

		fuelServices.removeAllFuels();

		return new ResponseEntity<Fuel>(HttpStatus.NO_CONTENT);
	}
	
}
