package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.Message;
import com.smarto.microlink.domain.User;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.DeviceServices;
import com.smarto.microlink.service.UserServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(ApiController.USER_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class UserController extends GenericController<User, String> {

    @Autowired
    private UserServices userServices;
    @Autowired
    private DeviceServices deviceServices;

    protected MongoRepository<User, String> getMongoRepository() {
        return userServices.getUserRepository();
    }

    /**
     * Find a User list
     *
     * @return User list
     */
    @ApiOperation(value = "Returns a User list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_USER_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<User>> getUsers() {
        return new ResponseEntity<List<User>>(userServices.findUsers(), HttpStatus.OK);
    }
    
    /**
     * Find a User list
     *
     * @return User list
     * @throws ServiceException 
     */
    @ApiOperation(value = "Returns a User list with last known position")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_USER_LAST_POSITION_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<User>> getUsersLastPosition() throws ServiceException {
        return new ResponseEntity<List<User>>(userServices.findUsersWithLastPosition(), HttpStatus.OK);
    }

    /**
     * Find a User object by userId
     *
     * @return User object
     */
    @ApiOperation(value = "Returns a User object ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_USER_BY_ID_ENDPOINT, produces = "application/json")
    public ResponseEntity<User> getUserById(
            @PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) {
        return new ResponseEntity<User>(userServices.findUserById(userId), HttpStatus.OK);
    }

    @ApiOperation(value = "Returns a User object ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_IS_MAIL_USER_EXIST_BY_ID_ENDPOINT)
    public ResponseEntity<Boolean> isMailExist(@PathVariable("mail") @ApiParam(value = "User mail") String mail)
            throws ServiceException {
        if (userServices.isMailExist(mail) == true)
            return new ResponseEntity<Boolean>(userServices.isMailExist(mail), HttpStatus.OK);
        else
            return new ResponseEntity<Boolean>(userServices.isMailExist(mail), HttpStatus.NOT_FOUND);
    }

    @ApiOperation(value = "Returns a User object ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_AUTHENTICATE_ENDPOINT)
    public Message authenticate(@PathVariable("mail") @ApiParam(value = "User mail") String mail,
                                @PathVariable("mdp") @ApiParam(value = "User mdp") String mdp) throws ServiceException {
        return userServices.authenticate(mail, mdp);
    }

    /**
     * create user
     *
     * @return positive message - if User object created - if not (already
     * exists) return negative message
     * @throws Exception
     */
    @ApiOperation(value = "created User. Returns positive message - if User object created - if not (already exists) return negative message")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_USER_ENDPOINT, produces = "application/json")
    public ResponseEntity<Message> createUser(@RequestBody @ApiParam(value = "User to save") User user)
            throws ServiceException {
        return new ResponseEntity<Message>(userServices.addUser(user), HttpStatus.OK);
    }

    /**
     * create user with specific device
     *
     * @return positive message - if User object created - if not (already
     * exists) return negative message
     * @throws Exception
     */
    @ApiOperation(value = "created User with specific device. Returns positive message - if User object created - if not (already exists) return negative message")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_USER_DEVICE_ENDPOINT, produces = "application/json")
    public ResponseEntity<Message> createUserWithDevice(@RequestBody @ApiParam(value = "User to save") User user)
            throws ServiceException {
        return new ResponseEntity<Message>(userServices.addUserDevice(user), HttpStatus.OK);
    }

    // @ApiOperation(value = "Resets password")
    // @RequestMapping(value = "/rest/password/{password}",
    // method = RequestMethod.POST,
    // produces = MediaType.APPLICATION_JSON_VALUE)
    // public ResponseEntity<String> resetPassword(
    // @PathVariable @ApiParam("password") String password) {
    // return null;
    // }

    /**
     * Update user
     * @return return user updated or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Update user. Returns user updated or null")
    @RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_USER_ENDPOINT, produces = "application/json")
    public ResponseEntity<User> updateUser(@RequestBody @ApiParam(value = "User Object") User updatedUser,
                                           @PathVariable @ApiParam(value = "User Code") String id) {
        return new ResponseEntity<User>(userServices.updateUser(updatedUser, id), HttpStatus.OK);
    }

    /**
     * Delete device from user document if isUsed==0 And setUp IsUsed Value to false in the device document
     * @param userId
     * @param idDevice
     * @param isUsed
     * @return
     */
    @ApiOperation(value = "Update user 's devices :remove device from user document if isUsed==0 And set IsUsed Value to false in the device document")
    @RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_USER_DEVICE, produces = "application/json")
    public ResponseEntity<User> removeUserDevice(@PathVariable("userId") String userId,@PathVariable("idDevice") String idDevice,@PathVariable("isUsed") String isUsed) {
        boolean used =(Integer.parseInt(isUsed) == 0)? false:true;
        if (isUsed != null && Integer.parseInt(isUsed) == 0) {
            // update device Document (update isUsed to false or true)
            deviceServices.updateIsUserDevice(idDevice,used);
            // remove device from list of devices of current user
            userServices.removeUserDevice(userId, idDevice);
        }
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }

    /**
     * Delete user
     *
     * @param userId - id of user
     * @return return user deleted or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Delete user. Returns user deleted or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_USER_ENDPOINT, produces = "application/json")
    public ResponseEntity<User> deleteUser(@PathVariable("userId") String userId) throws ServiceException {
        userServices.removeUser(userId);
        return new ResponseEntity<User>(HttpStatus.NO_CONTENT);
    }

    /**
     * Delete all users
     *
     * @return return non content or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Delete all users, return non content or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_USER_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<User>> deleteAllUsers() {
        userServices.removeAllUsers();
        return new ResponseEntity<List<User>>(HttpStatus.NO_CONTENT);
    }

    /**
     * return fees report by user (between two dates)
     *
     * @return fees report by user (between two dates)
     * context = 0 (between two dates)
     * @throws ServiceException
     */
    @ApiOperation(value = "generate PDF for fees -- Return fees report by user (between two dates) context = 0 (between two dates)")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FEE_PLUG_REPORT_BY_USER_BETWEEN_TWO_DATES, produces = "application/pdf")
    public ResponseEntity<byte[]> getFeesReportDates(
            @PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
            @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
            @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate,
            @PathVariable("context") @ApiParam(value = "Context") String context) throws ServiceException {
        DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy");
        String reportName = "FeesReport_" + userId + "_" + dateFormat.format(startDate) + "_" + dateFormat.format(endDate) + ".pdf";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.add("content-disposition", "attachment; filename=" + reportName);
        return new ResponseEntity<byte[]>(userServices.generateFeesReportDates(userId, startDate, endDate, context), headers, HttpStatus.OK);
    }

    /**
     * return fees report by user (Monthly)
     *
     * @return fees report by user (Monthly)
     * context = 1 (Monthly)
     * @throws ServiceException
     */
    @ApiOperation(value = "generate PDF for fees -- Return fees report by user (Monthly) context = 1 (Monthly)")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FEE_PLUG_REPORT_BY_USER_MONTH, produces = "application/pdf")
    public ResponseEntity<byte[]> getFeesReportMonthly(
            @PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
            @PathVariable("year") int year,
            @PathVariable("month") int month,
            @PathVariable("context") @ApiParam(value = "Context") String context) throws ServiceException {
        String reportName = "FeesReport_" + userId + "_" + year + "_" + month + ".pdf";
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.add("content-disposition", "attachment; filename=" + reportName);
        return new ResponseEntity<byte[]>(userServices.generateFeesReportMonth(userId, year, month, context), headers, HttpStatus.OK);
    }

    /**
     * return fees report by user (Yearly)
     *
     * @return fees report by user (Yearly)
     * context = 1 (Yearly)
     * @throws ServiceException
     */
    @ApiOperation(value = "generate PDF for fees -- Return fees report by user (Yearly) context = 2 (Yearly)")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FEE_PLUG_REPORT_BY_USER_YEAR, produces = "application/pdf")
    public ResponseEntity<byte[]> getFeesReportYearly(
            @PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
            @PathVariable("year") int year,
            @PathVariable("context") @ApiParam(value = "Context") String context) throws ServiceException {
        String reportName = "FeesReport_" + userId + "_" + year + ".pdf";;
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.parseMediaType("application/pdf"));
        headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
        headers.add("content-disposition", "attachment; filename=" + reportName);
        return new ResponseEntity<byte[]>(userServices.generateFeesReportYear(userId, year, context), headers, HttpStatus.OK);
    }

}
