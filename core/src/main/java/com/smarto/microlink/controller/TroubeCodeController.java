package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.TroubleCode;
import com.smarto.microlink.domain.TroubleCodeHeader;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.TroubleCodeServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/troublecodes")
@EnableMongoRepositories("com.smarto.microlink.*")
public class TroubeCodeController {

    @Autowired
    private TroubleCodeServices troubleCodeServices;

    /**
     * Find all TroubleCode
     *
     * @return TroubleCode list
     */
    @ApiOperation(value = "Returns a TroubleCode list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_TROUBLECODE_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<TroubleCode>> getAllTroubleCodes() {
        return new ResponseEntity<List<TroubleCode>>(troubleCodeServices.findAllTroubleCodes(), HttpStatus.OK);
    }

    /**
     * Find a TroubleCode object by Id
     *
     * @return TroubleCode object
     */
    @ApiOperation(value = "Returns a TroubleCode object")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TROUBLECODE_BY_ID_ENDPOINT, produces = "application/json")
    public ResponseEntity<TroubleCode> getTroubleCodeById(@PathVariable @ApiParam(value = "TroubleCodeId") String id) {
        return new ResponseEntity<TroubleCode>(troubleCodeServices.findTroubleCodeById(id), HttpStatus.OK);
    }

    /**
     * Find a TroubleCodeHeader list by CodeList and Manufactor
     *
     * @return TroubleCodeHeader list
     */
    @ApiOperation(value = "Returns a TroubleCode list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LIST_TROUBLECODE_HEADER_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<TroubleCodeHeader>> getListTroubleCodeHeader(
            @PathVariable(value = "codeList") @ApiParam(value = "TroubleCode Code list") List<String> codeList,
            @PathVariable(value = "manufactor") @ApiParam(value = "TroubleCode Manufactor") String manufactor) {

        return new ResponseEntity<List<TroubleCodeHeader>>(troubleCodeServices.findListTroubleCodeHeader(codeList, manufactor), HttpStatus.OK);
    }

    /**
     * Find a TroubleCode list by CodeList and Manufactor
     *
     * @return TroubleCode list
     */
    @ApiOperation(value = "Returns a TroubleCode list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LIST_TROUBLECODE_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<TroubleCode>> getListTroubleCode(
            @PathVariable(value = "codeList") @ApiParam(value = "TroubleCode Code list") List<String> codeList,
            @PathVariable(value = "manufactor") @ApiParam(value = "TroubleCode Manufactor") String manufactor) {

        return new ResponseEntity<List<TroubleCode>>(troubleCodeServices.findListTroubleCode(codeList, manufactor), HttpStatus.OK);
    }

    /**
     * add a new TroubleCode object
     *
     * @return TroubleCode object
     */
    @ApiOperation(value = "Returns a TroubleCode object ")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_TROUBLECODE_ENDPOINT, produces = "application/json")
    public ResponseEntity<TroubleCode> createTroubleCode(
            @RequestBody @ApiParam(value = "TroubleCode object") TroubleCode TroubleCode) {
        return new ResponseEntity<TroubleCode>(troubleCodeServices.addTroubleCode(TroubleCode), HttpStatus.OK);
    }

    /**
     * Update a TroubleCode object by Code
     *
     * @return TroubleCode object
     */
    @ApiOperation(value = "Returns a TroubleCode object ")
    @RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_TROUBLECODE_ENDPOINT, produces = "application/json")
    public ResponseEntity<TroubleCode> updateTroubleCode(
            @RequestBody @ApiParam(value = "TroubleCode Object") TroubleCode updatedTroubleCode,
            @PathVariable @ApiParam(value = "TroubleCode Code") String id) {

        return new ResponseEntity<TroubleCode>(troubleCodeServices.updateTroubleCode(updatedTroubleCode, id), HttpStatus.OK);
    }

    /**
     * Delete a TroubleCode object by Code
     *
     * @return TroubleCode object
     */
    @ApiOperation(value = "Returns a TroubleCode object ")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_TROUBLECODE_ENDPOINT, produces = "application/json")
    public ResponseEntity<TroubleCode> deleteTroubleCode(
            @PathVariable @ApiParam(value = "TroubleCode Code") String id) {

        return new ResponseEntity<TroubleCode>(troubleCodeServices.removeTroubleCode(id), HttpStatus.OK);
    }

    /**
     * Delete all TroubleCodes
     *
     * @return return non content or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Delete all TroubleCodes, return non content or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_TROUBLECODE_ENDPOINT, produces = "application/json")
    public ResponseEntity<TroubleCode> deleteAllTroubleCodes() {

        troubleCodeServices.removeAllTroubleCodes();

        return new ResponseEntity<TroubleCode>(HttpStatus.NO_CONTENT);
    }

}
