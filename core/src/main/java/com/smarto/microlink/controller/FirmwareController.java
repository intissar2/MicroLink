package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.doa.FirmwareRepository;
import com.smarto.microlink.domain.Firmware;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.FirmwareServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
//@RequestMapping(FIRMWARE_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class FirmwareController {

    @Autowired
    private FirmwareServices firmwareServices;

    @Autowired
    private FirmwareRepository firmwareRepository;

    /**
     * Find a Firmware by firmwareId
     *
     * @return Firmware
     */
    @ApiOperation(value = "Returns a Firmware ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FIRMWARE_BY_ID_ENDPOINT, produces = "application/json")
    public ResponseEntity<Firmware> getFirmwareById(
            @PathVariable(value = "id") @ApiParam(value = "Firmware Identifiant") String firmwareId) {

        return new ResponseEntity<Firmware>(firmwareServices.findById(firmwareId), HttpStatus.OK);
    }

    /**
     * Return all firmware version
     *
     * @return List of firmware version
     * @throws ServiceException
     */
    @ApiOperation(value = "Returns all Firmware version")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_FIRMWARE_ENDPOINT, produces = "application/json")
    public List<Firmware> getAllFirmware() throws ServiceException {

        return firmwareServices.findAllFirmware();
    }
//	public ResponseEntity<List<Firmware>> getAllFirmware() throws ServiceException {
//
//		return new ResponseEntity<List<Firmware>>(firmwareServices.findAllFirmware(), HttpStatus.OK);
//	}

    /**
     * Find a firmware list by firmware type
     *
     * @return firmware list
     */
    @ApiOperation(value = "Returns a firmware list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FIRMWARE_BY_TYPE_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<Firmware>> getFirmwareByType(
            @PathVariable("firmwareType") @ApiParam(value = "Firmware type") String firmwareType)
            throws ServiceException {

        return new ResponseEntity<List<Firmware>>(firmwareServices.findByType(firmwareType), HttpStatus.OK);
    }

    /**
     * Find a firmware list by firmware version
     *
     * @return firmware list
     */
    @ApiOperation(value = "Returns a firmware list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FIRMWARE_BY_VERSION_ENDPOINT, produces = "application/json")
    public ResponseEntity<Firmware> getFirmwareByVersion(
            @PathVariable("firmwareVersion") @ApiParam(value = "Firmware version") String firmwareVersion)
            throws ServiceException {

        return new ResponseEntity<Firmware>(firmwareServices.findByVersion(firmwareVersion), HttpStatus.OK);
    }

    /**
     * Find a firmware list by firmware date
     *
     * @return firmware list
     */
    @ApiOperation(value = "Returns a firmware list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FIRMWARE_BY_DATE_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<Firmware>> getFirmwareByDate(
            @PathVariable("firmwareDate") @ApiParam(value = "Firmware creation date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date firmwareDate)
            throws ServiceException {

        return new ResponseEntity<List<Firmware>>(firmwareServices.findByDate(firmwareDate), HttpStatus.OK);
    }

    /**
     * Verify firmware version
     *
     * @param date - date of firmware
     * @return true - if the version is the last false - in another case
     * @throws Exception
     */
    @ApiOperation(value = "Verifies firmware version by date. Returns true if version is the last")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_VERIFY_LAST_VERSION_FIRMWARE_ENDPOINT, produces = "application/json")
    public ResponseEntity<Boolean> verifyLastVersionFirmware(
            @RequestHeader @ApiParam(value = "Firmware date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date)
            throws ServiceException {

        return new ResponseEntity<Boolean>(firmwareServices.verifyLastVersionFirmware(date), HttpStatus.OK);
    }

    /**
     * create firmware
     *
     * @param firmware - firmware JSON Object
     * @return 0 - if firmware create 1 - if not (already exists)
     * @throws ServiceException
     */
    @ApiOperation(value = "Saves firmware. Returns 0 - created, 1 - already exists")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_FIRMWARE_ENDPOINT, produces = "application/json")
    public Integer createFirmware(@RequestBody @ApiParam(value = "Firmware to save") Firmware firmware)
            throws ServiceException {
        System.out.println("ccccccccc");
        return firmwareServices.addFirmware(firmware);
    }
//	public ResponseEntity<Integer> createFirmware(@RequestBody @ApiParam(value = "Firmware to save") Firmware firmware)
//			throws ServiceException {
//		System.out.println("rani hna");
//		return new ResponseEntity<Integer>(firmwareServices.addFirmware(firmware), HttpStatus.OK);
//	}

    /**
     * Find a Firmware by firmwareId
     *
     * @return Firmware JSON object
     */
    @ApiOperation(value = "Returns a Firmware JSON object ")
    @RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_FIRMWARE_ENDPOINT)
    public ResponseEntity<Firmware> updateFirmware(@RequestBody @ApiParam(value = "Firmware Obecjt") Firmware updatedFirmware,
                                                   @PathVariable @ApiParam(value = "Firmware Identifiant") String firmwareId) {

        return new ResponseEntity<Firmware>(firmwareServices.updateFirmware(updatedFirmware, firmwareId), HttpStatus.OK);
    }

    /**
     * Delete firmware
     *
     * @param firmwareId - Firmware Version
     * @return return firmware deleted or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Delete firmware. Returns firmware deleted or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_FIRMWARE_ENDPOINT)
    public ResponseEntity<Firmware> delete(
            @PathVariable("firmwareId") @ApiParam(value = "Firmware identifient to deleted") String firmwareId) {

        return new ResponseEntity<Firmware>(firmwareServices.removeFirmware(firmwareId), HttpStatus.NO_CONTENT);
    }

    /**
     * Delete firmware
     *
     * @param firmwareVer - Firmware Version
     * @return return firmware deleted or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Delete firmware. Returns firmware deleted or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_FIRMWARE_BY_VERSION_ENDPOINT)
    public ResponseEntity<Firmware> deleteFirmwareByVersion(
            @PathVariable("firmwareVer") @ApiParam(value = "Firmware version to deleted") String firmwareVer) {
        Firmware deleted = firmwareRepository.findByVersion(firmwareVer);

        firmwareServices.removeFirmware(deleted.getId());

        return new ResponseEntity<Firmware>(deleted, HttpStatus.NO_CONTENT);
    }

    /**
     * Delete all Firmwares
     *
     * @return return non content or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Delete all Firmwares, return non content or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_FIRMWARE_ENDPOINT)
    public ResponseEntity<Firmware> deleteAllFirmwares() {

        firmwareServices.removeAllFirmwares();

        return new ResponseEntity<Firmware>(HttpStatus.NO_CONTENT);
    }

    @ApiOperation(value = "get firmware after version")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FIRMWARE_LAST_VERSION_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<Firmware>> findFirmwareAfterVersion(@PathVariable("firmwareVer") String firmwareVer) {
        return new ResponseEntity<List<Firmware>>(firmwareServices.getFirwaresAfterVersion(firmwareVer), HttpStatus.OK);
    }

}
