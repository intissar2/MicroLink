package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.Vehicle;
import com.smarto.microlink.domain.VehicleHeader;
import com.smarto.microlink.service.VehicleServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiController.VEHICLE_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class VehicleController extends GenericController<Vehicle, String> {

    protected MongoRepository<Vehicle, String> getMongoRepository() {
        return vehicleServices.getVehicleRepository();
    }

    @Autowired
    private VehicleServices vehicleServices;

    /**
     * Find a Vehicle object by id
     *
     * @return Vehicle object
     */
    @ApiOperation(value = "Returns a Vehicle object ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_VEHICLE_BY_ID_ENDPOINT)
    public ResponseEntity<Vehicle> getVehicleById(@PathVariable("id") @ApiParam(value = "Vehicle Identifier") String id) {
        return new ResponseEntity<Vehicle>(vehicleServices.findVehicleById(id), HttpStatus.OK);
    }

    /**
     * Find all Vehicles
     *
     * @return Vehicle list
     */
    @ApiOperation(value = "Returns a Vehicle list ")
    @RequestMapping(method = RequestMethod.GET, value = "", produces = "application/json")
    public ResponseEntity<List<Vehicle>> getAllVehicles() {
        return new ResponseEntity<List<Vehicle>>(vehicleServices.findAllVehicles(), HttpStatus.OK);
    }

    /**
     * Find all Vehicles Marks
     *
     * @return String list
     */
    @ApiOperation(value = "Returns a String list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_VEHICLES_MAKES_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<String>> getAllVehiclesMakes() {
        return new ResponseEntity<List<String>>(vehicleServices.findAllVehiclesMakes(), HttpStatus.OK);
    }

    /**
     * Find all Vehicles Headers
     *
     * @return String list
     */
    @ApiOperation(value = "Returns a Vehicle list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_VEHICLES_HEADER_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<VehicleHeader>> getAllVehiclesHeader() {
        return new ResponseEntity<List<VehicleHeader>>(vehicleServices.findAllVehiclesHeader(), HttpStatus.OK);
    }

    /**
     * Find all Vehicles Headers by make
     *
     * @return Vehicles Headers list
     */
    @ApiOperation(value = "Returns a Vehicle Header list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LIST_VEHICLES_HEADER_BY_MAKE_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<VehicleHeader>> getListVehiclesHeaderByMake(
            @PathVariable("make") @ApiParam(value = "Vehicle Make") String make) {
        return new ResponseEntity<List<VehicleHeader>>(vehicleServices.findListVehiclesHeaderByMake(make),
                HttpStatus.OK);
    }

    /**
     * add a new Vehicle object
     *
     * @return Vehicle object
     */
    @ApiOperation(value = "Returns a Vehicle object ")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_VEHICLE_ENDPOINT, produces = "application/json")
    public ResponseEntity<Vehicle> createVehicle(@RequestBody @ApiParam(value = "Vehicle object") Vehicle vehicle) {

        return new ResponseEntity<Vehicle>(vehicleServices.addVehicle(vehicle), HttpStatus.OK);
    }

    /**
     * Update a Vehicle object by Id
     *
     * @return Vehicle object
     */
    @ApiOperation(value = "Returns a Vehicle object ")
    @RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_VEHICLE_ENDPOINT)
    public ResponseEntity<Vehicle> updateVehicle(
            @RequestBody @ApiParam(value = "Vehicle Object") Vehicle updatedVehicle,
            @PathVariable @ApiParam(value = "Vehicle Id") String id) {

        return new ResponseEntity<Vehicle>(vehicleServices.updateVehicle(updatedVehicle, id), HttpStatus.OK);
    }

    /**
     * Delete a Vehicle object by Id
     *
     * @return Vehicle object
     */
    @ApiOperation(value = "Returns a Vehicle object ")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_VEHICLE_ENDPOINT, produces = "application/json")
    public ResponseEntity<Vehicle> deleteVehicle(@PathVariable @ApiParam(value = "Vehicle Id") String id) {

        return new ResponseEntity<Vehicle>(vehicleServices.removeVehicle(id), HttpStatus.NO_CONTENT);
    }

    /**
     * Delete all Vehicles
     *
     * @return return non content or null
     * @throws ServiceException
     */
    /*@ApiOperation(value = "Delete all Vehicles, return non content or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_VEHICLE_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Vehicle>> deleteAllVehicles() {
		vehicleServices.removeAllVehicles();
		return new ResponseEntity<List<Vehicle>>(HttpStatus.NO_CONTENT);
	}*/

/*	@Override
	@ApiOperation(value = "patch Vehicle")
	@RequestMapping(method = RequestMethod.PATCH, value = ApiController.PATCH_VEHICLE_BY_ID_ENDPOINT, consumes = "application/json")
	public Vehicle patch(@RequestBody List<PatchOperation> patchOperations,@PathVariable("id") String id) throws IOException {
		return super.patch(patchOperations, id);
	}*/
}