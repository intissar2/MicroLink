package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.VersionBL;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.VersionBLServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(ApiController.VERSIONBL_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class VersionBLController extends GenericController<VersionBL, String>{

	@Autowired
	private VersionBLServices versionBLServices;

	@Override
	protected MongoRepository<VersionBL, String> getMongoRepository() {
		return versionBLServices.getVersionBLRepository();
	}

	/**
	 * Find last Version BL
	 * 
	 * @return last Version BL
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns last VersionBL")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_VERSIONBL, produces = "application/json")
	public ResponseEntity<List<VersionBL>> getLastTrip() throws ServiceException {
		// increment the version before get it
		versionBLServices.addVersionBL();
		return new ResponseEntity<List<VersionBL>>(versionBLServices.findLastVersionBL(), HttpStatus.OK);
	}

	/**
	 * insert Version BL
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@ApiOperation(value = "created Version BL. Returns 0 - created, 1 - already exists")
	@RequestMapping(method = RequestMethod.POST, value = ApiController.POST_VERSIONBL_ENDPOINT, produces = "application/json")
	public ResponseEntity<VersionBL> addVersionBL()
			throws ServiceException {
		return new ResponseEntity<VersionBL>(versionBLServices.addVersionBL(), HttpStatus.OK);
	}
}
