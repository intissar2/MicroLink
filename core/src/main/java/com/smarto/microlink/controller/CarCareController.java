package com.smarto.microlink.controller;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.CarCare;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.CarCareServices;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;

@RestController
@RequestMapping(ApiController.CARCARE_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class CarCareController  extends GenericController<CarCare, String>{

	@Autowired
	private CarCareServices carCareServices;

	@Override
	protected MongoRepository<CarCare, String> getMongoRepository() {
		return carCareServices.getCarCareRepository();
	}
	
	/**
	 * Return all CarCare version
	 * 
	 * @return List of CarCare version
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns all CarCare List")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_CARCARE_ENDPOINT, produces = "application/json")
	public List<CarCare> getAllCarCare() throws ServiceException {
		
		return carCareServices.findAllCarCare();
	}

	/**
	 * Find last CarCares list by User
	 * 
	 * @return last CarCares list by User
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns last CarCares list by User")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_CARCARE_BY_USER, produces = "application/json")
	public ResponseEntity<List<CarCare>> getCarCaresByUser(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {
		
		return new ResponseEntity<List<CarCare>>(carCareServices.findCarCareByUserId(userId), HttpStatus.OK);
	}
	
	/**
	 * Find last CarCares list by Vehicle
	 * 
	 * @return last CarCares list by Vehicle
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns last CarCares list by vehicle")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_CARCARE_BY_VEHICLE, produces = "application/json")
	public ResponseEntity<List<CarCare>> getCarCaresByVehicle(@PathVariable("vehicleId") @ApiParam(value = "User Identifiant") String vehicleId) throws ServiceException {
		return new ResponseEntity<List<CarCare>>(carCareServices.findCarCareByVehicleId(vehicleId), HttpStatus.OK);
	}

	/**
	 * Return PDF CAR CARE REPORTS between two dates by user & by vehicle
	 * 
	 * @return PDF Report between two dates by user & by vehicle
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns PDF report between two dates by user & by vehicle")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_CARCARE_BETWEEN_TWO_DATES_ENDPOINT, produces = "application/pdf")
	public ResponseEntity<byte[]> getCarCaresBetweenTwoDates(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifiant") String vehicleId,
			@PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date firstDate,
			@PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) throws ServiceException {
		
		return new ResponseEntity<byte[]>(carCareServices.generateCarCareReport(userId,vehicleId, firstDate, endDate), HttpStatus.OK);
	}
	
	/**
	 * return carCare report by user vehicle by period
	 * 
	 * @return carCare report by user vehicle by period
	 * @throws ServiceException
	 */
	@ApiOperation(value = "generate PDF by period -- Return carCare report by user vehicle by period")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_CARCARE_REPORT_BETWEEN_TWO_DATES_ENDPOINT, produces = "application/pdf")
	public ResponseEntity<byte[]> getCarCareReportByPeriod(
			@PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
			@PathVariable("firstDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date firstDate,
			@PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) throws ServiceException {
		String reportName = "CarCareReport_" + userId + "_" + firstDate + "_" + endDate+".pdf";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.add("content-disposition", "attachment; filename=" + reportName);
		return new ResponseEntity<byte[]>(carCareServices.generateCarCareReport(userId, vehicleId, firstDate, endDate), headers, HttpStatus.OK);
	}

	/**
	 * return carCare report by user vehicle by year, PDF
	 * 
	 * @return carCare report by user vehicle by year, PDF
	 * @throws ServiceException
	 */
	@ApiOperation(value = "generate PDF by year -- Return carCare report by user vehicle by year, PDF")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_CARCARE_REPORT_BY_YEAR, produces = "application/pdf")
	public ResponseEntity<byte[]> getCarCarePdfByYear(
			@PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
			@PathVariable("year") @ApiParam(value = "year") String year) throws ServiceException {
		String reportName = "CarCareReport_" + userId + "_" + year+".pdf" ;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.add("content-disposition", "attachment; filename=" + reportName);
		return new ResponseEntity<byte[]>(carCareServices.generateCarCareReportByYear(userId, vehicleId, year,"pdf"), headers, HttpStatus.OK);
	}
	
	/**
	 * return carCare report by user vehicle by month, PDF
	 * 
	 * @return carCare report by user vehicle by month, PDF
	 * @throws ServiceException
	 */
	@ApiOperation(value = "generate PDF by month -- Return carCare report by user vehicle by month, PDF")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_CARCARE_REPORT_BY_MONTH, produces = "application/pdf")
	public ResponseEntity<byte[]> getCarCarePdfByMonth(
			@PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
			@PathVariable("year") @ApiParam(value = "year") String year,
			@PathVariable("month") @ApiParam(value = "month") String month) throws ServiceException {
		String reportName = "CarCareReport_" + userId + "_" + month+ "_" + year+".pdf" ;
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.parseMediaType("application/pdf"));
		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
		headers.add("content-disposition", "attachment; filename=" + reportName);
		return new ResponseEntity<byte[]>(carCareServices.generateCarCareReportByMonth(userId, vehicleId, year, month,"pdf"), headers, HttpStatus.OK);
	}
	
//	/**
//	 * return carCare report by user vehicle by year, XLS
//	 * 
//	 * @return carCare report by user vehicle by year, XLS
//	 * @throws ServiceException
//	 */
//	@ApiOperation(value = "generate XLS by year -- Return carCare report by user vehicle by year, PDF")
//	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_CARCARE_REPORT_BY_YEAR, produces = "application/pdf")
//	public ResponseEntity<byte[]> getCarCareReportByYear(
//			@PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
//			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
//			@PathVariable("year") @ApiParam(value = "year") String year) throws ServiceException {
//		String reportName = "CarCareReport_" + userId + "_" + year ;
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.parseMediaType("application/pdf"));
//		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		headers.add("content-disposition", "attachment; filename=" + reportName);
//		return new ResponseEntity<byte[]>(carCareServices.generateCarCareReportByYear(userId, vehicleId, year,"xls"), headers, HttpStatus.OK);
//	}
//	
//	/**
//	 * return carCare report by user vehicle by month, XLS
//	 * 
//	 * @return carCare report by user vehicle by month, XLS
//	 * @throws ServiceException
//	 */
//	@ApiOperation(value = "generate PDF by month -- Return carCare report by user vehicle by month, PDF")
//	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_CARCARE_REPORT_BY_MONTH, produces = "application/pdf")
//	public ResponseEntity<byte[]> getCarCareReportByMonth(
//			@PathVariable("userId") @ApiParam(value = "User Identifier") String userId,
//			@PathVariable("vehicleId") @ApiParam(value = "Vehicle Identifier") String vehicleId,
//			@PathVariable("year") @ApiParam(value = "year") String year,
//			@PathVariable("month") @ApiParam(value = "month") String month) throws ServiceException {
//		String reportName = "CarCareReport_" + userId + "_" + year ;
//		HttpHeaders headers = new HttpHeaders();
//		headers.setContentType(MediaType.parseMediaType("application/pdf"));
//		headers.setCacheControl("must-revalidate, post-check=0, pre-check=0");
//		headers.add("content-disposition", "attachment; filename=" + reportName);
//		return new ResponseEntity<byte[]>(carCareServices.generateCarCareReportByMonth(userId, vehicleId, year, month,"xls"), headers, HttpStatus.OK);
//	}
//	
	
	/**
	 * create CarCare
	 * 
	 * @return 0 - if CarCare object created - if not (already exists)
	 * @throws ServiceException
	 */
	@ApiOperation(value = "created CarCare. Returns 0 - created, 1 - already exists")
	@RequestMapping(method = RequestMethod.POST, value = ApiController.POST_CARCARE_ENDPOINT, produces = "application/json")
	public ResponseEntity<CarCare> createCarCare(@RequestBody @ApiParam(value = "CarCare to save") CarCare carCare)
			throws ServiceException {
		
		return new ResponseEntity<CarCare>(carCareServices.addCarCare(carCare), HttpStatus.OK);
	}

	/**
	 * Update CarCare
	 * 
	 * @param carCareId
	 *            - id of CarCare
	 * @param updatedCarCare
	 *            - CarCare object
	 * 
	 * @return return CarCare updated or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Update CarCare. Returns CarCare updated or null")
	@RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_CARCARE_ENDPOINT, produces = "application/json")
	public ResponseEntity<CarCare> updateCarCare(@RequestBody @ApiParam(value = "CarCare Object") CarCare updatedCarCare,
			@PathVariable @ApiParam(value = "CarCare Code") String carCareId) {
		
		return new ResponseEntity<CarCare>(carCareServices.updateCarCare(updatedCarCare, carCareId), HttpStatus.OK);
	}

	/**
	 * Delete CarCare
	 * 
	 * @param carCareId
	 *            - id of CarCare
	 * 
	 * @return return CarCare deleted or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Delete CarCare. Returns CarCare deleted or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_CARCARE_ENDPOINT, produces = "application/json")
	public ResponseEntity<CarCare> deleteCarCare(@PathVariable("carCareId") String carCareId) {
		carCareServices.removeCarCare(carCareId);
		
		return new ResponseEntity<CarCare>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all CarCares
	 * 
	 * @return return non content or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Delete all CarCares, return non content or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_CARCARE_ENDPOINT)
	public ResponseEntity<CarCare> deleteAllCarCares() {

		carCareServices.removeAllCarCares();

		return new ResponseEntity<CarCare>(HttpStatus.NO_CONTENT);
	}
	
}
