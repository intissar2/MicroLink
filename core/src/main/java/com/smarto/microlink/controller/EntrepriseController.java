package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.Entreprise;
import com.smarto.microlink.domain.Message;
import com.smarto.microlink.domain.User;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.EntrepriseServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(ApiController.ENTREPRISE_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class EntrepriseController extends GenericController<Entreprise, String> {

    @Autowired
    private EntrepriseServices entrepriseServices;

    @Override
    protected MongoRepository<Entreprise, String> getMongoRepository() {
        return entrepriseServices.getEntrepriseRepository();
    }


  /*  @ApiOperation(value = "Returns a Entreprise list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_ENTREPRISE_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<Entreprise>> getEntreprise() {
        return new ResponseEntity<List<Entreprise>>(entrepriseServices.findEntreprise(), HttpStatus.OK);
    } */


    /* ************** */
    /**
     * create Entreprise
     *
     * @return positive message - if Entreprise object created - if not (already
     * exists) return negative message
     * @throws Exception
     */
    @ApiOperation(value = "created Entreprise. Returns positive message - if Entreprise object created - if not (already exists) return negative message")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_ENTREPRISE_ENDPOINT, produces = "application/json")
    public ResponseEntity<Message> createEntreprise(@RequestBody @ApiParam(value = "Entreprise to save") Entreprise entreprise)
            throws ServiceException {
        return new ResponseEntity<Message>(entrepriseServices.addEntreprise(entreprise), HttpStatus.OK);
    }







    /* ************************************************************** */


    /**
     * Update user
     *
     * @return return user updated or null
     * @throws //ServiceException
     */
  /*  @ApiOperation(value = "Update entreprise. Returns entreprise updated or null")
    @RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_ENTREPRISE_ENDPOINT, produces = "application/json")
    public ResponseEntity<Entreprise> updateEntreprise(@RequestBody @ApiParam(value = "Entreprise Object") Entreprise updatedEntreprise,
                                                       @PathVariable @ApiParam(value = "Entreprise Code") String id) {
        return new ResponseEntity<Entreprise>(entrepriseServices.updateEntreprise(updatedEntreprise, id), HttpStatus.OK);
    }
    */


}
