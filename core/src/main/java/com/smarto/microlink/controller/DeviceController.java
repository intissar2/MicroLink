package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.Device;
import com.smarto.microlink.domain.DeviceMessage;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.DeviceServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping(ApiController.DEVICE_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class DeviceController extends GenericController<Device, String> {

    @Autowired
    private DeviceServices deviceServices;

    @Override
    protected MongoRepository<Device, String> getMongoRepository() {
        return deviceServices.getDeviceRepository();
    }

    /**
     * Find a Trip object by tripId
     *
     * @return Trip object
     */
    /*@ApiOperation(value = "Returns a Trip object ")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIP_BY_ID_ENDPOINT)
	public ResponseEntity<Trip> getTripById(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId, 
			@PathVariable("tripId") @ApiParam(value = "Trip Identifiant") String tripId) {
		return new ResponseEntity<Trip>(deviceServices.findTripById(userId, tripId), HttpStatus.OK);
	}*/

    /**
     * Find a Device list
     *
     * @return Device list
     */
    @ApiOperation(value = "Returns a Device list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_DEVICE_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<Device>> getAllDevices() {

        return new ResponseEntity<List<Device>>(deviceServices.findAllDevices(), HttpStatus.OK);
    }

    /**
     * Find  Device list by operator and releaseDate > Date
     *
     * @return Device list
     */
    @ApiOperation(value = "Returns a Device list by operator and date> releaseDate ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_DEVICE_BY_OPERATOR_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<Device>> getAllDevicesByOperatorAndReleaseDate( @RequestParam("operatorName") @ApiParam(value = "operator Name")String operator,
                                                                               @RequestParam("releaseDate") @ApiParam(value = "release Date") String releaseDate) throws ParseException {

        return new ResponseEntity<List<Device>>(deviceServices.findAllDeviceByOperator(operator,releaseDate), HttpStatus.OK);
    }

    /**
     * Find last Trip
     *
     * @return last Trip
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Returns the last Trip")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_TRIP_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<TripHeader>> getLastTrip(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<List<TripHeader>>(deviceServices.findLastTrip(userId), HttpStatus.OK);
	}*/

    /**
     * Find Trips list last Week
     *
     * @return Trips list last Week
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Returns Trips list last Week")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIPS_LAST_WEEK_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<TripHeader>> getTripsLastWeek(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<List<TripHeader>>(deviceServices.findTripsLastWeek(userId), HttpStatus.OK);
	}*/

    /**
     * Find Trips list last Month
     *
     * @return Trips list last Month
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Returns Trips list last Month")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIPS_LAST_MONTH_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<TripHeader>> getTripsLastMonth(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<List<TripHeader>>(deviceServices.findTripsLastMonth(userId), HttpStatus.OK);
	}*/

    /**
     * Find Trips list last Year
     *
     * @return Trips list last Year
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Returns Trips list last Year")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIPS_LAST_YEAR_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<TripHeader>> getTripsLastYear(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<List<TripHeader>>(deviceServices.findTripsLastYear(userId), HttpStatus.OK);
	}*/

    /**
     * Find last Trips list by day
     *
     * @return last Trips list by day
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Returns last Trips list by day")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_TRIPS_FROM_DATE_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<TripHeader>> getLastTripsFromDate(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
			@PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) throws ServiceException {

		return new ResponseEntity<List<TripHeader>>(deviceServices.findLastTripsFromDate(userId, date), HttpStatus.OK);
	}*/

    /**
     * Find last Trips list by Page
     *
     * @return last Trips list by Page
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Returns last Trips list by Page")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_TRIPS_BY_PAGE_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<TripHeader>> getLastTripsByPage(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId, @PathVariable("pageNumber") int pageNumber,
			@PathVariable("pageSize") int pageSize) throws ServiceException {
		
		return new ResponseEntity<List<TripHeader>>(deviceServices.findLastTripsByPage(userId, pageNumber, pageSize), HttpStatus.OK);
	}*/

    /**
     * Find Trips list between two dates
     *
     * @return Trips list between two dates
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Returns Trips list between two dates")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_TRIPS_BETWEEN_TWO_DATES_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<TripHeader>> getTripsBetweenTwoDates(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
			@PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
			@PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) throws ServiceException {
		
		return new ResponseEntity<List<TripHeader>>(deviceServices.findTripsBetweenTwoDates(userId, startDate, endDate), HttpStatus.OK);
	}*/

    /**
     * create Device
     *
     * @return positive message - if Device object created - if not (already exists) return negative message
     * @throws ServiceException
     */
    @ApiOperation(value = "created Device. Returns positive message - if Device object created - if not (already exists) return negative message")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_DEVICE_ENDPOINT, produces = "application/json")
    public ResponseEntity<DeviceMessage> createDevice(@RequestBody @ApiParam(value = "Device to save") Device device)
            throws ServiceException {

        return new ResponseEntity<DeviceMessage>(deviceServices.addDevice(device), HttpStatus.OK);
    }


    @ApiOperation(value = "Delete Device. Returns Device deleted or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_DEVICE_ENDPOINT)
    public ResponseEntity<Void> deleteTrip(@PathVariable("deviceId") String deviceId) {
        deviceServices.removeDevice(deviceId);
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    /**
     * Update Trip
     *
     * @param tripId
     *            - id of Trip
     * @param trip
     *            - Trip object
     *
     * @return return Trip updated or null
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Update Trip. Returns Trip updated or null")
	@RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_TRIP_ENDPOINT, produces = "application/json")
	public ResponseEntity<Trip> updateTrip(@RequestBody @ApiParam(value = "Trip Object") Trip updatedTrip,
			@PathVariable @ApiParam(value = "Trip Code") String tripId) {
		
		return new ResponseEntity<Trip>(deviceServices.updateTrip(updatedTrip, tripId), HttpStatus.OK);
	}*/

    /**
     * Delete Trip
     *
     * @param TripId
     *            - id of Trip
     *
     * @return return Trip deleted or null
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Delete Trip. Returns Trip deleted or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_TRIP_ENDPOINT)
	public ResponseEntity<Void> deleteTrip(@PathVariable("tripId") String tripId) {
			
		deviceServices.removeTrip(tripId);
		
		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}*/

    /**
     * Delete all Trips
     *
     * @return return non content or null
     * @throws ServiceException
     */
	/*@ApiOperation(value = "Delete all Trips, return non content or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_TRIP_ENDPOINT)
	public ResponseEntity<Trip> deleteAllTrips() {

		deviceServices.removeAllTrips();

		return new ResponseEntity<Trip>(HttpStatus.NO_CONTENT);
	}*/

}
