package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.SupportedPID;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.SupportedPIDServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
// @RequestMapping("/supportedPIDs")
@EnableMongoRepositories("com.smarto.microlink.*")
public class SupportedPIDController {

    @Autowired
    private SupportedPIDServices supportedPIDServices;

    /**
     * Find all SupportedPID
     *
     * @return SupportedPID list
     */
    @ApiOperation(value = "Returns a SupportedPID list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_SUPPORTEDPID_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<SupportedPID>> getAllSupportedPIDs() {
        return new ResponseEntity<List<SupportedPID>>(supportedPIDServices.findAllSupportedPIDs(), HttpStatus.OK);
    }

    /**
     * Find a SupportedPID object by Code
     *
     * @return SupportedPID object
     */
    @ApiOperation(value = "Returns a SupportedPID object ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_SUPPORTEDPID_BY_CODE_ENDPOINT, produces = "application/json")
    public ResponseEntity<SupportedPID> getSupportedPIDByCode(
            @PathVariable(value = "code") @ApiParam(value = "SupportedPID Code") String code) {
        return new ResponseEntity<SupportedPID>(supportedPIDServices.findSupportedPIDByCode(code), HttpStatus.OK);
    }

    /**
     * Find a SupportedPID list by Code List
     *
     * @return SupportedPID list
     * @throws Exception
     */
    @ApiOperation(value = "Returns a SupportedPID list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LIST_SUPPORTEDPID_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<SupportedPID>> getListSupportedPID(
            @PathVariable(value = "codeList") @ApiParam(value = "SupportedPID Code list") List<String> codeList) {

        return new ResponseEntity<List<SupportedPID>>(supportedPIDServices.findListSupportedPID(codeList),
                HttpStatus.OK);
    }

    /**
     * add a new SupportedPID object
     *
     * @return SupportedPID object
     */
    @ApiOperation(value = "Returns a SupportedPID object ")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_SUPPORTEDPID_ENDPOINT, produces = "application/json")
    public ResponseEntity<SupportedPID> createSupportedPID(
            @RequestBody @ApiParam(value = "SupportedPID object") SupportedPID supportedPID) {

        return new ResponseEntity<SupportedPID>(supportedPIDServices.addSupportedPID(supportedPID), HttpStatus.OK);
    }

    /**
     * Update a SupportedPID object by Code
     *
     * @return SupportedPID object
     */
    @ApiOperation(value = "Returns a SupportedPID object ")
    @RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_SUPPORTEDPID_ENDPOINT)
    public ResponseEntity<SupportedPID> updateSupportedPID(
            @RequestBody @ApiParam(value = "SupportedPID Object") SupportedPID updatedSupportedPID,
            @PathVariable @ApiParam(value = "SupportedPID Code") String id) {

        return new ResponseEntity<SupportedPID>(supportedPIDServices.updateSupportedPID(updatedSupportedPID, id),
                HttpStatus.OK);
    }

    /**
     * Delete a SupportedPID object by Code
     *
     * @return SupportedPID object
     */
    @ApiOperation(value = "Returns a SupportedPID object ")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_SUPPORTEDPID_ENDPOINT, produces = "application/json")
    public ResponseEntity<SupportedPID> deleteSupportedPID(
            @PathVariable @ApiParam(value = "SupportedPID Code") String id) {

        return new ResponseEntity<SupportedPID>(supportedPIDServices.removeSupportedPID(id), HttpStatus.NO_CONTENT);
    }

    /**
     * Delete all SupportedPIDs
     *
     * @return return non content or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Delete all SupportedPIDs, return non content or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_SUPPORTEDPID_ENDPOINT)

    public ResponseEntity<SupportedPID> deleteAllSupportedPID() {

        supportedPIDServices.removeAllSupportedPIDs();

        return new ResponseEntity<SupportedPID>(HttpStatus.NO_CONTENT);
    }

}
