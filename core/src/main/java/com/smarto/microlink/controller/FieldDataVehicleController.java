package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.FieldDataVehicle;
import com.smarto.microlink.domain.Vehicle;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.FieldDataVehicleServices;
import com.smarto.microlink.service.VehicleServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@RestController
//@RequestMapping("/fieldDataVehicles")
@EnableMongoRepositories("com.smarto.microlink.*")
public class FieldDataVehicleController {

    @Autowired
    private FieldDataVehicleServices fieldDataVehicleServices;
    @Autowired
    private VehicleServices vehicleServices;

    /**
     * Find a FieldDataVehicle object by id
     *
     * @return FieldDataVehicle object
     */
    @ApiOperation(value = "Returns a FieldDataVehicle object")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_FIELD_DATA_VEHICLE_BY_ID_ENDPOINT)
    public ResponseEntity<FieldDataVehicle> getFieldDataVehicleById(@PathVariable("id") @ApiParam(value = "FieldDataVehicle Identifiant") String id) {
        return new ResponseEntity<FieldDataVehicle>(fieldDataVehicleServices.findFieldDataVehicleById(id), HttpStatus.OK);
    }

    /**
     * Find all FieldDataVehicles
     *
     * @return FieldDataVehicle list
     */
    @ApiOperation(value = "Returns a FieldDataVehicle list ")
    @RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_FIELD_DATA_VEHICLES_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<FieldDataVehicle>> getAllFieldDataVehicles() {
        return new ResponseEntity<List<FieldDataVehicle>>(fieldDataVehicleServices.findAllFieldDataVehicles(), HttpStatus.OK);
    }

    /**
     * add a new FieldDataVehicle object
     *
     * @return FieldDataVehicle object
     */
    @ApiOperation(value = "Returns a FieldDataVehicle object ")
    @RequestMapping(method = RequestMethod.POST, value = ApiController.POST_FIELD_DATA_VEHICLE_ENDPOINT, produces = "application/json")
    public ResponseEntity<Vehicle> createFieldDataVehicle(@RequestBody @ApiParam(value = "FieldDataVehicle object") FieldDataVehicle fieldDataVehicle) throws NoSuchAlgorithmException, UnsupportedEncodingException, ServiceException {
        fieldDataVehicleServices.addFieldDataVehicle(fieldDataVehicle);
        Vehicle vehicle = vehicleServices.getVehicleByVin(fieldDataVehicle);

        return new ResponseEntity<Vehicle>(vehicle, HttpStatus.OK);
    }

    /**
     * Update a FieldDataVehicle object by Code
     *
     * @return FieldDataVehicle object
     */
    @ApiOperation(value = "Returns a FieldDataVehicle object ")
    @RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_FIELD_DATA_VEHICLE_ENDPOINT)
    public ResponseEntity<FieldDataVehicle> updateFieldDataVehicle(
            @RequestBody @ApiParam(value = "FieldDataVehicle Object") FieldDataVehicle updatedFieldDataVehicle,
            @PathVariable @ApiParam(value = "FieldDataVehicle id") String id) {

        return new ResponseEntity<FieldDataVehicle>(fieldDataVehicleServices.updateFieldDataVehicle(updatedFieldDataVehicle, id), HttpStatus.OK);
    }

    /**
     * Delete a FieldDataVehicle object by Id
     *
     * @return FieldDataVehicle object
     */
    @ApiOperation(value = "Returns a FieldDataVehicle object ")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_FIELD_DATA_VEHICLE_ENDPOINT, produces = "application/json")
    public ResponseEntity<FieldDataVehicle> deleteFieldDataVehicle(@PathVariable @ApiParam(value = "FieldDataVehicle Id") String id) {

        return new ResponseEntity<FieldDataVehicle>(fieldDataVehicleServices.removeFieldDataVehicle(id), HttpStatus.NO_CONTENT);
    }

    /**
     * Delete all FieldDataVehicles
     *
     * @return return non content or null
     * @throws ServiceException
     */
    @ApiOperation(value = "Delete all FieldDataVehicles, return non content or null")
    @RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_FIELD_DATA_VEHICLE_ENDPOINT, produces = "application/json")
    public ResponseEntity<List<FieldDataVehicle>> deleteAllFieldDataVehicles() {
        fieldDataVehicleServices.removeAllFieldDataVehicles();
        return new ResponseEntity<List<FieldDataVehicle>>(HttpStatus.NO_CONTENT);
    }
}
