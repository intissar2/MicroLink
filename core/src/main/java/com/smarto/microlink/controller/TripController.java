package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.Score;
import com.smarto.microlink.domain.Trip;
import com.smarto.microlink.domain.TripHeader;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.service.TripServices;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@RestController
@RequestMapping(ApiController.TRIP_ENDPOINT)
@EnableMongoRepositories("com.smarto.microlink.*")
public class TripController extends GenericController<Trip, String> {

	@Autowired
	private TripServices tripServices;

	@Override
	protected MongoRepository<Trip, String> getMongoRepository() {
		return tripServices.getTripRepository();
	}

	/**
	 * Find a Trip object by tripId
	 *
	 * @return Trip object
	 */
	@ApiOperation(value = "Returns a Trip object ")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIP_BY_ID_ENDPOINT)
	public ResponseEntity<Trip> getTripById(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
											@PathVariable("tripId") @ApiParam(value = "Trip Identifiant") String tripId) {
		return new ResponseEntity<Trip>(tripServices.findTripById(userId, tripId), HttpStatus.OK);
	}


	/**
	 * Find a Trip object by tripId
	 *
	 * @return Trip object
	 */
	@ApiOperation(value = "Returns a Trip object ")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIP_BY_ID_ENDPOINT2)
	public ResponseEntity<Trip> getTripById2(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
											 @PathVariable("tripId") @ApiParam(value = "Trip Identifiant") String tripId) {
		return new ResponseEntity<Trip>(tripServices.findTripById2(userId, tripId), HttpStatus.OK);
	}

	/**
	 * Find a TripHeader list
	 *
	 * @return TripHeader list
	 */
	@ApiOperation(value = "Returns a Trip list ")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_TRIP_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getAllTrips(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		//return new ResponseEntity<List<TripHeader>>(tripServices.findAllTrips(userId), HttpStatus.OK);
		return new ResponseEntity<List<Trip>>(tripServices.findAllTrips2(userId), HttpStatus.OK);
	}

	/**
	 * Find last Trip
	 *
	 * @return last Trip
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns the last Trip")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_TRIP_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getLastTrip(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		//return new ResponseEntity<List<TripHeader>>(tripServices.findLastTrip(userId), HttpStatus.OK);
		return new ResponseEntity<List<Trip>>(tripServices.findLastTrip2(userId), HttpStatus.OK);
	}

	/**
	 * Find Trips list last Week
	 *
	 * @return Trips list last Week
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Trips list last Week")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIPS_LAST_WEEK_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getTripsLastWeek(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		//return new ResponseEntity<List<TripHeader>>(tripServices.findTripsLastWeek(userId), HttpStatus.OK);

		return new ResponseEntity<List<Trip>>(tripServices.findTripsLastWeek2(userId), HttpStatus.OK);

	}

	/**
	 * Find Trips list of Current Week
	 *
	 * @return Trips list of Current Week
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Trips list of Current Week")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIPS_CURRENT_WEEK_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getTripsCurrentWeek(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		//return new ResponseEntity<List<TripHeader>>(tripServices.findTripsCurrentWeek(userId), HttpStatus.OK);
		return new ResponseEntity<List<Trip>>(tripServices.findTripsCurrentWeek2(userId), HttpStatus.OK);
	}

	/**
	 * Find Trips list last Month
	 *
	 * @return Trips list last Month
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Trips list last Month")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIPS_LAST_MONTH_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getTripsLastMonth(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {


		//	return new ResponseEntity<List<TripHeader>>(tripServices.findTripsLastMonth(userId), HttpStatus.OK);

		return new ResponseEntity<List<Trip>>(tripServices.findTripsLastMonth2(userId), HttpStatus.OK);
	}

	/**
	 * Find Trips list last Year
	 *
	 * @return Trips list last Year
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Trips list last Year")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_TRIPS_LAST_YEAR_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getTripsLastYear(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		//return new ResponseEntity<List<TripHeader>>(tripServices.findTripsLastYear(userId), HttpStatus.OK);
		return new ResponseEntity<List<Trip>>(tripServices.findTripsLastYear2(userId), HttpStatus.OK);
	}

	/**
	 * Find last Trips list by day
	 *
	 * @return last Trips list by day
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns last Trips list by day")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_TRIPS_FROM_DATE_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getLastTripsFromDate(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
														   @PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) throws ServiceException {


		//return new ResponseEntity<List<TripHeader>>(tripServices.findLastTripsFromDate(userId, date), HttpStatus.OK);
		return new ResponseEntity<List<Trip>>(tripServices.findLastTripsFromDate2(userId, date), HttpStatus.OK);
	}

	/**
	 * Find last Trips list by Page
	 *
	 * @return last Trips list by Page
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns last Trips list by Page")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_TRIPS_BY_PAGE_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<TripHeader>> getLastTripsByPage(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId, @PathVariable("pageNumber") int pageNumber,
															   @PathVariable("pageSize") int pageSize) throws ServiceException {

		return new ResponseEntity<List<TripHeader>>(tripServices.findLastTripsByPage(userId, pageNumber, pageSize), HttpStatus.OK);
	}

	/**
	 * Find Trips list between two dates
	 *
	 * @return Trips list between two dates
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Trips list between two dates")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_TRIPS_BETWEEN_TWO_DATES_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getTripsBetweenTwoDates(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
															  @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
															  @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) throws ServiceException {
		//return new ResponseEntity<List<TripHeader>>(tripServices.findTripsBetweenTwoDates(userId, startDate, endDate), HttpStatus.OK);
		return new ResponseEntity<List<Trip>>(tripServices.findTripsBetweenTwoDates2(userId, startDate, endDate), HttpStatus.OK);
	}

	/**
	 * create Trip
	 *
	 * @return 0 - if Trip object created - if not (already exists)
	 * @throws ServiceException
	 */
	@ApiOperation(value = "created Trip. Returns 0 - created, 1 - already exists")
	@RequestMapping(method = RequestMethod.POST, value = ApiController.POST_TRIP_ENDPOINT, produces = "application/json")
	public ResponseEntity<Trip> createTrip(@RequestBody @ApiParam(value = "Trip to save") Trip trip,
										   @PathVariable @ApiParam(value = "Trip period between Ti & Ti-1") String period,
										   @PathVariable @ApiParam(value = "Trip minimum period to incremente nbStops") String minPeriodStops)
			throws ServiceException {
		return new ResponseEntity<Trip>(tripServices.addTrip(trip, period, minPeriodStops), HttpStatus.OK);
	}

	/**
	 * Update Trip
	 *
	 * @param tripId - id of Trip
	 * @param tripId - Trip object
	 * @return return Trip updated or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Update Trip. Returns Trip updated or null")
	@RequestMapping(method = RequestMethod.PUT, value = ApiController.PUT_TRIP_ENDPOINT, produces = "application/json")
	public ResponseEntity<Trip> updateTrip(@RequestBody @ApiParam(value = "Trip Object") Trip updatedTrip,
										   @PathVariable @ApiParam(value = "Trip Code") String tripId) {

		return new ResponseEntity<Trip>(tripServices.updateTrip(updatedTrip, tripId), HttpStatus.OK);
	}

	/**
	 * Delete Trip
	 *
	 * @param tripId - id of Trip
	 * @return return Trip deleted or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Delete Trip. Returns Trip deleted or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_TRIP_ENDPOINT)
	public ResponseEntity<Void> deleteTrip(@PathVariable("tripId") String tripId) {

		tripServices.removeTrip(tripId);

		return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
	}

	/**
	 * Delete all Trips
	 *
	 * @return return non content or null
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Delete all Trips, return non content or null")
	@RequestMapping(method = RequestMethod.DELETE, value = ApiController.DELETE_ALL_TRIP_ENDPOINT)
	public ResponseEntity<Trip> deleteAllTrips() {

		tripServices.removeAllTrips();

		return new ResponseEntity<Trip>(HttpStatus.NO_CONTENT);
	}

	/**
	 * get Score last Trip
	 *
	 * @return Score last Trip
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score of last Trip")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_SCORETRIP_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreLastTrip(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreLastTrip(userId), HttpStatus.OK);
	}

	/**
	 * get Score trips list last Week
	 *
	 * @return Score trips list last Week
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score Trips last Week")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_SCORETRIPS_LAST_WEEK_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreTripsLastWeek(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreTripsLastWeek(userId), HttpStatus.OK);
	}

	/**
	 * get Score Trips of Current Week
	 *
	 * @return get Score Trips of Current Week
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score Trips of Current Week")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_SCORETRIPS_CURRENT_WEEK_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreTripsCurrentWeek(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreTripsCurrentWeek(userId), HttpStatus.OK);
	}

	/**
	 * get Score Trips last Month
	 *
	 * @return Score Trips last Month
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score Trips last Month")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_SCORETRIPS_LAST_MONTH_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreTripsLastMonth(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreTripsLastMonth(userId), HttpStatus.OK);
	}

	/**
	 * get Score Trips last Year
	 *
	 * @return Score Trips last Year
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score Trips last Year")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_SCORETRIPS_LAST_YEAR_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreTripsLastYear(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreTripsLastYear(userId), HttpStatus.OK);
	}

	/**
	 * get Score last Trips by day
	 *
	 * @return Score last Trips by day
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score last Trips by day")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_SCORETRIPS_FROM_DATE_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreLastTripsFromDate(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
														   @PathVariable("date") @DateTimeFormat(pattern = "yyyy-MM-dd") Date date) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreLastTripsFromDate(userId, date), HttpStatus.OK);
	}

	/**
	 * Find Score last Trips by Page
	 *
	 * @return Score last Trips by Page
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score last Trips by Page")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_SCORETRIPS_BY_PAGE_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreLastTripsByPage(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId, @PathVariable("pageNumber") int pageNumber,
														 @PathVariable("pageSize") int pageSize) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreLastTripsByPage(userId, pageNumber, pageSize), HttpStatus.OK);
	}

	/**
	 * Find Score Trips between two dates
	 *
	 * @return Score Trips between two dates
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score Trips between two dates")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_LAST_SCORETRIPS_BETWEEN_TWO_DATES_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreTripsBetweenTwoDates(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId,
															  @PathVariable("startDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date startDate,
															  @PathVariable("endDate") @DateTimeFormat(pattern = "yyyy-MM-dd") Date endDate) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreTripsBetweenTwoDates(userId, startDate, endDate), HttpStatus.OK);
	}

	/**
	 * get Score Trips current Year
	 *
	 * @return Score Trips current Year
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score Trips current Year")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_SCORETRIPS_CURRENT_YEAR_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreTripsCurrentYear(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreTripsCurrentYear(userId), HttpStatus.OK);
	}

	/**
	 * get Score Trips current Month
	 *
	 * @return Score Trips current Month
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score Trips current Month")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_SCORETRIPS_CURRENT_MONTH_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreTripsCurrentMonth(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreTripsCurrentMonth(userId), HttpStatus.OK);
	}


	/**
	 * get Score Trips current Day
	 *
	 * @return Score Trips current Day
	 * @throws ServiceException
	 */
	@ApiOperation(value = "Returns Score Trips current Day")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_SCORETRIPS_CURRENT_DAY_ENDPOINT, produces = "application/json")
	public ResponseEntity<Score> getScoreTripsCurrentDay(@PathVariable("userId") @ApiParam(value = "User Identifiant") String userId) throws ServiceException {

		return new ResponseEntity<Score>(tripServices.getScoreTripsCurrentDay(userId), HttpStatus.OK);
	}

	/* ******************************************************************** */

	/**
	 * Find a TripHeader list
	 *
	 * @return TripHeader list
	 */
	@ApiOperation(value = "Returns a Trip list ")
	@RequestMapping(method = RequestMethod.GET, value = ApiController.GET_ALL_USERS_TRIP_ENDPOINT, produces = "application/json")
	public ResponseEntity<List<Trip>> getAllUsersTrips() throws ServiceException {

		//return new ResponseEntity<List<TripHeader>>(tripServices.findAllTrips(userId), HttpStatus.OK);
		return new ResponseEntity<List<Trip>>(tripServices.findUsersTrip(), HttpStatus.OK);
	}


}


