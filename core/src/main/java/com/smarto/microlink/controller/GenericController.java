package com.smarto.microlink.controller;

import com.smarto.microlink.api.ApiController;
import com.smarto.microlink.domain.PatchOperation;
import com.smarto.microlink.exception.ServiceException;
import com.smarto.microlink.utils.FieldHelper;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.web.bind.annotation.*;

import java.io.Serializable;
import java.util.List;

/**
 * Created by AAH on 26/12/2016.
 */

@RestController
abstract class GenericController<T, U extends Serializable> {

    private final static Logger LOGGER = LoggerFactory.getLogger(GenericController.class);

    @Autowired
    private FieldHelper fieldHelper;

    protected abstract MongoRepository<T, U> getMongoRepository();

    @ApiOperation(value = "patch data")
    @RequestMapping(method = RequestMethod.PATCH, value = ApiController.PATCH_BY_ID_ENDPOINT, consumes = "application/json")
    public T patch(@RequestBody List<PatchOperation> patchOperations, @PathVariable("id") U id) throws ServiceException {
        T in = getMongoRepository().findOne(id);
        return _patch(in, patchOperations);
    }

    public T _patch(T in, List<PatchOperation> patchOperations) throws ServiceException {
        for (PatchOperation op : patchOperations) {
            switch (op.getOp()) {
                case replace:
                    opReplace(in, op);
                    break;
                case add:
                    opAdd(in, op);
                    break;
                default:
                    LOGGER.error("not implemented yet");
            }
        }
        return in;
    }

    private T opReplace(T obj, PatchOperation patchOperation) throws ServiceException {
        fieldHelper.getField(obj.getClass(), patchOperation.getPath());

        fieldHelper.setFieldValue(obj, patchOperation.getPath(), patchOperation.getValue());
        getMongoRepository().save(obj);
        return obj;
    }

    private T opAdd(T obj, PatchOperation patchOperation) throws ServiceException {
        fieldHelper.getField(obj.getClass(), patchOperation.getPath());

        fieldHelper.setFieldValue(obj, patchOperation.getPath(), patchOperation.getValue());
        getMongoRepository().save(obj);
        return obj;
    }

}
