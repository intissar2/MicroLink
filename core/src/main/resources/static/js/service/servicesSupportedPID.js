App.factory('SupportedPID', ['$resource', function ($resource) {

    return $resource(
        'http://149.202.165.16:8080/api/v1/supportedPIDs/:id',
        {id: '@id'},
        {
            update: {
                method: 'PUT' // To send the HTTP Put request when calling this custom update method.
            }
        },
        {
            stripTrailingSlashes: false
        }
    );

}]);