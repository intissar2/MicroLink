var App = angular.module('myApp', ['ngResource', 'ngRoute', 'uiGmapgoogle-maps']);

App.config(['$routeProvider', '$httpProvider', function ($routeProvider, $httpProvider) {
    $routeProvider.when('/', {
        templateUrl: 'partials/home.html',
        controllerAs: 'FirmwareController'
        // templateUrl: 'login.html',
    }).when('/login', {
        templateUrl: 'partials/login.html',
    }).when('/signIn', {
        templateUrl: 'signIn.html',
    }).when('/resetPassword', {
        templateUrl: 'resetPassword.html',
    }).when('/dashboard', {
        templateUrl: 'dashboard.html',
    }).when('/firmwares', {
        templateUrl: 'partials/firmwaresPage.html',
        controller: 'FirmwareController',
        controllerAs: 'FirmwareController'
    }).when('/attributes', {
        templateUrl: 'partials/attributesPage.html',
        controller: 'FirmwareController',
        controllerAs: 'FirmwareController'
    }).when('/troublecodes', {
        templateUrl: 'partials/troubleCodesPage.html',
        controller: 'TroubleCodeController',
        controllerAs: 'troubleCodesPage'
    }).when('/supportedPIDs', {
        templateUrl: 'partials/supportedPIDsPage.html',
        controller: 'SupportedPIDController',
        controllerAs: 'supportedPIDsPage'
//	}).when('/users', {
//		templateUrl : 'partials/usersPage.html',
//		controller : 'UserController',
//		controllerAs: 'UserController'
    }).when('/about', {
        templateUrl: 'partials/about.html',
        controller: 'AboutController',
        controllerAs: 'AboutController'
    }).otherwise({
        redirectTo: '/'
    });

    $httpProvider.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
}]);