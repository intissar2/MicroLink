'use strict';

App.controller('SupportedPIDController', [
    '$scope',
    'SupportedPID',
    function ($scope, SupportedPID) {

        var self = this;

        self.supportedPID = new SupportedPID();
        self.supportedPIDs = [];

        self.fetchAllSupportedPIDs = function () {
            self.supportedPIDs = SupportedPID.query();
        };

        self.createSupportedPID = function () {
            console.log(self.supportedPID);
            self.supportedPID.$save(function () {
                console.log('Saving New SupportedPID', self.supportedPID);
                self.fetchAllSupportedPIDs();
                self.reset();
            });
        };

        self.updateSupportedPID = function (identity) {
            self.supportedPID.$update({
                id: identity
            }, function () {
                self.fetchAllSupportedPIDs();
                self.reset();
            });
        };

        self.deleteSupportedPID = function (identity) {
            self.supportedPID.$delete({
                id: identity
            }, function () {
                console.log('Deleting SupportedPID with id ', identity);
                self.fetchAllSupportedPIDs();
                self.reset();
            });
        };

        self.fetchAllSupportedPIDs();

        self.submit = function () {
            if (self.supportedPID.id == null) {
                console.log('Saving New SupportedPID', self.supportedPID);
                self.createSupportedPID();
            } else {
                console.log('Upddating SupportedPID with id ', self.supportedPID.id);
                self.updateSupportedPID(self.supportedPID.id);
                console.log('SupportedPID updated with id ', self.supportedPID.id);
            }
            self.reset();
        };

        self.edit = function (id) {
            console.log('id to be edited', id);
            for (var i = 0; i < self.supportedPIDs.length; i++) {
                if (self.supportedPIDs[i].id == id) {
                    self.supportedPID = angular.copy(self.supportedPIDs[i]);
                    break;
                }
            }
        };

        self.remove = function (id) {
            console.log('id to be deleted', id);
            for (var i = 0; i < self.supportedPIDs.length; i++) {
                if (self.supportedPIDs[i].id == id) {
                    console.log('id to be deleted', id);
                    self.deleteSupportedPID(id);
                    break;
                }
            }
        };

        self.reset = function () {
            self.supportedPID = new SupportedPID();
            $scope.myForm.$setPristine(); // reset Form
        };

    }]);