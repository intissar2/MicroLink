'use strict';

App.controller('AttributController', [
    '$scope',
    'Firmware',
    function ($scope, Firmware) {

        var self = this;

        self.attribute = {
            key: null,
            nom: '',
            type: '',
            value: ''
        };

        self.attributes = [];

        self.listFields = [];

        self.firmware = new Firmware();

        self.firmwares = [];

        // c bn
        self.addAttribut = function () {
            console.log("addAttribut() is called");

            self.attribute.key = self.firmwares[0].listFields.length++;
            self.firmwares = Firmware.query(function () {
                angular.forEach(self.firmwares, function (tmp) {
                    tmp.listFields.push(self.attribute);
                    tmp.$update(function () {
                        self.fetchAllAttributs();
                        self.resetAttribut();
                    });
                });
            });
        };

        // c bn
        self.fetchAllAttributs = function () {
            self.firmwares = Firmware.query(function () {
                if (self.firmwares == null || self.firmwares.length == 0) {
                    self.listFields.push(self.attribute);
                    self.attributes = self.listFields;
                } else {
                    if (self.firmwares[0].listFields == null
                        || self.firmwares[0].listFields.length == 0) {
                        self.listFields.push(self.attribute);
                        self.attributes = self.listFields;
                    } else {
                        self.listFields = self.firmwares[0].listFields;
                        self.attributes = self.listFields;
                    }

                }
            });
        };

        self.fetchAllAttributs();
        // c bn
        self.updateAttribut = function (key) {
            if (key == null) {
                console.log("key egale null");
            } else {
                self.firmwares = Firmware.query(function () {
                    angular.forEach(self.firmwares, function (tmpFirm) {
                        angular.forEach(tmpFirm.listFields, function (tmp) {
                            if (tmp.key == key) {
                                console.log("Update attribute");
                                tmp.nom = self.attribute.nom;
                                tmp.type = self.attribute.type;
                                tmpFirm.$update(function () {
                                    self.fetchAllAttributs();
                                    self.resetAttribut();
                                });
                                console.log("Attribute updated");
                            }
                        });
                    });
                });
            }
        };
        // c bn
        self.deleteAttribut = function (key) {
            var i = 0;
            self.firmwares = Firmware.query(function () {
                angular.forEach(self.firmwares, function (tmpFirm) {
                    angular.forEach(tmpFirm.listFields, function (tmp) {
                        if (tmp.key == key) {
                            console.log("Delete attribute");
                            tmpFirm.listFields.splice(i, 1);
                            tmpFirm.$update(function () {
                                self.fetchAllAttributs();
                                self.resetAttribut();
                            });
                            console.log("Attribute deleted");
                        }
                        i++;
                    });
                });
            });
        };

        self.isAttributeExistlistFields = function (nom) {
            var test = false;
            angular.forEach(self.firmwares[0].listFields, function (tmp) {
                if (tmp.nom == nom) {
                    test = true;
                }
            });
            return test;
        }
        // pas encore
        self.isAttributeExist = function (nom) {
            if (!angular.equals("version", nom)
                && !angular.equals("type", nom)
                && !angular.equals("creationDate", nom)
                && !angular.equals("checksum", nom)) {
                if (self.firmwares[0].listFields == null
                    || self.firmwares[0].listFields.length == 0) {
                    console.log("niv 0");
                    return false;
                } else {
                    if (self.isAttributeExistlistFields(nom)) {
                        console.log("niv 1");

                        return true;
                    } else {
                        console.log("niv 2");
                        return false;
                    }
                }
            } else {
                console.log("niv 3");
                return true;
            }
        }
        // c bn
        self.submitAttribut = function (key) {
            console.log("submitAttribut() is called");
            if (self.attribute.key == null && !self.isAttributeExist(self.attribute.nom)) {
                console.log('Saving New Attribute ', self.attribute);
                self.addAttribut();
                console.log('The new Attribute is Saved ', self.attribute);
            } else {
                console.log('Updating Attribute with nom ', self.attribute.nom);
                self.updateAttribut(self.attribute.key);
                console.log('Attribute updated with nom ', self.attribute.nom);
            }
        };
        // c bn
        self.editAttribut = function (key) {
            console.log('Attribute to be edited', key);
            for (var i = 0; i < self.firmwares[0].listFields.length; i++) {
                if (self.firmwares[0].listFields[i].key == key) {
                    self.attribute = angular.copy(self.firmwares[0].listFields[i]);
                    break;
                }
            }
        };

        self.removeAttribut = function (key) {
            console.log('id to be deleted', key);
            for (var i = 0; i < self.firmwares[0].listFields.length; i++) {
                if (self.firmwares[0].listFields[i].key == key) {
                    self.deleteAttribut(key);
                    break;
                }
            }
        };

        self.resetAttribut = function () {
            self.firmware = new Firmware();
            self.attribute = {};
            $scope.myForm.$setPristine(); // reset Form
        };

    }]);
