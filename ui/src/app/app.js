'use strict';

angular.module('CarAlgoPro', [
    'ngTableToCsv',
    'ngAnimate',
    'ui.bootstrap',
    'ui.sortable',
    'ui.router',
    'permission',
    'permission.ui',
    'ngTouch',
    'toastr',
    'smart-table',
    "xeditable",
    'ui.slimscroll',
    'ngJsTree',
    'angular-progress-button-styles',

    'CarAlgoPro.theme',
    'CarAlgoPro.pages'
]);