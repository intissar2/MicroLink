/**
 * @author v.lugovsky
 * created on 15.12.2015
 */
(function () {
  'use strict';

  angular.module('CarAlgoPro.theme', [
      'toastr',
      'chart.js',
      'angular-chartist',
      'angular.morris-chart',
      'textAngular',
      'CarAlgoPro.theme.components',
      'CarAlgoPro.theme.inputs'
  ]);

})();
