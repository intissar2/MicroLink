/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('CarAlgoPro.theme.components')
      .directive('pageTop', pageTop);

  /** @ngInject */
  function pageTop() {
    return {
      restrict: 'E',
      templateUrl: 'app/theme/components/pageTop/pageTop.html',
       // controller: 'roleCtrl',
        link: function($scope) {
            $scope.$watch(function () {
                $scope.name = "intissar";

                 $scope.roles = localStorage.dataRole;
                
            });
        }
    };
  }

})();