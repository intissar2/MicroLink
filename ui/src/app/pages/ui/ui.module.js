/**
 * @author k.danovsky
 * created on 12.01.2016
 */
(function() {
  'use strict';

  angular.module('CarAlgoPro.pages.ui', [
      'CarAlgoPro.pages.ui.typography',
      'CarAlgoPro.pages.ui.buttons',
      'CarAlgoPro.pages.ui.icons',
      'CarAlgoPro.pages.ui.modals',
      'CarAlgoPro.pages.ui.grid',
      'CarAlgoPro.pages.ui.alerts',
      'CarAlgoPro.pages.ui.progressBars',
      'CarAlgoPro.pages.ui.notifications',
      'CarAlgoPro.pages.ui.tabs',
      'CarAlgoPro.pages.ui.slider',
      'CarAlgoPro.pages.ui.panels',
    ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.ui', {
        url: '/ui',
        template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'UI Features',
        sidebarMeta: {
          icon: 'ion-android-laptop',
          order: 200,
        },
        authenticate: true
      });
  }

})();