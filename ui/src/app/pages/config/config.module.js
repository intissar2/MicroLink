(function() {
  'use strict';

  angular.module('CarAlgoPro.pages.config', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig() {
          var config = {
              apiKey: "AIzaSyAgx-shJAJEDL3bXlhT-ZgsUX_1LQbtqK0",
              authDomain: "caralgo-4a988.firebaseapp.com",
              databaseURL: "https://caralgo-4a988.firebaseio.com",
              projectId: "caralgo-4a988",
              storageBucket: "caralgo-4a988.appspot.com",
              messagingSenderId: "168858165154"
          };

          firebase.initializeApp(config);
  }

})();