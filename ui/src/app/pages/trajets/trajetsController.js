(function(){
    'use strict'
    angular.module("CarAlgoPro.pages.trajets")
        .controller('trajetsController', trajetsController);
    trajetsController.$inject = ['TrajetsService', '$stateParams', '$scope', '$rootScope'];

    function trajetsController(TrajetsService, $stateParams, $scope, $rootScope){
        var trajetsCtrl = this;
        trajetsCtrl.list = list;

        trajetsCtrl.userId = $stateParams.userId;
        trajetsCtrl.tripId = $stateParams.tripId;

        trajetsCtrl.list();


        function list(){
            TrajetsService.list().success(function(response){
                trajetsCtrl.Lists = response;
                var totalDistance = '';
                console.log("Liste des trajets",response);

                $scope.getTotal = function(){
                    var total = 0;
                    for(var i = 0; i < response.length; i++){
                        var distance = response[i].tripResult.totalDistance;
                        total += distance;
                    }
                    return total;
                };

            })
        }

    }

})();