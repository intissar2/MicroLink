/**
 * @author: intissar CHOUAIEB
 */
(function () {
    'use strict';

    angular.module('CarAlgoPro.pages.trajets', ['permission'])
        .config(routeConfig);


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.trajets', {
                url: '/trajets?userId',
                templateUrl: 'app/pages/trajets/trajets.html',
                title: 'Trajets',
                controller: 'trajetsController as trajetsCtrl',
                sidebarMeta: {
                    icon: 'ion-android-map',
                    order: 2,
                },
                restrict: 'E',
                bindings: {
                    user: '<'
                },
                authenticate: true,
                //  role: ["User"]
                data: {
                    role: {
                        only: ["Admin"]
                    }
                }

            });

        //  }

    }

})();

