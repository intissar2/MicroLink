(function () {
    'use strict';

    angular
        .module("CarAlgoPro.pages.trajets")
        .factory('TrajetsService', TrajetsService);

    function TrajetsService($http) {
        var TrajetsService = {};

        TrajetsService.list = list;


        return TrajetsService;

        function list() {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };

            return $http
                //.get('http://149.202.165.16:8080/api/v1/trips/allUserTrip', config);
                .get('http://localhost:8088/api/v1/trips/allUsersTrip/show', config);

        };



    }
})();