/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
  'use strict';

  angular.module('CarAlgoPro.pages.dashboard', [])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.dashboard', {
        url: '/dashboard?userId?connectedUser',
        templateUrl: 'app/pages/dashboard/dashboard.html',
        title: 'Tableau de bord',
        sidebarMeta: {
          icon: 'ion-android-home',
          order: 0,
        },
        authenticate: true,
        role: ['simpleUser']

      });
  }

})();