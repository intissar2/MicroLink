(function () {
    'use strict';

    angular
        .module("CarAlgoPro.pages.trip")
        .factory('UsersPositionsService', UsersPositionsService);

    function UsersPositionsService($http, $rootScope, localStorage) {

        var UsersPosUserServicesitionsService = {};

        UsersPositionsService.listOfVehicules = listOfVehicules;
        UsersPositionsService.getVehiculesRun = getVehiculesRun;
        UsersPositionsService.getVehiculesProblem = getVehiculesProblem;
        UsersPositionsService.getVehiculesRealTime = getVehiculesRealTime;

        return UsersPositionsService;

        function listOfVehicules() {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            return $http.get('http://localhost:8088/api/v1/vehicles', config);
        }

        function getVehiculesProblem(nbChildProblem) {
            var vehiculeProblem = 0;
            if (angular.isDefined(nbChildProblem)) {
                if (nbChildProblem !=null) {
                    vehiculeProblem ++;
                }
            }
            return vehiculeProblem;
        }

        function getVehiculesRun(nbChildRun) {
            var vehiculeRun = 0;
            if (angular.isDefined(nbChildRun)) {
                if (nbChildRun !=0) {
                    vehiculeRun ++;
                }
            }
            return vehiculeRun;
        }

        function getVehiculesRealTime(callback) {

            var ref = firebase.database().ref('/positions/');
            ref.on("value", function(snapshot) {
              //  console.log("snap", snapshot.val());


                var vehiculeRun =0;
                var vehiculeStop = 0;

                var vehiculeProblem = 0;
                var infoUser ={} ;
                var i=0;
                snapshot.forEach( function (child) {

                    var speed = child.val().Speed !=0;

                    var dtc = child.val().DTC ==0;


                        infoUser[i] = {
                            "idUser" : child.key,
                            "speed" : child.val().Speed,
                            "dtc" : child.val().DTC,
                            "kilometrage" : child.val().kilometrage,
                            "latitude":child.val().latitude,
                            "longitude":child.val().longitude
                    };
i++;
                    // var test =  UsersPositionsService.getVehiculesRun(speed);
                    // console.log('serv', test);


               //     var test =   UsersPositionsService.getVehiculesRun(speed);


                 //   console.log('serv2', test);

                    if(child.val().DTC !=null){
                        vehiculeProblem++;
                    }

                    if(child.val().Speed !=0){
                        vehiculeRun++;
                    }else{
                        vehiculeStop++;
                    }

                    callback(child);


                });

                localStorage.setObject('snapshot', infoUser);
               // console.log("infoUser", infoUser);

                /*console.log("infoUser", infoUser);
                console.log('RUN ' + vehiculeRun);
                console.log('STOP ' +vehiculeStop);
                console.log('DTC', +vehiculeProblem);*/
                $rootScope.$broadcast('vehiculeRuntime', {run: vehiculeRun, stop: vehiculeStop, dtc:vehiculeProblem, infoOfUser:infoUser});
                return {run: vehiculeRun, stop: vehiculeStop, dtc:vehiculeProblem, infoOfUser:infoUser};
            });
        }
    }

})();
