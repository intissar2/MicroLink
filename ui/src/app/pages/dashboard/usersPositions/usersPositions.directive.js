/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('CarAlgoPro.pages.dashboard')
      .directive('usersPositions', usersPositions);

  /** @ngInject */
  function usersPositions() {
    return {
      restrict: 'E',
      controller: 'UsersPositionsCtrl as usersPositionsCtrl',
      templateUrl: 'app/pages/dashboard/usersPositions/usersPositions.html'
    };
  }
})();