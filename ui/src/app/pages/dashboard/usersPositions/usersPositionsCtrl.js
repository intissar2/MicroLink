/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('CarAlgoPro.pages.dashboard')
        .controller('UsersPositionsCtrl', UsersPositionsCtrl);

    /** @ngInject */
    function UsersPositionsCtrl($scope, $filter,$stateParams, ConductorService, UsersPositionsService, TripService, $http, $rootScope, localStorage) {

        $scope.message = "";

        var usersPositionsCtrl = this;
        usersPositionsCtrl.userId = $stateParams.userId;
        usersPositionsCtrl.vehiculeId = $stateParams.vehiculeId;


        usersPositionsCtrl.connectedUser =  $rootScope.connectedUser;

     //   console.log("connected", usersPositionsCtrl.connectedUser);

        $scope.role = localStorage.getObject('dataRole');
    //    console.log("role Users", $scope.role);

        usersPositionsCtrl.list = list;
        usersPositionsCtrl.select = select;
        usersPositionsCtrl.listVehicules = listVehicules;

        usersPositionsCtrl.selectedUser = {};

        usersPositionsCtrl.list();
        usersPositionsCtrl.listVehicules();
        usersPositionsCtrl.usersInformations = {};



        $scope.$on('vehiculeRuntime', function(event, data) {

            event.currentScope.vehiculesStop = data.stop;
            event.currentScope.vehiculesRun = data.run;
            event.currentScope.vehiculeProblem = data.dtc;
            event.currentScope.vehiculeKm = data.kilometrage;

            /*event.currentScope.$apply(function() {
                usersPositionsCtrl.vehiculesStop = data.stop;
                usersPositionsCtrl.vehiculesRun = data.run;
            });*/

        });


        $scope.distanceTotale  = localStorage.getObject('distanceTotaleTrajet');
        //$scope.vehiculesRun  = localStorage.getObject('dataVehiculeRun');
/*
        $scope.$watch(function(){
            return  UsersPositionsService.getVehiculesRealTime(angular.noop);
        },  function(vehiculesStatus) {
            if(angular.isDefined(vehiculesStatus)) {
                usersPositionsCtrl.vehiculesStop = vehiculesStatus.stop;
                usersPositionsCtrl.vehiculesRun = vehiculesStatus.run;
            }
        }, true);*/

        function listVehicules() {
            UsersPositionsService.listOfVehicules().success(function (response) {
                usersPositionsCtrl.listVehicule = response;
                //console.log("verhicules", response);
            })
        }

        function list(){
            ConductorService.list(true).success(function(response){
                usersPositionsCtrl.conductorList = response;
             //   console.log("liste des users",response);




                /* ** FIREBASE ** */

                $scope.$on('vehiculeRuntime', function(event, data) {
                    var snapshotMap = {};
                    angular.forEach(data.infoOfUser, function(snap){
                        snapshotMap[snap.idUser] = snap;
                      //  console.log("SNAP", snap);
                        _.forEach(usersPositionsCtrl.conductorList, function(user){
                            if (!!snapshotMap[user.id]){
                               // user.dtc = snapshotMap[user.id].dtc;

                                var tab = [];
                                var snapTab = snapshotMap[user.id].dtc;
                               // console.log("snapTab", snapTab);
                                // if(snapTab =='undefined')
                                //  for(var i=0; i<snapTab.length; i++){
                                var i =0;
                                var isDuplicated = false;
                                _.forEach(snapTab, function(snap){

                                    tab[i] = snap;
                                    if(snap == tab[0] && i>0 && !isDuplicated){
                                        isDuplicated = true;
                                    }

                                    if(isDuplicated){

                                    }else{
                                        i++;
                                    }


                                })
                                if(tab.length>0 && isDuplicated){
                                    tab.splice(tab.length-1,1);
                                    user.dtc = tab;
                                }else{
                                    if(tab.length>0 && !isDuplicated){
                                        user.dtc = tab;
                                    }
                                }
                                user.kilometrage = snapshotMap[user.id].kilometrage;
                            }
                           // console.log("User kilometrage", user.kilometrage);
                            //console.log("vehiculeDTC2", user.id);

                        });
                    });
                });
                UsersPositionsService.getVehiculesRealTime(angular.noop);

                /* *************************************************** */
                usersPositionsCtrl.positionPerUserId = {};
                _.forEach(usersPositionsCtrl.conductorList, function(user) {
                    if (!!user.lastTrip && !!user.lastTrip.endTrip){
                        usersPositionsCtrl.positionPerUserId[user.id] = user.lastTrip.endTrip;
                        usersPositionsCtrl.usersInformations[user.id] = "<div class=\"feed-message\" style=\"border-bottom: none;\"><div class=\"message-icon\"><img class=\"photo-icon\" src=\"" +  $filter('profilePicture')(user.id,'jpg') + "\"></div><div class=\"text-block text-message\"><div class=\"message-header\"><span class=\"author\">" + user.firstName + " " + user.lastName + "</span></div><div class=\"message-content line-clamp\" ng-class=\"{'line-clamp-2' : !user.expanded}\">" + user.lastTrip.endTripAdress + "</div></div></div>"
                    }
                });
            });
        }

        function select(user){
            if (!!user.lastTrip && !!user.lastTrip.endTrip){
                usersPositionsCtrl.selectedUser.id = user.id;
                usersPositionsCtrl.selectedUser.position = user.lastTrip.endTrip;
            }
           // console.log("selectedUser",usersPositionsCtrl.selectedUser);
        }

        usersPositionsCtrl.expandUser = function(user){
            user.expanded = !user.expanded;
        }


    }
})();