/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
    'use strict';

    angular.module('CarAlgoPro.pages.dashboard')
        .directive('leafletMap', leafletMap);

    leafletMap.$inject = [
        'UsersPositionsService'
    ];

    /** @ngInject */
    function leafletMap(UsersPositionsService, usersPositionsCtrl) {

        var script = document.createElement('script');

        document.head.appendChild(script);

        return {

            template: '<div id="map" style="height:70vh"></div>',
            scope: {
                points : '=?',
                startTrip: '=?',
                endTrip: '=?',
                fixedPositions: '=?',
                selectedMarker: '=?',
                markersInformations: '=?',
                realTimePositions: '=?',
            },
            link: function(scope) {
                var markersList = {};
                var markers = null;
                var marker = null;
                var markersConnected = null;
                var map = L.map('map', {
                    zoom: 6,
                    minZoom: 3,
                });



                /* *****************************  */

// initialize the map on the "map" div with a given center and zoom
              /*  var map = new L.Map('map', {
                    zoom: 6,
                    minZoom: 3,
                });*/

// create a new tile layer
                    var tileUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png',
                        layer = new L.TileLayer(tileUrl,
                            {
                                attribution: 'CarAlgo Maps © <a href=\"www.openstreetmap.org/copyright\">OpenStreetMap</a> contributors',
                                maxZoom: 18
                            });

// add the layer to the map
                map.addLayer(layer);


                var londonParisRomeBerlinBucarest = [[46.507222, 2.0275],[49.507222, 2.9275]];

                var oldMarker=[];
                var newMarker=[];
                var newPoint=[];
                var marker2 =[];

                map.fitBounds(londonParisRomeBerlinBucarest);



                /* ************************************* */





             /*   L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
                    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                }).addTo(map);*/

                scope.$watch('selectedMarker', function(newValue, oldValue){
                    if(!!newValue && !!newValue.position){
                        map.flyTo(scope.selectedMarker.position, 12);
                        map.once('moveend', function() {
                            var m = markersList[scope.selectedMarker.id];
                            markers.zoomToShowLayer(m, function() {
                                m.openPopup();
                            });
                        });
                    }
                }, true);



                if(angular.isDefined(scope.fixedPositions)){


                    var output = document.getElementById("data");

                    UsersPositionsService.getVehiculesRealTime(function(child){
                        var string =""+child.key;


                        if(oldMarker[child.key]==null){
                            newMarker[child.key] = [ child.val().latitude,  child.val().longitude];
                            newPoint[child.key]=[newMarker[child.key],newMarker[child.key]];
                            oldMarker[child.key]=newMarker[child.key];
                            marker2[child.key] = L.Marker.movingMarker(newPoint[child.key],[2000], {autostart: true}).addTo(map).bindPopup('');
                            if(marker2[child.key].isPopupOpen()){


                                marker2[child.key].getPopup().setContent('<b>'+child.val().nom +'</b>'+ '<br><b>RPM: </b>'+child.val().RPM
                                    +'<br><b>Speed: </b>'+child.val().Speed+'<br><b>DTC: </b>'+child.val().DTC);
                            }else{
                                marker2[child.key].on('click', function() {


                                    marker2[child.key].bindPopup('<b>'+child.val().nom +'</b>'+ '<br><b>RPM: </b>'+child.val().RPM
                                        +'<br><b>Speed: </b>'+child.val().Speed+'<br><b>DTC: </b>'+child.val().DTC).openPopup();


                                });
                            }

                            // marker2 = L.Marker.moveTo(newMarker, [2000]);




                        }else{
                            newMarker[child.key] = [ child.val().latitude,  child.val().longitude];
                            newPoint[child.key]=[oldMarker[child.key],newMarker[child.key]];
                            oldMarker[child.key]=newMarker[child.key];
                            var newLatLng = new L.LatLng(child.val().latitude,  child.val().longitude);
                            marker2[child.key].moveTo(newLatLng, [2000]);


                            if(marker2[child.key].isPopupOpen()){

                                marker2[child.key].getPopup().setContent('<b>'+child.val().nom +'</b>'+ '<br><b>RPM: </b>'+child.val().RPM
                                    +'<br><b>Speed: </b>'+child.val().Speed+'<br><b>DTC: </b>'+child.val().DTC);
                            }else{
                                marker2[child.key].on('click', function() {


                                    marker2[child.key].bindPopup('<b>'+child.val().nom +'</b>'+ '<br><b>RPM: </b>'+child.val().RPM
                                        +'<br><b>Speed: </b>'+child.val().Speed+'<br><b>DTC: </b>'+child.val().DTC).openPopup();


                                });
                            }
                        }

                        L.polyline(newPoint[child.key], {color: '#'+string.substring(string.length-6,string.length)}).addTo(map);

                        if(child.val().DTC==null && child.val().Speed == 0){

                            var myIcon = L.icon({
                                iconUrl: 'http://149.202.165.16:8089/caralgo/image/car.png',
                                iconSize: [25, 25],

                            });

                            marker2[child.key].setIcon(myIcon);

                        }else  if(child.val().DTC==null && child.val().Speed != 0){
                            var myIcon = L.icon({
                                iconUrl: 'http://149.202.165.16:8089/caralgo/image/carR.png',
                                iconSize: [25, 25],

                            });

                            marker2[child.key].setIcon(myIcon);

                        }else  if(child.val().DTC!=null && child.val().Speed != 0){
                            var myIcon = L.icon({
                                iconUrl: 'http://149.202.165.16:8089/caralgo/image/car-DTCR.png',
                                iconSize: [25, 25],

                            });

                            marker2[child.key].setIcon(myIcon);

                    } else {

                            var myIcon = L.icon({
                                iconUrl: 'http://149.202.165.16:8089/caralgo/image/car-DTC.png',
                                iconSize: [25, 25],

                            });

                            marker2[child.key].setIcon(myIcon);
                        }
                    });

                    scope.$watch('fixedPositions', function (newValue, oldValue) {
                      //  console.log(newValue);
                        if(!!newValue && !!newValue.position){
                            map.flyTo(L.marker([position.latitude, position.longitude]));
                            map.once('moveend', function() {
                                var m = markersList[scope.selectedMarker.id];
                                markers.zoomToShowLayer(m, function() {

                                    m.openPopup();
                                })
                            });
                        }

                        //TODO A compléter en déplacant le markeur qui a changé
                        // Utiliser aussi https://github.com/mohsen1/leaflet-moving-marker
                        // pour déplacer le marker

                    }, true);
                    markers = L.markerClusterGroup();
                    _.forEach(scope.fixedPositions, function(position, id){
                        var marker = L.marker([position.lat, position.lon]);
                        marker.bindPopup(scope.markersInformations[id]);
                        markers.addLayer(marker);
                        markersList[id] = marker;
                    });
                    map.addLayer(markers);
                }



                if(angular.isDefined(scope.startTrip)){






                    var startIcon = L.icon({
                        iconUrl: 'assets/img/start-flag.png',
                        /* shadowUrl: 'leaf-shadow.png', */

                        iconSize:     [28, 28], // size of the icon
                        /* shadowSize:   [50, 64], // size of the shadow */
                        iconAnchor:   [7, 30] // point of the icon which will correspond to marker's location
                        /* shadowAnchor: [4, 62],  // the same for the shadow */
                        //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                    });
                    var endIcon = L.icon({
                        iconUrl: 'assets/img/end-flag.png',
                        iconSize:     [32, 32], // size of the icon
                        iconAnchor:   [7, 30] // point of the icon which will correspond to marker's location
                        //popupAnchor:  [-3, -76] // point from which the popup should open relative to the iconAnchor
                    });
                    L.marker([scope.startTrip.lat, scope.startTrip.lon], {icon: startIcon}).addTo(map);
                    L.marker([scope.endTrip.lat, scope.endTrip.lon], {icon: endIcon}).addTo(map);
                }
                /*  L.marker([51.5, -0.09]).addTo(map)
                     .bindPopup('A pretty CSS3 popup.<br> Easily customizable.')
                     .openPopup();
                     console.log(scope.points) */




                if(angular.isDefined(scope.points)){
                    var antPolyline = L.polyline(scope.points, {});

                    antPolyline.addTo(map);

                    map.fitBounds(antPolyline.getBounds());
                }
            }
        };
    }
})();