(function () {
    'use strict';

    angular
        .module("CarAlgoPro.pages.trip")
        .factory('TripService', TripService);

    function TripService($http) {
        var tripService = {};

        tripService.allTrip = allTrip;
        tripService.currentWeek = currentWeek;
        tripService.lastYear = lastYear;
        tripService.lastMonth = lastMonth;
        tripService.lastTrip = lastTrip;
        tripService.getById = getById;

        return tripService;

        function lastYear(userId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            return $http.get('http://localhost:8088/api/v1/trips/lastYear/' + userId, config);
        }

        function lastMonth(userId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            return $http.get('http://localhost:8088/api/v1/trips/lastMonth/' + userId, config);
        }

        function currentWeek(userId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            return $http.get('http://localhost:8088/api/v1/trips/currentWeek/' + userId, config);
        }

        function lastTrip(date, userId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            return $http.get('http://localhost:8088/api/v1/trips/from/'+ date + '/'+ userId, config);
        }

        function allTrip(userId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            return $http.get('http://localhost:8088/api/v1/trips/' + userId, config);
        }


        function getById(userId, tripId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            console.log("userId trip", userId);
            return $http.get('http://localhost:8088/api/v1/trips/2/' + userId + '/' + tripId, config);
        }

    }
})();