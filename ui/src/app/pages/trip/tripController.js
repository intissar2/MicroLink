(function(){
    'use strict'
    angular.module("CarAlgoPro.pages.trip")
        .controller('TripController', TripController);
    TripController.$inject = ['TripService', '$state', '$stateParams', '$filter', '$log', '$rootScope', 'editableOptions', 'editableThemes', 'localStorage', '$scope', 'baConfig', 'baUtil', '$timeout'];
    function TripController(TripService,  $state, $stateParams, $filter, $log, $rootScope, editableOptions, editableThemes, localStorage, $scope, baConfig, baUtil, $timeout){
        var tripCtrl = this;
        tripCtrl.tripId = $stateParams.tripId;
        tripCtrl.userId = $stateParams.userId;
        //tripCtrl.userId = $stateParams.userId || $rootScope.connectedUser;
      //  var connectedUsers =  $rootScope.connectedUser;
        tripCtrl.connectedUser =  $rootScope.connectedUser;

       // console.log("connected", tripCtrl.connectedUser);

        /* **** User connected **** */
        //tripCtrl.userId = $stateParams.userId || $rootScope.connectedUser;

        $scope.$on('vehiculeRuntime', function(event, data) {

            event.currentScope.vehiculesStop = data.stop;
            event.currentScope.vehiculesRun = data.run;
            event.currentScope.infoUser = data.infoOfUser;

           // console.log("infoUser", event.currentScope.infoUser);
           // console.log("infoUser", event.currentScope.infoUser);
            /*event.currentScope.$apply(function() {
                usersPositionsCtrl.vehiculesStop = data.stop;
                usersPositionsCtrl.vehiculesRun = data.run;
            });*/

        });

        $scope.smartTablePageSize = 10;

        $scope.show = 1;


        tripCtrl.lineOptions = {
            scaleShowGridLines : true,
            scaleGridLineColor : "rgba(0,0,0,.05)",
            scaleGridLineWidth : 1,
            bezierCurve : true,
            bezierCurveTension : 0.4,
            pointDot : true,
            pointDotRadius : 4,
            pointDotStrokeWidth : 1,
            pointHitDetectionRadius : 20,
            datasetStroke : true,
            datasetStrokeWidth : 2,
            datasetFill : true
        };
        tripCtrl.parameters = {
            'EngineLoad': {values: [], dates: []},
            'ManAbsPressure':{values: [], dates: []},
            'Batterie': {values: [], dates: []},
            'EngineRpm': {values: [], dates: []},
            'Speed': {values: [], dates: []},
            'IntAirTemperature': {values: [], dates: []},
            'AirFlowRate': {values: [], dates: []}
        }

        tripCtrl.lastMonth = lastMonth;
        tripCtrl.lastTrip = lastTrip;
        tripCtrl.lastWeek = lastWeek;
        tripCtrl.lastYear = lastYear;
        tripCtrl.allTrips = allTrips;

        (function(){
            if(angular.isUndefined(tripCtrl.tripId)){
                lastMonth();
                lastTrip();
                lastWeek();
                lastYear();
                allTrips();
            }
        })();

        $scope.role = localStorage.getObject('dataRole');


        function allTrips() {
            TripService.allTrip(tripCtrl.userId).success(function(response){
                tripCtrl.all = response;
                //console.log("lastYeartTripList", response);
            });

            TripService.allTrip(tripCtrl.connected).success(function(response){
                tripCtrl.all = response;
                //console.log("lastYeartTripListConnected", response);
            });
        }

        function lastYear() {
            TripService.lastYear(tripCtrl.userId).success(function(response){
                tripCtrl.lastYeartTripList = response;
                //console.log("lastYeartTripList", response);
            })
        }

        function lastWeek() {
            TripService.currentWeek(tripCtrl.userId).success(function(response){
                tripCtrl.currentWeektTripList = response;
               // console.log("currentWeektTripList", response);
            })
        }

        $scope.speed = 0;
        $scope.avgCO2 = 0;
        $scope.avgScore = 0;
        $scope.totalFuel = 0;
        $scope.distanceTotale = 0;

        var maxSpeed = 0;
        var CO2 = 0;
        var score = 0;
        var fuel = 0;
        var distance = 0;

        function lastTrip() {
            $scope.date = new Date();
            $scope.DateToStr = $filter('date')($scope.date , "yyyy-MM-dd");

            TripService.lastTrip($scope.DateToStr, tripCtrl.userId).success(function(response){
                tripCtrl.lastDayTripList = response;
               // console.log("lastTripDayList", response);
               // console.log("co2!! intial", CO2);

                _.forEach( tripCtrl.lastDayTripList, function(trip){
                    if(trip.tripResult.maxSpeed > maxSpeed){
                        maxSpeed = trip.tripResult.maxSpeed;
                    }
                    CO2 = CO2 + trip.tripResult.avgCo2Emiss;
                    score = score + trip.tripResult.standardScore;
                    fuel = fuel + trip.tripResult.consFuelFromDep;
                    distance = distance + trip.tripResult.totalDistance;

                   // console.log("co2!!", CO2);
                });
               // console.log("co2!22!", CO2);
                if(tripCtrl.lastDayTripList != null) {
                   // console.log("value", "not null");
                    if (tripCtrl.lastDayTripList.length != 0) {
                        $scope.speed = maxSpeed;
                        $scope.avgCO2 = CO2 / tripCtrl.lastDayTripList.length;
                        $scope.avgScore = (score / tripCtrl.lastDayTripList.length) * 10;
                        $scope.totalFuel = fuel;
                        $scope.distanceTotale = distance;
                    } else {
                        $scope.speed = 0;
                        $scope.avgCO2 = 0;
                        $scope.avgScore = 0;
                        $scope.totalFuel = 0;
                        $scope.distanceTotale = 0;
                    }
                }else{
                   // console.log("value", "Null");
                    $scope.speed = 0;
                    $scope.avgCO2 = 0;
                    $scope.avgScore = 0;
                    $scope.totalFuel = 0;
                    $scope.distanceTotale = 0;
                }
            })


        }



        function lastMonth(){
            TripService.lastMonth(tripCtrl.userId).success(function(response){
                tripCtrl.tripList = response;
               // console.log("lastMonth", response);
               // localStorage.setObject('dataTripList', response);
            })
        }


        /* **************************************************************************************************************** */







        $scope.showOne = true;
        $scope.showTwo = false;
        $scope.showThree = false;
        $scope.showFour = false;
        $scope.showFive = false;

        $scope.changeView = function(val){
            if(val == 'showOne'){
             //   $scope.content = "showOne content";
                var maxSpeed = 0;
                var CO2 = 0;
                var score = 0;
                var fuel = 0;
                var distance = 0;
                _.forEach( tripCtrl.lastDayTripList, function(trip){
                    if(trip.tripResult.maxSpeed > maxSpeed){
                        maxSpeed = trip.tripResult.maxSpeed;
                    }
                    CO2 = CO2 + trip.tripResult.avgCo2Emiss;
                    score = score + trip.tripResult.standardScore;
                    fuel = fuel + trip.tripResult.consFuelFromDep;
                    distance = distance + trip.tripResult.totalDistance;
                });
                if(tripCtrl.lastDayTripList != null) {
                    if (tripCtrl.lastDayTripList.length != 0) {
                        $scope.speed = maxSpeed;
                        $scope.avgCO2 = CO2 / tripCtrl.lastDayTripList.length;
                        $scope.avgScore = (score / tripCtrl.lastDayTripList.length) * 10;
                        $scope.totalFuel = fuel;
                        $scope.distanceTotale = distance;
                    } else {
                        $scope.speed = 0;
                        $scope.avgCO2 = 0;
                        $scope.avgScore = 0;
                        $scope.totalFuel = 0;
                        $scope.distanceTotale = 0;
                    }
                }

                $scope.showOne = true;
                $scope.showTwo = false;
                $scope.showThree = false;
                $scope.showFour = false;
                $scope.showFive = false;
            } else if (val == 'showTwo') {
                var maxSpeed = 0;
                var CO2 = 0;
                var score = 0;
                var fuel = 0;
                var distance = 0;
                _.forEach( tripCtrl.currentWeektTripList, function(trip){
                    if(trip.tripResult.maxSpeed > maxSpeed){
                        maxSpeed = trip.tripResult.maxSpeed;
                    }
                    CO2 = CO2 + trip.tripResult.avgCo2Emiss;
                    score = score + trip.tripResult.standardScore;
                    fuel = fuel + trip.tripResult.consFuelFromDep;
                    distance = distance + trip.tripResult.totalDistance;
                });
                if(tripCtrl.currentWeektTripList != null) {
                    if (tripCtrl.currentWeektTripList.length != 0) {
                        $scope.speed = maxSpeed;
                        $scope.avgCO2 = CO2 / tripCtrl.currentWeektTripList.length;
                        $scope.avgScore = (score / tripCtrl.currentWeektTripList.length) * 10;
                        $scope.totalFuel = fuel;
                        $scope.distanceTotale = distance;
                    } else {
                        $scope.speed = 0;
                        $scope.avgCO2 = 0;
                        $scope.avgScore = 0;
                        $scope.totalFuel = 0;
                        $scope.distanceTotale = 0;
                    }
                }

                $scope.showOne = false;
                $scope.showTwo = true;
                $scope.showThree = false;
                $scope.showFour = false;
                $scope.showFive = false;
            } else if (val == 'showThree') {

                var maxSpeed = 0;
                var CO2 = 0;
                var score = 0;
                var fuel = 0;
                var distance = 0;
                _.forEach( tripCtrl.tripList, function(trip){
                    if(trip.tripResult.maxSpeed > maxSpeed){
                        maxSpeed = trip.tripResult.maxSpeed;
                    }
                    CO2 = CO2 + trip.tripResult.avgCo2Emiss;
                    score = score + trip.tripResult.standardScore;
                    fuel = fuel + trip.tripResult.consFuelFromDep;
                    distance = distance + trip.tripResult.totalDistance;
                });
                if(tripCtrl.tripList != null) {
                    if (tripCtrl.tripList.length != 0) {
                        $scope.speed = maxSpeed;
                        $scope.avgCO2 = CO2 / tripCtrl.tripList.length;
                        $scope.avgScore = (score / tripCtrl.tripList.length) * 10;
                        $scope.totalFuel = fuel;
                        $scope.distanceTotale = distance;
                    } else {
                        $scope.speed = 0;
                        $scope.avgCO2 = 0;
                        $scope.avgScore = 0;
                        $scope.totalFuel = 0;
                        $scope.distanceTotale = 0;
                    }
                }

                $scope.showOne = false;
                $scope.showTwo = false;
                $scope.showThree = true;
                $scope.showFour = false;
                $scope.showFive = false;

            }
            else if (val == 'showFour') {

                var maxSpeed = 0;
                var CO2 = 0;
                var score = 0;
                var fuel = 0;
                var distance = 0;
                _.forEach( tripCtrl.lastYeartTripList, function(trip){
                    if(trip.tripResult.maxSpeed > maxSpeed){
                        maxSpeed = trip.tripResult.maxSpeed;
                    }
                    CO2 = CO2 + trip.tripResult.avgCo2Emiss;
                    score = score + trip.tripResult.standardScore;
                    fuel = fuel + trip.tripResult.consFuelFromDep;
                    distance = distance + trip.tripResult.totalDistance;

                });
                if(tripCtrl.lastYeartTripList != null){
                if(tripCtrl.lastYeartTripList.length !=0 ){
                    $scope.speed =  maxSpeed;
                    $scope.avgCO2 =  CO2 / tripCtrl.lastYeartTripList.length;
                    $scope.avgScore = (score / tripCtrl.lastYeartTripList.length) *10;
                    $scope.totalFuel = fuel;
                    $scope.distanceTotale = distance;
                }else{
                    $scope.speed =  0;
                    $scope.avgCO2 =  0;
                    $scope.avgScore = 0;
                    $scope.totalFuel = 0;
                    $scope.distanceTotale = 0;
                }
                }

                $scope.showOne = false;
                $scope.showTwo = false;
                $scope.showThree = false;
                $scope.showFour = true;
                $scope.showFive = false;

            }
            else if (val == 'showFive') {

                var maxSpeed = 0;
                var CO2 = 0;
                var score = 0;
                var fuel = 0;
                var distance = 0;
                _.forEach( tripCtrl.all, function(trip){
                    if(trip.tripResult.maxSpeed > maxSpeed){
                        maxSpeed = trip.tripResult.maxSpeed;
                    }
                    CO2 = CO2 + trip.tripResult.avgCo2Emiss;
                    score = score + trip.tripResult.standardScore;
                    fuel = fuel + trip.tripResult.consFuelFromDep;
                    distance = distance + trip.tripResult.totalDistance;
                });
                if(tripCtrl.all != null) {
                    if (tripCtrl.all.length != 0) {
                        $scope.speed = maxSpeed;
                        $scope.avgCO2 = CO2 / tripCtrl.all.length;
                        $scope.avgScore = (score / tripCtrl.all.length) * 10;
                        $scope.totalFuel = fuel;
                        $scope.distanceTotale = distance;
                    } else {
                        $scope.speed = 0;
                        $scope.avgCO2 = 0;
                        $scope.avgScore = 0;
                        $scope.totalFuel = 0;
                        $scope.distanceTotale = 0;
                    }
                }

                $scope.showOne = false;
                $scope.showTwo = false;
                $scope.showThree = false;
                $scope.showFour = false;
                $scope.showFive = true;
            }

        };



        /* ****************************************************************************************************************** */

        if(angular.isDefined(tripCtrl.tripId)){
            TripService.getById(tripCtrl.userId, tripCtrl.tripId).success(function(response){
                tripCtrl.tripDetails = response;
              // console.log("tripDetails:", response);
               // $rootScope.tripDetails = response.id;
                var speed = tripCtrl.tripDetails.tripResult.maxSpeed;
                var co2 = tripCtrl.tripDetails .tripResult.avgCo2Emiss;
                var fuel = tripCtrl.tripDetails .tripResult.avgFuelCons;
                var distance = tripCtrl.tripDetails .tripResult.totalDistance;
                var note = tripCtrl.tripDetails .tripResult.standardScore;
                var username = tripCtrl.tripDetails.user.lastName;
                var first =  tripCtrl.tripDetails.user.firstName;
                var date = tripCtrl.tripDetails.dateTrip;
                var startAdress = tripCtrl.tripDetails.startTripAdress;
               // var startAdress = tripCtrl.tripDetails.startTripAdress.split(',');
              //  var startAdress = startAdress2[1] + startAdress2[2];
                var endAdress = tripCtrl.tripDetails.endTripAdress;



                $rootScope.$broadcast('vehiculeData', {maxSpeed: speed, avgCo2Emiss: co2, avgFuel: fuel, distanceTotale : distance,
                    ecoDrive: note, lastName: username, firstName: first,
                    dateTrip: date, startTripAdress : startAdress,  endTripAdress: endAdress});
                //return {maxSpeed: speed, avgCo2Emiss: co2, avgFuel: fuel, distanceTotale : distance};




               // console.log(tripCtrl.points);
                tripCtrl.points = _.map(tripCtrl.tripDetails.points, function(point){
                    return [point.lat, point.lon];
                });

               // console.log(tripCtrl.points);
                _.forEach( tripCtrl.tripDetails.parameters, function(parameter, index){
                    //if(index%20 === 0 || index ===0){
                        tripCtrl.parameters[parameter.name].values.push(parameter.value);
                        tripCtrl.parameters[parameter.name].dates.push($filter('date')(parameter.date, 'hh:mm:ss'));
                    //}
                });
                tripCtrl.lineDataDashboard = {
                    labels: tripCtrl.parameters['EngineRpm'].dates,
                    datasets: [
                        {
                            label: "Example dataset",
                            fillColor: "rgba(220,220,220,0.5)",
                            strokeColor: "rgba(220,220,220,1)",
                            pointColor: "rgba(220,220,220,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(220,220,220,1)",
                            data: [65, 59, 40, 51, 36, 25, 40]
                        },
                        {
                            label: "Engine RPM",
                            fillColor: "rgba(26,179,148,0.5)",
                            strokeColor: "rgba(26,179,148,0.7)",
                            pointColor: "rgba(26,179,148,1)",
                            pointStrokeColor: "#fff",
                            pointHighlightFill: "#fff",
                            pointHighlightStroke: "rgba(26,179,148,1)",
                            data: tripCtrl.parameters['EngineRpm'].values
                        }
                    ]
                };
             //   console.log(tripCtrl.tripDetails);
            });
            $scope.$on('vehiculeData', function(event, data) {

                event.currentScope.speed = data.maxSpeed;
                event.currentScope.co2 = data.avgCo2Emiss;
                event.currentScope.fuel = data.avgFuel;
                event.currentScope.distance = data.distanceTotale;
                event.currentScope.note = data.ecoDrive * 10;
                event.currentScope.username = data.lastName;
                event.currentScope.first = data.firstName;
                event.currentScope.date = data.dateTrip;
                event.currentScope.startAdress = data.startTripAdress;
                event.currentScope.endAdress = data.endTripAdress;


                $('.refresh-data').on('click', function () {
                    $('.pie-charts .chart').each(function(index, chart) {
                        $(chart).data('easyPieChart').update(event.currentScope.note);
                    });
                });

            });
        }

        $scope.showGroup = function(user) {
            if(user.group && $scope.groups.length) {
                var selected = $filter('filter')($scope.groups, {id: user.group});
                return selected.length ? selected[0].text : 'Not set';
            } else return 'Not set'
        };

        $scope.showStatus = function(user) {
            var selected = [];
            if(user.status) {
                selected = $filter('filter')($scope.statuses, {value: user.status});
            }
            return selected.length ? selected[0].text : 'Not set';
        };


        $scope.removeUser = function(index) {
            $scope.users.splice(index, 1);
        };

        $scope.addUser = function() {
            $scope.inserted = {
                id: $scope.users.length+1,
                name: '',
                status: null,
                group: null
            };
            $scope.users.push($scope.inserted);
        };

        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';

/* ---------------------------------------------------PIE CHART ----------------------------------------------------------*/

        var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);
      //  $scope.value = $rootScope.tripDetails;


      //  console.log("charts:",  $scope.value);

        /* Start Adress  */


        function getRandomArbitrary(min, max) {
            return Math.random() * (max - min) + min;
        }

        function loadPieCharts() {
            $('.chart').each(function () {
                var chart = $(this);
                chart.easyPieChart({
                    easing: 'easeOutBounce',
                    onStep: function (from, to, percent) {
                        $(this.el).find('.percent').text(Math.round(percent));
                    },
                    barColor: chart.attr('rel'),
                    trackColor: 'rgba(0,0,0,0)',
                    size: 65,
                    scaleLength: 0,
                    animation: 2000,
                    lineWidth: 9,
                    lineCap: 'round',
                });
            });

        }




        $timeout(function () {
            loadPieCharts();

           // updatePieCharts();
        }, 1000);


    }
})();