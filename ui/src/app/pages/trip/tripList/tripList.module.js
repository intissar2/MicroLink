(function() {
  'use strict';

  angular
    .module("CarAlgoPro.pages.trip.tripList", [])
    .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.trip.tripList', {
                url: '/tripList?userId?connectedUser',
                templateUrl: 'app/pages/trip/tripList/tripList.html',
               title: 'Liste des trajets',
                controller: 'TripController',
                controllerAs: 'tripCtrl',
                sidebarMeta: {
                    order: 1000,
                    icon: 'ion-stats-bars'
                },
                authenticate: true
            });
    }

})();