/**
 * @author : intissar CHOUAIEB
 * created : 13/11/2017
 */
(function() {
    'use strict';

    angular.module('CarAlgoPro.pages.trip', [
        'CarAlgoPro.pages.trip.tripDetails',
        'CarAlgoPro.pages.trip.tripList'
    ])
        .config(routeConfig);


    /** @ngInject */
    function routeConfig($stateProvider, $urlRouterProvider) {
        $stateProvider
            .state('main.trip', {
                url: '/trip',
                template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
                abstract: true,
               // controller: 'TablesPageCtrl',
               // title: 'Trip',
               /* sidebarMeta: {
                    icon: 'ion-grid',
                    order: 300,
                },*/
                authenticate: true
            })
    }

})();
