(function() {
    'use strict';

    angular.module("CarAlgoPro.pages.trip.tripDetails", [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.trip.tripDetails', {
                url: '/tripDetails?userId&tripId',
                templateUrl: 'app/pages/trip/tripDetails/tripDetails.html',
                title: 'Détail du trajet',
                controller: 'TripController',
                controllerAs: 'tripCtrl',
                sidebarMeta: {
                    order: 900,
                    icon: 'ion-stats-bars'
                },
                authenticate: true
            });
    }

})();