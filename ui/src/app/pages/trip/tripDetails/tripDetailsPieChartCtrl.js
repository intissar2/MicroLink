/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('CarAlgoPro.pages.trip.tripDetails')
      .controller('tripDetailsPieChartCtrl', tripDetailsPieChartCtrl);

  /** @ngInject */
  function tripDetailsPieChartCtrl($scope, $timeout, baConfig, baUtil, $rootScope, TripService, $stateParams) {
    var pieColor = baUtil.hexToRGB(baConfig.colors.defaultText, 0.2);


      tripDetailsPieChartCtrl.tripId = $stateParams.tripId;
      tripDetailsPieChartCtrl.userId = $stateParams.userId ;

      tripDetailsPieChartCtrl.connectedUser =  $rootScope.connectedUser;

      //$scope.listChart = localStorage.getObject('dataTripList') ;
      //$scope.chart = localStorage.getObject('dataTripDetails') ;
      $scope.value = $rootScope.tripDetails;


      console.log("charts:",  $scope.value);





      /* Start Adress  */



    $scope.charts = [{
      color: pieColor,
      description: '{{value}}',
      stats: '57,820',
      icon: 'person',
    }, {
      color: pieColor,
      description: 'test',
      stats: '$ 89,745',
      icon: 'money',
    }, {
      color: pieColor,
      description: 'Active Users',
      stats: '178,391',
      icon: 'face',
    }, {
      color: pieColor,
      description: 'Returned',
      stats: '32,592',
      icon: 'refresh',
    }
    ];

    function getRandomArbitrary(min, max) {
      return Math.random() * (max - min) + min;
    }

    function loadPieCharts() {
      $('.chart').each(function () {
        var chart = $(this);
        chart.easyPieChart({
          easing: 'easeOutBounce',
          onStep: function (from, to, percent) {
            $(this.el).find('.percent').text(Math.round(percent));
          },
          barColor: chart.attr('rel'),
          trackColor: 'rgba(0,0,0,0)',
          size: 84,
          scaleLength: 0,
          animation: 2000,
          lineWidth: 9,
          lineCap: 'round',
        });
      });

      $('.refresh-data').on('click', function () {
        updatePieCharts();
      });
    }

    function updatePieCharts() {
      $('.pie-charts .chart').each(function(index, chart) {
        $(chart).data('easyPieChart').update(getRandomArbitrary(55, 90));
      });
    }

    $timeout(function () {
      loadPieCharts();
      updatePieCharts();
    }, 1000);
  }
})();