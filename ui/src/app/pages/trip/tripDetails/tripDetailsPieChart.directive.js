/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

    angular.module('CarAlgoPro.pages.trip.tripDetails')
      .directive('tripDetailsPieChart', tripDetailsPieChart);

  /** @ngInject */
  function tripDetailsPieChart() {
    return {
      restrict: 'E',
      controller: 'tripDetailsPieChartCtrl',
      templateUrl: 'app/pages/trip/tripDetails/tripDetailsPieChart.html'
    };
  }
})();