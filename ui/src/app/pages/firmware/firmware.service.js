(function () {
    'use strict';

    angular.module("CarAlgoPro.pages.firmware")
        .factory('Firmware', function Firmware($resource) {


        return $resource(
            'http://localhost:8088/api/v1/firmwares/:id',
            {id: '@id'},//parameters
            {
                update: {
                    method: 'PUT' // To send the HTTP Put request when calling this custom update method.
                }
            },
            {
                stripTrailingSlashes: false
            }
        );

    })
})();