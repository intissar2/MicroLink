(function(){
    'use strict'
    angular.module("CarAlgoPro.pages.firmware")
        .controller('FirmwareController', function FirmwareController($scope, Firmware) {

        var self = this;

        self.attribute = {
            key: null,
            nom: '',
            type: '',
            value: ''
        };


        self.attributes = [];

        self.listFields = [];

        self.attributsListNull = [];

        self.firmware = new Firmware();

        self.firmwares = [];

        self.fetchAllFirmwares = function () {
            self.firmwares = Firmware.query();
        };

        self.showContent = function ($fileContent) {
            self.content = $fileContent;
        };

        self.createFirmware = function () {
            console.log("Create Firmware called");
            self.firmware.fileBytes = self.content;
            self.firmware.listFields = self.attributes;
            self.firmware.$save(function () {
                console.log('Saving New Firmware', self.firmware);
                self.fetchAllFirmwares();
                self.reset();
            });
        };

        self.updateFirmware = function (identity) {
            console.log("Updated Firmware called");
            self.firmware.listFields = self.attributes;
            self.firmware.$update({
                id: identity
            }, function () {
                self.fetchAllFirmwares();
                self.reset();
            });
        };

        self.deleteFirmware = function (identity) {
            self.firmware.$delete({
                id: identity
            }, function () {
                console.log('Deleting firmware with id ', identity);
                self.fetchAllFirmwares();
                self.reset();
            });
        };

        self.fetchAllFirmwares();

        self.submit = function () {
            if (self.firmware.id == null) {
                console.log('Saving New Firmware', self.firmware);
                self.createFirmware();
            } else {
                console.log('Upddating Firmware with id ', self.firmware.id);
                self.updateFirmware(self.firmware.id);
                console.log('Firmware updated with id ', self.firmware.id);
            }
            self.reset();
        };

        self.edit = function (id) {
            console.log('id to be edited', id);
            for (var i = 0; i < self.firmwares.length; i++) {
                if (self.firmwares[i].id == id) {
                    self.firmware = angular.copy(self.firmwares[i]);
                    self.attributes = angular.copy(self.firmwares[i].listFields);
                    break;
                }
            }
        };

        self.remove = function (id) {
            console.log('id to be deleted', id);
            for (var i = 0; i < self.firmwares.length; i++) {
                if (self.firmwares[i].id == id) {
                    console.log('id to be deleted', id);
                    self.deleteFirmware(id);
                    break;
                }
            }
        };

        self.reset = function () {
            self.firmware = new Firmware();
            self.resetListFields();
            $scope.myForm.$setPristine(); // reset Form
        };

        /*---------------------------------------------------------------------------------------*/

        self.resetListFields = function () {
            self.firmwares = Firmware.query(function () {
                if (self.firmwares == null || self.firmwares.length == 0) {
                    self.listFields.push(self.attribute);
                    self.attributes = self.listFields;
                } else {
                    if (self.firmwares[0].listFields == null
                        || self.firmwares[0].listFields.length == 0) {
                        self.listFields.push(self.attribute);
                        self.attributes = self.listFields;
                    } else {
                        self.listFields = self.firmwares[0].listFields;
                        angular.forEach(self.listFields, function (tmp) {
                            tmp.value = "";
                        });
                        self.attributes = self.listFields;
                    }
                }
            });
        }

        self.fetchAllAttributs = function () {
            self.firmwares = Firmware.query(function () {
                if (self.firmwares == null || self.firmwares.length == 0) {
                    self.listFields.push(self.attribute);
                    self.attributes = self.listFields;
                } else {
                    if (self.firmwares[0].listFields == null
                        || self.firmwares[0].listFields.length == 0) {
                        self.listFields.push(self.attribute);
                        self.attributes = self.listFields;
                    } else {
                        self.listFields = self.firmwares[0].listFields;
                        self.attributes = self.listFields;

                        self.attributsListNull = self.listFields;
                        console.log("1.listFields " + self.listFields[0].value);
                        console.log("2.attributsListNull " + self.attributsListNull[0].value);
                        angular.forEach(self.attributsListNull, function (tmp) {
                            tmp.value = "";
                        });
                        console.log("3.listFields : " + self.listFields[0].value);
                        console.log("4.attributsListNull " + self.attributsListNull[0].value);
                    }
                }
            });
        };

        self.fetchAllAttributs();

        self.addAttribut = function () {
            console.log("addAttribut() is called");

            self.attribute.key = self.firmwares[0].listFields.length++;
            self.firmwares = Firmware.query(function () {
                angular.forEach(self.firmwares, function (tmp) {
                    tmp.listFields.push(self.attribute);
                    tmp.$update(function () {
                        self.fetchAllAttributs();
                        self.resetAttribut();
                    });
                });
            });
        };

        self.updateAttribut = function (key) {
            if (key == null) {
                console.log("key egale null");
            } else {
                self.firmwares = Firmware.query(function () {
                    angular.forEach(self.firmwares, function (tmpFirm) {
                        angular.forEach(tmpFirm.listFields, function (tmp) {
                            if (tmp.key == key) {
                                console.log("Update attribute");
                                tmp.nom = self.attribute.nom;
                                tmp.type = self.attribute.type;
                                tmpFirm.$update(function () {
                                    self.fetchAllAttributs();
                                    self.resetAttribut();
                                });
                                console.log("Attribute updated");
                            }
                        });
                    });
                });
            }
        };

        self.deleteAttribut = function (key) {
            var i = 0;
            self.firmwares = Firmware.query(function () {
                angular.forEach(self.firmwares, function (tmpFirm) {
                    angular.forEach(tmpFirm.listFields, function (tmp) {
                        if (tmp.key == key) {
                            console.log("Delete attribute");
                            tmpFirm.listFields.splice(i, 1);
                            tmpFirm.$update(function () {
                                self.fetchAllAttributs();
                                self.resetAttribut();
                            });
                            console.log("Attribute deleted");
                        }
                        i++;
                    });
                });
            });
        };

        self.isAttributeExistlistFields = function (nom) {
            var test = false;
            angular.forEach(self.firmwares[0].listFields, function (tmp) {
                if (tmp.nom == nom) {
                    test = true;
                }
            });
            return test;
        }

        self.isAttributeExist = function (nom) {
            if (!angular.equals("version", nom)
                && !angular.equals("type", nom)
                && !angular.equals("creationDate", nom)
                && !angular.equals("checksum", nom)) {
                if (self.firmwares[0].listFields == null
                    || self.firmwares[0].listFields.length == 0) {
                    console.log("niv 0");
                    return false;
                } else {
                    if (self.isAttributeExistlistFields(nom)) {
                        console.log("niv 1");

                        return true;
                    } else {
                        console.log("niv 2");
                        return false;
                    }
                }
            } else {
                console.log("niv 3");
                return true;
            }
        }

        self.submitAttribut = function (key) {
            console.log("submitAttribut() is called");
            if (self.attribute.key == null
                && !self.isAttributeExist(self.attribute.nom)) {
                console.log('Saving New Attribute ', self.attribute);
                self.addAttribut();
                console.log('The new Attribute is Saved ', self.attribute);
            } else {
                console.log('Updating Attribute with nom ',
                    self.attribute.nom);
                self.updateAttribut(self.attribute.key);
                console.log('Attribute updated with nom ',
                    self.attribute.nom);
            }
        };

        self.editAttribut = function (key) {
            console.log('Attribute to be edited', key);
            for (var i = 0; i < self.firmwares[0].listFields.length; i++) {
                if (self.firmwares[0].listFields[i].key == key) {
                    self.attribute = angular
                        .copy(self.firmwares[0].listFields[i]);
                    break;
                }
            }
        };

        self.removeAttribut = function (key) {
            console.log('id to be deleted', key);
            for (var i = 0; i < self.firmwares[0].listFields.length; i++) {
                if (self.firmwares[0].listFields[i].key == key) {
                    self.deleteAttribut(key);
                    break;
                }
            }
        };

        self.resetAttribut = function () {
            self.firmware = new Firmware();
            self.attribute = {};
            $scope.myForm.$setPristine(); // reset Form
        };

        console.log(self.firmware.listFields );
    })

    })();
