/**
 * @author: intissar CHOUAIEB
 */
(function () {
    'use strict';

    angular.module('CarAlgoPro.pages.firmware',  ['ngRoute'])
        .config(routeConfig);


    /** @ngInject */
    function routeConfig($stateProvider) {
        // var test = false;
        // noinspection JSAnnotator
        // equals(localStorage.dataRole, 'Admin') = test;
        // console.log("coucou");
        //console.log(angular.equals(localStorage.dataRole, 'Admin'));
        // if (angular.equals(localStorage.dataRole, 'Admin')){
        // if(localStorage.dataRole === 'Admin'){
        //  if(test = true){
        $stateProvider
            .state('main.firmware', {
                url: '/firmware',
                templateUrl: 'app/pages/firmware/firmwaresPage.html',
                title: 'Firmware',
                sidebarMeta: {
                    icon: 'ion-android-home',
                    order: 2,
                },
                restrict: 'E',
                bindings: {
                    user: '<'
                },
                authenticate: true,
                //  role: ["User"]
                /*data: {
                    role: {
                        only: ["Admin"]
                    }
                }*/

            });

        //  }

    }
})();