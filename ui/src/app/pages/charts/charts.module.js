/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
  'use strict';

  angular.module('CarAlgoPro.pages.charts', [
      'CarAlgoPro.pages.charts.amCharts',
      'CarAlgoPro.pages.charts.chartJs',
      'CarAlgoPro.pages.charts.chartist',
      'CarAlgoPro.pages.charts.morris'
    ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.charts', {
        url: '/charts',
        abstract: true,
        template: '<div ui-view  autoscroll="true" autoscroll-body-top></div>',
        title: 'Charts',
        sidebarMeta: {
          icon: 'ion-stats-bars',
          order: 150,
        },
        authenticate: true
      });
  }

})();