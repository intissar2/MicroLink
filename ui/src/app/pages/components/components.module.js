/**
 * @author k.danovsky
 * created on 15.01.2016
 */
(function() {
  'use strict';

  angular.module('CarAlgoPro.pages.components', [
      'CarAlgoPro.pages.components.mail',
      'CarAlgoPro.pages.components.timeline',
      'CarAlgoPro.pages.components.tree',
    ])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
    $stateProvider
      .state('main.components', {
        url: '/components',
        template: '<ui-view  autoscroll="true" autoscroll-body-top></ui-view>',
        abstract: true,
        title: 'Components',
        sidebarMeta: {
          icon: 'ion-gear-a',
          order: 100,
        },
        authenticate: true
      });
  }

})();