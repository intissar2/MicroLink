(function(){
    'use strict';
    angular.module("CarAlgoPro.pages.vehicules")
        .controller('vehiculeController', vehiculeController);
    vehiculeController.$inject = ['ConductorService','VehiculeService', '$rootScope', '$stateParams', '$scope', '$filter', 'editableOptions', 'editableThemes', 'localStorage', 'UsersPositionsService', '$uibModal', '$window'];


    function vehiculeController(ConductorService, VehiculeService, $rootScope, $stateParams,$scope, $filter, editableOptions, editableThemes, localStorage, UsersPositionsService, $uibModal, $window){
        var vehiculeCtrl = this;

        vehiculeCtrl.userId = $stateParams.userId;
        vehiculeCtrl.dtc = $stateParams.dtc;

        vehiculeCtrl.connectedUser =  $rootScope.connectedUser;

       // console.log("connected vehicule User", vehiculeCtrl.connectedUser);

        $scope.role = localStorage.getObject('dataRole');

        vehiculeCtrl.list = list;
       
        vehiculeCtrl.list();

        $scope.show = 1;

        function list(){
            ConductorService.list(true).success(function(response){
            	// TODO THIS not VEhicle LIST THIS IS USER LIST you should take as vehicle as user have
                vehiculeCtrl.vehiculeList = response;
              //  console.log("liste vehicules", response);
                //$scope.group = angular.bind( vehiculeCtrl.vehiculeList, vehiculeDTC);
                $scope.$on('vehiculeRuntime', function(event, data) {
                	var snapshotMap = {};
               	 angular.forEach(data.infoOfUser, function(snap){
                        snapshotMap[snap.idUser] = snap;
                    // console.log("SNAP", snap);
                        _.forEach(vehiculeCtrl.vehiculeList, function(user){
                        	if (!!snapshotMap[user.id]){


                        	    var tab = [];
                        	    var snapTab = snapshotMap[user.id].dtc;
                        	   // console.log("snapTab", snapTab);
                        	   // if(snapTab =='undefined')
                              //  for(var i=0; i<snapTab.length; i++){
                                var i =0;
                                var isDuplicated = false;
                                _.forEach(snapTab, function(snap){

                                    tab[i] = snap;
                                    if(snap == tab[0] && i>0 && !isDuplicated){
                                        isDuplicated = true;
                                    }

                                    if(isDuplicated){

                                    }else{
                                        i++;
                                    }


                                })
                                if(tab.length>0 && isDuplicated){
                                    tab.splice(tab.length-1,1);
                        		    user.dtc = tab;
                                }else{
                                    if(tab.length>0 && !isDuplicated){
                                        user.dtc = tab;
                                    }
                                }
                                user.kilometrage = snapshotMap[user.id].kilometrage;
                        	}
                        	//console.log("vehiculeDTC2", user.dtc);
                          //  console.log("vehiculeDTC2", user.id);

                        });
                    });
               });
                UsersPositionsService.getVehiculesRealTime(angular.noop);

            });
        }
        $scope.smartTablePageSize = 10;

        var snapshot = localStorage.getObject('snapshot') ;
      //  console.log("mes snaps", snapshot);


        $scope.showGroup = function(user) {
            if(user.group && $scope.groups.length) {
                var selected = $filter('filter')($scope.groups, {id: user.group});
                return selected.length ? selected[0].text : 'Not set';
            } else return 'Not set'
        };

        $scope.showStatus = function(user) {
            var selected = [];
            if(user.status) {
                selected = $filter('filter')($scope.statuses, {value: user.status});
            }
            return selected.length ? selected[0].text : 'Not set';
        };


        $scope.removeUser = function(index) {
            $scope.users.splice(index, 1);
        };

        $scope.addUser = function() {
            $scope.inserted = {
                id: $scope.users.length+1,
                name: '',
                status: null,
                group: null
            };
            $scope.users.push($scope.inserted);
        };

        // TODO:SMARTO move all this to config file and in html not in JS
        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';


        $scope.roles = localStorage.dataRole;

        $scope.viewDTC = function(user) {

          //  $window.confirm("https://www.obd-codes.com/"+user.dtc);
            $window.confirm("https://www.obd-codes.com/"+user.dtc, "width=300,height=200,left=10,top=150");
        }


        $scope.open = function (size) {
            $uibModal.open({
                animation: true,
                templateUrl: "app/pages/vehicules/infoModal.html",
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });
        };

        /*   var dialogOptions = {
             controller: 'app/pages/vehicules/modalTemplates/infoModalController',
             templateUrl: 'app/pages/vehicules/modalTemplates/infoModal.html'
         };

       $scope.edit = function(user){

             var itemToEdit = user;

             $dialog.dialog(angular.extend(dialogOptions, {resolve: {user: angular.copy(itemToEdit)}}))
                 .open()
                 .then(function(result) {
                     if(result) {
                         angular.copy(result, itemToEdit);
                     }
                     itemToEdit = undefined;
                 });
         }; */
    }

})();