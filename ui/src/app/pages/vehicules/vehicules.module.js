/**
 * @author: intissar CHOUAIEB
 */
(function () {
    'use strict';

    angular.module('CarAlgoPro.pages.vehicules', [])
        .config(routeConfig);


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.vehicules', {
                url: '/vehicules?userId&vehiculeId',
                templateUrl: 'app/pages/vehicules/vehicules.html',
                title: 'Vehicules',
                controller: 'vehiculeController as vehiculeCtrl',
                sidebarMeta: {
                    icon: 'ion-android-car',
                    order: 2,
                },
                restrict: 'E',
                bindings: {
                    user: '<'
                },
                authenticate: true,
                //  role: ["User"]
                data: {
                    role: {
                        only: ["Admin"]
                    }
                }

            });

        //  }

    }

})();

