(function () {
    'use strict';

    angular
        .module("CarAlgoPro.pages.vehicules")
        .factory('VehiculeService', VehiculeService);

    function VehiculeService($http) {
        var VehiculeService = {};

        VehiculeService.list = list;


        return VehiculeService;

        function list() {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };

            return $http
                .get('http://localhost:8088/api/v1/vehicles', config);

        };



    }
})();