(function(){
    'use strict';
    angular.module("CarAlgoPro.pages.vehicules")
        .controller('infoModalController', infoModalController);
    infoModalController.$inject = ['$scope', '$modalInstance', 'vehicules'];



    function infoModalController ($scope, $modalInstance, vehicules) {

        $scope.vehicules = vehicules;
        $scope.selected = {
            vehicule: $scope.vehicules[0]
        };

        $scope.ok = function () {
            $modalInstance.close($scope.selected.vehicule);
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        }
    }
    })();