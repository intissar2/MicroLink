(function() {
    'use strict';

    angular
        .module("CarAlgoPro.pages.vehicules.listeVehicules", [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.vehicules.listeVehicules', {
                url: '/vehicules?userId&vehiculeId',
                templateUrl: 'app/pages/vehicules/listeVehicules/vehicules.html',
                title: 'Vehicules',
                controller: 'vehiculeController as vehiculeCtrl',
                sidebarMeta: {
                    icon: 'ion-android-car',
                    order: 2,
                },
                restrict: 'E',
                bindings: {
                    user: '<'
                },
                authenticate: true,
                //  role: ["User"]
                data: {
                    role: {
                        only: ["Admin"]
                    }
                }

            });

        //  }

    }

})();