/**
 * @author v.lugovsky
 * created on 16.12.2015
 */
(function() {
  'use strict';


  angular.module('CarAlgoPro.pages', [
      'ui.router',
    //  ('ct.ui.router.extras.core'),
      'permission', 'permission.ui',
      //'ngResource',
      'CarAlgoPro.pages.services',
      'CarAlgoPro.pages.config',
      'CarAlgoPro.pages.main',
      'CarAlgoPro.pages.dashboard',
     // 'CarAlgoPro.pages.ui',
     //'CarAlgoPro.pages.components',
      //'CarAlgoPro.pages.form',
     // 'CarAlgoPro.pages.tables',
     // 'CarAlgoPro.pages.charts',
      //'CarAlgoPro.pages.maps',
      'CarAlgoPro.pages.profile',
      'CarAlgoPro.pages.auth',
      'CarAlgoPro.pages.conductor',
      'CarAlgoPro.pages.entreprise',
      'CarAlgoPro.pages.trip',
      'CarAlgoPro.pages.trajets',
      'CarAlgoPro.pages.vehicules',
    //  'CarAlgoPro.pages.trip.tripDetails'
      //'CarAlgoPro.pages.lists'
     // 'CarAlgoPro.pages.detailConductor',
      //'CarAlgoPro.pages.firmware',
     // 'CarAlgoPro.pages.troubleCodePage',
    //  'CarAlgoPro.pages.dashboard.usersPositions.usersRealTime'
  ])
    .config(routeConfig);


    /** @ngInject */
  function routeConfig($urlRouterProvider, baSidebarServiceProvider) {
      $urlRouterProvider.otherwise('/signIn');
//debugger;
//console.log(baSidebarServiceProvider);

  /*  baSidebarServiceProvider.addStaticItem({
      title: 'Pages',
      icon: 'ion-document',
      subMenu: [{
        title: 'Sign In',
        fixedHref: 'auth.html',
        blank: true
      }, {
        title: 'Sign Up',
        fixedHref: 'reg.html',
        blank: true
      }, {
        title: 'User Profile',
        stateRef: 'profile'
      }, {
        title: '404 Page',
        fixedHref: '404.html',
        blank: true
      }]
    });
    baSidebarServiceProvider.addStaticItem({
      title: 'Menu Level 1',
      icon: 'ion-ios-more',
      subMenu: [{
        title: 'Menu Level 1.1',
        disabled: true
      }, {
        title: 'Menu Level 1.2',
        subMenu: [{
          title: 'Menu Level 1.2.1',
          disabled: true
        }]
      }]
    });*/
  }

})();