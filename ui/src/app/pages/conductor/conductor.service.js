(function () {
    'use strict';

    angular
        .module("CarAlgoPro.pages.conductor")
        .factory('ConductorService', ConductorService);

    function ConductorService($http) {
        var conductorService = {};

        conductorService.list = list;
        conductorService.myListUsers = myListUsers;

        conductorService.tripList = tripList;

        conductorService.tripById = tripById;
        conductorService.putListUsers = putListUsers;
        conductorService.deleteUser = deleteUser

        return conductorService;

        function putListUsers(userId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            return $http.put('http://localhost:8088/api/v1/users/'+userId, config);
        }



        function myListUsers() {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };

            return $http
                .get('http://localhost:8088/api/v1/users', config);

        };

        function list(withLastPosition) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };

            return $http
                .get('http://localhost:8088/api/v1/users/' + (withLastPosition ? 'allUserlastPosition' : ''), config);

        };

        function deleteUser(userId) {
            console.log("walid", userId);
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };

            return $http
                .delete('http://localhost:8088/api/v1/users/' + userId, config);

        };


        function tripList(dataId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };


            return $http
                .get("http://localhost:8088/api/v1/trips/lastMonth/"+ dataId, config);

        }


        function tripById(userId, tripId) {
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };
            return $http
                .get('http://localhost:8088/api/v1/trips/' + userId + '/' + tripId, config);
        }


    }
})();