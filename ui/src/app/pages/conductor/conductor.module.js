/**
 * @author: intissar CHOUAIEB
 */
(function () {
    'use strict';

    angular.module('CarAlgoPro.pages.conductor', ['permission'])
        .config(routeConfig);


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.conductor', {
                url: '/conductor?userId',
                templateUrl: 'app/pages/conductor/conductor.html',
                title: 'Conducteurs',
                controller: 'conductorController as conductorCtrl',
                sidebarMeta: {
                    icon: 'ion-person',
                    order: 2,
                },
                restrict: 'E',
                bindings: {
                    user: '<'
                },
                authenticate: true,
                //  role: ["User"]
                data: {
                    role: {
                        only: ["Admin"]
                    }
                }

            });

        //  }

    }

})();

