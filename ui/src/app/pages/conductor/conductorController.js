(function(){
    'use strict'
    angular.module("CarAlgoPro.pages.conductor")
        .controller('conductorController', conductorController);
    conductorController.$inject = ['ConductorService', '$rootScope', '$stateParams', '$scope', '$filter', 'editableOptions', 'editableThemes', 'localStorage','$timeout', '$uibModal', '$window'];

    function conductorController(ConductorService, $rootScope, $stateParams,$scope, $filter, editableOptions, editableThemes, localStorage, $timeout, $uibModal, $window){
        var conductorCtrl = this;
        conductorCtrl.list = list;
        //conductorCtrl.deleteConductor = deleteConductor;

        conductorCtrl.list();
     //   conductorCtrl.deleteConductor();

        conductorCtrl.userId = $stateParams.userId;

      //  $scope.role = localStorage.getObject('dataRole');

        conductorCtrl.connectedUser =  $rootScope.connectedUser;

        console.log("connected", conductorCtrl.connectedUser);





      //  var roles = PermRoleStore.getRoleDefinition('roleName');


       /* conductorCtrl.run(function ($rootScope, $state, $stateParams) {
            $rootScope.$state = $state;
            $rootScope.$stateParams = $stateParams;
        }); */


         /*   if(roles == 'User'){
                console.log("C'est un User");
                $scope.user = $routeParams.role;

            }else{
            if(roles == 'Admin'){
                console.log("C'est un admin");
                $scope.admin = $routeParams.role;
            }
        }*/

         $scope.refresh = function () {
             ConductorService.list(true).success(function (response) {
                 $scope.myData = response;
                 console.log("funciotnRef", $scope.myData);
             });
         };
        $scope.show = 1;


        $scope.selUser  = function(user) {
           // if ($window.confirm("Etes vous sûr de vouloir supprimer " +  user.firstName  + " " +  user.lastName  + " ?")) {
                ConductorService.list(user.id).success(function (response) {
                   $scope.id = user.id;
                    $scope.firstName = user.firstName;
                    $scope.mail = user.mail;
                    $scope.score = (user.lastTrip.tripResult.standardScore) *10;
                    console.log("clients", $scope.id + ',' +  $scope.firstName+ ',' +  $scope.mail+ ',' +  $scope.score);
                    /*$scope.selected_user = user;
                    angular.element('#myModal').modal();*/
                });




      /*  $scope.isSelected = function(user) {
            return $scope.selected_user === user;
        };*/
        };

          //  console.log("valider", $scope.valider_modification( conductorCtrl.conductorList));


      /*  $scope.open = function (size) {
            $uibModal.open({
                animation: true,
                templateUrl: "app/pages/conductor/modalTemplates/infoModal.html",
                size: size,
                resolve: {
                    items: function () {
                        return $scope.items;
                    }
                }
            });
        };*/






        $scope.removeUser = function(user) {

            if ($window.confirm("Etes vous sûr de vouloir supprimer " +  user.firstName  + " " +  user.lastName  + " ?")) {
                ConductorService.deleteUser(user.id).success(function(data) {
                    //  $scope.deleteRow= function (i) {

                    var i = conductorCtrl.conductorList.indexOf(user);

                    if(i !=-1)
                        conductorCtrl.conductorList.splice(i, 1);
                    // };
                    //  $scope.users.splice(index, 1);
                })
            } else {

            }

        };





        function list(){
        	ConductorService.list(true).success(function(response){
                conductorCtrl.conductorList = response;
              localStorage.setObject('dataListConductor', response);
              //  console.log("list conducteur", data);
                //  conductorCtrl.selected = conductorCtrl.conductorList[0].id;
            });
        }
        $scope.smartTablePageSize = 10;


        $scope.showGroup = function(user) {
            if(user.group && $scope.groups.length) {
                var selected = $filter('filter')($scope.groups, {id: user.group});
                return selected.length ? selected[0].text : 'Not set';
            } else return 'Not set'
        };

        $scope.showStatus = function(user) {
            var selected = [];
            if(user.status) {
                selected = $filter('filter')($scope.statuses, {value: user.status});
            }
            return selected.length ? selected[0].text : 'Not set';
        };


    /*    $scope.removeUser = function(index) {
            ConductorService.deleteUser(true).success(function(userId) {
                //  $scope.deleteRow= function (i) {
                conductorCtrl.conductorList.splice(i, 1);
                // };
                //  $scope.users.splice(index, 1);
            })
        };
       */

        $scope.addUser = function() {
            $scope.inserted = {
                id: $scope.users.length+1,
                name: '',
                status: null,
                group: null
            };
            $scope.users.push($scope.inserted);
        };

        // TODO:SMARTO move all this to config file and in html not in JS
        editableOptions.theme = 'bs3';
        editableThemes['bs3'].submitTpl = '<button type="submit" class="btn btn-primary btn-with-icon"><i class="ion-checkmark-round"></i></button>';
        editableThemes['bs3'].cancelTpl = '<button type="button" ng-click="$form.$cancel()" class="btn btn-default btn-with-icon"><i class="ion-close-round"></i></button>';


        $scope.roles = localStorage.dataRole;




        // gets the template to ng-include for a table row / item
        $scope.getTemplate = function (contact) {
            if (contact.id === $scope.conductorCtrl.selected.id) return 'edit';
            else return 'display';
        };

        $scope.editContact = function (contact) {
            $scope.conductorCtrl.selected = angular.copy(contact);
        };

        $scope.saveContact = function (idx) {
            console.log("Saving contact");
            $scope.conductorCtrl.conductorList[idx] = angular.copy($scope.conductorCtrl.selected);
            $scope.reset();
        };

        $scope.reset = function () {
            $scope.conductorCtrl.selected = {};
        };

    }

})();
