/**
 * @author intissar CHOUAIEB
  */

(function() {
  'use strict';

  angular.module('CarAlgoPro.pages.auth.signIn')
    .controller('signInCtrl', signInCtrl)
  ;

  /** @ngInject */
  function signInCtrl($scope, localStorage, $state, authService,toastr, $rootScope) {
    var vm = this;

    vm.signIn = signIn;


    init();

    function init() {
      localStorage.clear();
    }

    function signIn() {

      var role = '';

      var dadosUser = {
        user: vm.user,
        passWord: vm.passWord
      };
      authService.signIn(vm.user, vm.passWord)
        .then(function (response) {
        	  localStorage.setObject('dataUser', dadosUser);
            $state.go('main.dashboard');


            role = response.data.role;
            var userId = response.data.id;
            localStorage.setObject('dataRole', role);
            localStorage.setObject('dataUserId', userId);

            $rootScope.connectedUser = userId;

            $rootScope.role = response.data.role;

            $scope.roles = localStorage.dataRole;


            //var val = JSON.stringify(role);
            //window.localStorage.setItem(key1, val);


         //   var role = PermRoleStore.getRoleDefinition(response.data.role);
         //   console.log(role);



              if(role == 'User'){
                  console.log("C'est un User");

              }
            if(role == 'Admin'){
                console.log("C'est un admin");
            }
          
        })
        .catch(function(err){
          toastr.error('Incorrect username or password', 'Authentication', {
            "autoDismiss": false,
            "positionClass": "toast-top-center",
            "type": "error",
            "timeOut": "5000",
            "extendedTimeOut": "2000",
            "allowHtml": false,
            "closeButton": false,
            "tapToDismiss": true,
            "progressBar": false,
            "newestOnTop": true,
            "maxOpened": 0,
            "preventDuplicates": false,
            "preventOpenDuplicates": false
          })
        });
      
    }


  }

})();