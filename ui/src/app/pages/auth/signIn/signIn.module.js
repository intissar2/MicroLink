/**
 * @author intissar CHOUAIEB
 */

(function() {
  'use strict';

  angular.module('CarAlgoPro.pages.auth.signIn', ['toastr'])
    .config(routeConfig);

  /** @ngInject */
  function routeConfig($stateProvider) {
      $stateProvider
          .state('signIn', {
              url: '/signIn',
              templateUrl: 'app/pages/auth/signIn/signIn.html',
              title: 'Sign in',
              controller: 'signInCtrl',
              sidebarMeta: {
                  order: 800,
              },
              authenticate: false
          })
        /*  .when('/signIn', {
              templateUrl: 'app/pages/auth/signIn/signIn.html',
              controller: 'signInCtrl',
              role: ["Admin"]
          });*/
  }
})();