/**
 * @author rhaddad
 * created on 04/11/2017
 */
(function () {
  'use strict';

  angular.module('CarAlgoPro.pages.auth')
    .factory('authService', authService);

  /** @ngInject */
  function authService($http, md5) {
    var service = {
      signIn: signIn
    }



    return service;

    function signIn(login, password) {
    	/* @author : intissar */
      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        },
        withCredentials: true
      };

      return $http
        .post('http://localhost:8088/api/v1/login', "username=" + login + "&password=" + password, config);
    }

    function signOut(login, password) {
    
      var config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
        },
        withCredentials: true
      };

      return $http
        .post('http://localhost:8088/api/v1/logout', null, config);
    }

  }

})();