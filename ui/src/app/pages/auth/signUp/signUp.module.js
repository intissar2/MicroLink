(function() {
    'use strict';

    angular.module('CarAlgoPro.pages.auth.signUp', [])
        .config(routeConfig);

    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('signUp', {
                url: '/signUp',
                templateUrl: 'app/pages/auth/signUp/signUp.html',
                title: 'Sign up',
                controller: 'signUpCtrl',
                sidebarMeta: {
                    order: 800,
                },
                authenticate: false
            });
    }

})();