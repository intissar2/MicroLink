(function() {
    'use strict';

    angular.module('CarAlgoPro.pages.auth.signUp')
        .controller('signUpCtrl', signUpCtrl);



    /** @ngInject */
    function signUpCtrl($scope, $http, md5, localStorage, $state, toastr) {
      //   var vm = this;

      //   vm.signUp = signUp;

  /*      init();
        function init() {
            localStorage.clear();
        }

        var dadosUser = {
            firstName : vm.firstName,
            lastName: vm.lastName,
            passWord: vm.passWord,
            mail : vm.mail
        };

        authSignUpService.signUp(vm.firstName, vm.lastName, vm.passWord, vm.mail ).then(function (response) {
            if (!!response.data.user){
                localStorage.setObject('dataUser', dadosUser);
                $state.go('signIn');
            }else{
                toastr.error('Incorrect username or password', 'Authentication', {
                    "autoDismiss": false,
                    "positionClass": "toast-top-center",
                    "type": "error",
                    "timeOut": "5000",
                    "extendedTimeOut": "2000",
                    "allowHtml": false,
                    "closeButton": false,
                    "tapToDismiss": true,
                    "progressBar": false,
                    "newestOnTop": true,
                    "maxOpened": 0,
                    "preventDuplicates": false,
                    "preventOpenDuplicates": false
                })
            }
        });

        */

        $scope.formInfo = {};
        $scope.signUp = function(response) {
            $scope.lastName = '';
            $scope.firstName = '';
            $scope.mail = '';
            $scope.passWord = '';

            if (!$scope.formInfo.firstName) {
                $scope.lastNameRequired = 'Last Name Required';
            }

            if (!$scope.formInfo.lastName) {
                $scope.firstNameRequired = 'First Name Required';
            }

            if (!$scope.formInfo.mail) {
                $scope.mailRequired = 'Mail Required';
            }

            if (!$scope.formInfo.passWord) {
                $scope.passWordRequired = 'Password Required';
            }

            $scope.fetched = $scope.formInfo;

            return $http
                .put('http://localhost:8088/api/v1/users/ObjectId("'+md5.createHash($scope.formInfo.passWord)+'")',{ 'firstName': $scope.formInfo.firstName, 'lasttName': $scope.formInfo.lastName, 'mdp': md5.createHash($scope.formInfo.passWord) , 'mail': $scope.formInfo.mail });


                if (!!response.data.user){
                    localStorage.setObject('dataUser', dadosUser);
                    $state.go('signIn');
                }else{
                    toastr.error('Incorrect username or password', 'Authentication', {
                        "autoDismiss": false,
                        "positionClass": "toast-top-center",
                        "type": "error",
                        "timeOut": "5000",
                        "extendedTimeOut": "2000",
                        "allowHtml": false,
                        "closeButton": false,
                        "tapToDismiss": true,
                        "progressBar": false,
                        "newestOnTop": true,
                        "maxOpened": 0,
                        "preventDuplicates": false,
                        "preventOpenDuplicates": false
                    })
                }

        }


    }

})();