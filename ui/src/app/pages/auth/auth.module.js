/**
 * @author rhaddad
 * created on 04.11.2017
 */
(function() {
  'use strict';

  angular.module('CarAlgoPro.pages.auth', [
	  'angular-md5',
      'CarAlgoPro.pages.auth.signIn',
      'CarAlgoPro.pages.auth.signUp'
    ]);

})();