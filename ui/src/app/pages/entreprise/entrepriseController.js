(function () {
    'use strict'
    angular.module("CarAlgoPro.pages.entreprise")
        .controller('entrepriseController', entrepriseController);
   entrepriseController.$inject = ['$scope', '$http', 'md5', 'localStorage', '$state', 'toastr', 'entrepriseService'];

    function entrepriseController ($scope, $http, md5, localStorage, $state, toastr, entrepriseService) {
        var entrepriseCtrl = this;

      //  entrepriseCtrl.createEp = createEp;

        entrepriseCtrl.attribute = {
            key: null,
            nom: '',
            type: '',
            value: ''
        };

        entrepriseCtrl.entreprises = [];

        entrepriseCtrl.listFields = [];




        $scope.createEp = function(name, logo, num, street, town, zipCode) {
           /* $scope.name = '';
            $scope.logo = '';
            $scope.num = '';
            $scope.street = '';
            $scope.town = '';
            $scope.zipCode = '';*/


            var ep = {
                name: name,
                logo: logo,
                num: num,
                street: street,
                town: town,
                zipCode: zipCode
            };


          /*  entrepriseService.createEntreprise().then(function(rest, status) {
                if (status) {
                    if (rest.ep)
                        $scope.lblMsg = "post data submit successfully!";
                } else {
                    $scope.lblMsg = "faild!";
                }
            }); */

           entrepriseService.createEntreprise(ep).success(function () {



              // alert(JSON.stringify(ep));

               console.log("create EP OK!!", ep);

               /* var students = entrepriseService.createEntreprise(true).success(function (resp) {
                    return resp.data;
                }); */

                $scope.status = 'Inserted Entreprise! Refreshing Entreprise list.';
               //entrepriseCtrl.push(ep);
                console.log($scope.status);
            }).error(function (error) {
                $scope.status = 'Unable to insert Entreprise: ' + error.message;
               console.log("create EP Error", ep);
                console.log($scope.status);
            });
        }

       /* entrepriseCtrl.fetchAllEntreprises = function () {
            entrepriseCtrl.entreprises = entrepriseService.query();
        }; */

        //var Entreprise = $resource(entrepriseService);

       //var Entreprise = new entrepriseService();

       /* entrepriseCtrl.createEp = function () {
            console.log("Create Entreprise called");
            entrepriseCtrl.Entreprise.fileBytes = entrepriseCtrl.content;
            entrepriseCtrl.Entreprise.listFields = entrepriseCtrl.attributes;
            Entreprise.$save(function () {
                console.log('Saving New Entreprise', entrepriseCtrl.Entreprise);
                entrepriseCtrl.fetchAllEntreprises();
                entrepriseCtrl.reset();
            });
        }; */





   /*     entrepriseCtrl.createEp = function (key) {
            console.log("submitAttribut() is called");
            if (entrepriseCtrl.createEp.key == null
                && !self.isAttributeExist(self.attribute.nom)) {
                console.log('Saving New Attribute ', self.attribute);
                self.addAttribut();
                console.log('The new Attribute is Saved ', self.attribute);
            } else {
                console.log('Updating Attribute with nom ',
                    self.attribute.nom);
                self.updateAttribut(self.attribute.key);
                console.log('Attribute updated with nom ',
                    self.attribute.nom);
            }
        }; */

    }

})();