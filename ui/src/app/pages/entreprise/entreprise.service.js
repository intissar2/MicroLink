/**
 * @author rhaddad
 * created on 04/11/2017
 */
(function () {
    'use strict';

    angular.module("CarAlgoPro.pages.entreprise")
        .factory('entrepriseService', entrepriseService);

    /** @ngInject */
    function entrepriseService($http) {
        var entrepriseService = {};

        entrepriseService.createEntreprise = createEntreprise;


        return entrepriseService;

        function createEntreprise(ep) {
            /* @author : intissar */
            var config = {
                headers: {
                    'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                },
                withCredentials: true
            };

            return $http
                .post('http://localhost:8088/api/v1/entreprise/addEntreprise', "entreprise=" + ep, config);

        }



        /*
               return $http({
                method: 'POST',
                url: 'http://localhost:8088/api/v1/entreprise/addEntreprise',
                data: entreprise,
                headers: {'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'}
            });
         */

    }

})();