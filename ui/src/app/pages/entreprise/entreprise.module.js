/**
 * @author: intissar CHOUAIEB
 */
(function () {
    'use strict';

    angular.module('CarAlgoPro.pages.entreprise', [])
        .config(routeConfig);


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.entreprise', {
                url: '/entreprise',
                templateUrl: 'app/pages/entreprise/entreprise.html',
                title: 'Entreprise',
                controller: 'entrepriseController as entrepriseCtrl',
                sidebarMeta: {
                    icon: 'ion-briefcase',
                    order: 2,
                },
                restrict: 'E',
                bindings: {
                    user: '<'
                },
                authenticate: true
                //  role: ["User"]

            });

        //  }

    }

})();

