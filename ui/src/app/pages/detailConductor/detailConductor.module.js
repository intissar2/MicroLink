/**
 * @author: intissar CHOUAIEB
 */
(function () {
    'use strict';

    angular.module('CarAlgoPro.pages.detailConductor', ['permission'])
        .config(routeConfig);


    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state('main.detailConductor', {
                url: '/detailConductor?userId',
                templateUrl: 'app/pages/detailConductor/detailConductor.html',
                title: 'Conductor Detail',
                controller: 'conductorController as conductorCtrl',
                sidebarMeta: {
                    icon: 'ion-person',
                    order: 2,
                },
                restrict: 'E',
                bindings: {
                    user: '<'
                },
                authenticate: true,
                //  role: ["User"]
                data: {
                    role: {
                        only: ["Admin"]
                    }
                }

            });

        //  }

    }

})();

