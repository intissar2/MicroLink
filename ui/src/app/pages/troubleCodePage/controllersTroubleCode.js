(function() {
    'use strict'
    angular.module("CarAlgoPro.pages.troubleCodePage")
        .controller('TroubleCodeController',  function TroubleCodeController($scope, TroubleCode) {

        var self = this;

        self.troublecode = new TroubleCode();
        self.troublecodes = [];

        self.fetchAllTroubleCodes = function () {
            self.troublecodes = TroubleCode.query();
        };

        self.createTroubleCode = function () {
            console.log(self.troublecode);
            self.troublecode.$save(function () {
                console.log('Saving New TroubleCode', self.troublecode);
                self.fetchAllTroubleCodes();
                self.reset();
            });
        };

        self.updateTroubleCode = function (identity) {
            self.troublecode.$update({
                id: identity
            }, function () {
                self.fetchAllTroubleCodes();
                self.reset();
            });
        };

        self.deleteTroubleCode = function (identity) {
            self.troublecode.$delete({
                id: identity
            }, function () {
                console.log('Deleting TroubleCode with id ', identity);
                self.fetchAllTroubleCodes();
                self.reset();
            });
        };

        self.fetchAllTroubleCodes();

        self.submit = function () {
            if (self.troublecode.id == null) {
                console.log('Saving New TroubleCode', self.troublecode);
                self.createTroubleCode();
            } else {
                console.log('Upddating TroubleCode with id ', self.troublecode.id);
                self.updateTroubleCode(self.troublecode.id);
                console.log('TroubleCode updated with id ', self.troublecode.id);
            }
            self.reset();
        };

        self.edit = function (id) {
            console.log('id to be edited', id);
            for (var i = 0; i < self.troublecodes.length; i++) {
                if (self.troublecodes[i].id == id) {
                    self.troublecode = angular.copy(self.troublecodes[i]);
                    break;
                }
            }
        };

        self.remove = function (id) {
            console.log('id to be deleted', id);
            for (var i = 0; i < self.troublecodes.length; i++) {
                if (self.troublecodes[i].id == id) {
                    console.log('id to be deleted', id);
                    self.deleteTroubleCode(id);
                    break;
                }
            }
        };

        self.reset = function () {
            self.troublecode = new TroubleCode();
            $scope.myForm.$setPristine(); // reset Form
        };

    });
})();